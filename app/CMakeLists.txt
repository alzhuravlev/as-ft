# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_definitions("-DDEBUG -DPROF_ -DGLM_FORCE_RADIANS -DPLATFORM_ANDROID")

include_directories(
                src/main/cpp/
                src/main/cpp/glm/glm/
                src/main/cpp/glm/
                src/main/cpp/json
                src/main/cpp/native_app_glue
                src/main/cpp/png
                src/main/cpp/engine
                src/main/cpp/ft
)

add_library( ft-android SHARED
                src/main/cpp/ft/ft_box_screen.cpp
                src/main/cpp/ft/ft_door_effect.cpp
                src/main/cpp/ft/ft_game_screen.cpp
                src/main/cpp/ft/ft_key_collecting_effect.cpp
                src/main/cpp/ft/ft_level_screen.cpp
                src/main/cpp/ft/ft_main_screen.cpp
                src/main/cpp/ft/ft_monster_die_effect.cpp
                src/main/cpp/ft/ft_mosquito_collecting_effect.cpp
                src/main/cpp/ft/ft_padlock_collecting_effect.cpp
                src/main/cpp/ft/ft_pogo_walk_effect.cpp
                src/main/cpp/ft/ft_start.cpp
                src/main/cpp/ft/ft_tower.cpp
                src/main/cpp/ft/ft_tower_item.cpp
                src/main/cpp/ft/ft_zzz_effect.cpp

                src/main/cpp/engine/actor.cpp
                src/main/cpp/engine/box2d_utils.cpp
                src/main/cpp/engine/engine.cpp
                src/main/cpp/engine/font_renderer.cpp
                src/main/cpp/engine/fte_math.cpp
                src/main/cpp/engine/fte_utils.cpp
                src/main/cpp/engine/fte_utils_3d.cpp
                src/main/cpp/engine/fte_utils_gl.cpp
                src/main/cpp/engine/gjk.cpp
                src/main/cpp/engine/gpc.cpp
                src/main/cpp/engine/interpolate_utils.cpp
                src/main/cpp/engine/l10n_loader.cpp
                src/main/cpp/engine/loaders.cpp
                src/main/cpp/engine/map_utils.cpp
                src/main/cpp/engine/obj_model_loader.cpp
                src/main/cpp/engine/particle_effect.cpp
                src/main/cpp/engine/particle_emitter.cpp
                src/main/cpp/engine/particle_strategy.cpp
                src/main/cpp/engine/polygon_builder.cpp
                src/main/cpp/engine/preferences.cpp
                src/main/cpp/engine/ring_anim.cpp
                src/main/cpp/engine/ring_fade_anim.cpp
                src/main/cpp/engine/road_utils.cpp
                src/main/cpp/engine/scene.cpp
                src/main/cpp/engine/screen.cpp
                src/main/cpp/engine/spine_data.cpp
                src/main/cpp/engine/spine_renderer.cpp
                src/main/cpp/engine/statable.cpp
                src/main/cpp/engine/texture_atlas_loader.cpp
                src/main/cpp/engine/updater.cpp

                src/main/cpp/spine/Animation.c
                src/main/cpp/spine/AnimationState.c
                src/main/cpp/spine/AnimationStateData.c
                src/main/cpp/spine/Atlas.c
                src/main/cpp/spine/AtlasAttachmentLoader.c
                src/main/cpp/spine/Attachment.c
                src/main/cpp/spine/AttachmentLoader.c
                src/main/cpp/spine/Bone.c
                src/main/cpp/spine/BoneData.c
                src/main/cpp/spine/BoundingBoxAttachment.c
                src/main/cpp/spine/Event.c
                src/main/cpp/spine/EventData.c
                src/main/cpp/spine/extension.c
                src/main/cpp/spine/Json.c
                src/main/cpp/spine/MeshAttachment.c
                src/main/cpp/spine/RegionAttachment.c
                src/main/cpp/spine/Skeleton.c
                src/main/cpp/spine/SkeletonBounds.c
                src/main/cpp/spine/SkeletonData.c
                src/main/cpp/spine/SkeletonJson.c
                src/main/cpp/spine/Skin.c
                src/main/cpp/spine/SkinnedMeshAttachment.c
                src/main/cpp/spine/Slot.c
                src/main/cpp/spine/SlotData.c

                src/main/cpp/png/png.c
                src/main/cpp/png/pngerror.c
                src/main/cpp/png/pngget.c
                src/main/cpp/png/pngmem.c
                src/main/cpp/png/pngpread.c
                src/main/cpp/png/pngread.c
                src/main/cpp/png/pngrio.c
                src/main/cpp/png/pngrtran.c
                src/main/cpp/png/pngrutil.c
                src/main/cpp/png/pngset.c
                src/main/cpp/png/pngtrans.c
                src/main/cpp/png/pngwio.c
                src/main/cpp/png/pngwrite.c
                src/main/cpp/png/pngwtran.c
                src/main/cpp/png/pngwutil.c

                src/main/cpp/native_app_glue/android_native_app_glue.c

                src/main/cpp/json/json_reader.cpp
                src/main/cpp/json/json_value.cpp
                src/main/cpp/json/json_writer.cpp

                src/main/cpp/Box2D/Collision/b2BroadPhase.cpp
                src/main/cpp/Box2D/Collision/b2CollideCircle.cpp
                src/main/cpp/Box2D/Collision/b2CollideEdge.cpp
                src/main/cpp/Box2D/Collision/b2CollidePolygon.cpp
                src/main/cpp/Box2D/Collision/b2Collision.cpp
                src/main/cpp/Box2D/Collision/b2Distance.cpp
                src/main/cpp/Box2D/Collision/b2DynamicTree.cpp
                src/main/cpp/Box2D/Collision/b2TimeOfImpact.cpp
                src/main/cpp/Box2D/Collision/Shapes/b2ChainShape.cpp
                src/main/cpp/Box2D/Collision/Shapes/b2CircleShape.cpp
                src/main/cpp/Box2D/Collision/Shapes/b2EdgeShape.cpp
                src/main/cpp/Box2D/Collision/Shapes/b2PolygonShape.cpp
                src/main/cpp/Box2D/Common/b2BlockAllocator.cpp
                src/main/cpp/Box2D/Common/b2Draw.cpp
                src/main/cpp/Box2D/Common/b2Math.cpp
                src/main/cpp/Box2D/Common/b2Settings.cpp
                src/main/cpp/Box2D/Common/b2StackAllocator.cpp
                src/main/cpp/Box2D/Common/b2Timer.cpp
                src/main/cpp/Box2D/Dynamics/b2Body.cpp
                src/main/cpp/Box2D/Dynamics/b2ContactManager.cpp
                src/main/cpp/Box2D/Dynamics/b2Fixture.cpp
                src/main/cpp/Box2D/Dynamics/b2Island.cpp
                src/main/cpp/Box2D/Dynamics/b2World.cpp
                src/main/cpp/Box2D/Dynamics/b2WorldCallbacks.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2ChainAndCircleContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2CircleContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2Contact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2ContactSolver.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.cpp
                src/main/cpp/Box2D/Dynamics/Contacts/b2PolygonContact.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2DistanceJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2FrictionJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2GearJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2Joint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2MouseJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2PrismaticJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2PulleyJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2RevoluteJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2RopeJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2WeldJoint.cpp
                src/main/cpp/Box2D/Dynamics/Joints/b2WheelJoint.cpp
                src/main/cpp/Box2D/Rope/b2Rope.cpp

                src/main/cpp/android_main.cpp )


target_link_libraries(
                       ft-android
                       log
                       android
                       GLESv2
                       EGL
                       z)