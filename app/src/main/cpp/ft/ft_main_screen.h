#ifndef ft_main_screen_H
#define ft_main_screen_H

#include "engine.h"
#include "texture.h"
#include "updater.h"
#include "screen.h"
#include "command.h"
#include "buffer.h"
#include "sprite.h"
#include "texture_atlas_loader.h"
#include "camera.h"
#include "anim.h"
#include "polygon_builder.h"
#include "touch_helper.h"
#include "ft_button.h"

#include "ft_base_screen.h"

#include <string>

using namespace fte;

namespace ft {

class MainScreen: public BaseScreen {
private:
	Polygon* bg;

	Button play;
	Button credit;

	bool open_box;
	bool open_credits;

	fte::FTE_ENV env;

protected:

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader texture_atlas(std::string("screens/main.atlas"), env);
		texture_atlas.add_texture_descriptors(command);
	}

	void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader atlas_loader(std::string("screens/main.atlas"), env);

		bg = PolygonBuiler::create_sprite(atlas_loader, "bg", true, false);
		bg->set_origin_to(ORIGIN_CENTER);

		play.init(&atlas_loader, "play_bg;play_text:0.65", "play_bg_active;play_text_active:0.65");
		credit.init(&atlas_loader, "credit_bg;credit_text:0.65", "credit_bg_active;credit_text_active:0.65");

		open_box = false;
		open_credits = false;

		fade_in();
	}

	void do_init(FTE_ENV env) {
		this->env = env;
	}

	void do_resize(FTE_ENV env) {
		BaseScreen::do_resize(env);

		float square = get_screen_ratio() * .8;

		play.set_width(square);
		play.set_height(square * .5);
		play.set_translate_y(square * .2);
		play.update_rect();

		credit.set_width(square);
		credit.set_height(square * .5);
		credit.set_translate_y(-square * .2);
		credit.update_rect();
	}

	virtual void do_fade_out_done() {
		if (open_box)
			do_open_box();
		else if (open_credits)
			do_open_credits();
	}

	void do_update(float delta, RenderCommand* command) {
		const TouchHelper & th = get_touch_helper();

		float width = get_width();
		float height = get_height();

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, get_ortho_camera().get_view_proj_matrix());
		{
			command->add(bg);

			play.render_button(delta, command, th, get_ortho_camera(), width, height);
			credit.render_button(delta, command, th, get_ortho_camera(), width, height);
		}
		command->end_batch();

		switch (get_state()) {
		case SCREEN_STATE_DEFAULT:
			if (th.is_back_pressed()) {
				finish_application(env);
			} else if (play.is_button_tapped(th, get_ortho_camera(), width, height)) {
				fade_out();
				open_box = true;
			} else if (credit.is_button_tapped(th, get_ortho_camera(), width, height)) {
				fade_out();
				open_credits = true;
			}
			break;
		}

		BaseScreen::do_update(delta, command);
	}

	void do_open_box();
	void do_open_credits();

public:
	MainScreen() {
		bg = NULL;
	}

	~MainScreen() {
		DELETE(bg);
	}
}
;
} // namespace ft

#endif /* ft_main_screen_H */
