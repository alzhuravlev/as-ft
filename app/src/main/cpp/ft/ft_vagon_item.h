#ifndef ft_vagon_item_H
#define ft_vagon_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "touch_helper.h"
#include "ft_door_item.h"
#include "polygon_builder.h"
#include "ft_pogo_walk_effect.h"
#include "ft_zipper_fly_item.h"
#include "ft_base_mc_item.h"

namespace ft {

class VagonItem: public BaseMainCharacterItem {
private:

	Polygon* wheel1;
	Polygon* wheel2;
	Polygon* wheel3;
	Polygon* wheel4;

	b2Body* wheel_body1;
	b2Body* wheel_body2;

	b2WheelJoint* wheel_joint1;
	b2WheelJoint* wheel_joint2;

protected:

	virtual void do_release_polygons() {
		BaseMainCharacterItem::do_release_polygons();
		DELETE(wheel1);
		DELETE(wheel2);
		DELETE(wheel3);
		DELETE(wheel4);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseMainCharacterItem::do_create_polygons(cache_lighting, env);

//		create_mc_polygons("vagon", env);

		ObjModelLoader* model_loader = get_tower()->get_loaders().get_obj_model_loader("items/vagon.obj", env);

		wheel1 = PolygonBuiler::create_model(*model_loader, "wheel", true, false);
		wheel2 = PolygonBuiler::create_model(*model_loader, "wheel", true, false);
		wheel3 = PolygonBuiler::create_model(*model_loader, "wheel", true, false);
		wheel4 = PolygonBuiler::create_model(*model_loader, "wheel", true, false);

		float scale = get_scale_factor() * get_tower()->get_scale_factor_y();

		wheel1->set_scale(scale);
		wheel2->set_scale(scale);
		wheel3->set_scale(scale);
		wheel4->set_scale(scale);
	}

	virtual void do_reset_to_default() {
		BaseMainCharacterItem::do_reset_to_default();

		body->SetAngularDamping(4.0f);

		body->SetLinearVelocity(b2Vec2_zero);
		wheel_body1->SetLinearVelocity(b2Vec2_zero);
		wheel_body2->SetLinearVelocity(b2Vec2_zero);

		body->SetAngularVelocity(0.0f);
		wheel_body1->SetAngularVelocity(0.0f);
		wheel_body2->SetAngularVelocity(0.0f);

		b2Vec2 car_pos(col_to_map_x(), row_to_map_y());
		b2Vec2 wheel_pos1(car_pos.x - 0.6f * get_scale_factor(), car_pos.y - 0.15f * get_scale_factor());
		b2Vec2 wheel_pos2(car_pos.x + 0.6f * get_scale_factor(), car_pos.y - 0.15f * get_scale_factor());

		wheel_body1->SetTransform(wheel_pos1, 0.0f);
		wheel_body2->SetTransform(wheel_pos2, 0.0f);

		wheel_joint1->SetMotorSpeed(0.0f);
		wheel_joint2->SetMotorSpeed(0.0f);
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		BaseMainCharacterItem::do_create_child_items(items);
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BaseMainCharacterItem::do_create_body(box2d_loader);

		create_mc_body(box2d_loader, "mc_vagon", false, 2.0f, 1.0f, 0.7f, 0.0f);

		b2Vec2 car_pos(col_to_map_x(), row_to_map_y());
		b2Vec2 wheel_pos1(car_pos.x - 0.6f * get_scale_factor(), car_pos.y - 0.15f * get_scale_factor());
		b2Vec2 wheel_pos2(car_pos.x + 0.6f * get_scale_factor(), car_pos.y - 0.15f * get_scale_factor());

		Box2dItem* wheel_item = box2d_loader.find_item("mc_wheel");

		b2BodyDef def;
		def.type = b2_dynamicBody;

		def.position = wheel_pos1;
		wheel_body1 = get_tower()->get_world()->CreateBody(&def);

		def.position = wheel_pos2;
		wheel_body2 = get_tower()->get_world()->CreateBody(&def);

		b2FixtureDef fd;

		// wheels
		fd.density = 2.0f;
		fd.friction = 0.6f;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		attach_fixture_to_body(wheel_body1, &fd, wheel_item, get_scale_factor(), get_scale_factor());
		attach_fixture_to_body(wheel_body2, &fd, wheel_item, get_scale_factor(), get_scale_factor());

		// wheels join

		b2WheelJointDef jd;
		b2Vec2 axis1(0.0f, 1.0f);
		b2Vec2 axis2(0.0f, 1.0f);

		float frequencyHz = 4.0f;
		float dampingRatio = 0.5f;
		float motor_torque = 1500.0f;

		jd.Initialize(body, wheel_body1, wheel_body1->GetPosition(), axis1);
		jd.collideConnected = true;
		jd.motorSpeed = 0.0f;
		jd.maxMotorTorque = motor_torque;
		jd.enableMotor = true;
		jd.frequencyHz = frequencyHz;
		jd.dampingRatio = dampingRatio;
		wheel_joint1 = (b2WheelJoint*) get_tower()->get_world()->CreateJoint(&jd);

		jd.Initialize(body, wheel_body2, wheel_body2->GetPosition(), axis2);
		jd.collideConnected = true;
		jd.motorSpeed = 0.0f;
		jd.maxMotorTorque = motor_torque;
		jd.enableMotor = true;
		jd.frequencyHz = frequencyHz;
		jd.dampingRatio = dampingRatio;
		wheel_joint2 = (b2WheelJoint*) get_tower()->get_world()->CreateJoint(&jd);
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseMainCharacterItem::do_update(delta, camera);

//		update_polygon_from_body(wheel_body1, wheel1, &camera, get_distance_from_base_radius(), 0.0f);
//		update_polygon_from_body(wheel_body1, wheel2, &camera, get_distance_from_base_radius(), 0.0f);
//		update_polygon_from_body(wheel_body2, wheel3, &camera, get_distance_from_base_radius(), 0.0f);
//		update_polygon_from_body(wheel_body2, wheel4, &camera, get_distance_from_base_radius(), 0.0f);
	}

	virtual void do_render(float delta, RenderCommand* command, Camera & camera) {
		BaseMainCharacterItem::do_render(delta, command, camera);

		command->add(wheel1);
		command->add(wheel2);
		command->add(wheel3);
		command->add(wheel4);
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		BaseMainCharacterItem::do_render_effects(delta, command);
	}

	virtual void do_change_state(int state) {
		BaseMainCharacterItem::do_change_state(state);
	}

	virtual void change_vel(int dir) {
		float desired_motor_speed;
		float alpha = 0.1;
		switch (dir) {
		case MC_CHANGE_VEL_LEFT:
			desired_motor_speed = 40.0f;
			break;
		case MC_CHANGE_VEL_RIGHT:
			desired_motor_speed = -40.0f;
			break;
		case MC_CHANGE_VEL_STOP:
			desired_motor_speed = 0.0f;
			alpha = 0.05;
			break;
		}

		float current_motor_speed = wheel_joint1->GetMotorSpeed();
		current_motor_speed = current_motor_speed + (desired_motor_speed - current_motor_speed) * alpha;
		wheel_joint1->SetMotorSpeed(current_motor_speed);
		wheel_joint2->SetMotorSpeed(current_motor_speed);
	}

public:
	VagonItem(Tower* tower, float col, float row) :
			BaseMainCharacterItem(tower, col, row, tower->get_vagon_scale()) {
		wheel1 = NULL;
		wheel2 = NULL;
		wheel3 = NULL;
		wheel4 = NULL;
	}

	~VagonItem() {
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}

	int get_control_type() {
		return CONTROL_TYPE_SCREEN_SIDES;
	}

	void control_inactive() {
		BaseMainCharacterItem::control_inactive();
	}

	void control_tap() {
		BaseMainCharacterItem::control_tap();
	}

	void control_upper_left_pressed() {
		BaseMainCharacterItem::control_upper_left_pressed();
	}

	void control_left_pressed() {
		BaseMainCharacterItem::control_left_pressed();
	}

	void control_upper_right_pressed() {
		BaseMainCharacterItem::control_upper_right_pressed();
	}

	void control_right_pressed() {
		BaseMainCharacterItem::control_right_pressed();
	}

	void control_down_pressed() {
		BaseMainCharacterItem::control_down_pressed();
	}

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		BaseMainCharacterItem::begin_contact(my_fixture, other_fixture, contact);
	}

	virtual void end_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		BaseMainCharacterItem::end_contact(my_fixture, other_fixture, contact);
	}

	virtual void pre_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		BaseMainCharacterItem::pre_solve_contact(my_fixture, other_fixture, contact, f);
	}

	virtual void post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		BaseMainCharacterItem::post_solve_contact(my_fixture, other_fixture, contact, f);
	}
}
;

} // namespace ft

#endif /* ft_vagon_item_H */
