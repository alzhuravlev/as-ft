#ifndef ft_key_item_H
#define ft_key_item_H

#include "ft_base_collectable_item.h"
#include "ft_key_collecting_effect.h"
#include "ft_key_spine_animated_character.h"

namespace ft {

class KeyItem: public BaseCollectableItem {
private:
	KeyCollectingEffect collecting_effect;

	int color;
	bool hidden;

protected:

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new KeySpineAnimatedCharacter(color);
	}

	virtual void do_reset_to_default() {
		reset_collectable_to_default(hidden ? COLLECTABLE_STATE_HIDDEN : COLLECTABLE_STATE_ACTIVE);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseCollectableItem::do_create_polygons(cache_lighting, env);

		Box2dLoader* particle_shape_loader = get_tower()->get_particles_shape_loader(env);
		TextureAtlasLoader* particle_atlas = get_tower()->get_particles_atlas(env);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);
		collecting_effect.init(get_tower()->get_loaders().get_obj_model_loader("items/mosquito.obj", env), env);
		collecting_effect.set_params_xyz(x, y, z);
		collecting_effect.set_params_scale(get_scale_for_particles());

		RingFadeAnimConfig r_anim_config;
		r_anim_config.scale = get_tower()->get_scale_factor_y() * get_scale_factor() * 2.5f;
		r_anim_config.amplitude = 0.06f;
		r_anim_config.waves_count = 2;
		r_anim_config.velocity = 100.0f;
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_collectable_body(box2d_loader, "key");
	}

	virtual void do_change_state(int state) {
		BaseCollectableItem::do_change_state(state);

		switch (state) {
		case COLLECTABLE_STATE_ACTIVE:
			break;

		case COLLECTABLE_STATE_COLLECTING:
			get_tower()->collect_key(color);
			collecting_effect.start(true);
			break;

		case COLLECTABLE_STATE_INACTIVE:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseCollectableItem::do_update(delta, camera);
		collecting_effect.update(delta, camera);

		switch (get_state()) {
		case COLLECTABLE_STATE_COLLECTING:
			if (!collecting_effect.is_active() && !is_animation_playing())
				change_state(COLLECTABLE_STATE_INACTIVE);
			break;

		case COLLECTABLE_STATE_SHOWING:
			if (!is_animation_playing())
				change_state(COLLECTABLE_STATE_ACTIVE);
			break;

		}
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		collecting_effect.render(command);
	}

public:
	KeyItem(Tower* tower, int col, float row, float color, bool hidden = false) :
			BaseCollectableItem(tower, col, row, 1.0f) {
		this->color = color;
		this->hidden = hidden;
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}
};

} // namespace ft

#endif /* ft_key_item_H */
