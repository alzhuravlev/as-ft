#ifndef ft_block_item_H
#define ft_block_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "ft_base_monster_item.h"
#include "ft_block_spine_animated_character.h"

namespace ft {

class BlockItem: public BaseMonsterItem {
private:

protected:

	virtual void do_release_polygons() {
		BaseMonsterItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseMonsterItem::do_create_polygons(cache_lighting, env);
	}

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new BlockSpineAnimatedCharacter;
	}
	virtual void do_reset_to_default() {
		BaseMonsterItem::do_reset_to_default();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BaseMonsterItem::do_create_body(box2d_loader);

		create_monster_body(box2d_loader, "block", 0.0f, 200.0f, 0.0f, 0.0f);

		b2PrismaticJointDef pjd;

		b2Vec2 axis(0.0f, 1.0f);
		axis.Normalize();
		pjd.Initialize(get_tower()->get_ground_item()->get_body(), body, b2Vec2(col_to_map_x(), row_to_map_y()), axis);

		pjd.lowerTranslation = 0.0f;
		pjd.upperTranslation = 0.0f;

		pjd.enableLimit = true;
		pjd.enableMotor = false;

		b2PrismaticJoint* joint = (b2PrismaticJoint*) get_tower()->get_world()->CreateJoint(&pjd);

	}

	virtual void do_change_state(int state) {
		BaseMonsterItem::do_change_state(state);
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseMonsterItem::do_update(delta, camera);
	}

public:
	BlockItem(Tower* tower, float col, float row, int key_color = 0) :
			BaseMonsterItem(tower, col, row, key_color, tower->get_block_scale()) {
	}

	~BlockItem() {
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}
}
;

} // namespace ft

#endif /* ft_block_item_H */
