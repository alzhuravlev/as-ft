#ifndef ft_button_for_platform_item_H
#define ft_button_for_platform_item_H

#include "polygon_cache.h"
#include "camera.h"
#include "command.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "ft_base_button_item.h"
#include "spine_renderer.h"

namespace ft {

class ButtonForPlatformItem: public BaseButtonItem {
private:
	int color;

protected:

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_button_body(box2d_loader, "button");
	}

	virtual void do_reset_to_default() {
		BaseButtonItem::do_reset_to_default();
		change_state(get_tower()->is_button_active(color) ? BUTTON_STATE_ON : BUTTON_STATE_OFF, true);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseButtonItem::do_create_polygons(cache_lighting, env);
		create_button_polygons("button", "button", get_button_texture_name(color), env);
	}

	virtual void do_change_state(int state) {
		BaseButtonItem::do_change_state(state);

		switch (state) {
		case BUTTON_STATE_ON:
			get_tower()->set_button_active(color, true);
			break;
		case BUTTON_STATE_OFF:
			get_tower()->set_button_active(color, false);
			break;

		case BUTTON_STATE_TURNING_ON:
			get_tower()->set_button_active(color, true);
			break;
		case BUTTON_STATE_TURNING_OFF:
			get_tower()->set_button_active(color, false);
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseButtonItem::do_update(delta, camera);

		switch (get_state()) {
		case BUTTON_STATE_ON:
			if (!get_tower()->is_button_active(color))
				change_state(BUTTON_STATE_TURNING_OFF);
			break;
		case BUTTON_STATE_OFF:
			if (get_tower()->is_button_active(color))
				change_state(BUTTON_STATE_TURNING_ON);
			break;

		case BUTTON_STATE_TURNING_ON:
			break;
		case BUTTON_STATE_TURNING_OFF:
			break;
		}
	}

public:
	ButtonForPlatformItem(Tower* tower, float col, float row, int color) :
			BaseButtonItem(tower, col, row, 0.6f) {
		this->color = color;
	}

	virtual void toggle_button() {
		BaseButtonItem::toggle_button();
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}
};

} // namespace ft

#endif /* ft_button_for_platform_item_H */
