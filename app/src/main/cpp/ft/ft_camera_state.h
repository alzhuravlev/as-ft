#ifndef ft_camera_state_H
#define ft_camera_state_H

#include "statable.h"
#include "camera.h"
#include "anim.h"

using namespace fte;

namespace ft {

const int CAMERA_STATE_FOLLOW = 1;
const int CAMERA_STATE_ROTATE = 2;
const int CAMERA_STATE_START = 3;
const int CAMERA_STATE_RESTARTING = 4;
const int CAMERA_STATE_FINISHED = 5;
const int CAMERA_STATE_SHAKE = 6;

// tuning

const float CAMERA_ALPHA_ANGLE_Y = 10.0f;
const float CAMERA_ALPHA_ANGLE_X = 1.0f;
const float CAMERA_ALPHA_SCALE = 2.0f;
const float CAMERA_ALPHA_Y = 10.0f;
const float CAMERA_ALPHA_AXIS_ANGLE = 6.0f;

const float CAMERA_FOLLOW_SCALE = 0.83f;
const float CAMERA_ROTATE_SCALE = CAMERA_FOLLOW_SCALE * 0.70f;
const float CAMERA_START_SCALE = CAMERA_FOLLOW_SCALE * 0.65f;
const float CAMERA_RESTART_SCALE = CAMERA_FOLLOW_SCALE * 1.3f;
const float CAMERA_MAIN_CHARACTER_MOVE_SCALE = CAMERA_FOLLOW_SCALE * 0.95f;
const float CAMERA_FINISHED_SCALE = CAMERA_FOLLOW_SCALE * 0.75f;

const float CAMERA_RESTARTING_DURATION = 2.0f;
const float CAMERA_RESTARTING_ANGLE = 3.0f * M_2xPI;

const float CAMERA_SHAKE_DURATION = 0.65f;
const float CAMERA_SHAKE_AMPLITUDE = M_PI_8;
const float CAMERA_SHAKE_FREQ = 2.0f * M_2xPI / CAMERA_SHAKE_DURATION;

const float CAMERA_ROTATE_ANGLE_X = M_PI_32;
const float CAMERA_FOLLOW_ANGLE_X = M_PI_16;
const float CAMERA_MAIN_CHARACTER_MOVE_ANGLE_X = M_PI_32;
const float CAMERA_START_ANGLE_X = M_PI_4;
const float CAMERA_FINISHED_ANGLE_X = M_PI_8;

class CameraState: public Statable {
private:

	float from_angle, to_angle, duration;
	LinearInterpolatedValue last_scale;
	LinearInterpolatedValue last_y;
	LinearInterpolatedValue last_angle;
	LinearInterpolatedValue last_angle_x;
	LinearInterpolatedValue last_axis_angle;

	float default_scale;

	AccelerateDecelerateInterpolator interpolator;

protected:

	virtual void do_change_state(int state) {
		switch (state) {
		case CAMERA_STATE_FOLLOW:
			last_axis_angle.set_target(0.0f);

			last_scale.set_target(CAMERA_FOLLOW_SCALE);

			last_angle_x.set_target(CAMERA_FOLLOW_ANGLE_X);
			break;

		case CAMERA_STATE_ROTATE:
			last_axis_angle.set_target(0.0f);

			from_angle = last_angle.get_value();

			last_scale.set_target(CAMERA_ROTATE_SCALE);

			last_angle_x.set_target(CAMERA_ROTATE_ANGLE_X);
			break;

		case CAMERA_STATE_START:
			last_axis_angle.set_target(0.0f);
			last_axis_angle.set_current(0.0f);

			last_angle.set_current(0.0f);
			last_y.set_current(0.0f);

			last_scale.set_current(CAMERA_START_SCALE);
			last_scale.set_target(CAMERA_FOLLOW_SCALE);

			last_angle_x.set_current(CAMERA_START_ANGLE_X);
			last_angle_x.set_target(CAMERA_FOLLOW_ANGLE_X);

			break;

		case CAMERA_STATE_FINISHED:
			last_axis_angle.set_target(0.0f);

			last_scale.set_target(CAMERA_FINISHED_SCALE);

			last_angle_x.set_target(CAMERA_FINISHED_ANGLE_X);

			break;

		case CAMERA_STATE_RESTARTING:
			last_axis_angle.set_target(0.0f);
			last_axis_angle.set_current(0.0f);

			last_scale.set_target(CAMERA_RESTART_SCALE);
			break;
		}
	}

public:

	CameraState() {
		from_angle = 0.0f;
		to_angle = 0.0f;
		duration = 0.0f;
		default_scale = 0.0f;

		last_angle.set_alpha(CAMERA_ALPHA_ANGLE_Y);
		last_axis_angle.set_alpha(CAMERA_ALPHA_AXIS_ANGLE);
		last_scale.set_alpha(CAMERA_ALPHA_SCALE);
		last_y.set_alpha(CAMERA_ALPHA_Y);
		last_angle_x.set_alpha(CAMERA_ALPHA_ANGLE_X);
	}

	inline void update(float delta, Camera & camera, float y, float angle) {
		update_state(delta);

		if (F_EQ(last_y.get_value(), 0.0f))
			last_y.set_current(y);

		if (F_EQ(last_angle.get_value(), 0.0f))
			last_angle.set_current(angle);

		switch (get_state()) {

		case CAMERA_STATE_FINISHED:
		case CAMERA_STATE_FOLLOW:
		case CAMERA_STATE_START:
			last_angle.set_target(angle);

			last_angle.clamp_cycle_target(M_2xPI);
			last_angle.update(delta);

			break;

		case CAMERA_STATE_ROTATE: {
			float st = get_state_time();
			if (st > duration) {
				st = duration;
				change_state(CAMERA_STATE_FOLLOW);
			}
			float k = interpolator.interpolate(st / duration);
			last_angle.set_current(from_angle + (to_angle - from_angle) * k);
		}
			break;

		case CAMERA_STATE_RESTARTING: {

			float st = get_state_time();
			if (st > CAMERA_RESTARTING_DURATION) {
				st = CAMERA_RESTARTING_DURATION;
				change_state(CAMERA_STATE_FOLLOW);
			}
			float k = interpolator.interpolate(st / CAMERA_RESTARTING_DURATION);

			last_axis_angle.set_target(CAMERA_RESTARTING_ANGLE * k);

			last_angle.set_target(angle);

			last_angle.clamp_cycle_target(M_2xPI);
			last_angle.update(delta);
		}
			break;

		case CAMERA_STATE_SHAKE: {

			float st = get_state_time();
			if (st > CAMERA_SHAKE_DURATION) {
				st = CAMERA_SHAKE_DURATION;
				change_state(CAMERA_STATE_FOLLOW);
			}
			float k = sin_fast(CAMERA_SHAKE_FREQ * st);

			last_axis_angle.set_target(CAMERA_SHAKE_AMPLITUDE * k);

			last_angle.set_target(angle);

			last_angle.clamp_cycle_target(M_2xPI);
			last_angle.update(delta);
		}
			break;

		}

		last_scale.update(delta);

		last_angle_x.update(delta);

		last_y.set_target(y);
		last_y.update(delta);

		last_axis_angle.update(delta);

		camera.set_translate_y(last_y.get_value());
		camera.set_scale(last_scale.get_value() * default_scale);
		camera.set_angle_y(-last_angle.get_value());
		camera.set_angle_x(last_angle_x.get_value());
		camera.set_axis(0.0f, 0.0f, 1.0f, last_axis_angle.get_value());

		camera.update_view_proj_matrix();
	}

	void follow() {
		change_state(CAMERA_STATE_FOLLOW);
	}

	void start() {
		change_state(CAMERA_STATE_START, true);
	}

	void finish() {
		change_state(CAMERA_STATE_FINISHED);
	}

	void rotate(float to_angle, float duration) {
		this->duration = duration;
		this->to_angle = to_angle;
		change_state(CAMERA_STATE_ROTATE);
	}

	void main_character_move() {
		last_scale.set_target(CAMERA_MAIN_CHARACTER_MOVE_SCALE);
		last_angle_x.set_target(CAMERA_MAIN_CHARACTER_MOVE_ANGLE_X);
	}

	void main_character_idle() {
		last_scale.set_target(CAMERA_FOLLOW_SCALE);
		last_angle_x.set_target(CAMERA_FOLLOW_ANGLE_X);
	}

	void set_default_scale(float default_scale) {
		this->default_scale = default_scale;
	}

	void restart() {
		change_state(CAMERA_STATE_RESTARTING);
	}

	void shake() {
		change_state(CAMERA_STATE_SHAKE);
	}
}
;

} // namespace ft

#endif /* ft_camera_state_H */
