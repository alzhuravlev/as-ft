#ifndef ft_padlock_item_H
#define ft_padlock_item_H

#include "ft_base_collectable_item.h"
#include "ft_padlock_collecting_effect.h"
#include "ft_padlock_spine_animated_character.h"

namespace ft {

class PadlockItem: public BaseCollectableItem {
private:
	PadlockCollectingEffect collecting_effect;

	int color;

protected:

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new PadlockSpineAnimatedCharacter(color);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseCollectableItem::do_create_polygons(cache_lighting, env);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);
		collecting_effect.init(get_tower()->get_loaders().get_obj_model_loader("items/mosquito.obj", env), env);
		collecting_effect.set_params_xyz(x, y, z);
		collecting_effect.set_params_scale(get_scale_for_particles());
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_collectable_body(box2d_loader, "padlock");
	}

	virtual void do_change_state(int state) {
		BaseCollectableItem::do_change_state(state);

		switch (state) {
		case COLLECTABLE_STATE_ACTIVE:
			break;

		case COLLECTABLE_STATE_COLLECTING:
			collecting_effect.start(true);
			break;

		case COLLECTABLE_STATE_INACTIVE:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseCollectableItem::do_update(delta, camera);

		switch (get_state()) {
		case COLLECTABLE_STATE_COLLECTING:
			collecting_effect.update(delta, camera);
			if (!collecting_effect.is_active() && !is_animation_playing())
				change_state(COLLECTABLE_STATE_INACTIVE);
			break;
		case COLLECTABLE_STATE_BLINK:
			if (!is_animation_playing())
				change_state(COLLECTABLE_STATE_ACTIVE);
			break;
		}

	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		collecting_effect.render(command);
	}

public:
	PadlockItem(Tower* tower, float col, float row, int color) :
			BaseCollectableItem(tower, col, row, 1.4f) {
		this->color = color;
		this->set_distance_from_base_radius(get_tower()->get_platform_gap());
	}

	virtual bool can_collect() {
		return get_tower()->has_key(color);
	}

	void blink() {
		switch (get_state()) {
		case COLLECTABLE_STATE_ACTIVE:
			change_state(COLLECTABLE_STATE_BLINK);
			break;
		}
	}
};

} // namespace ft

#endif /* ft_padlock_item_H */
