#ifndef ft_pogo_walk_effect_H
#define ft_pogo_walk_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower_item_consts.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class PogoWalkEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleStartingRotationStrategy_RandomizeAngularVelocity particleRotationResolverStrategy_RandomizeAngularVelocity;
	ParticleStartingColorResolverStrategy_Simple particleStartingColorResolverStrategy_Simple;

	ParticleRotationResolverStrategy_FaceToCameraAndAngularVelocity particleRotationResolverStrategy_FaceToCamera;
	ParticleScaleResolverStrategy_AppearDisapear particleScaleResolverStrategy_AppearDisapear;
	ParticleAlphaResolverStrategy_AppearDisapearAndPulse particleAlphaResolverStrategy_AppearDisapearAndPulse;

	TextureAtlasLoader* atlas;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 3;
		config.max_count = 5;

		config.min_delay = 0.0f;
		config.max_delay = 0.5f;

		config.min_life_time = 0.4f;
		config.max_life_time = 0.9f;

		particleScaleResolverStrategy_AppearDisapear.set_normal_scale(BASE_RADIUS * 0.02f, BASE_RADIUS * 0.12f);
		particleRotationResolverStrategy_RandomizeAngularVelocity.set_angular_velocity(-M_2xPI, M_2xPI);

		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleRotationResolverStrategy_RandomizeAngularVelocity);
		pe.add_initial_strategy(&particleStartingColorResolverStrategy_Simple);

		pe.add_strategy(&particleScaleResolverStrategy_AppearDisapear);
		pe.add_strategy(&particleAlphaResolverStrategy_AppearDisapearAndPulse);
		pe.add_strategy(&particleRotationResolverStrategy_FaceToCamera);

		pe.init_with_sprite(config, *atlas, "cloud");

		emitters.push_back(&pe);
	}

public:

	void init(TextureAtlasLoader* atlas, FTE_ENV env) {
		this->atlas = atlas;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleStartingPositionStrategy_Sphere.set_radius(scale * 0.2f);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
	}

	void set_params_count(int min_count, int max_count) {
		pe.set_count(min_count, max_count);
	}

	void set_params_color(uint32_t color) {
		uint32_t mixed_color;
		mix_color_c(color, 0xffffffff, 0.7f, mixed_color);
		particleStartingColorResolverStrategy_Simple.set_color(mixed_color);
	}
};

} // namespace ft

#endif /* ft_pogo_walk_effect_H */
