#include "ft_main_screen.h"
#include "ft_box_screen.h"

namespace ft {

void MainScreen::do_open_box() {
	get_updater()->set_pending_screen(new BoxScreen);
}

void MainScreen::do_open_credits() {
}

} // namespace ft
