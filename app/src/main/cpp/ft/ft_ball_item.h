#ifndef ft_ball_item_H
#define ft_ball_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "ft_base_monster_item.h"
#include "ft_ball_spine_animated_character.h"
#include "ft_micro_cat_keyframe_animated_character.h"
#include "ft_micro_cow_keyframe_animated_character.h"
#include "ft_micro_pig_keyframe_animated_character.h"
#include "ft_micro_rooster_keyframe_animated_character.h"
#include "ft_micro_bat_keyframe_animated_character.h"
#include "ft_micro_demon_keyframe_animated_character.h"

namespace ft {

class BallItem: public BaseMonsterItem {
private:

	float push_delay;
	float push_vel_multiplier;

protected:

	virtual void do_release_polygons() {
		BaseMonsterItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseMonsterItem::do_create_polygons(cache_lighting, env);
	}

	virtual BaseAnimatedCharacter* create_animated_character() {
//		return new BallSpineAnimatedCharacter(get_aggressive_level());
//		return new MicroPigKeyframeAnimatedCharacter;
//		return new MicroPigKeyframeAnimatedCharacter;
		return new MicroDemonKeyframeAnimatedCharacter;
//		return new MicroCowKeyframeAnimatedCharacter;
//		return new MicroCatKeyframeAnimatedCharacter;
//		return new MicroBatKeyframeAnimatedCharacter;
	}

	virtual void do_reset_to_default() {
		BaseMonsterItem::do_reset_to_default();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BaseMonsterItem::do_create_body(box2d_loader);

		create_monster_body(box2d_loader, "ball", 0.0f, 2.0f, 0.0f, 0.7f);
	}

	virtual void do_change_state(int state) {
		BaseMonsterItem::do_change_state(state);

		switch (state) {
		case MONSTER_STATE_SLEEPING:
			break;

		case MONSTER_STATE_WAKING:
			break;

		case MONSTER_STATE_ACTIVE:
			break;

		case MONSTER_STATE_PUNCH:
			break;

		case MONSTER_STATE_DYING:
			get_tower()->catch_ball();
			break;

		case MONSTER_STATE_DIED:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseMonsterItem::do_update(delta, camera);

		switch (get_state()) {
		case MONSTER_STATE_SLEEPING:
			break;

		case MONSTER_STATE_WAKING:
			break;

		case MONSTER_STATE_ACTIVE:
			push_to_main_character();
			break;

		case MONSTER_STATE_PUNCH:
			break;

		case MONSTER_STATE_DYING:
			break;

		case MONSTER_STATE_DIED:
			break;

		case MONSTER_STATE_INACTIVE:
			break;
		}

	}

	float get_push_delay() {
		switch (get_aggressive_level()) {
		case MONSTER_AGGRESSIVE_LEVEL_DEFAULT:
		case MONSTER_AGGRESSIVE_LEVEL_LOW:
			return 0.0f;
		case MONSTER_AGGRESSIVE_LEVEL_MED:
			return 0.0f;
		case MONSTER_AGGRESSIVE_LEVEL_HIGH:
			return 0.0f;
		}
		return 1.0f;
	}

	float get_push_vel_multiplier() {
		switch (get_aggressive_level()) {
		case MONSTER_AGGRESSIVE_LEVEL_DEFAULT:
		case MONSTER_AGGRESSIVE_LEVEL_LOW:
			return 1.5f;
		case MONSTER_AGGRESSIVE_LEVEL_MED:
			return 3.5f;
		case MONSTER_AGGRESSIVE_LEVEL_HIGH:
			return 4.5f;
		}
		return 1.0f;
	}

	void push_to_main_character() {

		if (get_state_time() < push_delay)
			return;

		set_state_time(0.0f);

		const b2Vec2 & vel = body->GetLinearVelocity();

		const b2Vec2 & my_pos = get_map_position();

		b2Vec2 dest_pos;
		get_tower()->get_main_character_map_position(dest_pos);

		if (fabsf(my_pos.x - dest_pos.x) > get_tower()->get_map_width_2())
			if (my_pos.x < dest_pos.x)
				dest_pos.x -= get_tower()->get_map_width();
			else
				dest_pos.x += get_tower()->get_map_width();

		b2Vec2 desired_vel = dest_pos - my_pos;
		desired_vel.Normalize();

		desired_vel *= push_vel_multiplier;

		desired_vel -= vel;
		desired_vel *= body->GetMass();

		body->ApplyForceToCenter(desired_vel);
	}

public:
	BallItem(Tower* tower, float col, float row, int key_color = 0, int aggressive_level = MONSTER_AGGRESSIVE_LEVEL_DEFAULT) :
			BaseMonsterItem(tower, col, row, key_color, tower->get_ball_scale(), aggressive_level) {
		tower->add_ball();
		push_delay = get_push_delay();
		push_vel_multiplier = get_push_vel_multiplier();
	}

	~BallItem() {
	}
}
;

} // namespace ft

#endif /* ft_ball_item_H */
