#ifndef ft_mosquito_item_H
#define ft_mosquito_item_H

#include "ft_base_collectable_item.h"
#include "ft_mosquito_collecting_effect.h"
#include "ft_mosquito_spine_animated_character.h"

namespace ft {

const static float MOSQUITO_COLLECT_INC_TIME_LEFT = 2.0f;

class MosquitoItem: public BaseCollectableItem {
private:
	MosquitoCollectingEffect collecting_effect;

//	SpineRenderer* spine_text_collect;
	Vec3 spine_text_position;

protected:

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new MosquitoSpineAnimatedCharacter;
	}

	virtual void do_release_polygons() {
		BaseCollectableItem::do_release_polygons();
//		DELETE(spine_text_collect);
		collecting_effect.release();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseCollectableItem::do_create_polygons(cache_lighting, env);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		collecting_effect.set_params_color(get_tower()->get_bubble_color_i());
		collecting_effect.set_params_xyz(x, y + get_onscreen_dim(), z);
		collecting_effect.set_params_scale(get_scale_for_particles());
		collecting_effect.init(get_tower()->get_particles_atlas(env), env);

//		spine_text_collect = create_spine_text(to_string(int(MOSQUITO_COLLECT_INC_TIME_LEFT), true), 0x7700ff00, env);
	}

	virtual void do_reset_to_default() {
		BaseCollectableItem::do_reset_to_default();
//		spine_text_collect->clear_animation();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_collectable_body(box2d_loader, "mosquito");
	}

	virtual void do_change_state(int state) {
		BaseCollectableItem::do_change_state(state);

		switch (state) {
		case COLLECTABLE_STATE_ACTIVE:
			break;

		case COLLECTABLE_STATE_COLLECTING:

			get_tower()->catch_mosquito();

			collecting_effect.start(true);

			get_tower()->inc_time_left(MOSQUITO_COLLECT_INC_TIME_LEFT);
//			spine_text_collect->clear_animation();
//			spine_text_collect->set_animation("active", false);
			float a;
			get_tower()->get_main_character_world_position(spine_text_position.x, spine_text_position.y, spine_text_position.z, a);

			get_tower()->play_mosquito_collected();

			break;

		case COLLECTABLE_STATE_INACTIVE:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseCollectableItem::do_update(delta, camera);

		collecting_effect.update(delta, camera);

//		if (!spine_text_collect->is_animation_complete())
//			spine_text_collect->update(delta, spine_text_position, Vec3(camera.get_angle_x(), camera.get_angle_y(), camera.get_angle_z()), Vec3(0.3f, 0.3f, 0.3f));

		switch (get_state()) {
		case COLLECTABLE_STATE_COLLECTING:
			if (!collecting_effect.is_active() && !is_animation_playing() /*&& spine_text_collect->is_animation_complete()*/)
				change_state(COLLECTABLE_STATE_INACTIVE);
			break;
		}
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		collecting_effect.render(command);

//		if (!spine_text_collect->is_animation_complete())
//			spine_text_collect->render(command);
	}

public:
	MosquitoItem(Tower* tower, float col, float row) :
			BaseCollectableItem(tower, col, row, tower->get_mosquito_scale()) {
		tower->add_mosquito();
//		spine_text_collect = NULL;
	}
};

} // namespace ft

#endif /* ft_mosquito_item_H */
