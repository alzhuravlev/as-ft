#ifndef ft_door_with_padlock_item_H
#define ft_door_with_padlock_item_H

#include "ft_tower.h"
#include "ft_base_door_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "anim.h"
#include "ft_padlock_item.h"

namespace ft {

class DoorWithPadLockItem: public BaseDoorItem {
private:

	PadlockItem* padlock_item;

	int color;

protected:

	virtual void do_release_polygons() {
		BaseDoorItem::do_release_polygons();
	}

	virtual void do_reset_to_default() {
		BaseDoorItem::do_reset_to_default();
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		BaseDoorItem::do_create_child_items(items);

		int opposite_col = (int) (get_col() + get_tower()->get_map_width_2()) % get_tower()->get_cols();

		PadlockItem* opposite_padlock_item = new PadlockItem(get_tower(), opposite_col, get_row(), color);
		DoorWithPadLockItem* opposite_door_item = new DoorWithPadLockItem(get_tower(), opposite_col, get_row(), color, opposite_padlock_item);

		items.push_back(opposite_door_item);
		items.push_back(opposite_padlock_item);

		items.push_back(padlock_item = new PadlockItem(get_tower(), get_col(), get_row(), color));

		set_opposite_door_item(opposite_door_item);
		opposite_door_item->set_opposite_door_item(this);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseDoorItem::do_create_polygons(cache_lighting, env);

		create_door_polygons(env, "door");
	}

	virtual void do_change_state(int state) {
		BaseDoorItem::do_change_state(state);

		switch (state) {
		case DOOR_STATE_CLOSE:
			break;
		case DOOR_STATE_OPEN:
			break;
		case DOOR_STATE_CLOSING:
			break;
		case DOOR_STATE_OPENING:
			break;
		}
	}

public:
	DoorWithPadLockItem(Tower* tower, float col, float row, int color, PadlockItem* padlock_item = NULL) :
			BaseDoorItem(tower, col, row, TYPE_DOOR, tower->get_door_scale_y()) {
		this->color = color;
		this->padlock_item = padlock_item;
	}

	~DoorWithPadLockItem() {
	}

	bool can_walk_through_door() {
		return get_tower()->has_key(color);
	}

	virtual void open_door() {
		if (get_tower()->has_key(color))
			BaseDoorItem::open_door();
		else
			padlock_item->blink();
	}

	virtual void close_door() {
		BaseDoorItem::close_door();
	}
};

}
// namespace ft

#endif /* ft_door_with_padlock_item_H */
