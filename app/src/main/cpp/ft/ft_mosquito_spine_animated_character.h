#ifndef ft_mosquito_spine_animated_character_H
#define ft_mosquito_spine_animated_character_H

#include "ft_spine_animated_character.h"

namespace ft {

class MosquitoSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

protected:

	void do_release() {
		SpineAnimatedCharacter::do_release();
	}

	void do_reset() {
		SpineAnimatedCharacter::do_reset();
	}

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {
		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/mosquito.obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, "items/mosquito.atlas", "items/mosquito.json", "mosquito", item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y(), true, true, false, false, false, env);
		spine_data->set_mix("showing", "active", 0.3f);
		spine_data->set_mix("active", "collecting", 0.3f);
		spine_data->set_mix("active", "blink", 0.3f);
		spine_data->set_mix("blink", "active", 0.3f);

		spine = new SpineRenderer(spine_data);
	}

public:

	MosquitoSpineAnimatedCharacter() {
	}

};

} // namespace ft

#endif /* ft_mosquito_spine_animated_character_H */
