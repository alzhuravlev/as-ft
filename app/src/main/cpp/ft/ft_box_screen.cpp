#include "ft_box_screen.h"
#include "ft_main_screen.h"
#include "ft_level_screen.h"

namespace ft {

const std::string BoxScreen::BoxScreen_current_box_index = "BoxScreen_current_box_index";

void BoxScreen::do_open_back() {
	get_updater()->set_pending_screen(new MainScreen);
}

void BoxScreen::do_open_levels() {
	if (current_box_index >= 0 && current_box_index <= boxes.size()) {
		BoxItem box_item = boxes[current_box_index];
		get_updater()->set_pending_screen(new LevelScreen(box_item));
	}
}

} // namespace ft
