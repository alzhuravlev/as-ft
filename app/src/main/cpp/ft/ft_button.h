#ifndef ft_button_H
#define ft_button_H

#include "transformable.h"
#include "polygon.h"
#include "polygon_builder.h"
#include "fte_utils.h"
#include "camera.h"
#include "touch_helper.h"

using namespace fte;

namespace ft {

class Button: public Transformable {
private:
	std::vector<Polygon*> polygons_active;
	std::vector<Polygon*> polygons_inactive;

	struct _PARAMS {
		float scale;
		float alpha;
		uint32_t color;
		float offset_x;
		float offset_y;
	};

	std::vector<_PARAMS> polygons_params_active;
	std::vector<_PARAMS> polygons_params_inactive;

	Rect rect;
	bool enabled;

	float w, h;

	void update_rect(const Rect & rect, std::vector<Polygon*> & polygons, const std::vector<_PARAMS> & polygons_params) {
		std::vector<Polygon*>::iterator it = polygons.begin();
		std::vector<Polygon*>::iterator end = polygons.end();

		std::vector<_PARAMS>::const_iterator params_it = polygons_params.begin();
		std::vector<_PARAMS>::const_iterator params_end = polygons_params.end();

		for (; it != end && params_it != params_end; ++it, ++params_it) {
			Polygon* p = *it;
			_PARAMS params = *params_it;

			Rect r;
			r = rect;
			r.translate(w * params.offset_x, h * params.offset_y);
			r.mul(params.scale);
			r.translate(x, y);

			p->set_bounds_inside_preserve_ratio(r);
			p->set_alpha(alpha * params.alpha);
			p->set_color(params.color);
		}
	}

	void create_polygon_for_layer(const TextureAtlasLoader* atlas, const FontLoader* font_loader, const std::string & layer, std::vector<Polygon*> & polygons, std::vector<_PARAMS> & polygons_params, Sprite* font_sprite) {

		std::vector<std::string> layer_details;
		split(layer, ':', layer_details);

		if (layer_details.size() == 0)
			return;

		Polygon* p = NULL;
		int idx_offset;

		if (layer_details[0] == "t") {
			if (font_loader && layer_details.size() > 1) {
				std::wstring wtext;
				convert_string_to_wstring(layer_details[1], wtext);
				p = PolygonBuiler::create_static_text(*font_loader, wtext, font_sprite);
				idx_offset = 2;
			}
		} else {
			if (atlas) {
				p = PolygonBuiler::create_sprite(*atlas, layer_details[0], true, false);
				idx_offset = 1;
			}
		}

		if (p) {
			p->set_origin_to(ORIGIN_CENTER);
			polygons.push_back(p);

			_PARAMS params;
			params.scale = layer_details.size() > idx_offset ? atof(layer_details[idx_offset].c_str()) : 1.0f;
			params.alpha = layer_details.size() > idx_offset + 1 ? atof(layer_details[idx_offset + 1].c_str()) : 1.0f;
			params.color = layer_details.size() > idx_offset + 2 ? atoi(layer_details[idx_offset + 2].c_str()) : 0xffffffff;
			params.offset_x = layer_details.size() > idx_offset + 3 ? atof(layer_details[idx_offset + 3].c_str()) : 0.0f;
			params.offset_y = layer_details.size() > idx_offset + 4 ? atof(layer_details[idx_offset + 4].c_str()) : 0.0f;

			polygons_params.push_back(params);
		}
	}

public:
	Button() {
		enabled = true;
	}

	~Button() {
		delete_vector_elements(polygons_active);
		delete_vector_elements(polygons_inactive);
	}

	void init(const TextureAtlasLoader* atlas, const std::string & layers_str, const std::string & active_layers_str) {

		std::vector<std::string> layers;
		split(layers_str, ';', layers);

		std::vector<std::string> active_layers;
		split(active_layers_str.size() == 0 ? layers_str : active_layers_str, ';', active_layers);

		for (std::vector<std::string>::iterator it = layers.begin(); it != layers.end(); ++it) {
			const std::string & layer = *it;
			create_polygon_for_layer(atlas, NULL, layer, polygons_inactive, polygons_params_inactive, NULL);
		}

		for (std::vector<std::string>::iterator it = active_layers.begin(); it != active_layers.end(); ++it) {
			const std::string & layer = *it;
			create_polygon_for_layer(atlas, NULL, layer, polygons_active, polygons_params_active, NULL);
		}
	}

	void init_with_text(const TextureAtlasLoader* atlas, const FontLoader * font_loader, const std::string & layers_str, const std::string & active_layers_str, Sprite* font_sprite = NULL) {

		std::vector<std::string> layers;
		split(layers_str, ';', layers);

		std::vector<std::string> active_layers;
		split(active_layers_str.size() == 0 ? layers_str : active_layers_str, ';', active_layers);

		for (std::vector<std::string>::iterator it = layers.begin(); it != layers.end(); ++it) {
			const std::string & layer = *it;
			create_polygon_for_layer(atlas, font_loader, layer, polygons_inactive, polygons_params_inactive, font_sprite);
		}

		for (std::vector<std::string>::iterator it = active_layers.begin(); it != active_layers.end(); ++it) {
			const std::string & layer = *it;
			create_polygon_for_layer(atlas, font_loader, layer, polygons_active, polygons_params_active, font_sprite);
		}
	}

	void update_rect() {
		rect.set(-w * 0.5f, -h * 0.5f, w * 0.5f, h * 0.5f);
		rect.translate(-origin_x, -origin_y);

		update_rect(rect, polygons_inactive, polygons_params_inactive);
		update_rect(rect, polygons_active, polygons_params_active);

		rect.translate(x, y);
	}

	bool render_button(float delta, RenderCommand* command, const TouchHelper & th, const Camera & camera, float width, float height) {

		bool active = false;

		Rect r;
		if (th.is_button_active(rect, camera, width, height)) {
			active = true;
			command->add(polygons_active);
		} else {
			command->add(polygons_inactive);
		}

		return active;
	}

	bool is_button_tapped(const TouchHelper & th, const Camera & camera, float width, float height) {
		if (!enabled)
			return false;
		bool b = th.is_button_tapped(rect, camera, width, height);
		return b;
	}

	void set_width(float width) {
		this->w = width;
	}

	void set_height(float height) {
		this->h = height;
	}

	void set_enabled(bool enabled) {
		this->enabled = enabled;
	}

	bool is_enabled() {
		return this->enabled;
	}
};

} // namespace ft

#endif /* ft_button_H */
