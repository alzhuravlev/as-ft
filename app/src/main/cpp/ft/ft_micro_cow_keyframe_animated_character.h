#ifndef ft_micro_cow_keyframe_animated_character_H
#define ft_micro_cow_keyframe_animated_character_H

#include "ft_keyframe_animated_character.h"
#include "texture_atlas_loader.h"

namespace ft {

class MicroCowKeyframeAnimatedCharacter: public KeyframeAnimatedCharacter {
private:
	std::string base_name;
protected:

	virtual const std::string & get_base_name() {
		return base_name;

	}

	virtual void init_frame_animation(FrameAnimationExt & frame_animation, TowerItem* item, FTE_ENV env) {
		base_name = "micro_cow";

		TextureAtlasLoader* atlas_loader = item->get_tower()->get_items_atlas(env);
		item->get_tower()->get_frame_animation_polygon_cache().add_to_cache_polygon_direct_load("items/micro_cow/", *atlas_loader, "micro_cow", "micro_cow", 2, 151, env);

		frame_animation.add_data(FrameAnimationExtData("idle_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("idle", 11, 50, 1.5f));
		frame_animation.add_data(FrameAnimationExtData("idle_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("move_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("move", 11, 50, 0.5f));
		frame_animation.add_data(FrameAnimationExtData("move_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("jump", 120, 151, 1.0f, true));
		frame_animation.add_data(FrameAnimationExtData("hit", 80, 109, 2.0f, true));
		frame_animation.add_data(FrameAnimationExtData("dying", 80, 109, 2.0f, true));
	}

};

} // namespace ft

#endif /* ft_micro_cow_keyframe_animated_character_H */
