#ifndef ft_keyframe_animated_character_H
#define ft_keyframe_animated_character_H

#include "ft_base_animated_character.h"
#include "anim.h"

namespace ft {

class KeyframeAnimatedCharacter: public BaseAnimatedCharacter {
private:
	FrameAnimationExt frame_animation;

protected:

	virtual const std::string & get_base_name() = 0;

	virtual void init_frame_animation(FrameAnimationExt & frame_animation, TowerItem* item, FTE_ENV env) = 0;

	void do_init(TowerItem* item, FTE_ENV env) {
		init_frame_animation(frame_animation, item, env);
	}

	void do_release() {
		frame_animation.clear_data();
	}

	void do_reset() {
	}

	void do_update(TowerItem* item, float delta, const Camera & camera) {
		frame_animation.update(delta);
	}

	void do_render(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		int current_frame = frame_animation.get_current_frame();

		Polygon* p = item->get_tower()->get_frame_animation_polygon_cache().get(get_base_name(), current_frame);
		if (!p)
			return;

		float x, y, z, angle;
		item->get_world_position_from_map(x, y, z, angle);

		float ax, ay, az;
		ax = item->get_angle_x();
		ay = item->get_angle_y();
		az = item->get_angle_z();

		ay -= angle;
		az = cos_fast(ay) > 0.0f ? az : -az;

		float scale = item->get_onscreen_dim();

		p->set_translate(x, y, z);
		p->set_angle_xyz(ax, ay, az);
		p->set_scale(scale);

		command->add(p);
	}

	void do_render_effects(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
	}

	void do_render_tails(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
	}

	bool do_is_animation_playing(const std::string & name) {
		return frame_animation.is_animation_playing(name);
	}

	void do_play(const std::string & name, bool loop) {
		frame_animation.play(name, loop);
	}

	void do_play_next(const std::string & name, bool loop) {
		frame_animation.play(name, loop);
	}

	void do_stop(bool immediate) {
		frame_animation.stop(immediate);
	}

};

} // namespace ft

#endif /* ft_keyframe_animated_character_H */
