#ifndef ft_shaders_consts_H
#define ft_shaders_consts_H

namespace ft {

const int SHADER_ID_PL = 10;
const int SHADER_ID_PL_NM = 11;
const int SHADER_ID_PL_MODEL = 12;

const int SHADER_ID_DL = 20;
const int SHADER_ID_DL_NM = 21;
const int SHADER_ID_DL_MODEL = 22;

const int SHADER_ID_SL = 30;
const int SHADER_ID_SL_NM = 31;
const int SHADER_ID_SL_MODEL = 32;

const int SHADER_ID_NL = 100;
const int SHADER_ID_NL_MODEL = 101;

const int SHADER_ID_PARTICLES = 1000;

const int SHADER_ID_TOON = 2000;
const int SHADER_ID_TOON_MODEL = 2001;

const int SHADER_ID_DOOR_HOLE = 10000;

} // namespace ft

#endif /* ft_shaders_consts_H */
