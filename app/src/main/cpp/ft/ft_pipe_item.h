#ifndef ft_pipe_item_H
#define ft_pipe_item_H

#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "ft_base_pipe_item.h"

namespace ft {

class PipeItem: public BasePipeItem {
private:

	std::vector<Polygon*> polygons;

protected:

	virtual void do_release_polygons() {
		BasePipeItem::do_release_polygons();
		delete_vector_elements<Polygon*>(polygons);
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		TowerItem* above = get_tower()->get_item_at_top(get_col(), get_row(), TYPE_STEP | TYPE_STEP_SLIPPY);
		if (above) {
			items.push_back(new PipeItem(get_tower(), get_col(), get_row() + 1.0f));
		}
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BasePipeItem::do_create_polygons(cache_lighting, env);

		TextureAtlasLoader* atlas = get_tower()->get_tower_atlas(env);

		create_pipe_polygons(atlas, get_tower()->get_sprite_for_pipe(), get_tower()->get_sprite_for_pipe_nm(), get_tower()->get_pipe_roundness(), get_tower()->get_pipe_radius(), get_tower()->get_pipe_radius_amplitude(),
				get_tower()->get_pipe_radius_cycles(), true, polygons);

		cache_lighting->add(polygons);
		delete_vector_elements<Polygon*>(polygons);
	}

	virtual void do_reset_to_default() {
		BasePipeItem::do_reset_to_default();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BasePipeItem::do_create_body(box2d_loader);
		create_pipe_body(box2d_loader, "pipe");
	}

public:
	PipeItem(Tower* tower, float col, float row) :
			BasePipeItem(tower, col, row, tower->get_pipe_smoonthness(), 1.0f, 0.0f, 1.0f / tower->get_pipe_smoonthness(), TYPE_PIPE) {
	}

	~PipeItem() {
	}

	virtual bool is_need_to_update() {
		return false;
	}
};

} // namespace ft

#endif /* ft_pipe_item_H */
