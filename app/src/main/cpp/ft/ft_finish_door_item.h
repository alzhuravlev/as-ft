#ifndef ft_finish_door_item_H
#define ft_finish_door_item_H

#include "ft_tower.h"
#include "ft_base_door_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "anim.h"

namespace ft {

class FinishDoorItem: public BaseDoorItem {
private:

protected:

	virtual void do_release_polygons() {
		BaseDoorItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseDoorItem::do_create_polygons(cache_lighting, env);
		create_door_polygons(env, "finish_door");
	}

	virtual void do_change_state(int state) {
		BaseDoorItem::do_change_state(state);

		switch (state) {
		case DOOR_STATE_CLOSE:
			break;

		case DOOR_STATE_OPEN:
			break;

		case DOOR_STATE_CLOSING:
			break;

		case DOOR_STATE_OPENING:
			break;

		case DOOR_STATE_FINISHING:
			break;

		case DOOR_STATE_FINISHED:
			get_tower()->tower_request_complete_screen();
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseDoorItem::do_update(delta, camera);

		switch (get_state()) {
		case DOOR_STATE_CLOSE:
			break;

		case DOOR_STATE_OPEN:
			break;

		case DOOR_STATE_CLOSING:
			break;

		case DOOR_STATE_OPENING:
			break;

		case DOOR_STATE_FINISHING:
			if (get_state_time() > 3.0f)
				change_state(DOOR_STATE_FINISHED);
			break;

		case DOOR_STATE_FINISHED:
			break;
		}
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		BaseDoorItem::do_render_effects(delta, command);
	}

	virtual uint32_t get_door_effect_color() {
		return 0xff2fedf4;
	}

public:
	FinishDoorItem(Tower* tower, float col, float row) :
			BaseDoorItem(tower, col, row, TYPE_DOOR, tower->get_door_scale_y()) {
	}

	~FinishDoorItem() {
	}

	virtual void tower_finished() {
		change_state(DOOR_STATE_FINISHING);
	}
};

}
// namespace ft

#endif /* ft_finish_door_item_H */
