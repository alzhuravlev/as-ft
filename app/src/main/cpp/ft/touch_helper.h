#ifndef touch_helper_H
#define touch_helper_H

#include "engine.h"
#include "external.h"

namespace fte {

const float EPSILON_OFFSET = 8.0f;

class TouchHelper {
private:
	bool down;

	bool was_moved;
	bool was_tap;
	bool was_up;

	float down_x;
	float down_y;

	float up_x;
	float up_y;

	float curr_x;
	float curr_y;

	int64_t down_time;
	int64_t up_time;
	int64_t curr_time;

	// pixels per sec
	Vec3 velocity;

	bool back_pressed;

public:

	TouchHelper() {
		down = false;
		was_moved = false;
		was_tap = false;
		was_up = false;

		curr_x = 0;
		curr_y = 0;

		down_time = 0;
		up_time = 0;
		curr_time = 0;

		down_x = 0;
		down_y = 0;

		up_x = 0;
		up_y = 0;

		back_pressed = false;
	}

	void process(TouchEvent events[], int size) {

		for (int i = 0; i < size; i++) {

			TouchEvent & event = events[i];

			switch (event.action) {

			case ATOUCH_DOWN: {

				velocity.set_to_zero();

				down = true;
				was_moved = false;
				was_tap = false;
				was_up = false;

				curr_x = event.x;
				curr_y = event.y;

				down_x = event.x;
				down_y = event.y;

				up_x = 0;
				up_y = 0;

				down_time = event.nano_time;
				curr_time = event.nano_time;

			}
				break;

			case ATOUCH_MOVE: {

				float x = event.x;
				float y = event.y;
				int64_t t = event.nano_time;

				float dt = ((t - curr_time) * 1e-9);

				float dx = event.x - curr_x;
				float dy = event.y - curr_y;

				float vx = dx / dt;
				float vy = dy / dt;

				velocity.set(vx, vy, 0.);

				curr_x = event.x;
				curr_y = event.y;

				curr_time = t;

				if (!was_moved)
					was_moved = fabsf(dx) > EPSILON_OFFSET || fabsf(dy) > EPSILON_OFFSET;
			}
				break;

			case ATOUCH_UP: {
				down = false;

				up_x = event.x;
				up_y = event.y;

				up_time = event.nano_time;

				was_tap = !was_moved && ((up_time - down_time) < 200e6);
				was_up = true;
			}
				break;

			case ATOUCH_BACK_PRESSED:
				back_pressed = true;
				break;
			}
		}
//		LOG_TOUCH("fte: TouchHelper: process state = %d", state);
	}

	inline bool is_down() const {
		return down;
	}

	inline bool is_moved() const {
		return was_moved;
	}

	inline bool is_tap() const {
		return was_tap;
	}

	inline bool is_up() const {
		return was_up;
	}

	inline bool is_back_pressed() const {
		return back_pressed;
	}

	float get_offset_x(float width) const {
		return (curr_x - down_x) / width;
	}

	float get_offset_y(float height) const {
		return (curr_y - down_y) / height;
	}

	void get_curr_xy(float & x, float & y) const {
		x = curr_x;
		y = curr_y;
	}

	void get_down_xy(float & x, float & y) const {
		x = down_x;
		y = down_y;
	}

	/**
	 * r must be in screen coords (origin at top-left, y - up-down)
	 */
	bool is_current_in_rect(const Rect & r) const {
		if (!down)
			return false;

		if (!r.is_point_in_rect(curr_x, curr_y))
			return false;

		return true;
	}

	bool is_button_active(const Rect & r, const Camera & camera, float w, float h) const {
		if (!down)
			return false;

		float x, y;

		x = curr_x;
		y = h - curr_y;
		camera.unproject(x, y, w, h);

		if (!r.is_point_in_rect(x, y))
			return false;

		x = down_x;
		y = h - down_y;
		camera.unproject(x, y, w, h);

		if (!r.is_point_in_rect(x, y))
			return false;

		return true;
	}

	bool is_button_tapped(const Rect & r, const Camera & camera, float w, float h) const {
		if (!was_up)
			return false;

		float x, y;

		x = up_x;
		y = h - up_y;
		camera.unproject(x, y, w, h);

		if (!r.is_point_in_rect(x, y))
			return false;

		x = down_x;
		y = h - down_y;
		camera.unproject(x, y, w, h);

		if (!r.is_point_in_rect(x, y))
			return false;

		return true;
	}

	inline void clear_state() {
		was_tap = false;
		was_up = false;
		velocity.set_to_zero();
		back_pressed = false;
	}

	const Vec3 & get_vel() const {
//		LOG_TOUCH("touch vel = (%f, %f)", velocity.x, velocity.y);
		return velocity;
	}
}
;

} // namespace fte

#endif /* touch_helper_H */
