#ifndef ft_lift_spine_animated_character_H
#define ft_lift_spine_animated_character_H

#include "ft_spine_animated_character.h"

namespace ft {

class LiftSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

protected:

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);
		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/lift.obj", env);

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, "items/lift.atlas", "items/lift.json", "lift", item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y(), true, true, false, false, false, env);

		spine_data->set_mix("idle", "running1", 0.3f);

		spine_data->set_mix("running1", "running3", 0.3f);

		spine_data->set_mix("running2", "running3", 0.3f);

		spine_data->set_mix("running3", "idle", 0.3f);
		spine_data->set_mix("running3", "running1", 0.3f);

		spine = new SpineRenderer(spine_data);
		spine->set_animation("idle", true);
	}

};

} // namespace ft

#endif /* ft_lift_spine_animated_character_H */
