#ifndef ft_zipper_fly_spine_animated_character_H
#define ft_zipper_fly_spine_animated_character_H

#include "ft_spine_animated_character.h"

namespace ft {

class ZipperFlySpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

protected:

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {
		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/zipperfly.obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, "items/zipperfly.atlas", "items/zipperfly.json", "zipperfly", item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y(), true, true, true, false, false, env);

		spine = new SpineRenderer(spine_data);
		spine->set_animation("move", true);
	}

};

} // namespace ft

#endif /* ft_zipper_fly_spine_animated_character_H */
