#ifndef ft_splash_screen_H
#define ft_splash_screen_H

#include "engine.h"
#include "texture.h"
#include "updater.h"
#include "screen.h"
#include "command.h"
#include "buffer.h"
#include "sprite.h"
#include "colored_polygon.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "camera.h"
#include "polygon_builder.h"
#include "ring_anim.h"

#include "ft_base_screen.h"
#include "ft_main_screen.h"

#include <string>

using namespace fte;

namespace ft {

class SplashScreen: public BaseScreen {
private:

	RingAnim ra;

	ColoredPolygon bg_white;

	float duration;

protected:

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader texture_atlas(std::string("screens/splash.atlas"), env);
		texture_atlas.add_texture_descriptors(command);
	}

	void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader atlas_loader("screens/splash.atlas", env);
		Box2dLoader shape_loader("screens/splash.json", env);

		float square = get_screen_ratio() * .5;

		bg_white.set_vert_xyz(0, -get_screen_max_dim(), -get_screen_max_dim(), 0.);
		bg_white.set_vert_xyz(1, get_screen_max_dim(), -get_screen_max_dim(), 0.);
		bg_white.set_vert_xyz(2, get_screen_max_dim(), get_screen_max_dim(), 0.);
		bg_white.set_vert_xyz(3, -get_screen_max_dim(), get_screen_max_dim(), 0.);

		bg_white.set_vert_color(0, 0xFFFFFFFF);
		bg_white.set_vert_color(1, 0xFFFFFFFF);
		bg_white.set_vert_color(2, 0xFFFFFFFF);
		bg_white.set_vert_color(3, 0xFFFFFFFF);

		duration = 3.;

		RingAnimConfig config;

		config.cycle = false;

		config.waves_count = 1;

		config.amplitude = .08;

//		config.scale_start = square * .9;
//		config.scale_mid = square;
//		config.scale_end = square * 5.;

		config.velocity_start = 2.;
		config.velocity_end = 2.;

		ra.init_with_textured_shape(config, duration, atlas_loader, shape_loader, "bg", "bg2");

		fade_in();
	}

	virtual void do_fade_out_done() {
		get_updater()->set_pending_screen(new MainScreen());
	}

	void do_update(float delta, RenderCommand* command) {

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, get_ortho_camera().get_view_proj_matrix());
		{
			command->add(&bg_white);

			ra.update(delta);
			ra.render(command);
		}
		command->end_batch();

		switch (get_state()) {
		case SCREEN_STATE_DEFAULT:
			if (get_state_time() > 1.)
				fade_out();
			break;
		}

		BaseScreen::do_update(delta, command);
	}

public:
	SplashScreen() :
			bg_white(4) {
	}

	~SplashScreen() {
	}
};

} // namespace ft

#endif /* ft_splash_screen_H */
