#ifndef ft_padlock_spine_animated_character_H
#define ft_padlock_spine_animated_character_H

#include "ft_spine_animated_character.h"
#include "ft_tower_item_consts.h"

namespace ft {

class PadlockSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:
	int color;

protected:

	void do_release() {
		SpineAnimatedCharacter::do_release();
	}

	void do_reset() {
		SpineAnimatedCharacter::do_reset();
	}

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {
		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/padlock.obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, "items/padlock.atlas", "items/padlock.json", get_key_texture_name(color), item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y(), true, true, false, false, false, env);

		spine_data->set_mix("showing", "active", 0.3f);
		spine_data->set_mix("active", "collecting", 0.3f);
		spine_data->set_mix("active", "blink", 0.3f);
		spine_data->set_mix("blink", "active", 0.3f);

		spine = new SpineRenderer(spine_data);
	}

public:

	PadlockSpineAnimatedCharacter(int color) {
		this->color = color;
	}

};

} // namespace ft

#endif /* ft_padlock_spine_animated_character_H */
