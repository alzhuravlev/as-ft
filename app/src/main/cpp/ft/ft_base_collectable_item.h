#ifndef ft_base_collectable_item_H
#define ft_base_collectable_item_H

#include "polygon_cache.h"
#include "camera.h"
#include "command.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "ft_tower_item.h"

namespace ft {

#define COLLECTABLE_CAN_CONTACT_WITH 		TYPE_MAIN_CHARACTER

const static int COLLECTABLE_STATE_ACTIVE = 0;
const static int COLLECTABLE_STATE_COLLECTING = 1;
const static int COLLECTABLE_STATE_INACTIVE = 2;
const static int COLLECTABLE_STATE_BLINK = 3;
const static int COLLECTABLE_STATE_HIDDEN = 4;
const static int COLLECTABLE_STATE_SHOWING = 5;

class BaseCollectableItem: public TowerItem {
private:
protected:

	void create_collectable_body(Box2dLoader & box2d_loader, const std::string & shape_name) {
		b2BodyDef def;
		body = get_tower()->get_world()->CreateBody(&def);

		body->SetUserData(this);

		b2FixtureDef fd;
		fd.density = 10.;
		fd.friction = 0.0;
		fd.restitution = 1.;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;
		fd.isSensor = true;

		Box2dItem* item = box2d_loader.find_item(shape_name);
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

		body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
	}

	virtual void do_release_polygons() {
	}

	void reset_collectable_to_default(int state) {
		body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
		change_state(state, true);
	}

	virtual void do_reset_to_default() {
		reset_collectable_to_default(COLLECTABLE_STATE_ACTIVE);
	}

	virtual void do_change_state(int state) {
		switch (state) {
		case COLLECTABLE_STATE_ACTIVE:
			body_active = true;
			visible = true;
			play_animation("active", true);
			break;

		case COLLECTABLE_STATE_COLLECTING:
			play_animation("collecting", false);
			break;

		case COLLECTABLE_STATE_BLINK:
			play_animation("blink", false);
			break;

		case COLLECTABLE_STATE_INACTIVE:
			body_active = false;
			visible = false;
			active = false;
			break;

		case COLLECTABLE_STATE_SHOWING:
			body_active = true;
			visible = true;
			play_animation("showing", false);
			break;

		case COLLECTABLE_STATE_HIDDEN:
			body_active = false;
			visible = false;
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {

		switch (get_state()) {
		case COLLECTABLE_STATE_ACTIVE:
			break;

		case COLLECTABLE_STATE_COLLECTING:
			break;

		case COLLECTABLE_STATE_BLINK:
			break;

		case COLLECTABLE_STATE_INACTIVE:
			break;

		case COLLECTABLE_STATE_SHOWING:
			break;

		case COLLECTABLE_STATE_HIDDEN:
			break;
		}
	}

public:
	BaseCollectableItem(Tower* tower, float col, float row, float scale_factor = 1.0f) :
			TowerItem(tower, col, row, TYPE_COLLECTABLE, COLLECTABLE_CAN_CONTACT_WITH, scale_factor) {
	}

	virtual bool can_collect() {
		switch (get_state()) {
		case COLLECTABLE_STATE_HIDDEN:
		case COLLECTABLE_STATE_SHOWING:
			return false;
		};
		return true;
	}

	void show_at(float map_x, float map_y) {
		switch (get_state()) {
		case COLLECTABLE_STATE_HIDDEN:
			body->SetTransform(b2Vec2(map_x, map_y), 0.0f);
			change_state(COLLECTABLE_STATE_SHOWING);
			break;
		};
	}

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		if (can_collect()) {
			if (other_fixture->GetFilterData().categoryBits == TYPE_MAIN_CHARACTER && !other_fixture->IsSensor()) {
				switch (get_state()) {
				case COLLECTABLE_STATE_ACTIVE:
					change_state(COLLECTABLE_STATE_COLLECTING);
					break;
				}
			}
		} else {
			switch (get_state()) {
			case COLLECTABLE_STATE_ACTIVE:
				change_state(COLLECTABLE_STATE_BLINK);
				break;
			}
		}
	}

	virtual bool is_need_to_render_effects() {
		return true;
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}
};

} // namespace ft

#endif /* ft_base_collectable_item_H */
