#ifndef ft_level_screen_H
#define ft_level_screen_H

#include "engine.h"
#include "command.h"
#include "camera.h"
#include "updater.h"
#include "screen.h"
#include "texture_atlas_loader.h"
#include "polygon_builder.h"
#include "ft_button.h"
#include "ft_base_screen.h"
#include "ft_box_loader.h"

#include "font_loader.h"
#include "l10n_loader.h"

#include "ft_tower_preferences.h"

#include <string>

using namespace fte;

namespace ft {

class LevelScreen: public BaseScreen {
private:
	Polygon* bg;

	std::vector<Button*> buttons;

	Button back;

	BoxItem box_item;

	bool open_back;
	bool open_game;

	int tapped_button_index;

protected:

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader texture_atlas("screens/level.atlas", env);
		texture_atlas.add_texture_descriptors(command);
	}

	void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader atlas_loader("screens/level.atlas", env);

		bg = PolygonBuiler::create_sprite(atlas_loader, "bg", true, false);
		bg->set_origin_to(ORIGIN_CENTER);

		back.init(&atlas_loader, "back_bg;back_icon:0.75", "back_bg_active;back_icon_active:0.75");

		open_back = false;
		open_game = false;

		init_buttons(atlas_loader, env);

		fade_in();
	}

	void do_resize(FTE_ENV env) {
		BaseScreen::do_resize(env);

		float width = get_width();
		float height = get_height();

		float w = get_screen_ratio() * 0.2f;
		float x = width > height ? -0.5f + w * 0.5f : -get_screen_ratio() * 0.5f + w * 0.5f;
		float y = width > height ? get_screen_ratio() * 0.5f - w * 0.5f : 0.5f - w * 0.5f;

		back.set_width(w);
		back.set_height(w);
		back.set_translate_xy(x, y);
		back.update_rect();

		layout_buttons();
	}

	virtual void do_fade_out_done() {
		if (open_back)
			do_open_back();
		else if (open_game)
			do_open_game();
	}

	void do_update(float delta, RenderCommand* command) {
		const TouchHelper & th = get_touch_helper();
		float width = get_width();
		float height = get_height();

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, get_ortho_camera().get_view_proj_matrix());
		{
			command->add(bg);
			back.render_button(delta, command, th, get_ortho_camera(), width, height);
			render_buttons(delta, command);
		}
		command->end_batch();

		if (get_state() == SCREEN_STATE_DEFAULT) {
			if (th.is_back_pressed() || back.is_button_tapped(th, get_ortho_camera(), width, height)) {
				open_back = true;
				fade_out();
			} else
				check_button_tapped();
		}

		BaseScreen::do_update(delta, command);
	}

	void init_buttons(const TextureAtlasLoader & atlas_loader, FTE_ENV env) {
		FontLoader font_loader("fonts/default_font.fnt", env);
		Sprite* font_sprite = PolygonBuiler::create_sprite(atlas_loader, "default_font", true, false);

		int count = box_item.levels.size();
		for (int i = 0; i < count; i++) {

			Button* b = new Button;

			std::vector<std::string> v_active;
			std::vector<std::string> v_inactive;

			std::string s_active;
			std::string s_inactive;

			int level_state = TowerPreferences::get_level_state(get_pref(), box_item.id, i);

			b->set_enabled(level_state == 1);

			if (level_state == 0) {
				v_inactive.push_back("level:1.0:0.4");
				v_active.push_back("level:1.0:0.4");
				v_inactive.push_back("t:" + to_string(i + 1) + ":0.75:0.4");
				v_active.push_back("t:" + to_string(i + 1) + ":0.75:0.4");
				v_inactive.push_back("locked:0.75");
				v_active.push_back("locked:0.9");
			} else {
				v_inactive.push_back("level:1.0:0.4");
				v_active.push_back("level:1.2:1.0");

				float score = TowerPreferences::get_score(get_pref(), box_item.format_tower_file_name(i));
				v_inactive.push_back("t:" + to_string(i + 1) + ":0.75");
				v_active.push_back("t:" + to_string(i + 1) + ":0.9");
			}

			join(v_inactive, ';', s_inactive);
			join(v_active, ';', s_active);

			b->init_with_text(&atlas_loader, &font_loader, s_inactive, s_active, font_sprite);
			buttons.push_back(b);
		}

		DELETE(font_sprite);
	}

	void render_buttons(float delta, RenderCommand* command) {
		const TouchHelper & th = get_touch_helper();
		float width = get_width();
		float height = get_height();

		for (std::vector<Button*>::const_iterator it = buttons.begin(); it != buttons.end(); ++it) {
			Button* b = *it;
			b->render_button(delta, command, th, get_ortho_camera(), width, height);
		}
	}

	void check_button_tapped() {
		const TouchHelper & th = get_touch_helper();
		float width = get_width();
		float height = get_height();
		int i = 0;
		for (std::vector<Button*>::const_iterator it = buttons.begin(); it != buttons.end(); ++it) {
			Button* b = *it;
			if (b->is_button_tapped(th, get_ortho_camera(), width, height)) {
				if (i < box_item.levels.size() && i >= 0) {
					fade_out();
					open_game = true;
					tapped_button_index = i;
				}
				break;
			}
			i++;
		}
	}

	void layout_buttons() {
		float width = get_width();
		float height = get_height();

		int count = buttons.size();

		Rect rect;

		if (width > height) {

			// landscape
			rect.x1 = 0. - get_screen_width() * .5 + get_screen_ratio() * .2 * 1.1;
			rect.y1 = 0. - get_screen_height() * .5;

			rect.x2 = get_screen_width() * .5;
			rect.y2 = get_screen_height() * .5 - get_screen_ratio() * .2 * .5;

		} else {

			// portrait
			rect.x1 = 0. - get_screen_width() * .5;
			rect.y1 = 0. - get_screen_height() * .5;

			rect.x2 = get_screen_width() * .5;
			rect.y2 = get_screen_height() * .5 - get_screen_ratio() * .2 * 1.1;
		}

		float rect_w = rect.x2 - rect.x1;
		float rect_h = rect.y2 - rect.y1;

		float r = rect_w / rect_h;
		int rows, cols;
		cols = roundf(sqrtf(count * r));
		rows = count / cols;

		if (rows * cols < count) {
			if (rows > cols)
				cols++;
			else
				rows++;
		}

		float bw, bh;

		bw = rect_w / cols;
		bh = rect_h / rows;

		float dim = std::min(bw, bh);

		float padding_between = dim * .1;
		dim = dim * .9;

		float ox = rect.x1 + (rect_w - cols * dim - (cols - 1) * padding_between) * .5 + dim * .5;
		float oy = rect.y2 - (rect_h - rows * dim - (rows - 1) * padding_between) * .5 - dim * .5;

		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++) {

				int i = cols * r + c;
				if (i >= count)
					continue;

				float x, y;

				x = ox + c * padding_between + c * dim;
				y = oy - r * padding_between - r * dim;

				Button* b = buttons.at(i);
				b->set_translate_xy(x, y);
				b->set_width(dim);
				b->set_height(dim);
				b->update_rect();
			}
	}

	void do_open_back();
	void do_open_game();

public:
	LevelScreen(const BoxItem & box_item) {
		this->bg = NULL;
		this->box_item = box_item;
	}

	~LevelScreen() {
		DELETE(bg);
		delete_vector_elements<Button*>(buttons);
	}
};

} // namespace ft

#endif /* ft_level_screen_H */
