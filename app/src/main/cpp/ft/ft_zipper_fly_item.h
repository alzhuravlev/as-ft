#ifndef ft_zipper_fly_item_H
#define ft_zipper_fly_item_H

#include "ft_tower_item.h"
#include "anim.h"
#include "comet_tail.h"
#include "ft_zipper_fly_spine_animated_character.h"

namespace ft {

#define ZUPPER_FLY_CAN_CONTACT_WITH 		TYPE_ALL_FILTER

class ZipperFlyItem: public TowerItem {
private:

	BaseAnimatedCharacter* animated_character;

	RingAnim r_anim;

	LinearInterpolatedValue map_x;
	LinearInterpolatedValue map_y;

	TowerItem* last_nearest_item;

	b2Vec2 map_pos;

	Vec3 eye;

//	CometTail tail;

protected:

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {

		animated_character = new ZipperFlySpineAnimatedCharacter;
		animated_character->init(this, env);

		RingAnimConfig r_anim_config;
		r_anim_config.point_count = 16;
		r_anim_config.cycle = true;
		r_anim_config.amplitude = 0.4f;
		r_anim_config.waves_count = 5;
		r_anim_config.velocity_start = r_anim_config.velocity_end = 1.0f;

		Box2dLoader* particle_shape_loader = get_tower()->get_particles_shape_loader(env);
		TextureAtlasLoader* particle_atlas = get_tower()->get_particles_atlas(env);

		r_anim.init_with_textured_shape(r_anim_config, 2.0f, *particle_atlas, *particle_shape_loader, "shine", "shine");
		r_anim.update_polygon_scale(get_tower()->get_scale_factor_y() * get_scale_factor() * 7.5f);
		r_anim.start();

		map_x.set_alpha(get_tower()->get_zipperfly_movement_alpha());
		map_y.set_alpha(get_tower()->get_zipperfly_movement_alpha());

//		tail.init_textured(*particle_atlas, "tail", 16, 20.0f, get_onscreen_dim() * 0.01f, get_onscreen_dim() * 0.8f);
	}

	virtual void do_release_polygons() {
		DELETE(animated_character);
//		tail.release();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
	}

	virtual void do_reset_to_default() {
//		tail.reset();
		last_nearest_item = NULL;
		get_tower()->get_main_character_map_position(map_pos);
		map_x.set_current(map_pos.x);
		map_y.set_current(map_pos.y);
	}

	virtual void do_change_state(int state) {
	}

	virtual void do_update(float delta, const Camera & camera) {
		get_tower()->get_main_character_map_position(map_pos);

		TowerItem* nearest_item = NULL;
		if ((!last_nearest_item || !last_nearest_item->is_active() || !last_nearest_item->is_in_frustrum()) && get_state_time() > 6.0f)
			nearest_item = get_tower()->get_nearest_item(map_pos, 5.0f, TYPE_MONSTERS_FILTER);

		if (nearest_item && nearest_item != last_nearest_item) {
			last_nearest_item = nearest_item;
			set_state_time(0.0f);
		} else if ((last_nearest_item && get_state_time() > 4.0f) || (last_nearest_item && (!last_nearest_item->is_active() || !last_nearest_item->is_in_frustrum()))) {
			last_nearest_item = NULL;
			set_state_time(0.0f);
		}

		if (last_nearest_item) {
			map_pos = last_nearest_item->get_map_position();
			map_x.set_alpha(1.0f);
			map_y.set_alpha(1.0f);
		} else {
			map_x.set_alpha(4.0f);
			map_y.set_alpha(4.0f);
		}

		map_x.set_target(map_pos.x);
		map_x.clamp_cycle_target(get_tower()->get_map_width());

		map_y.set_target(map_pos.y + get_tower()->get_zipperfly_distance_y());

		map_x.update(delta);
		map_y.update(delta);

		map_pos.x = map_x.get_value();
		map_pos.y = map_y.get_value();

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		r_anim.update(delta);
		r_anim.update_polygon_position(Vec3(x, y, z));
		r_anim.update_polygon_rotation(Vec3(camera.get_angle_x(), camera.get_angle_y(), camera.get_angle_z()));

		const Vec3 & Z = camera.get_Z();
		eye.x = -Z.x;
		eye.y = -Z.y;
		eye.z = -Z.z;

		animated_character->update(this, delta, camera);
//		tail.update(delta, Vec3(x, y, z), camera);
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {

//		spine->render_batches(command, get_tower()->get_shader_id_model(), 0, true, true, camera);
		animated_character->render(this, delta, command, camera);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		Vec3 direction(x, 0.0f, z);
		vec_normalize(direction);

		command->set_light_direction(direction);
		command->set_light_position(Vec3(x, y, z));
		command->set_light_eye(eye);
		command->set_light_ambient_light(get_tower()->get_light_ambient());
		command->set_light_specular_exp(get_tower()->get_light_specular_exp());
		command->set_light_attenuation(get_tower()->get_light_attenuation());
		command->set_light_fog_a(get_tower()->get_fog_a());
		command->set_light_fog_b(get_tower()->get_fog_b());
		command->set_light_spot_exp(get_tower()->get_light_spot_exp());
		command->set_light_spot_cos_cutoff(get_tower()->get_light_spot_cos_cutoff());

		Vec3 fog_color;
		get_tower()->get_fog_color(fog_color.x, fog_color.y, fog_color.z);
		command->set_light_fog_color(fog_color);

		float bg_r, bg_g, bg_b;
		get_tower()->get_bg_color(bg_r, bg_g, bg_b);
		command->set_clear_color(bg_r, bg_g, bg_b, 1.0f);
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		r_anim.render(command);
	}

	virtual void do_render_tails(float delta, RenderCommand* command) {
//		tail.render(command);
	}

public:

	ZipperFlyItem(Tower* tower, float col, float row) :
			TowerItem(tower, col, row, TYPE_ZIPPER_FLY, ZUPPER_FLY_CAN_CONTACT_WITH, tower->get_zipperfly_scale()) {
		this->set_distance_from_base_radius(get_tower()->get_zipperfly_distance_from_base_radius());
		this->animated_character = NULL;
		this->last_nearest_item = NULL;
	}

	~ZipperFlyItem() {
	}

	virtual bool is_need_to_render_no_toon() {
		return true;
	}

	virtual const b2Vec2 & get_map_position() {
		return map_pos;
	}

};

} // namespace ft

#endif /* ft_zipper_fly_item_H */
