#ifndef ft_step_falsy_item_H
#define ft_step_falsy_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "ft_step_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"

namespace ft {

const static int STEP_FALSY_STATE_DEFAULT = 0;
const static int STEP_FALSY_STATE_CRUMBLING1 = 1;
const static int STEP_FALSY_STATE_CRUMBLING2 = 2;
const static int STEP_FALSY_STATE_DESTROYED = 3;

const static float STEP_FALSY_CRUMBLING_DURATION1 = 0.2f;
const static float STEP_FALSY_CRUMBLING_DURATION2 = 0.5f;

const static float STEP_FALSY_CRUMBLING_MOVE_DISTANCE = 3.0f;
const static float STEP_FALSY_CRUMBLING_SHAKE_DISTANCE = 0.15f;
const static float STEP_FALSY_WAVE_DISTANCE = 0.08f;

const static float STEP_FALSY_ALPHA = 1.0f;

class StepFalsyItem: public BaseStepItem {
private:
	std::vector<Polygon*> polygons;
	std::vector<Vec3> positions;

	AccelerateInterpolator interpolator;

protected:

	virtual void do_release_polygons() {
		delete_vector_elements(polygons);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {

		const float PARTS_COUNT = 3.0f;
		const float STEP = 1.0f / PARTS_COUNT;

		std::vector<Vec3> interpolated_map_points;
		interpolate_road(get_col(), get_row(), interpolated_map_points, 1.0f / PARTS_COUNT);

		if (interpolated_map_points.size() < 1) {
			LOGE("ft: StepFalsyItem::do_create_polygons. interpolated_map_points must have at least 2 points");
			return;
		}

		TextureAtlasLoader* items_atlas = get_tower()->get_items_atlas(env);

		PolygonCache tmp;

		const std::vector<Vec3>::const_iterator begin = interpolated_map_points.begin();
		const std::vector<Vec3>::const_iterator end = interpolated_map_points.end();

		std::vector<Vec3>::const_iterator first = begin;
		std::vector<Vec3>::const_iterator second = begin;
		++second;
		++second;

		float u1_scale = 0.0f;

		while (true) {
			tmp.reset();
			create_step_polygon_for_points(first, second, &tmp, *items_atlas, "step_falsy", "stepside_falsy", "", "", env, true, true, true, true, u1_scale, u1_scale + STEP);
			Polygon* p = tmp.create_composite();
			p->set_fixed_rotation(true);

			float x1, x2, y1, y2, z1, z2;
			p->calculate_aabb(x1, x2, y1, y2, z1, z2);

			Vec3 position;
			position.x = x1 + (x2 - x1) * 0.5f;
			position.y = y1 + (y2 - y1) * 0.5f;
			position.z = z1 + (z2 - z1) * 0.5f;

			p->set_origin_xyz(position.x, position.y, position.z);
			p->set_translate(position.x, position.y, position.z);

			p->set_alpha(STEP_FALSY_ALPHA);

			polygons.push_back(p);
			positions.push_back(position);

			if (second == end)
				break;

			++first;
			++second;

			u1_scale += STEP;
		};
	}

	virtual void do_reset_to_default() {
		change_state(STEP_FALSY_STATE_DEFAULT, true);

		int cnt = polygons.size();
		for (int i = 0; i < cnt; ++i) {
			Polygon* p = polygons[i];
			const Vec3 & position = positions[i];
			p->set_translate(position.x, position.y, position.z);
			p->set_alpha(STEP_FALSY_ALPHA);
		}
	}

	virtual void do_change_state(int state) {
		switch (state) {
		case STEP_FALSY_STATE_DEFAULT:
			body_active = true;
			visible = true;
			break;

		case STEP_FALSY_STATE_CRUMBLING1:
			get_tower()->play_crump();
			break;

		case STEP_FALSY_STATE_CRUMBLING2:
			body_active = false;
			break;

		case STEP_FALSY_STATE_DESTROYED:
			visible = false;
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		switch (get_state()) {
		case STEP_FALSY_STATE_DEFAULT: {
			wave_parts();
			const Vec3 & pos = positions.front();
			in_frustrum = camera.sphere_in_frustrum(pos.x, pos.y, pos.z, get_onscreen_dim_2());
		}
			break;

		case STEP_FALSY_STATE_CRUMBLING1:
			if (get_state_time() > STEP_FALSY_CRUMBLING_DURATION1) {
				change_state(STEP_FALSY_STATE_CRUMBLING2);
			}
			shake_parts();
			in_frustrum = true;
			break;

		case STEP_FALSY_STATE_CRUMBLING2:
			if (get_state_time() > STEP_FALSY_CRUMBLING_DURATION2) {
				change_state(STEP_FALSY_STATE_DESTROYED);
			}
			fall_down_parts();
			in_frustrum = true;
			break;

		case STEP_FALSY_STATE_DESTROYED:
			break;
		}
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
		command->add(polygons);
	}

	void shake_parts() {
		int cnt = polygons.size();
		for (int i = 0; i < cnt; ++i) {
			Polygon* p = polygons[i];
			const Vec3 & position = positions[i];
			float y = position.y + STEP_FALSY_CRUMBLING_SHAKE_DISTANCE * sin_fast((i % 2) * M_PI + M_PI * get_state_time() / STEP_FALSY_CRUMBLING_DURATION1);
			p->set_translate_y(y);
		}
	}

	void wave_parts() {
		int cnt = polygons.size();
		float f = M_PI / cnt;
		for (int i = 0; i < cnt; ++i) {
			Polygon* p = polygons[i];
			const Vec3 & position = positions[i];
			float y = position.y + STEP_FALSY_WAVE_DISTANCE * sin_fast(i * f + 6.0f * get_state_time());
			p->set_translate_y(y);
		}
	}

	void fall_down_parts() {
		int cnt = polygons.size();
		for (int i = 0; i < cnt; ++i) {
			Polygon* p = polygons[i];
			const Vec3 & position = positions[i];

			float y1 = position.y;
			float y2 = y1 - STEP_FALSY_CRUMBLING_MOVE_DISTANCE;

			float k = (i * 0.5f + 1.0f) * get_state_time() / STEP_FALSY_CRUMBLING_DURATION2;
			k = interpolator.interpolate(CLAMP(k, 0.0f, 1.0f));
			p->set_translate_y(y1 + (y2 - y1) * k);
			p->set_alpha((1.0f - k) * STEP_FALSY_ALPHA);
		}
	}

public:
	StepFalsyItem(Tower* tower, float col, float row) :
			BaseStepItem(tower, col, row) {
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}

	int get_platform_temporary_group() {
		return -1;
	}

	void platform_contact_from_top_begin() {
		switch (get_state()) {
		case STEP_FALSY_STATE_DEFAULT:
			change_state(STEP_FALSY_STATE_CRUMBLING1);
			break;
		}
	}
};

} // namespace ft

#endif /* ft_step_falsy_item_H */
