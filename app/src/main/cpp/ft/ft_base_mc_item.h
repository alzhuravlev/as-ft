#ifndef ft_base_mc_item_H
#define ft_base_mc_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "touch_helper.h"
#include "ft_door_item.h"
#include "polygon_builder.h"
#include "ft_pogo_walk_effect.h"
#include "ft_zipper_fly_item.h"

namespace ft {

#define MC_CAN_CONTACT_WITH 		TYPE_BUTTON | TYPE_MONSTER | TYPE_PLATFORMS_FILTER | TYPE_DOOR | TYPE_GROUND | TYPE_COLLECTABLE

const static int MC_STATE_DEFAULT = 0;
const static int MC_STATE_MOVE_LEFT = 2;
const static int MC_STATE_MOVE_RIGHT = 3;
const static int MC_STATE_JUMP_LEFT = 4;
const static int MC_STATE_JUMP_RIGHT = 5;
const static int MC_STATE_HIT = 6;
const static int MC_STATE_BEFORE_DOOR_MOVE_LEFT = 7;
const static int MC_STATE_BEFORE_DOOR_MOVE_RIGHT = 8;
const static int MC_STATE_WALK_INTO_DOOR = 9;
const static int MC_STATE_WALK_OUT_DOOR = 10;
const static int MC_STATE_DYING = 11;
const static int MC_STATE_FINISHED = 12;

const static int MC_SENSOR_BOTTOM = 1;
const static int MC_SENSOR_BUTTON = 2;
const static int MC_SENSOR_DOOR = 3;

const static int MC_CHANGE_VEL_LEFT = 0;
const static int MC_CHANGE_VEL_RIGHT = 1;
const static int MC_CHANGE_VEL_STOP = 2;

// move tuning
const static float MC_DEFAULT_GRAVITY_SCALE = 2.0f;
const static float MC_DEFAULT_DENSITY = 2.0f;
const static float MC_DEFAULT_FRICTION = 0.0f;
const static float MC_DEFAULT_RESTITUTION = 0.0f;

const static float MC_WALK_INTO_DOOR_TIME = 0.4f;
const static float MC_WALK_OUT_DOOR_TIME = 0.8f;

const static float MC_IN_AIR_LINEAR_DAMPING = 1.5f;
const static float MC_ON_PLATFORM_LINEAR_DAMPING = 0.0f;

// face to...
const float MC_FACE_TO_LEFT = M_PI;
const float MC_FACE_TO_RIGHT = 0.;
const float MC_FACE_TO_TOWER_CENTER_FROM_LEFT = 1.5 * M_PI;
const float MC_FACE_TO_TOWER_CENTER_FROM_RIGHT = -M_PI_2;
const float MC_FACE_TO_CAMERA = M_PI_2;

class BaseMainCharacterItem: public TowerItem {
private:
	LinearInterpolatedValue angle_y;

	ZipperFlyItem* zipper_item;

	int platform_on_bottom;
	int slippy_dir;
	int any_contact;
	int door_on_left_right;
	int button_on_left_right;

	TowerItem* door;

	TowerItem* source_door;
	TowerItem* target_door;

	TowerItem* button;

	float face_to;

	b2Vec2 tmp_vec2;

protected:

	virtual void do_release_polygons() {
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		TowerItem::do_create_polygons(cache_lighting, env);
	}

	virtual BaseAnimatedCharacter* create_animated_character() {
		return NULL;
	}

	virtual void do_reset_to_default() {
		body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
		face_to = MC_FACE_TO_RIGHT;
		change_state(MC_STATE_DEFAULT, true);
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		zipper_item = new ZipperFlyItem(get_tower(), get_col(), get_row());
		items.push_back(zipper_item);
	}

	void create_mc_body(Box2dLoader & box2d_loader, const std::string & base_name, bool fixed_rotation = true, float gravity_scale = MC_DEFAULT_GRAVITY_SCALE, float density = MC_DEFAULT_DENSITY, float friction = MC_DEFAULT_FRICTION, float restitution =
			MC_DEFAULT_RESTITUTION) {
		b2BodyDef def;
		def.type = b2_dynamicBody;
		def.fixedRotation = fixed_rotation;
		def.gravityScale = gravity_scale;
		def.position = b2Vec2(col_to_map_x(), row_to_map_y());
		body = get_tower()->get_world()->CreateBody(&def);
		body->SetUserData(this);

		b2FixtureDef fd;
		fd.density = density;
		fd.friction = friction;
		fd.restitution = restitution;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		Box2dItem* item = box2d_loader.find_item(base_name);
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

		fd.isSensor = true;

		item = box2d_loader.find_item("mc_sensor_bottom");
		fd.userData = (void*) MC_SENSOR_BOTTOM;
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

		item = box2d_loader.find_item("mc_sensor_button");
		fd.userData = (void*) MC_SENSOR_BUTTON;
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

		item = box2d_loader.find_item("mc_sensor_door");
		fd.userData = (void*) MC_SENSOR_DOOR;
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());
	}

	// http://www.iforce2d.net/b2dtut/projected-trajectory
	float calculateVerticalVelocityForHeight(float desiredHeight) {
		if (desiredHeight <= 0)
			return 0; //wanna go down? just let it drop

		//gravity is given per second but we want time step values here
		float t = 1 / 60.0f;
		b2Vec2 stepGravity = body->GetGravityScale() * t * t * get_tower()->get_world()->GetGravity(); // m/s/s

				//quadratic equation setup (ax² + bx + c = 0)
		float a = 0.5f / stepGravity.y;
		float b = 0.5f;
		float c = desiredHeight;

		//check both possible solutions
		float quadraticSolution1 = (-b - b2Sqrt(b * b - 4 * a * c)) / (2 * a);
		float quadraticSolution2 = (-b + b2Sqrt(b * b - 4 * a * c)) / (2 * a);

		//use the one which is positive
		float v = quadraticSolution1;
		if (v < 0)
			v = quadraticSolution2;

		//convert answer back to seconds
		return v * 60.0f;
	}

	virtual void do_update(float delta, const Camera & camera) {

		// damping
		switch (get_state()) {
		case MC_STATE_JUMP_LEFT:
		case MC_STATE_JUMP_RIGHT:
		case MC_STATE_MOVE_LEFT:
		case MC_STATE_MOVE_RIGHT:
		case MC_STATE_HIT:
			body->SetLinearDamping(MC_IN_AIR_LINEAR_DAMPING);
			break;
		default:
			if (platform_on_bottom)
				body->SetLinearDamping(MC_ON_PLATFORM_LINEAR_DAMPING);
			else
				body->SetLinearDamping(MC_IN_AIR_LINEAR_DAMPING);
			break;
		}

		//

		switch (get_state()) {
		case MC_STATE_DEFAULT:
			if (platform_on_bottom)
				change_vel(MC_CHANGE_VEL_STOP);
			break;

		case MC_STATE_MOVE_LEFT:
			change_vel(MC_CHANGE_VEL_LEFT);
			break;
		case MC_STATE_MOVE_RIGHT:
			change_vel(MC_CHANGE_VEL_RIGHT);
			break;

		case MC_STATE_JUMP_LEFT: {
			change_vel(MC_CHANGE_VEL_LEFT);
			if (get_state_time() > 0.4f && platform_on_bottom) {
				change_state(MC_STATE_DEFAULT);
			}
		}
			break;

		case MC_STATE_JUMP_RIGHT: {
			change_vel(MC_CHANGE_VEL_RIGHT);
			if (get_state_time() > 0.4f && platform_on_bottom) {
				change_state(MC_STATE_DEFAULT);
			}
		}
			break;

		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
			change_vel(MC_CHANGE_VEL_LEFT);
			if (door == NULL) {
				change_state(MC_STATE_DEFAULT);
			} else if (door->get_map_position().x >= get_map_position().x)
				change_state(MC_STATE_WALK_INTO_DOOR);
			break;

		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
			change_vel(MC_CHANGE_VEL_RIGHT);
			if (door == NULL) {
				change_state(MC_STATE_DEFAULT);
			} else if (door->get_map_position().x <= get_map_position().x)
				change_state(MC_STATE_WALK_INTO_DOOR);
			break;

		case MC_STATE_WALK_INTO_DOOR: {
			body->SetTransform(source_door->get_map_position(), 0.0f);
			body->SetLinearVelocity(b2Vec2_zero);

			float st = get_state_time();
			if (st > MC_WALK_INTO_DOOR_TIME) {
				if (target_door)
					change_state(MC_STATE_WALK_OUT_DOOR);
				else
					change_state(MC_STATE_FINISHED);
				st = MC_WALK_INTO_DOOR_TIME;
			}
			float k = st / MC_WALK_INTO_DOOR_TIME;
			set_distance_from_base_radius(-BASE_RADIUS * k);
		}
			break;

		case MC_STATE_WALK_OUT_DOOR: {
			body->SetTransform(target_door->get_map_position(), 0.0f);
			body->SetLinearVelocity(b2Vec2_zero);

			float st = get_state_time();
			if (st > MC_WALK_OUT_DOOR_TIME) {
				st = MC_WALK_OUT_DOOR_TIME;
				change_state(MC_STATE_DEFAULT);
			}
			float k = 1.0f - st / MC_WALK_OUT_DOOR_TIME;
			set_distance_from_base_radius(-BASE_RADIUS * k);
		}
			break;

		case MC_STATE_HIT:
			if (any_contact < 1 && !is_animation_playing()) {
				change_state(MC_STATE_DEFAULT);
			}
			break;

		case MC_STATE_DYING:
			if (!is_animation_playing()) {
				get_tower()->restart();
			}
			break;

		case MC_STATE_FINISHED:
			body->SetTransform(source_door->get_map_position(), 0.0f);
			body->SetLinearVelocity(b2Vec2_zero);
			break;

		}

		open_the_door();

		correct_body_pos();

		angle_y.update(delta);
	}

	float get_angle_y() {
		return angle_y.get_value();
	}

	virtual void do_change_state(int state) {

		switch (state) {
		case MC_STATE_DEFAULT:
			play_animation("idle", true);

			get_tower()->get_camera_state().main_character_idle();

			reset_contact_mask(MC_CAN_CONTACT_WITH);

			set_distance_from_base_radius(get_tower()->get_default_distance_from_base_radius());

			break;

		case MC_STATE_HIT: {
			play_animation("hit", false);

			get_tower()->get_camera_state().shake();

			face_to = MC_FACE_TO_CAMERA;

//			b2Vec2 vel = body->GetLinearVelocity();
//			vel.Normalize();
//			vel *= 2.0;
//			body->SetLinearVelocity(vel);
		}
			break;

		case MC_STATE_MOVE_LEFT:
			get_tower()->get_camera_state().main_character_move();

			play_animation("move", true);

			face_to = MC_FACE_TO_LEFT;
			break;
		case MC_STATE_MOVE_RIGHT:
			get_tower()->get_camera_state().main_character_move();

			play_animation("move", true);

			face_to = MC_FACE_TO_RIGHT;
			break;

		case MC_STATE_JUMP_LEFT: {
			play_animation("jump", false);
			face_to = MC_FACE_TO_LEFT;
		}
			break;

		case MC_STATE_JUMP_RIGHT: {
			play_animation("jump", false);
			face_to = MC_FACE_TO_RIGHT;
		}
			break;

		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
			play_animation("move", true);
			face_to = MC_FACE_TO_LEFT;
			break;
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
			play_animation("move", true);
			face_to = MC_FACE_TO_RIGHT;
			break;

		case MC_STATE_WALK_INTO_DOOR:
			play_animation("move", true);
			body->SetLinearVelocity(b2Vec2_zero);

			disable_monsters_contacts();

			if (target_door) {
				float target_angle;
				target_door->get_world_angle(target_angle);
				get_tower()->get_camera_state().rotate(target_angle, MC_WALK_INTO_DOOR_TIME + MC_WALK_OUT_DOOR_TIME);
			}

			face_to = is_face_to_right() ? MC_FACE_TO_TOWER_CENTER_FROM_RIGHT : MC_FACE_TO_TOWER_CENTER_FROM_LEFT;
			break;

		case MC_STATE_WALK_OUT_DOOR:
			play_animation("move", true);
			body->SetLinearVelocity(b2Vec2_zero);

			face_to = MC_FACE_TO_CAMERA;
			angle_y.set_current(MC_FACE_TO_CAMERA);

			break;

		case MC_STATE_DYING:
			play_animation("dying", false);
			get_tower()->get_camera_state().restart();
			face_to = MC_FACE_TO_CAMERA;
			break;

		case MC_STATE_FINISHED:
			get_tower()->tower_finished();
			get_tower()->get_camera_state().finish();
			play_animation("idle", true);
			break;
		}

		turn();
	}

	inline int get_platform_on_bottom() {
		return platform_on_bottom;
	}

	void disable_monsters_contacts() {
		reset_contact_mask((TYPE_GROUND) | ((MC_CAN_CONTACT_WITH) & ~(TYPE_MONSTERS_FILTER)));
	}

	void turn() {
		angle_y.set_alpha(7.0f);
		angle_y.set_target(face_to);
	}

	void open_the_door() {
		if (door) {
			switch (get_state()) {
			case MC_STATE_WALK_OUT_DOOR:
				door->open_door();
				break;

			default:
				if (platform_on_bottom)
					door->open_door();
				break;
			}
		}
	}

	virtual void change_vel(int dir) {
	}

	void try_to_lift_updown() {
		b2Fixture* my_fixture;
		TowerItem* other_item;
		for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next) {

			if (!edge->contact->IsTouching())
				continue;

			my_fixture = NULL;
			other_item = NULL;

			if (edge->contact->GetFixtureA()->GetBody() == body) {
				my_fixture = edge->contact->GetFixtureA();
				other_item = (TowerItem*) edge->contact->GetFixtureB()->GetBody()->GetUserData();
			} else if (edge->contact->GetFixtureB()->GetBody() == body) {
				my_fixture = edge->contact->GetFixtureB();
				other_item = (TowerItem*) edge->contact->GetFixtureA()->GetBody()->GetUserData();
			}

			if (my_fixture && other_item)
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_BOTTOM) {
					if (other_item->get_contact_type() == TYPE_LIFT)
						other_item->toggle_lift();
				}
		}
	}

	void toggle_button() {
		if (button)
			button->toggle_button();
	}

	void walk_through_door() {
		if (door == NULL)
			return;

		if (!door->can_walk_through_door())
			return;

		switch (get_state()) {
		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
		case MC_STATE_WALK_INTO_DOOR:
		case MC_STATE_WALK_OUT_DOOR:
			return;
		}

		source_door = door;
		target_door = ((BaseDoorItem*) door)->get_opposite_door_item();

		if (door->get_map_position().x > get_map_position().x)
			change_state(MC_STATE_BEFORE_DOOR_MOVE_RIGHT);
		else
			change_state(MC_STATE_BEFORE_DOOR_MOVE_LEFT);
	}

	bool is_face_to_right() {
		return cos_fast(face_to) > 0.;
	}

public:
	BaseMainCharacterItem(Tower* tower, float col, float row, float scale_factor = 1.0f) :
			TowerItem(tower, col, row, TYPE_MAIN_CHARACTER, MC_CAN_CONTACT_WITH, scale_factor) {

		platform_on_bottom = 0;
		door_on_left_right = 0;
		button_on_left_right = 0;
		any_contact = 0;

		door = NULL;
		button = NULL;

		zipper_item = NULL;

		face_to = 0.0f;

		source_door = NULL;
		target_door = NULL;
	}

	~BaseMainCharacterItem() {
	}

	virtual const b2Vec2 & get_map_position() {

		switch (get_state()) {
		case MC_STATE_WALK_INTO_DOOR:
			if (source_door)
				return source_door->get_map_position();
			break;

		case MC_STATE_WALK_OUT_DOOR:
			if (target_door)
				return target_door->get_map_position();
			break;
		}

		return TowerItem::get_map_position();
	}

	virtual void control_inactive() {
		switch (get_state()) {

		case MC_STATE_MOVE_LEFT:
		case MC_STATE_MOVE_RIGHT:
			change_state(MC_STATE_DEFAULT);
			break;
		}
	}

	virtual void control_tap() {
		if (platform_on_bottom) {
			if (door && door->can_walk_through_door()) {
				walk_through_door();
			} else if (button) {
				toggle_button();
			} else {
				try_to_lift_updown();
			}
		}
	}

	virtual void control_upper_left_pressed() {
		if (platform_on_bottom)
			switch (get_state()) {
			case MC_STATE_DEFAULT:
			case MC_STATE_MOVE_LEFT:
			case MC_STATE_MOVE_RIGHT:
			case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
			case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
				change_state(MC_STATE_JUMP_LEFT);
				break;
			}
	}

	virtual void control_left_pressed() {
		switch (get_state()) {
		case MC_STATE_DEFAULT:
		case MC_STATE_MOVE_RIGHT:
		case MC_STATE_JUMP_RIGHT:
		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
			change_state(MC_STATE_MOVE_LEFT);
			break;
		}
	}

	virtual void control_upper_right_pressed() {
		if (platform_on_bottom)
			switch (get_state()) {
			case MC_STATE_DEFAULT:
			case MC_STATE_MOVE_RIGHT:
			case MC_STATE_MOVE_LEFT:
			case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
			case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
				change_state(MC_STATE_JUMP_RIGHT);
				break;
			}
	}

	virtual void control_right_pressed() {
		switch (get_state()) {
		case MC_STATE_DEFAULT:
		case MC_STATE_MOVE_LEFT:
		case MC_STATE_JUMP_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
			change_state(MC_STATE_MOVE_RIGHT);
			break;
		}
	}

	virtual void control_down_pressed() {
	}

	virtual int get_control_up() {
		return CONTROL_UP_NONE;
	}

	virtual bool is_need_to_render_effects() {
		return true;
	}

	virtual bool is_need_to_render_no_toon() {
		return true;
	}

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		if (my_fixture->GetBody() != this->body)
			return;

		if (other_fixture->GetFilterData().categoryBits == TYPE_GROUND) {
			change_state(MC_STATE_DYING);
			return;
		}

		TowerItem* other_item = (TowerItem*) other_fixture->GetBody()->GetUserData();
		if (other_item == NULL)
			return;

		if (my_fixture->IsSensor()) {
			if (other_item->is_platform()) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_BOTTOM) {
					platform_on_bottom++;
					other_item->platform_contact_from_top_begin();
				}
			} else if (other_item->get_contact_type() == TYPE_DOOR) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_DOOR) {
					door_on_left_right++;
					door = other_item;
				}
			} else if (other_item->get_contact_type() == TYPE_BUTTON) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_BUTTON) {
					button_on_left_right++;
					button = other_item;
					other_item->button_contact_begin();
				}
			}
		} else {
			any_contact++;
		}
	}

	virtual void end_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		if (my_fixture->GetBody() != this->body)
			return;

		TowerItem* other_item = (TowerItem*) other_fixture->GetBody()->GetUserData();
		if (other_item == NULL)
			return;

		if (my_fixture->IsSensor()) {
			if (other_item->is_platform()) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_BOTTOM) {
					platform_on_bottom--;
					other_item->platform_contact_from_top_end();
				}
			} else if (other_item->get_contact_type() == TYPE_DOOR) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_DOOR) {
					door_on_left_right--;
					if (door_on_left_right == 0)
						door = NULL;
					other_item->close_door();
				}
			} else if (other_item->get_contact_type() == TYPE_BUTTON) {
				if (my_fixture->GetUserData() == (void*) MC_SENSOR_BUTTON) {
					button_on_left_right--;
					if (button_on_left_right == 0)
						button = NULL;
					other_item->button_contact_end();
				}
			}
		} else {
			any_contact--;
		}
	}

	virtual void pre_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		switch (get_state()) {
		case MC_STATE_HIT:
			contact->SetEnabled(false);
			break;

		default:
			TowerItem* other_item = (TowerItem*) other_fixture->GetBody()->GetUserData();
			if (other_item && other_item->is_monster()) {
				if (!other_item->is_monster_active())
					contact->SetEnabled(false);
				else
					switch (get_state()) {
					case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
					case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
					case MC_STATE_WALK_INTO_DOOR:
					case MC_STATE_WALK_OUT_DOOR:
					case MC_STATE_DYING:
						contact->SetEnabled(false);
						break;
					}
			}
			break;
		}
	}

	virtual void post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {

		TowerItem* other_item = (TowerItem*) other_fixture->GetBody()->GetUserData();

		if (other_item && other_item->is_monster()) {

			b2WorldManifold mf;
			contact->GetWorldManifold(&mf);

			b2Vec2 v(mf.normal);
			v *= f;

			if (-0.99f < v.x && v.x < 0.99f && v.y < 0.0f) {

				other_item->monster_die();

				if (is_face_to_right())
					change_state(MC_STATE_JUMP_RIGHT, true);
				else
					change_state(MC_STATE_JUMP_LEFT, true);

			} else if (other_item->is_monster_active()) {
				change_state(MC_STATE_HIT, true);
				other_item->monster_punch();
			}
		}
	}
}
;

} // namespace ft

#endif /* ft_base_mc_item_H */
