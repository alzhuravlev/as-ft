#ifndef ft_step_item_H
#define ft_step_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "ft_base_step_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "ft_mosquito_item.h"

namespace ft {

class StepItem: public BaseStepItem {
private:

protected:

	virtual void do_release_polygons() {
		BaseStepItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseStepItem::do_create_polygons(cache_lighting, env);

		TextureAtlasLoader* atlas = get_tower()->get_tower_atlas(env);

		create_step_polygon(cache_lighting, *atlas, get_tower()->get_sprite_for_step(), get_tower()->get_sprite_for_stepside(), get_tower()->get_sprite_for_step_nm(), get_tower()->get_sprite_for_stepside_nm(), env, true, true);
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		BaseStepItem::do_create_child_items(items);

		TowerItem* up = get_tower()->get_item_at(get_col(), get_row() + 1.0f, TYPE_ALL_FILTER);
		TowerItem* up_up = get_tower()->get_item_at(get_col(), get_row() + 2.0f, TYPE_ALL_FILTER);
		TowerItem* up_up_up = get_tower()->get_item_at(get_col(), get_row() + 3.0f, TYPE_ALL_FILTER);
		TowerItem* up_up_up_up = get_tower()->get_item_at(get_col(), get_row() + 4.0f, TYPE_ALL_FILTER);

		if (!up && !up_up && !up_up_up && !up_up_up_up) {
			items.push_back(new MosquitoItem(get_tower(), get_col(), get_row() + 1.0f));
		}
	}

public:
	StepItem(Tower* tower, float col, float row) :
			BaseStepItem(tower, col, row) {
	}

	~StepItem() {
	}

	virtual bool is_need_to_update() {
		return false;
	}
}
;

} // namespace ft

#endif /* ft_step_item_H */
