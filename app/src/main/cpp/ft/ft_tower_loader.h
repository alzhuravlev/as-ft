#ifndef ft_tower_loader_H
#define ft_tower_loader_H

#include "engine.h"
#include "external.h"
#include "fte_utils.h"
#include "ft_tower_item_consts.h"

#include <string>
#include <vector>
#include <map>

using namespace fte;

namespace ft {

const static float BASE_RADIUS = 1.5f;

const static int DEFAULT_ZIPPERFLY_MOVEMENT_ALPHA = 1.0f;
const static float DEFAULT_ZIPPERFLY_DISTANCE_Y = 1.0f;

const static float DEFAULT_DOOR_SCALE_X = 3.0f;
const static float DEFAULT_DOOR_SCALE_Y = 1.5f;
const static float DEFAULT_EYE_SCALE = 0.8f;
const static float DEFAULT_BALL_SCALE = 0.7f;
const static float DEFAULT_BLOCK_SCALE = 0.4f;
const static float DEFAULT_VAGON_SCALE = 1.2f;
const static float DEFAULT_POGO_SCALE = 0.5f;
const static float DEFAULT_LIFT_SCALE = 1.2f;
const static float DEFAULT_MOSQUITO_SCALE = 0.7f;
const static float DEFAULT_ZIPPERFLY_SCALE = 0.15f;

const static float DEFAULT_BRICK_HEIGHT_SCALE = 0.5f;

const static float DEFAULT_PLATFORM_HEIGHT_SCALE = 0.5f;
const static float DEFAULT_PLATFORM_DEPTH = 0.4f;
const static float DEFAULT_PLATFORM_GAP = 0.3f;
const static float DEFAULT_PLATFORM_AMPLITUDE = 0.05f;
const static float DEFAULT_PLATFORM_CYCLES = 2.0f;
const static float DEFAULT_PLATFORM_INCLINE_AMPLITUDE = 0.04f;
const static float DEFAULT_PLATFORM_INCLINE_CYCLES = 2.0f;

const static float DEFAULT_POGO_MOVE_VEL_X = 3.5f;
const static float DEFAULT_POGO_MOVE_ACCEL_X = 1.5f;
const static float DEFAULT_POGO_JUMP_VEL_X = 2.0f;
const static float DEFAULT_POGO_JUMP_HEIGHT = 2.5f;

const static int LIGHT_TYPE_DL = 0;
const static int LIGHT_TYPE_PL = 1;
const static int LIGHT_TYPE_SL = 2;
const static int LIGHT_TYPE_NL = 3;

const static int TOWER_DATA_QUALITY_LEVEL_LOW = 0;
const static int TOWER_DATA_QUALITY_LEVEL_MED = 1;
const static int TOWER_DATA_QUALITY_LEVEL_HIGH = 2;

struct TowerDataItem {
	float col, row;
	std::vector<std::string> params;
};

struct TowerDataLine {
	int fly_time;
};

struct TowerData {
	int cols, rows;
	int max_cols;

	bool active_at_start_button1;
	bool active_at_start_button2;
	bool active_at_start_button3;

	std::string sprite_for_tower_tile;
	std::string sprite_for_door;
	std::string sprite_for_step;
	std::string sprite_for_stepside;
	std::string sprite_for_pipe;

	std::string sprite_for_tower_tile_nm;
	std::string sprite_for_step_nm;
	std::string sprite_for_stepside_nm;
	std::string sprite_for_pipe_nm;

	std::vector<TowerDataLine> lines;
	std::vector<TowerDataItem> items;

	int bricks_in_tile;
	int tiles_in_cache;
	float brick_height_scale;

	float platform_gap;
	float platform_depth;
	float platform_height_scale;
	float platform_smoothness;
	float platform_amplitude;
	float platform_cycles;
	float platform_incline_amplitude;
	float platform_incline_cycles;
	std::string platform_shape_name;

	bool platform_follow_tower_surface_distortion_angle;
	bool platform_follow_tower_surface_distortion;

	float door_scale_x;
	float door_scale_y;
	float eye_scale;
	float ball_scale;
	float block_scale;
	float pogo_scale;
	float vagon_scale;
	float lift_scale;
	float mosquito_scale;
	float zipperfly_scale;

	float pogo_move_vel_x;
	float pogo_move_accel_x;
	float pogo_jump_vel_x;
	float pogo_jump_height;

	int door_frame_smoothness;
	int door_frame_roundness;
	float door_frame_radius;

	int pipe_smoonthness;

	float lift_pipe_radius;
	int lift_pipe_roundness;
	float lift_pipe_radius_amplitude;
	float lift_pipe_radius_cycles;

	float pipe_radius;
	int pipe_roundness;
	float pipe_radius_amplitude;
	float pipe_radius_cycles;

	int tower_num_edges;
	int tower_num_edges_for_texture;

	float tower_step_vertical;

	float tower_surface_y_amplitude;
	float tower_surface_y_sin_coef;
	float tower_surface_y_cos_coef;
	float tower_surface_angle_amplitude;
	float tower_surface_angle_sin_angle_coef;
	float tower_surface_angle_sin_y_coef;

	int tower_tile_count_v;
	int tower_tile_count_h;

	int light_type;

	float light_ambient;
	float light_specular_exp;
	float light_spot_exp;
	float light_spot_cos_cutoff;
	float light_attenuation;

	float zipperfly_distance_from_base_radius;
	float zipperfly_movement_alpha;
	float zipperfly_distance_y;

	float bg_color_r;
	float bg_color_g;
	float bg_color_b;

	float fog_color_r;
	float fog_color_g;
	float fog_color_b;

	float bubble_color_r;
	float bubble_color_g;
	float bubble_color_b;

	float toon_fog_color_r;
	float toon_fog_color_g;
	float toon_fog_color_b;

	float toon_fog_alpha_threshold;

	float fog_a;
	float fog_b;

	float vertex_vibration_freq;
	float vertex_vibration_amplitude;

	float time_limit;

	TowerData() {
		cols = 16;
		max_cols = 0;
		rows = 20;

		active_at_start_button1 = false;
		active_at_start_button2 = false;
		active_at_start_button3 = false;

		sprite_for_tower_tile = "tower_tile1";
		sprite_for_door = "door1";
		sprite_for_step = "step1";
		sprite_for_stepside = "stepside1";
		sprite_for_pipe = "pipe1";

		sprite_for_tower_tile_nm = "tower_tile1_nm";
		sprite_for_step_nm = "step1_nm";
		sprite_for_stepside_nm = "stepside1_nm";
		sprite_for_pipe_nm = "pipe1_nm";

		bricks_in_tile = 2;
		tiles_in_cache = 6;

		brick_height_scale = DEFAULT_BRICK_HEIGHT_SCALE;

		platform_height_scale = DEFAULT_PLATFORM_HEIGHT_SCALE;
		platform_depth = BASE_RADIUS * DEFAULT_PLATFORM_DEPTH;
		platform_gap = BASE_RADIUS * DEFAULT_PLATFORM_GAP;
		platform_smoothness = 0.34f;
		platform_amplitude = BASE_RADIUS * DEFAULT_PLATFORM_AMPLITUDE;
		platform_cycles = DEFAULT_PLATFORM_CYCLES;
		platform_incline_amplitude = BASE_RADIUS * DEFAULT_PLATFORM_INCLINE_AMPLITUDE;
		platform_incline_cycles = DEFAULT_PLATFORM_INCLINE_CYCLES;
		platform_shape_name = "step_shape";

		platform_follow_tower_surface_distortion_angle = true;
		platform_follow_tower_surface_distortion = true;

		door_scale_x = DEFAULT_DOOR_SCALE_X;
		door_scale_y = DEFAULT_DOOR_SCALE_Y;
		eye_scale = DEFAULT_EYE_SCALE;
		ball_scale = DEFAULT_BALL_SCALE;
		block_scale = DEFAULT_BLOCK_SCALE;
		pogo_scale = DEFAULT_POGO_SCALE;
		vagon_scale = DEFAULT_VAGON_SCALE;
		lift_scale = DEFAULT_LIFT_SCALE;
		mosquito_scale = DEFAULT_MOSQUITO_SCALE;
		zipperfly_scale = DEFAULT_ZIPPERFLY_SCALE;

		pogo_move_vel_x = DEFAULT_POGO_MOVE_VEL_X;
		pogo_move_accel_x = DEFAULT_POGO_MOVE_ACCEL_X;
		pogo_jump_vel_x = DEFAULT_POGO_JUMP_VEL_X;
		pogo_jump_height = DEFAULT_POGO_JUMP_HEIGHT;

		door_frame_smoothness = 22;
		door_frame_roundness = 20;
		door_frame_radius = 0.1f;

		pipe_smoonthness = 2;

		lift_pipe_radius = BASE_RADIUS * 0.02f;
		lift_pipe_roundness = 6;
		lift_pipe_radius_amplitude = 0.12f;
		lift_pipe_radius_cycles = 6.0f;

		pipe_roundness = 6;
		pipe_radius = BASE_RADIUS * 0.045f;
		pipe_radius_amplitude = 0.33f;
		pipe_radius_cycles = 10.0f;

		tower_num_edges = 12;
		tower_num_edges_for_texture = 2;

		tower_step_vertical = 0.25f;

		tower_surface_y_amplitude = BASE_RADIUS * 0.03f;
		tower_surface_y_sin_coef = 2.1f;
		tower_surface_y_cos_coef = 3.1f;
		tower_surface_angle_amplitude = BASE_RADIUS * 0.04f;
		tower_surface_angle_sin_angle_coef = 2.0f;
		tower_surface_angle_sin_y_coef = 1.0f;

		tower_tile_count_v = 1;
		tower_tile_count_h = 1;

		light_type = LIGHT_TYPE_PL;

		light_ambient = 0.5f;
		light_specular_exp = 128.0f;

		light_spot_exp = 1.0f;
		light_spot_cos_cutoff = 0.70710678118654752440084436210485f; // ~45 Deg
		light_attenuation = 64.0f;

		zipperfly_distance_from_base_radius = BASE_RADIUS * 0.2f;
		zipperfly_movement_alpha = DEFAULT_ZIPPERFLY_MOVEMENT_ALPHA;
		zipperfly_distance_y = DEFAULT_ZIPPERFLY_DISTANCE_Y;

		bg_color_r = 0.424f;
		bg_color_g = 0.808f;
		bg_color_b = 0.98f;

		fog_color_r = bg_color_r;
		fog_color_g = bg_color_g;
		fog_color_b = bg_color_b;

		bubble_color_r = CLAMP(bg_color_r * 0.3f, 0.0f, 1.0f);
		bubble_color_g = CLAMP(bg_color_g * 0.4f, 0.0f, 1.0f);
		bubble_color_b = CLAMP(bg_color_b * 0.6f, 0.0f, 1.0f);

		toon_fog_color_r = 0.0f;
		toon_fog_color_g = 0.0f;
		toon_fog_color_b = 0.0f;

		toon_fog_alpha_threshold = 0.3f;

		fog_a = 1.5f;
		fog_b = 0.0f;

		vertex_vibration_freq = 0.4f;
		vertex_vibration_amplitude = 0.3f;

		time_limit = 30.0f;
	}

	std::string format_tile_name(const std::string & base_name, int h, int v) {
		return base_name + to_string(h) + "x" + to_string(v);
	}

	void set_quality_level(int level) {
		switch (level) {

		case TOWER_DATA_QUALITY_LEVEL_MED:
			break;

		case TOWER_DATA_QUALITY_LEVEL_HIGH:
			break;

		default:

			break;
		}
	}
};

class TowerLoader {

private:
	TowerData tower_data;

	void load(std::string file_name, FTE_ENV env) {

		std::vector<std::string> lines;
		read_resource_lines(file_name, env, lines);

		std::map<std::string, std::string> metas;
		std::vector<std::string> tower_lines;
		std::vector<std::string> flys;

		// just read the file

		for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::string line = *it;

			remove_chars(line, "\n\r");

			if (starts_with(line, "//"))
				continue;

			if (starts_with(line, "#"))
				continue;

			if (line.length() == 0)
				continue;

			std::vector<std::string> el;
			split(line, ':', el);

			if (el.size() == 0)
				continue;

			if (el[0] == "w") {
				tower_data.cols = atoi(el[1].c_str());

			} else if (el[0] == "light_type") {
				if (el[1] == "dl")
					tower_data.light_type = LIGHT_TYPE_DL;
				else if (el[1] == "pl")
					tower_data.light_type = LIGHT_TYPE_PL;
				else if (el[1] == "sl")
					tower_data.light_type = LIGHT_TYPE_SL;
				else if (el[1] == "nl")
					tower_data.light_type = LIGHT_TYPE_NL;

			} else if (el[0] == "light_ambient") {
				tower_data.light_ambient = atof(el[1].c_str());

			} else if (el[0] == "light_specular_exp") {
				tower_data.light_specular_exp = atof(el[1].c_str());

			} else if (el[0] == "light_spot_exp") {
				tower_data.light_spot_exp = atof(el[1].c_str());

			} else if (el[0] == "light_spot_cos_cutoff") {
				tower_data.light_spot_cos_cutoff = atof(el[1].c_str());

			} else if (el[0] == "light_attenuation") {
				tower_data.light_attenuation = atof(el[1].c_str());

			} else if (el[0] == "zipperfly_distance_from_base_radius") {
				tower_data.zipperfly_distance_from_base_radius = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "zipperfly_movement_alpha") {
				tower_data.zipperfly_movement_alpha = atof(el[1].c_str());

			} else if (el[0] == "zipperfly_distance_y") {
				tower_data.zipperfly_distance_y = atof(el[1].c_str());

			} else if (el[0] == "tower_tile_count_v") {
				tower_data.tower_tile_count_v = atoi(el[1].c_str());

			} else if (el[0] == "tower_tile_count_h") {
				tower_data.tower_tile_count_h = atoi(el[1].c_str());

			} else if (el[0] == "platform_depth") {
				tower_data.platform_depth = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "platform_gap") {
				tower_data.platform_gap = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "platform_smoothness") {
				tower_data.platform_smoothness = atof(el[1].c_str());

			} else if (el[0] == "platform_amplitude") {
				tower_data.platform_amplitude = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "platform_cycles") {
				tower_data.platform_cycles = atof(el[1].c_str());

			} else if (el[0] == "platform_incline_amplitude") {
				tower_data.platform_incline_amplitude = atof(el[1].c_str());

			} else if (el[0] == "platform_incline_cycles") {
				tower_data.platform_incline_cycles = atof(el[1].c_str());

			} else if (el[0] == "platform_follow_tower_surface_distortion_angle") {
				tower_data.platform_follow_tower_surface_distortion_angle = el[1] == "true";

			} else if (el[0] == "platform_follow_tower_surface_distortion") {
				tower_data.platform_follow_tower_surface_distortion = el[1] == "true";

			} else if (el[0] == "platform_shape_name") {
				tower_data.platform_shape_name = el[1];

			} else if (el[0] == "bricks_in_tile") {
				tower_data.bricks_in_tile = atoi(el[1].c_str());

			} else if (el[0] == "tiles_in_cache") {
				tower_data.tiles_in_cache = atoi(el[1].c_str());

			} else if (el[0] == "brick_height_scale") {
				tower_data.brick_height_scale = atof(el[1].c_str());

			} else if (el[0] == "platform_height_scale") {
				tower_data.platform_height_scale = atof(el[1].c_str());

			} else if (el[0] == "pogo_move_vel_x") {
				tower_data.pogo_move_vel_x = atof(el[1].c_str());

			} else if (el[0] == "pogo_move_accel_x") {
				tower_data.pogo_move_accel_x = atof(el[1].c_str());

			} else if (el[0] == "pogo_jump_vel_x") {
				tower_data.pogo_jump_vel_x = atof(el[1].c_str());

			} else if (el[0] == "pogo_jump_height") {
				tower_data.pogo_jump_height = atof(el[1].c_str());

			} else if (el[0] == "pogo_scale") {
				tower_data.pogo_scale = atof(el[1].c_str());

			} else if (el[0] == "lift_scale") {
				tower_data.lift_scale = atof(el[1].c_str());

			} else if (el[0] == "mosquito_scale") {
				tower_data.mosquito_scale = atof(el[1].c_str());

			} else if (el[0] == "zipperfly_scale") {
				tower_data.zipperfly_scale = atof(el[1].c_str());

			} else if (el[0] == "pipe_smoonthness") {
				tower_data.pipe_smoonthness = atoi(el[1].c_str());

			} else if (el[0] == "pipe_roundness") {
				tower_data.pipe_roundness = atoi(el[1].c_str());

			} else if (el[0] == "pipe_radius") {
				tower_data.pipe_radius = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "lift_pipe_radius") {
				tower_data.lift_pipe_radius = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "pipe_radius_amplitude") {
				tower_data.pipe_radius_amplitude = atof(el[1].c_str());

			} else if (el[0] == "pipe_radius_cycles") {
				tower_data.pipe_radius_cycles = atof(el[1].c_str());

			} else if (el[0] == "lift_pipe_radius_amplitude") {
				tower_data.lift_pipe_radius_amplitude = atof(el[1].c_str());

			} else if (el[0] == "lift_pipe_radius_cycles") {
				tower_data.lift_pipe_radius_cycles = atof(el[1].c_str());

			} else if (el[0] == "door_frame_smoothness") {
				tower_data.door_frame_smoothness = atoi(el[1].c_str());

			} else if (el[0] == "door_frame_roundness") {
				tower_data.door_frame_roundness = atoi(el[1].c_str());

			} else if (el[0] == "door_frame_radius") {
				tower_data.door_frame_radius = atof(el[1].c_str());

			} else if (el[0] == "vagon_scale") {
				tower_data.vagon_scale = atof(el[1].c_str());

			} else if (el[0] == "door_scale_x") {
				tower_data.door_scale_x = atof(el[1].c_str());

			} else if (el[0] == "door_scale_y") {
				tower_data.door_scale_y = atof(el[1].c_str());

			} else if (el[0] == "eye_scale") {
				tower_data.eye_scale = atof(el[1].c_str());

			} else if (el[0] == "ball_scale") {
				tower_data.ball_scale = atof(el[1].c_str());

			} else if (el[0] == "block_scale") {
				tower_data.block_scale = atof(el[1].c_str());

			} else if (el[0] == "tower_num_edges") {
				tower_data.tower_num_edges = atoi(el[1].c_str());

			} else if (el[0] == "tower_num_edges_for_texture") {
				tower_data.tower_num_edges_for_texture = atoi(el[1].c_str());

			} else if (el[0] == "tower_step_vertical") {
				tower_data.tower_step_vertical = atof(el[1].c_str());

			} else if (el[0] == "tower_surface_y_amplitude") {
				tower_data.tower_surface_y_amplitude = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "tower_surface_y_sin_coef") {
				tower_data.tower_surface_y_sin_coef = atof(el[1].c_str());

			} else if (el[0] == "tower_surface_y_cos_coef") {
				tower_data.tower_surface_y_cos_coef = atof(el[1].c_str());

			} else if (el[0] == "tower_surface_angle_amplitude") {
				tower_data.tower_surface_angle_amplitude = BASE_RADIUS * atof(el[1].c_str());

			} else if (el[0] == "tower_surface_angle_sin_angle_coef") {
				tower_data.tower_surface_angle_sin_angle_coef = atof(el[1].c_str());

			} else if (el[0] == "tower_surface_angle_sin_y_coef") {
				tower_data.tower_surface_angle_sin_y_coef = atof(el[1].c_str());

			} else if (el[0] == "sprite_for_tower_tile") {
				tower_data.sprite_for_tower_tile = el[1];

			} else if (el[0] == "sprite_for_tower_tile_nm") {
				tower_data.sprite_for_tower_tile_nm = el[1];

			} else if (el[0] == "sprite_for_door") {
				tower_data.sprite_for_door = el[1];

			} else if (el[0] == "sprite_for_step") {
				tower_data.sprite_for_step = el[1];

			} else if (el[0] == "sprite_for_step_nm") {
				tower_data.sprite_for_step_nm = el[1];

			} else if (el[0] == "sprite_for_stepside") {
				tower_data.sprite_for_stepside = el[1];

			} else if (el[0] == "sprite_for_stepside_nm") {
				tower_data.sprite_for_stepside_nm = el[1];

			} else if (el[0] == "sprite_for_pipe_nm") {
				tower_data.sprite_for_pipe_nm = el[1];

			} else if (el[0] == "sprite_for_pipe") {
				tower_data.sprite_for_pipe = el[1];

			} else if (el[0] == "button1") {
				tower_data.active_at_start_button1 = el[1] == "on" || el[1] == "true";

			} else if (el[0] == "button2") {
				tower_data.active_at_start_button2 = el[1] == "on" || el[1] == "true";

			} else if (el[0] == "button3") {
				tower_data.active_at_start_button3 = el[1] == "on" || el[1] == "true";

			} else if (el[0] == "bg_color") {

				std::vector<std::string> _el_;
				split(el[1], ';', _el_);

				if (_el_.size() > 0) {
					uint32_t c = (uint32_t) strtoul(_el_[0].c_str(), NULL, 16);
					float a;
					unpack_color_f(c, tower_data.bg_color_r, tower_data.bg_color_g, tower_data.bg_color_b, a);
				}

				float k;

				k = _el_.size() > 1 ? atof(_el_[1].c_str()) : 1.0f;
				tower_data.fog_color_r = CLAMP(tower_data.bg_color_r * k, 0.0, 1.0);
				tower_data.fog_color_g = CLAMP(tower_data.bg_color_g * k, 0.0, 1.0);
				tower_data.fog_color_b = CLAMP(tower_data.bg_color_b * k, 0.0, 1.0);

				k = _el_.size() > 2 ? atof(_el_[2].c_str()) : 0.5f;
				tower_data.bubble_color_r = CLAMP(tower_data.bg_color_r * k, 0.0, 1.0);
				tower_data.bubble_color_g = CLAMP(tower_data.bg_color_g * k, 0.0, 1.0);
				tower_data.bubble_color_b = CLAMP(tower_data.bg_color_b * k, 0.0, 1.0);

				k = _el_.size() > 3 ? atof(_el_[3].c_str()) : 0.0f;
				tower_data.toon_fog_color_r = CLAMP(tower_data.bg_color_r * k, 0.0, 1.0);
				tower_data.toon_fog_color_g = CLAMP(tower_data.bg_color_g * k, 0.0, 1.0);
				tower_data.toon_fog_color_b = CLAMP(tower_data.bg_color_b * k, 0.0, 1.0);

			} else if (el[0] == "fog_color") {
				uint32_t c = (uint32_t) strtoul(el[1].c_str(), NULL, 16);
				float a;
				unpack_color_f(c, tower_data.fog_color_r, tower_data.fog_color_g, tower_data.fog_color_b, a);

			} else if (el[0] == "bubble_color") {
				uint32_t c = (uint32_t) strtoul(el[1].c_str(), NULL, 16);
				float a;
				unpack_color_f(c, tower_data.bubble_color_r, tower_data.bubble_color_g, tower_data.bubble_color_b, a);

			} else if (el[0] == "toon_fog_color") {
				uint32_t c = (uint32_t) strtoul(el[1].c_str(), NULL, 16);
				float a;
				unpack_color_f(c, tower_data.toon_fog_color_r, tower_data.toon_fog_color_g, tower_data.toon_fog_color_b, a);

			} else if (el[0] == "toon_fog_alpha_threshold") {
				tower_data.toon_fog_alpha_threshold = atof(el[1].c_str());

			} else if (el[0] == "fog_a") {
				tower_data.fog_a = atof(el[1].c_str());

			} else if (el[0] == "fog_b") {
				tower_data.fog_b = atof(el[1].c_str());

			} else if (el[0] == "vertex_vibration_freq") {
				tower_data.vertex_vibration_freq = atof(el[1].c_str());

			} else if (el[0] == "vertex_vibration_amplitude") {
				tower_data.vertex_vibration_amplitude = atof(el[1].c_str());

			} else if (el[0] == "time_limit") {
				tower_data.time_limit = atof(el[1].c_str());

			} else if (el[0] == "r") {
				flys.push_back(el.size() > 1 ? el[1] : "");
				tower_lines.push_back(el.size() > 2 ? el[2] : "");

			} else if (el[0] == "meta") {
				metas[el[1]] = el[2];
			}
		}

		// now we are ready to construct actual lines and items

		tower_data.rows = tower_lines.size();

		int i = 0;
		int cols = tower_data.cols;
		int rows = tower_data.rows;
		float row;

		for (std::vector<std::string>::const_iterator it = tower_lines.begin(); it != tower_lines.end(); ++it) {

			row = rows - i - 1.0f;

			TowerDataLine tll;
			tll.fly_time = atoi(flys.at(i++).c_str());
			tower_data.lines.push_back(tll);

			std::string line = *it;

			int idx = 0;
			for (std::string::const_iterator s_it = line.begin(); s_it != line.end(); ++s_it) {

				if (idx >= cols)
					break;

				float col = idx;
				char c = *s_it;

				if (c != 0 && c != 32 && c != 10 && c != 13) {

					std::string s(1, c);

					std::map<std::string, std::string>::const_iterator res_it = metas.find(s);

					if (res_it != metas.end()) {

						TowerDataItem tli;
						tli.col = col;
						tli.row = row;
						split(res_it->second, ';', tli.params);

						if (tli.params.size() == 0) {
							LOGW("fte: TowerLoader: tower '%s' has wrong meta '%s'. at least type must be defined!", file_name.c_str(), s.c_str());
							continue;
						}

						if (col > tower_data.max_cols)
							tower_data.max_cols = col;

						tower_data.items.push_back(tli);

					} else {
						LOGW("fte: TowerLoader: tower '%s' has unknown symbol '%s' (%d) in row %d, col %d", file_name.c_str(), s.c_str(), c, row, col);
					}
				}
				idx++;
			}
		}

		// put ground
		TowerDataItem tli;
		tli.col = 0.0f;
		tli.row = 0.0f;
		tli.params.push_back("GroundItem");
		tower_data.items.push_back(tli);
		//

		LOGD("Tower: %s", file_name.c_str());
		LOGD("Tower.cols %d", tower_data.cols);
		LOGD("Tower.rows %d", tower_data.rows);
		LOGD("Tower.items.size %d", tower_data.items.size());
	}

public:
	TowerLoader(std::string file_name, FTE_ENV env) {
		load(file_name, env);
	}

	const TowerData & get_tower_data() {
		return tower_data;
	}
}
;

} // namespace ft

#endif /* ft_tower_loader_H */
