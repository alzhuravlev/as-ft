#include "ft_game_screen.h"
#include "ft_level_screen.h"
#include "ft_box_screen.h"

namespace ft {

void GameScreen::do_open_level_screen() {
	get_updater()->set_pending_screen(new LevelScreen(box_item));
}

void GameScreen::do_open_next_level() {
	int next_level_index = level_index + 1;
	if (next_level_index < box_item.levels.size())
		get_updater()->set_pending_screen(new GameScreen(box_item, next_level_index));
	else
		get_updater()->set_pending_screen(new BoxScreen());
}

} // namespace ft
