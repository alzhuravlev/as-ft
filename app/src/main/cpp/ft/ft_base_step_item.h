#ifndef ft_base_step_item_H
#define ft_base_step_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "interpolate_utils.h"
#include "polygon_builder.h"
#include "map_utils.h"

namespace ft {

class BaseStepItem: public TowerItem {
private:
protected:

	bool stub_on_left;
	bool stub_on_right;
	bool door_on_top;

	void create_body_col_row(Box2dLoader & box2d_loader, float col, float row) {
		if (!body) {
			b2BodyDef def;
			body = get_tower()->get_world()->CreateBody(&def);
			body->SetUserData(this);
		}

		b2FixtureDef fd;
		fd.friction = 0.8f;
		fd.restitution = 0.0f;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		std::vector<Vec3> interpolated_map_points;
		interpolate_road(col, row, interpolated_map_points, get_tower()->get_platform_smoothness());
		std::vector<b2Vec2> points;
		convert_vec3_to_b2vec2(interpolated_map_points, points);

		attach_edge_fixture_to_body(body, &fd, points, get_tower()->get_brick_height_scale() * get_tower()->get_platform_height_scale());
	}

	bool need_divider_between_me_and_other(TowerItem* other) {
		int group = other->get_platform_temporary_group();
		if (group < 0)
			return true;
		if (group > 0)
			return group != get_platform_temporary_group();
		return false;
	}

	void interpolate_road(float col, float row, std::vector<Vec3> & interpolated_map_points, float alpha) {
		int left_pos = STEP_POINT_MID;
		int right_pos = STEP_POINT_MID;

		TowerItem* door_above_item = get_tower()->get_item_at_top(col, row, TYPE_DOOR);

		TowerItem* top_item = get_tower()->get_item_at_top(col, row, TYPE_STEPS_FILTER);
		TowerItem* connect_to_left_item = get_tower()->get_item_at_left(col, row, TYPE_STEPS_FILTER);
		TowerItem* connect_to_right_item = get_tower()->get_item_at_right(col, row, TYPE_STEPS_FILTER);
		if (top_item == NULL) {
			TowerItem* left_top_item = get_tower()->get_item_at_left_top(col, row, TYPE_STEPS_FILTER);
			TowerItem* left_bottom_item = get_tower()->get_item_at_left_bottom(col, row, TYPE_STEPS_FILTER);
			if (left_top_item) {
				left_pos = STEP_POINT_UP;
				connect_to_left_item = left_top_item;
			} else if (connect_to_left_item)
				left_pos = STEP_POINT_MID;
			else if (left_bottom_item) {
				left_pos = STEP_POINT_DOWN;
				connect_to_left_item = left_bottom_item;
			}

			TowerItem* right_top_item = get_tower()->get_item_at_right_top(col, row, TYPE_STEPS_FILTER);
			TowerItem* right_bottom_item = get_tower()->get_item_at_right_bottom(col, row, TYPE_STEPS_FILTER);
			if (right_top_item) {
				right_pos = STEP_POINT_UP;
				connect_to_right_item = right_top_item;
			} else if (connect_to_right_item)
				right_pos = STEP_POINT_MID;
			else if (right_bottom_item) {
				right_pos = STEP_POINT_DOWN;
				connect_to_right_item = right_bottom_item;
			}
		}

		stub_on_left = top_item || !connect_to_left_item || (need_divider_between_me_and_other(connect_to_left_item));
		stub_on_right = top_item || !connect_to_right_item || (need_divider_between_me_and_other(connect_to_right_item));
		door_on_top = door_above_item != NULL;

		Vec3 dir45(1.0f, 1.0f, 0.0f);
		vec_normalize(dir45);
		vec_mul(dir45, 0.05f);

		Vec3 dir45n(1.0f, -1.0f, 0.0f);
		vec_normalize(dir45n);
		vec_mul(dir45n, 0.05f);

		Vec3 dir0(1.0f, 0.0f, 0.0f);
		vec_normalize(dir0);
		vec_mul(dir0, 0.05f);

		Vec3 mp0;
		Vec3 mp1;
		Vec3 mp2;
		Vec3 mp3;

		Vec3 sp0;
		Vec3 sp1;
		Vec3 sp2;
		Vec3 sp3;

		float brick_height_scale = get_tower()->get_brick_height_scale();

		// mp0
		switch (left_pos) {
		case STEP_POINT_UP:
			mp0.set(col, (row + 1.0f + 0.5f) * brick_height_scale, 0.0f);
			vec_add(mp0, dir45n, sp0);
			break;
		case STEP_POINT_MID:
			mp0.set(col, (row + 1.0f) * brick_height_scale, 0.0f);
			vec_add(mp0, dir0, sp0);
			break;
		case STEP_POINT_DOWN:
			mp0.set(col, (row + 1.0f - 0.5f) * brick_height_scale, 0.0f);
			vec_add(mp0, dir45, sp0);
			break;
		}

		// mp3
		switch (right_pos) {
		case STEP_POINT_UP:
			mp3.set(col + 1.0f, (row + 1.0f + 0.5f) * brick_height_scale, 0.0f);
			vec_add(mp3, dir45, sp3);
			break;
		case STEP_POINT_MID:
			mp3.set(col + 1.0f, (row + 1.0f) * brick_height_scale, 0.0f);
			vec_add(mp3, dir0, sp3);
			break;
		case STEP_POINT_DOWN:
			mp3.set(col + 1.0f, (row + 1.0f - 0.5f) * brick_height_scale, 0.0f);
			vec_add(mp3, dir45n, sp3);
			break;
		}

		if (door_above_item) {
			mp1.set(col + 0.45f, (row + 1.0f) * brick_height_scale, 0.0f);
			vec_add(mp1, dir0, sp1);

			mp2.set(col + 0.55f, (row + 1.0f) * brick_height_scale, 0.0f);
			vec_add(mp2, dir0, sp2);
		}

		std::vector<Vec3> map_points;
		std::vector<Vec3> support_points;

		map_points.push_back(mp0);
		if (door_above_item) {
			map_points.push_back(mp1);
			map_points.push_back(mp2);
		}
		map_points.push_back(mp3);

		support_points.push_back(sp0);
		if (door_above_item) {
			support_points.push_back(sp1);
			support_points.push_back(sp2);
		}
		support_points.push_back(sp3);

		beizer_interpolate(map_points, support_points, interpolated_map_points, alpha);
	}

	void create_step_polygon_for_points(const std::vector<Vec3>::const_iterator interpolated_map_points_begin, const std::vector<Vec3>::const_iterator interpolated_map_points_end, PolygonCache* cache, const TextureAtlasLoader & atlas,
			const std::string & texture_name, const std::string & texture_name_side, const std::string & texture_name_nm, const std::string & texture_name_side_nm, FTE_ENV env, bool stub_on_left, bool stub_on_right, bool use_color, bool use_normal,
			float u1_scale = 0.0f, float u2_scale = 1.0f) {

		std::vector<Vec3> angular_points;
		map_points_to_angular_points(interpolated_map_points_begin, interpolated_map_points_end, angular_points, get_tower()->get_scale_factor_x(), get_tower()->get_scale_factor_y());

		float amplitude = get_tower()->get_platform_amplitude();
		float cycles = get_tower()->get_platform_cycles();

		float amplitude_incline = get_tower()->get_platform_incline_amplitude();
		float cycles_incline = get_tower()->get_platform_incline_cycles();

		RADIUS_FUNC func;
		if (get_tower()->get_platform_follow_tower_surface_distortion()) {
			func = get_tower()->get_platform_follow_tower_surface_distortion_angle() ? radius_func : radius_func_ignore_angle;
		} else
			func = radius_func_fixed;

		Box2dLoader* shape_loader = get_tower()->get_items_shapes(env);

		PolygonBuiler::create_textured_road(*shape_loader, get_tower()->get_platform_shape_name(), atlas, texture_name, texture_name_side, texture_name_nm, texture_name_side_nm, cache, func, get_tower()->get_platform_depth(),
				get_tower()->get_tower_brick_height(), angular_points, use_color, use_normal, stub_on_left, stub_on_right, door_on_top, amplitude, cycles, amplitude_incline, cycles_incline, u1_scale, u2_scale);
	}

	void create_step_polygon(PolygonCache* cache, const TextureAtlasLoader & atlas, const std::string & texture_name, const std::string & texture_name_side, const std::string & texture_name_nm, const std::string & texture_name_side_nm, FTE_ENV env,
			bool use_color, bool use_normal) {

		std::vector<Vec3> interpolated_map_points;
		interpolate_road(get_col(), get_row(), interpolated_map_points, get_tower()->get_platform_smoothness());

		create_step_polygon_for_points(interpolated_map_points.begin(), interpolated_map_points.end(), cache, atlas, texture_name, texture_name_side, texture_name_nm, texture_name_side_nm, env, stub_on_left, stub_on_right, use_color, use_normal);
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		float cols = get_tower()->get_map_width();
		if (F_EQ(get_col(), 0.0f))
			create_body_col_row(box2d_loader, cols, get_row());
		else if (F_EQ(get_col(), cols - 1.0f))
			create_body_col_row(box2d_loader, -1.0f, get_row());
		create_body_col_row(box2d_loader, get_col(), get_row());
	}

public:
	BaseStepItem(Tower* tower, float col, float row, CONTACT_TYPE contact_type = TYPE_STEP) :
			TowerItem(tower, col, row, contact_type, TYPE_ALL_FILTER, 1.0f) {
		this->stub_on_left = false;
		this->stub_on_right = false;
	}

	~BaseStepItem() {
	}
}
;

} // namespace ft

#endif /* ft_base_step_item_H */
