#include "ft_start.h"
#include "ft_splash_screen.h"
#include "ft_main_screen.h"
#include "ft_box_screen.h"
#include "ft_game_screen.h"
#include "ft_test_screen.h"
#include "ft_test_anim_screen.h"

namespace ft {

fte::FTE_SCREEN ftCreateScreen() {
//	return new TestAnimScreen;
//	return new TestScreen;
	return new BoxScreen;
//	return new SplashScreen;
}

} // namespace fte
