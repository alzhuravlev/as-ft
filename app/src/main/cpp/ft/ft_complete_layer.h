#ifndef ft_complete_layer_H
#define ft_complete_layer_H

#include "engine.h"
#include "command.h"
#include "touch_helper.h"
#include "fte_utils.h"
#include "anim.h"
#include "polygon_builder.h"
#include "ft_button.h"
#include "ft_base_screen.h"
#include "ft_tower_item.h"

using namespace fte;

namespace ft {

class CompleteLayer: public BaseScreen {
private:

	Tower* tower;

	Button continue_play_button;
	Button menu_button;
	Button replay_button;

	Polygon* bg;

protected:

	virtual void do_resize(FTE_ENV env) {

		float alpha = 1.0f;

		float big_w = get_screen_ratio() * 0.4f;
		float big_shift_y = get_screen_ratio() * 0.2f;

		float small_w = get_screen_ratio() * 0.3f;
		float small_shift_x = get_screen_ratio() * 0.2f;
		float small_shift_y = get_screen_ratio() * 0.2f;

		continue_play_button.set_width(big_w);
		continue_play_button.set_height(big_w);
		continue_play_button.set_alpha(alpha);
		continue_play_button.set_translate_xy(0.0f, big_shift_y);
		continue_play_button.update_rect();

		menu_button.set_width(small_w);
		menu_button.set_height(small_w);
		menu_button.set_alpha(alpha);
		menu_button.set_translate_xy(small_shift_x, -small_shift_y);
		menu_button.update_rect();

		replay_button.set_width(small_w);
		replay_button.set_height(small_w);
		replay_button.set_alpha(alpha);
		replay_button.set_translate_xy(-small_shift_x, -small_shift_y);
		replay_button.update_rect();

		Rect bg_rect;
		bg_rect.set(-0.5f * get_screen_width(), -0.5f * get_screen_height(), 0.5f * get_screen_width(), 0.5f * get_screen_height());

		bg->set_bounds(bg_rect);
	}

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader* atlas = tower->get_controls_atlas(env);
		atlas->add_texture_descriptors(command);
	}

	virtual void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader* atlas = tower->get_controls_atlas(env);

		continue_play_button.init(atlas, "continue", "continue_active");
		menu_button.init(atlas, "menu", "menu_active");
		replay_button.init(atlas, "replay", "replay_active");

		bg = PolygonBuiler::create_sprite(*atlas, "bg", true, false);
	}

	virtual void do_init(FTE_ENV env) {
	}

	virtual void do_show_as_layer() {
	}

	virtual void do_update(float delta, RenderCommand* command) {

		const TouchHelper & th = get_touch_helper();
		const Camera & camera = get_ortho_camera();

		float width = get_width();
		float height = get_height();

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, camera.get_view_proj_matrix());
		{
			command->add(bg);
			continue_play_button.render_button(delta, command, th, camera, width, height);
			menu_button.render_button(delta, command, th, camera, width, height);
			replay_button.render_button(delta, command, th, camera, width, height);
		}
		command->end_batch();

		if (continue_play_button.is_button_tapped(th, camera, width, height))
			tower->next_level();
		else if (menu_button.is_button_tapped(th, camera, width, height))
			tower->quit_to_menu();
		else if (replay_button.is_button_tapped(th, camera, width, height)) {
			tower->restart();
		} else if (th.is_tap()) {
			tower->next_level();
		}

		BaseScreen::do_update(delta, command);
	}

	virtual void do_update_once(RenderCommand* command) {
	}

public:

	CompleteLayer(Tower* tower) {
		this->tower = tower;
		this->bg = NULL;
	}

	~CompleteLayer() {
		DELETE(bg);
	}

};

} // namespace ft

#endif /* ft_complete_layer_H */
