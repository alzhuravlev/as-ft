#ifndef ft_base_pipe_item_H
#define ft_base_pipe_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"

namespace ft {

#define PIPE_CAN_CONTACT_WITH 		TYPE_MONSTER | TYPE_MAIN_CHARACTER

class BasePipeItem: public TowerItem {
private:
	float height_scale;
	int part_count;
	float v1_scale;
	float v_step;

protected:

	void create_pipe_body(Box2dLoader & box2d_loader, const std::string & shape_name) {
		b2BodyDef def;
		body = get_tower()->get_world()->CreateBody(&def);
		body->SetUserData(this);

		b2FixtureDef fd;
		fd.friction = 0.4f;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		Box2dItem* item = box2d_loader.find_item(shape_name);
		attach_fixture_to_body(body, &fd, item, 1.0f, height_scale * get_tower()->get_brick_height_scale());

		body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
	}

	void create_pipe_polygons(TextureAtlasLoader* atlas, const std::string & texture_name, const std::string & texture_name_nm, int roundness, float radius, float radius_amplitude, float radius_cycles, bool use_color, std::vector<Polygon*> & polygons) {

		std::vector<Vec3> path;
		float step;
		get_angular_points(path, step);

		BasePipeItem* prev_pipe_item = get_prev_item();
		BasePipeItem* next_pipe_item = get_next_item();

		if (prev_pipe_item) {
			Vec3 v;
			prev_pipe_item->get_last_angular_point(v);
			v.y -= step;
			path.insert(path.begin(), v);
		} else {
			Vec3 v = path.front();
			v.y -= 0.001f;
			path.insert(path.begin(), v);
		}

		if (next_pipe_item) {
			Vec3 v;
			next_pipe_item->get_first_angular_point(v);
			v.y += step;
			path.push_back(v);
		} else {
			Vec3 v = path.back();
			v.y += 0.001f;
			path.push_back(v);
		}

		if (path.size() < 4) {
			LOGE("fte: create_pipe_polygons: unable to create pipe. too low points. expected at least 4, actual %d", path.size());
			return;
		}

		Sprite* pipe_sprite = PolygonBuiler::create_sprite(*atlas, texture_name, use_color, true);
		Sprite* pipe_sprite_nm = PolygonBuiler::create_sprite(*atlas, texture_name_nm, false, false);
		PolygonCache tmp;

		float __v1_scale = v1_scale;

		const std::vector<Vec3>::const_iterator begin = path.begin();
		const std::vector<Vec3>::const_iterator end = path.end();

		std::vector<Vec3>::const_iterator it1 = begin;

		std::vector<Vec3>::const_iterator it2 = begin;
		++it2;

		std::vector<Vec3>::const_iterator it3 = begin;
		++it3;
		++it3;

		std::vector<Vec3>::const_iterator it4 = begin;
		++it4;
		++it4;
		++it4;

		RADIUS_FUNC func;
		if (get_tower()->get_platform_follow_tower_surface_distortion()) {
			func = get_tower()->get_platform_follow_tower_surface_distortion_angle() ? radius_func : radius_func_ignore_angle;
		} else
			func = radius_func_fixed;

		for (; it4 != end; ++it1, ++it2, ++it3, ++it4) {
			std::vector<Vec3> tmp_v;

			tmp_v.push_back(*it1);
			tmp_v.push_back(*it2);
			tmp_v.push_back(*it3);
			tmp_v.push_back(*it4);

			create_tube_around_path(&tmp, pipe_sprite, pipe_sprite_nm, roundness, radius, radius_amplitude, radius_cycles, tmp_v, func, false, __v1_scale, __v1_scale + v_step);

			Polygon* p = tmp.create_composite();
			p->set_preserve_vert_transform(true);
			polygons.push_back(p);

			tmp.reset();

			__v1_scale += v_step;
		}

		DELETE(pipe_sprite);
	}

public:
	BasePipeItem(Tower* tower, float col, float row, int part_count, float height_scale, float v1_scale, float v_step, CONTACT_TYPE contact_type) :
			TowerItem(tower, col, row, contact_type, PIPE_CAN_CONTACT_WITH, 1.0f) {
		this->part_count = part_count;
		this->height_scale = height_scale;
		this->v1_scale = v1_scale;
		this->v_step = v_step;
	}

	~BasePipeItem() {
	}

	void get_first_angular_point(Vec3 & v) {
		v.x = col_to_map_x() * get_tower()->get_scale_factor_x();
		v.y = row_to_map_y() * get_tower()->get_scale_factor_y();
	}

	void get_last_angular_point(Vec3 & v) {
		v.x = col_to_map_x() * get_tower()->get_scale_factor_x();
		v.y = row_to_map_y(get_row() + height_scale) * get_tower()->get_scale_factor_y();
	}

	void get_angular_points(std::vector<Vec3> & points, float & step) {
		Vec3 first_v, last_v;

		get_first_angular_point(first_v);
		get_last_angular_point(last_v);

		const int SMOOTHNESS = part_count;
		step = (last_v.y - first_v.y) / SMOOTHNESS;

		Vec3 v;
		v.x = first_v.x;
		v.y = first_v.y;

		int cnt = SMOOTHNESS + 1;
		for (int i = 0; i != cnt; i++) {
			v.y = first_v.y + step * i;

			if (v.y > last_v.y)
				v.y = last_v.y;

			points.push_back(v);
		}
	}

	virtual BasePipeItem* get_prev_item() {
		return (BasePipeItem*) get_tower()->get_item_at(get_col(), get_row() - height_scale, get_contact_type());
	}

	virtual BasePipeItem* get_next_item() {
		return (BasePipeItem*) get_tower()->get_item_at(get_col(), get_row() + height_scale, get_contact_type());
	}
};

} // namespace ft

#endif /* ft_base_pipe_item_H */
