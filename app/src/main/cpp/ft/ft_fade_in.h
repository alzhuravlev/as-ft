#ifndef ft_fade_in_H
#define ft_fade_in_H

#include "screen_transition.h"
#include "polygon.h"
#include "anim.h"
#include "ring_anim.h"

using namespace fte;

namespace ft {

class FadeIn: public ScreenTransition {
private:

	Polygon* p;

	VertexRingAnimation v_anim;
	ScaleToAnimation s_anim;
	RotateToZAnimation r_anim;
	AnimationBundle ab;

	AccelerateInterpolator accelerate_interpolator;
	DecelerateInterpolator decelerate_interpolator;

protected:

	virtual void do_update(float delta, RenderCommand* command, Camera & camera) {
		ab.update(p, delta);

		command->begin_batch(SHADER_ID_DEFAULT_COLORED, 0, false, false, camera.get_view_proj_matrix());
		{
			command->add(p);
		}
		command->end_batch();

		if (!ab.is_active())
			change_state(SCREEN_TRANS_STATE_DONE);
	}

	virtual void do_restart() {
		float duration = 0.4f;

		float amplitude = 0.00009f;
		int waves_count = 1;
		float velocity_start = 3.0f;
		float velocity_end = 3.0f;

		float scale_start = 1.0f;
		float scale_end = 10000.0f;

		v_anim.set_duration(duration);
		v_anim.set(amplitude, p, velocity_start, velocity_end, waves_count);

		s_anim.set_duration(duration);
		s_anim.set_scale(scale_start, scale_end);
		s_anim.set_interpolator(&accelerate_interpolator);

		r_anim.set_duration(duration);
		r_anim.set_rotate(0., M_2xPI);
		r_anim.set_interpolator(&accelerate_interpolator);

		ab.start();
	}

public:

	FadeIn() :
			decelerate_interpolator(2.0f), accelerate_interpolator(2.0f) {

		ab.add(&v_anim);
		ab.add(&s_anim);
		ab.add(&r_anim);

		int point_count = 128;
		float inner_radius = 0.0001f;
		float outer_radius = 10.0f;
		float aa = 0.00001f;
		uint32_t inner_color = 0xff000000;
		uint32_t mid_color = 0xff000000;
		uint32_t outer_color = 0xff000000;
		change_alpha(inner_color, 0.0f);

		p = PolygonBuiler::create_ring_sprite_antialiasing(point_count, inner_radius, outer_radius, inner_color, mid_color, outer_color, aa);
	}

	~FadeIn() {
		DELETE(p);
	}
};

} // namespace ft

#endif /* ft_fade_in_H */
