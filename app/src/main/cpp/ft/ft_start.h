#ifndef ft_start_H
#define ft_start_H

#include "engine.h"

namespace ft {

fte::FTE_SCREEN ftCreateScreen();

} // namespace ft

#endif /* ft_start_H */
