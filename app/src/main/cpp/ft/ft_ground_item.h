#ifndef ft_ground_item_H
#define ft_ground_item_H

#include "ft_tower_item.h"

namespace ft {

#define GROUND_CAN_CONTACT_WITH 		TYPE_MAIN_CHARACTER | TYPE_MONSTERS_FILTER

class GroundItem: public TowerItem {
private:

protected:
	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
	}

	virtual void do_release_polygons() {
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		b2BodyDef def;
		body = get_tower()->get_world()->CreateBody(&def);

		b2EdgeShape shape;
		shape.Set(b2Vec2(-100.0f, -1.0f), b2Vec2(4000.0f, -1.0f));

		b2FixtureDef fd;
		fd.shape = &shape;
		fd.filter.categoryBits = TYPE_GROUND;
		fd.filter.maskBits = GROUND_CAN_CONTACT_WITH;

		body->CreateFixture(&fd);
	}

	virtual void do_reset_to_default() {
	}

	virtual void do_change_state(int state) {
	}

	virtual void do_update(float delta, const Camera & camera) {
	}

	virtual void do_render(float delta, RenderCommand* command) {
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
	}

public:
	GroundItem(Tower* tower) :
			TowerItem(tower, 0.0f, 0.0f, TYPE_GROUND, GROUND_CAN_CONTACT_WITH, 1.0f) {
	}

	~GroundItem() {
	}

	virtual bool is_need_to_update() {
		return false;
	}
};

} // namespace ft

#endif /* ft_ground_item_H */
