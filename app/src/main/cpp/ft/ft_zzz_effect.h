#ifndef ft_zzz_effect_H
#define ft_zzz_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class ZzzEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleRotationResolverStrategy_FaceToCamera particleRotationResolverStrategy_FaceToCamera;
	ParticleScaleResolverStrategy_PulseWithInterpolator<AccelerateDecelerateInterpolator> particleScaleResolverStrategy_PulseWithInterpolator;
	ParticleStartingColorResolverStrategy_Simple particleStartingColorResolverStrategy_Simple;
	ParticleColorResolverStrategy_DistanceFromStart particleColorResolverStrategy_DistanceFromStart;
	ParticleAlphaResolverStrategy_AppearDisapearAndPulse particleAlphaResolverStrategy_AppearDisapearAndPulse;
	ParticleCullingResolverStrategy_Tower particleCullingResolverStrategy_Tower;
	ParticleStartingLinearVelocityStrategy_AroundVector particleStartingLinearVelocityStrategy_AroundVector;
	ParticleLinearVelocityResolverStrategy_Simple particleLinearVelocityResolverStrategy_Simple;

	TextureAtlasLoader* atlas;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 0;
		config.max_count = 10;

		config.min_delay = 0.0f;
		config.max_delay = 0.0f;

		config.min_life_time = 0.4f;
		config.max_life_time = 3.2f;

		particleCullingResolverStrategy_Tower.set_radius(BASE_RADIUS);

		particleStartingLinearVelocityStrategy_AroundVector.set_epsilon_angle(M_PI_32);
		particleStartingLinearVelocityStrategy_AroundVector.set_scalar(0.7f, 0.9f);
		particleStartingLinearVelocityStrategy_AroundVector.set_vector(Vec3(0.0f, 1.0f, 0.0f));

		particleLinearVelocityResolverStrategy_Simple.set_damping(1.0f);

		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleStartingColorResolverStrategy_Simple);
		pe.add_initial_strategy(&particleStartingLinearVelocityStrategy_AroundVector);

//		pe.add_strategy(&particleCullingResolverStrategy_Tower);
		pe.add_strategy(&particleScaleResolverStrategy_PulseWithInterpolator);
		pe.add_strategy(&particleAlphaResolverStrategy_AppearDisapearAndPulse);
		pe.add_strategy(&particleRotationResolverStrategy_FaceToCamera);
		pe.add_strategy(&particleLinearVelocityResolverStrategy_Simple);
//		pe.add_strategy(&particleColorResolverStrategy_DistanceFromStart);

		pe.init_with_sprite(config, *atlas, "bubble");

		emitters.push_back(&pe);
	}

public:

	void init(TextureAtlasLoader* atlas, FTE_ENV env) {
		this->atlas = atlas;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleStartingPositionStrategy_Sphere.set_radius(scale * 0.0f);
		particleScaleResolverStrategy_PulseWithInterpolator.set_scale(scale * 0.1f, scale * 0.4f);
		particleScaleResolverStrategy_PulseWithInterpolator.set_pulse(0.01f, 0.7f);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
//		particleColorResolverStrategy_DistanceFromStart.set_start_position(centroid);
	}

	void set_params_color(uint32_t color) {
		particleStartingColorResolverStrategy_Simple.set_color(color);
	}

	void set_params_count(int min, int max) {
		pe.set_count(min, max);
	}

	void set_params_vel(float min, float max) {
		particleStartingLinearVelocityStrategy_AroundVector.set_scalar(min, max);
	}

	void set_params_epsilon_angle(float a) {
		particleStartingLinearVelocityStrategy_AroundVector.set_epsilon_angle(a);
	}

	void set_params_vector(const Vec3 & v) {
		particleStartingLinearVelocityStrategy_AroundVector.set_vector(v);
	}
};

} // namespace ft

#endif /* ft_zzz_effect_H */
