#ifndef ft_eye_item_H
#define ft_eye_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils.h"
#include "fte_utils_3d.h"
#include "spine_renderer.h"
#include "ring_anim.h"
#include "ft_base_monster_item.h"
#include "ft_eye_spine_animated_character.h"

#include <math.h>

namespace ft {

class EyeItem: public BaseMonsterItem {
private:
	b2Vec2 vel;

	b2PrismaticJoint* joint;

protected:

	virtual void do_release_polygons() {
		BaseMonsterItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseMonsterItem::do_create_polygons(cache_lighting, env);
	}

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new EyeSpineAnimatedCharacter(get_aggressive_level());
	}

	virtual void do_reset_to_default() {
		BaseMonsterItem::do_reset_to_default();
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BaseMonsterItem::do_create_body(box2d_loader);

		create_monster_body(box2d_loader, "eye", 0.0f, 2.0f, 1.0f, 1.0f);

		b2PrismaticJointDef pjd;

		// Bouncy limit
		b2Vec2 axis(vel);
		axis.Normalize();
		pjd.Initialize(get_tower()->get_ground_item()->get_body(), body, b2Vec2(col_to_map_x(), row_to_map_y()), axis);

		pjd.enableLimit = false;
		pjd.enableMotor = false;

		joint = (b2PrismaticJoint*) get_tower()->get_world()->CreateJoint(&pjd);
	}

	virtual void do_change_state(int state) {
		BaseMonsterItem::do_change_state(state);

		switch (state) {
		case MONSTER_STATE_SLEEPING:
			break;

		case MONSTER_STATE_WAKING:
			break;

		case MONSTER_STATE_ACTIVE:
			body->SetLinearVelocity(vel);
			break;

		case MONSTER_STATE_PUNCH:
			break;

		case MONSTER_STATE_DYING:
			break;

		case MONSTER_STATE_DIED:
			break;

		case MONSTER_STATE_INACTIVE:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseMonsterItem::do_update(delta, camera);

		float x, y, z, angle;

		get_world_position_from_map(x, y, z, angle);

		switch (get_state()) {
		case MONSTER_STATE_SLEEPING:
			break;

		case MONSTER_STATE_WAKING:
			break;

		case MONSTER_STATE_ACTIVE:
			break;

		case MONSTER_STATE_PUNCH:
			break;

		case MONSTER_STATE_DYING:
			break;

		case MONSTER_STATE_DIED:
			break;
		}
	}

//	virtual void do_render_once(RenderCommand* command) {
//		render_monster_once(START_EYE_BATCH_ID, command);
//	}

//	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
//		render_monster(START_EYE_BATCH_ID, delta, command, camera);
//	}

public:
	EyeItem(Tower* tower, float col, float row, int vel_x, int vel_y, int key_color = 0, int aggressive_level = MONSTER_AGGRESSIVE_LEVEL_DEFAULT) :
			BaseMonsterItem(tower, col, row, key_color, tower->get_eye_scale(), aggressive_level) {

		vel.x = vel_x;
		vel.y = vel_y;
		vel.Normalize();

		switch (aggressive_level) {
		case MONSTER_AGGRESSIVE_LEVEL_DEFAULT:
		case MONSTER_AGGRESSIVE_LEVEL_LOW:
			vel *= 2.0f;
			break;
		case MONSTER_AGGRESSIVE_LEVEL_MED:
			vel *= 3.0f;
			break;
		case MONSTER_AGGRESSIVE_LEVEL_HIGH:
			vel *= 5.0f;
			break;
		}

		joint = NULL;
	}

	~EyeItem() {
	}

	virtual void post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		if (my_fixture->GetBody() != this->body)
			return;

		b2WorldManifold mf;
		contact->GetWorldManifold(&mf);
		float nx, ny;
		nx = f * mf.normal.x;
		ny = f * mf.normal.y;

		float vx = nx < 0.0f ? vel.x : -vel.x;
		float vy = ny < 0.0f ? vel.y : -vel.y;

		this->body->SetLinearVelocity(b2Vec2(vx, vy));
	}
};

} // namespace ft

#endif /* ft_eye_item_H */
