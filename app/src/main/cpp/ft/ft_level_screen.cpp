#include "ft_level_screen.h"
#include "ft_box_screen.h"
#include "ft_game_screen.h"

namespace ft {

void LevelScreen::do_open_back() {
	get_updater()->set_pending_screen(new BoxScreen);
}

void LevelScreen::do_open_game() {
	get_updater()->set_pending_screen(new GameScreen(box_item, tapped_button_index));
}

} // namespace ft
