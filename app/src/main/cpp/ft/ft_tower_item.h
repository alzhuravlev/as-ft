#ifndef ft_tower_item_H
#define ft_tower_item_H

#include "command.h"
#include "camera.h"
#include "polygon_cache.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "obj_model_loader.h"
#include "spine_renderer.h"
#include "box2d_utils.h"
#include "fte_math.h"
#include "map_utils.h"
#include "ft_tower_item_consts.h"
#include "ft_base_animated_character.h"

#include "Box2D/Box2D.h"

namespace ft {

class Tower;
class TowerItem;

//#define CLAMP_ANGLE(a) while(a > M_PI) a -= M_2xPI; while(a < -M_PI) a += M_2xPI

class TowerItem: public Statable {
private:
	float scale_factor;
	float onscreen_dim;
	float onscreen_dim_2;

	float distance_from_base_radius;

	float col, row;
	Tower* tower;

	BaseAnimatedCharacter* animated_character;

protected:

	b2Body* body;
	bool active;
	bool body_active;

	bool visible;
	bool in_frustrum;

	TowerItem* parent;
	std::vector<TowerItem*> children;

	float distance_from_base_radius_saved;
	b2Vec2 map_position_saved;
	Vec3 world_position_saved;
	float world_position_angle_saved;

	// contacts
	CONTACT_TYPE contact_type;
	CONTACT_TYPE contact_mask;

	virtual BaseAnimatedCharacter* create_animated_character();

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env);

	virtual void do_release_polygons();

	virtual void do_create_child_items(std::vector<TowerItem*> & items);

	virtual void do_create_body(Box2dLoader & box2d_loader);

	virtual void do_reset_to_default();

	virtual void do_update(float delta, const Camera & camera);

	virtual void do_render_once(RenderCommand* command);

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera);

	virtual void do_render_standalone(float delta, RenderCommand* command, const Camera & camera);

	virtual void do_render_effects(float delta, RenderCommand* command);

	virtual void do_render_tails(float delta, RenderCommand* command);

	virtual void do_change_state(int state);

	SpineRenderer* create_spine_text(const std::string & text, uint32_t color, FTE_ENV env);

	void correct_body_pos(b2Body* body_mirror = NULL) {

		b2Vec2 pos = body->GetPosition();
		bool b = false;

		if (pos.x > tower->get_map_width()) {
			b = true;
			pos.x -= tower->get_map_width();
		}

		if (pos.x < 0.0f) {
			b = true;
			pos.x += tower->get_map_width();
		}

		if (b) {
			body->SetTransform(pos, body->GetAngle());
			pos = body->GetPosition();
		}

		if (body_mirror)
			if (pos.x < 1.0f) {
				body_mirror->SetTransform(b2Vec2(tower->get_map_width() + pos.x, pos.y), body->GetAngle());
				body_mirror->SetActive(true);
				body_mirror->SetLinearVelocity(body->GetLinearVelocity());
				body_mirror->SetAngularVelocity(body->GetAngularVelocity());
			} else if (pos.x > (tower->get_map_width() - 1.0f)) {
				body_mirror->SetTransform(b2Vec2(pos.x - tower->get_map_width(), pos.y), body->GetAngle());
				body_mirror->SetActive(true);
				body_mirror->SetLinearVelocity(body->GetLinearVelocity());
				body_mirror->SetAngularVelocity(body->GetAngularVelocity());
			} else
				body_mirror->SetActive(false);
	}
	/*
	 void update_polygon(float distance_from_base_radius, float map_x, float map_y, Polygon* polygon, float angle_y, float angle_z) {
	 float x, y, z, angle;
	 get_world_position_from_map(distance_from_base_radius, map_x, map_y, x, y, z, angle);

	 polygon->set_translate(x, y, z);
	 polygon->set_angle_y(-angle + angle_y);
	 polygon->set_angle_z(angle_z);
	 }

	 inline void update_polygon(float map_x, float map_y, Polygon* polygon) {
	 update_polygon(this->distance_from_base_radius, map_x, map_y, polygon, 0.0f, 0.0f);
	 }

	 inline void update_polygon(float distance_from_base_radius, float map_x, float map_y, Polygon* polygon) {
	 update_polygon(distance_from_base_radius, map_x, map_y, polygon, 0.0f, 0.0f);
	 }

	 inline bool update_polygon(float map_x, float map_y, Polygon* polygon, const Camera* camera) {
	 return update_polygon(map_x, map_y, polygon, camera, this->distance_from_base_radius, 0.0f, 0.0f);
	 }

	 inline bool update_polygon(float map_x, float map_y, Polygon* polygon, const Camera* camera, float distance_from_base_radius, float angle_y, float angle_z) {
	 float x, y, z, angle;
	 get_world_position_from_map(distance_from_base_radius, map_x, map_y, x, y, z, angle);

	 if (camera) {
	 in_frustrum = camera->sphere_in_frustrum(x, y + onscreen_dim_2, z, onscreen_dim_2);
	 if (!in_frustrum)
	 return false;
	 }

	 polygon->set_translate(x, y, z);
	 polygon->set_angle_y(-angle + angle_y);
	 polygon->set_angle_z(angle_z);

	 return true;
	 }

	 inline bool update_polygon_from_body(b2Body* body, Polygon* polygon, const Camera* camera, float distance_from_base_radius, float angle_y) {
	 const b2Vec2 & pos = body->GetPosition();
	 float angle_z = body->GetAngle();

	 return update_polygon(pos.x, pos.y, polygon, camera, distance_from_base_radius, angle_y, angle_z);
	 }

	 inline bool update_polygon_from_body(Polygon* polygon, const Camera* camera, float distance_from_base_radius, float angle_y) {
	 const b2Vec2 & pos = get_map_position();
	 float angle_z = body->GetAngle();

	 return update_polygon(pos.x, pos.y, polygon, camera, distance_from_base_radius, angle_y, angle_z);
	 }

	 inline void update_spine_from_body(float delta, SpineRenderer* spine, const Camera* camera, float angle_x, float angle_y, bool sync_angle_x_to_tower_surface) {
	 float x, y, z, angle;
	 update_spine_from_body(delta, spine, camera, angle_x, angle_y, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline void update_spine_from_body(float delta, SpineRenderer* spine, const Camera* camera, bool sync_angle_x_to_tower_surface) {
	 float x, y, z, angle;
	 update_spine_from_body(delta, spine, camera, 0.0f, 0.0f, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline bool update_spine_from_body(float delta, SpineRenderer* spine, const Camera* camera, float angle_x, float angle_y, bool sync_angle_x_to_tower_surface, float & x, float & y, float & z, float & angle) {
	 const b2Vec2 & pos = get_map_position();
	 float angle_z = body->GetAngle();

	 return update_spine(pos.x, pos.y, delta, spine, camera, this->distance_from_base_radius, angle_x, angle_y, angle_z, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline void update_spine(float map_x, float map_y, float delta, SpineRenderer* spine, const Camera* camera, float angle_x, float angle_y, float angle_z, bool sync_angle_x_to_tower_surface) {
	 float x, y, z, angle;
	 update_spine(map_x, map_y, delta, spine, camera, this->distance_from_base_radius, angle_x, angle_y, angle_z, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline bool update_spine(float map_x, float map_y, float delta, SpineRenderer* spine, const Camera* camera, bool sync_angle_x_to_tower_surface, float & x, float & y, float & z, float & angle) {
	 return update_spine(map_x, map_y, delta, spine, camera, distance_from_base_radius, 0.0f, 0.0f, 0.0f, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline bool update_spine(float map_x, float map_y, float delta, SpineRenderer* spine, const Camera* camera, float distance_from_base_radius, float angle_x, float angle_y, float angle_z, bool sync_angle_x_to_tower_surface) {
	 float x, y, z, angle;
	 return update_spine(map_x, map_y, delta, spine, camera, distance_from_base_radius, angle_x, angle_y, angle_z, sync_angle_x_to_tower_surface, x, y, z, angle);
	 }

	 inline bool update_spine(float map_x, float map_y, float delta, SpineRenderer* spine, const Camera* camera, float distance_from_base_radius, float angle_x, float angle_y, float angle_z, bool sync_angle_x_to_tower_surface, float & x, float & y,
	 float & z, float & angle) {

	 get_world_position_from_map(distance_from_base_radius, map_x, map_y, x, y, z, angle);
	 if (camera) {
	 in_frustrum = camera->sphere_in_frustrum(x, y + onscreen_dim_2, z, onscreen_dim_2);
	 if (!in_frustrum) {
	 spine->update_animation(delta);
	 return false;
	 }
	 }

	 Vec3 translate(x, y, z);
	 Vec3 rotate;
	 Vec3 scale;

	 // item is now faced left or right
	 float face_to_angle = angle_y;

	 if (sync_angle_x_to_tower_surface && get_tower()->get_platform_follow_tower_surface_distortion()) {
	 float tan = radius_derivative_func_y(y, angle);
	 angle_x = -atanf(tan);
	 }

	 rotate.x = angle_x;
	 rotate.y = -angle + face_to_angle;
	 rotate.z = cos_fast(face_to_angle) > 0.0f ? angle_z : -angle_z;

	 scale.x = scale.y = scale.z = onscreen_dim;

	 spine->update(delta, translate, rotate, scale);

	 return true;
	 }
	 */
	void reset_contact_mask(int mask) {
		for (b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext()) {
			b2Filter fd;
			fd.categoryBits = contact_type;
			fd.maskBits = mask;
			f->SetFilterData(fd);
		}
	}

public:

	TowerItem(Tower* tower, float col, float row, CONTACT_TYPE contact_type, CONTACT_TYPE contact_mask, float scale_factor);

	virtual ~TowerItem();

	void create_polygons(PolygonCache* cache_lighting, FTE_ENV env);

	void release_polygons();

	void create_child_items(std::vector<TowerItem*> & items);

	void create_body(Box2dLoader & box2d_loader);

	void reset_to_default();

	void update(float delta, const Camera & camera);

	inline void render_once(RenderCommand* command) {
		do_render_once(command);
	}

	inline void render(float delta, RenderCommand* command, const Camera & camera) {
		if (active && in_frustrum && visible) {
//			int vc1 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

			if (animated_character)
				animated_character->render(this, delta, command, camera);

			do_render(delta, command, camera);
//			int vc2 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;
//			int vc = vc2 - vc1;
//			if (vc)
//				LOGD("do_render %d    vc = %d", get_contact_type(), vc);
		}
	}

	inline void render_standalone(float delta, RenderCommand* command, const Camera & camera) {
		if (active && in_frustrum && visible) {
			do_render_standalone(delta, command, camera);
		}
	}

	inline void render_effects(float delta, RenderCommand* command, const Camera & camera) {
		if (active && visible) {
//			int vc1 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

			if (animated_character)
				animated_character->render_effects(this, delta, command, camera);

			do_render_effects(delta, command);
//			int vc2 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;
//			int vc = vc2 - vc1;
//			if (vc)
//				LOGD("render_effects %d    vc = %d", get_contact_type(), vc);
		}
	}

	inline void render_tails(float delta, RenderCommand* command, const Camera & camera) {
		if (active && visible) {
//			int vc1 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

			if (animated_character)
				animated_character->render_tails(this, delta, command, camera);

			do_render_tails(delta, command);
//			int vc2 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;
//			int vc = vc2 - vc1;
//			if (vc)
//				LOGD("render_tails %d    vc = %d", get_contact_type(), vc);
		}
	}

	virtual bool is_need_to_update();

	virtual bool is_need_to_render_toon();

	virtual bool is_need_to_render_no_toon();

	virtual bool is_need_to_render_standalone();

	virtual bool is_need_to_render_effects();

	virtual bool is_need_to_render_tails();

	/**
	 * Cached items are not moved nor animated.
	 * Typically they are put in global PolygonCache at construction time.
	 * It's position calculates once, do_update and do_render are never called for such items.
	 */

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact);
	virtual void end_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact);

	virtual void pre_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f);
	virtual void post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f);

	inline Tower* get_tower() {
		return tower;
	}

	inline float get_col() {
		return col;
	}

	inline float get_row() {
		return row;
	}

	inline float col_to_map_x(float col) {
		return col + 0.5f;
	}

	inline float col_to_map_x() {
		return col + 0.5f;
	}

	inline float row_to_map_y(float row) {
		return row * get_tower()->get_brick_height_scale();
	}

	inline float row_to_map_y() {
		return row * get_tower()->get_brick_height_scale();
	}

	void update_in_frustrum(const Camera & camera) {
		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);
		in_frustrum = camera.sphere_in_frustrum(x, y + onscreen_dim_2, z, onscreen_dim_2);
	}

	// item's position

	virtual const b2Vec2 & get_map_position();

	void get_world_position_from_map(float &x, float &y, float &z, float &angle) {

		const b2Vec2 & map_position = get_map_position();

		if (F_EQ(distance_from_base_radius, distance_from_base_radius_saved) && F_EQ(map_position_saved.x, map_position.x) && F_EQ(map_position_saved.y, map_position.y)) {
			x = world_position_saved.x;
			y = world_position_saved.y;
			z = world_position_saved.z;
			angle = world_position_angle_saved;
			return;
		}

		distance_from_base_radius_saved = distance_from_base_radius;
		map_position_saved.x = map_position.x;
		map_position_saved.y = map_position.y;

		Vec3 map_point;

		map_point.x = map_position.x;
		map_point.y = map_position.y;
		map_point.z = 0.0f;

		Vec3 angular_point;
		Vec3 world_point;

		bool use_radius_func = get_tower()->get_platform_follow_tower_surface_distortion();
		bool use_angle_in_radius_func = get_tower()->get_platform_follow_tower_surface_distortion_angle();

		RADIUS_FUNC func;
		if (use_radius_func) {
			func = use_angle_in_radius_func ? radius_func : radius_func_ignore_angle;
		} else
			func = radius_func_fixed;

		map_point_to_angular_point(map_point, angular_point, get_tower()->get_scale_factor_x(), get_tower()->get_scale_factor_y());
		angular_point_to_world_point(angular_point, world_point, func, distance_from_base_radius);

		world_position_saved.x = x = world_point.x;
		world_position_saved.y = y = world_point.y;
		world_position_saved.z = z = world_point.z;

		world_position_angle_saved = angle = angular_point.x;
	}

	void get_world_angle_from_map(const float & map_x, float &angle) {
		angle = map_x * tower->get_scale_factor_x();
	}

	void get_world_angle_and_y_from_map(const float & map_x, const float & map_y, float & y, float &angle) {
		y = map_y * tower->get_scale_factor_y();
		angle = map_x * tower->get_scale_factor_x();
	}

	void get_world_y_from_map(const float & map_y, float & y) {
		y = map_y * tower->get_scale_factor_y();
	}

	inline void get_world_angle(float &angle) {
		const b2Vec2 & pos = get_map_position();
		get_world_angle_from_map(pos.x, angle);
	}

	inline void get_world_angle_and_y(float &y, float &angle) {
		const b2Vec2 & pos = get_map_position();
		get_world_angle_and_y_from_map(pos.x, pos.y, y, angle);
	}

	inline void get_world_y(float &y) {
		const b2Vec2 & pos = get_map_position();
		get_world_y_from_map(pos.y, y);
	}

	float get_angle_x_relative_to_tower_surface();

	virtual float get_angle_x();

	virtual float get_angle_y();

	virtual float get_angle_z();

	inline float get_distance_from_base_radius() const {
		return distance_from_base_radius;
	}

	inline void set_distance_from_base_radius(float v) {
		distance_from_base_radius = v;
	}

	inline void add_distance_from_base_radius(float v) {
		distance_from_base_radius += v;
	}

	inline float get_scale_for_particles() const {
		return scale_factor * tower->get_scale_factor_x() * BASE_RADIUS;
	}

	inline float get_scale_factor() const {
		return scale_factor;
	}

	inline float get_onscreen_dim() const {
		return onscreen_dim;
	}

	inline float get_onscreen_dim_2() const {
		return onscreen_dim_2;
	}

	inline CONTACT_TYPE get_contact_type() const {
		return contact_type;
	}

	inline bool is_active() const {
		return active;
	}

	inline bool is_in_frustrum() const {
		return in_frustrum;
	}

	inline bool is_visible() const {
		return visible;
	}

	inline bool is_platform() const {
		return (contact_type & TYPE_PLATFORMS_FILTER);
	}

	inline bool is_monster() const {
		return (contact_type & TYPE_MONSTERS_FILTER);
	}

	inline TowerItem* get_parent() const {
		return parent;
	}

	inline b2Body* get_body() const {
		return body;
	}

	inline void play_animation(const std::string & name, bool loop) const {
		if (animated_character)
			animated_character->play(name, loop);
	}

	inline void play_next_animation(const std::string & name, bool loop) const {
		if (animated_character)
			animated_character->play_next(name, loop);
	}

	inline void stop_animation(bool immediate = false) const {
		if (animated_character)
			animated_character->stop(immediate);
	}

	inline bool is_animation_playing() const {
		if (animated_character)
			return animated_character->is_animation_playing();
		return false;
	}

	inline TowerItem* get_child(int index) const {
		if (index < children.size())
			return children[index];
		return NULL;
	}

	inline int get_children_count() const {
		return children.size();
	}

// item's specific methods

	virtual void monster_die();

	virtual void monster_wakeup();

	virtual void monster_punch();

	virtual bool is_monster_active();

	virtual void open_door();

	virtual void close_door();

	virtual void button_contact_begin();

	virtual void button_contact_end();

	virtual void toggle_button();

	virtual bool can_walk_through_door();

	virtual int get_platform_temporary_group();

	virtual void platform_contact_from_top_begin();

	virtual void platform_contact_from_top_end();

	virtual void toggle_lift();

	virtual void control_inactive();

	virtual void control_left_pressed();

	virtual void control_upper_left_pressed();

	virtual void control_right_pressed();

	virtual void control_upper_right_pressed();

	virtual void control_down_pressed();

	virtual void control_tap();

	virtual int get_control_type();

	virtual void tower_finished();
};

} // namespace ft

#endif /* ft_tower_item_H */
