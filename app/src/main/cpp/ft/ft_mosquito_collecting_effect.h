#ifndef ft_mosquito_collecting_effect_H
#define ft_mosquito_collecting_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower_item_consts.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class MosquitoCollectingEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleStartingColorResolverStrategy_Randomize particleColorResolverStrategy_Randomize;
	ParticleScaleResolverStrategy_PulseWithInterpolator<AccelerateDecelerateInterpolator> particleScaleResolverStrategy_PulseWithInterpolator;
	ParticleStartingRotationStrategy_RandomizeAngularVelocity particleStartingRotationStrategy_RandomizeAngularVelocity;
	ParticleRotationResolverStrategy_FaceToCameraAndAngularVelocity particleRotationResolverStrategy_FaceToCamera;
	ParticleAlphaResolverStrategy_AppearDisapearAndPulse particleAlphaResolverStrategy_AppearDisapearAndPulse;

	TextureAtlasLoader* atlas;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 1;
		config.max_count = 2;

		config.min_delay = 0.0f;
		config.max_delay = 0.5f;

		config.min_life_time = 1.0f;
		config.max_life_time = 1.0f;

		particleStartingRotationStrategy_RandomizeAngularVelocity.set_angular_velocity(-M_PI, M_PI);

		pe.add_initial_strategy(&particleColorResolverStrategy_Randomize);
		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleStartingRotationStrategy_RandomizeAngularVelocity);

		pe.add_strategy(&particleRotationResolverStrategy_FaceToCamera);
		pe.add_strategy(&particleScaleResolverStrategy_PulseWithInterpolator);
		pe.add_strategy(&particleAlphaResolverStrategy_AppearDisapearAndPulse);

		pe.init_with_sprite(config, *atlas, "triangle");

		emitters.push_back(&pe);
	}

public:

	void init(TextureAtlasLoader* atlas, FTE_ENV env) {
		this->atlas = atlas;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleStartingPositionStrategy_Sphere.set_radius(scale * 0.2f);
		particleScaleResolverStrategy_PulseWithInterpolator.set_scale(scale * 0.5f, scale * 2.5f);
		particleScaleResolverStrategy_PulseWithInterpolator.set_pulse(0.2f, 0.7f);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
	}

	void set_params_color(uint32_t color) {
		particleColorResolverStrategy_Randomize.set_color(color, color, false);
	}
};

} // namespace ft

#endif /* ft_mosquito_collecting_effect_H */
