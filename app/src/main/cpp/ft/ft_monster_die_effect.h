#ifndef ft_monster_die_effect_H
#define ft_monster_die_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class MonsterDieEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleStartingRotationStrategy_RandomizeAngularVelocity particleStartingRotationStrategy_RandomizeAngularVelocity;
	ParticleStartingLinearVelocityStrategy_AroundVector particleStartingLinearVelocityStrategy_AroundVector;
	ParticleStartingColorResolverStrategy_Simple particleStartingColorResolverStrategy_Simple;
	ParticleLinearVelocityResolverStrategy_WithGravity particleLinearVelocityResolverStrategy_WithGravity;
	ParticleRotationResolverStrategy_FaceToLinearVelocity particleRotationResolverStrategy_FaceToLinearVelocity;
	ParticleScaleXYResolverStrategy_WithInterpolator<AccelerateDecelerateInterpolator> particleScaleXYResolverStrategy_WithInterpolator;
	ParticleAlphaResolverStrategy_AppearDisapearAndPulse particleAlphaResolverStrategy_AppearDisapearAndPulse;

	TextureAtlasLoader* atlas;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 0;
		config.max_count = 70;

		config.min_delay = 0.0f;
		config.max_delay = 0.0f;

		config.min_life_time = 0.7f;
		config.max_life_time = 1.4f;

		particleStartingRotationStrategy_RandomizeAngularVelocity.set_angular_velocity(-M_PI, M_PI);

		particleStartingLinearVelocityStrategy_AroundVector.set_epsilon_angle(M_PI_2);
		particleStartingLinearVelocityStrategy_AroundVector.set_scalar(0.5f, 4.0f);
		particleStartingLinearVelocityStrategy_AroundVector.set_vector(Vec3(0.0f, 1.0f, 0.0f));

		particleLinearVelocityResolverStrategy_WithGravity.set_damping(0.3f);
		particleLinearVelocityResolverStrategy_WithGravity.set_gravity(Vec3(0.0f, -4.0f, 0.0f));

		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleStartingRotationStrategy_RandomizeAngularVelocity);
		pe.add_initial_strategy(&particleStartingLinearVelocityStrategy_AroundVector);
		pe.add_initial_strategy(&particleStartingColorResolverStrategy_Simple);

		particleAlphaResolverStrategy_AppearDisapearAndPulse.set_time(0.4f, 0.2f);

		pe.add_strategy(&particleLinearVelocityResolverStrategy_WithGravity);
		pe.add_strategy(&particleScaleXYResolverStrategy_WithInterpolator);
		pe.add_strategy(&particleAlphaResolverStrategy_AppearDisapearAndPulse);
		pe.add_strategy(&particleRotationResolverStrategy_FaceToLinearVelocity);

		pe.init_with_sprite(config, *atlas, "ray", ORIGIN_BOTTOM_CENTER);

		emitters.push_back(&pe);
	}

public:

	void init(TextureAtlasLoader* atlas, FTE_ENV env) {
		this->atlas = atlas;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleScaleXYResolverStrategy_WithInterpolator.set_scale(scale * 0.5f, scale * 0.1f, scale * 0.8f, scale * 0.1f);
		particleStartingPositionStrategy_Sphere.set_radius(scale * 0.0f);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
	}

	void set_params_color(uint32_t color) {
		particleStartingColorResolverStrategy_Simple.set_color(color);
	}
};

} // namespace ft

#endif /* ft_monster_die_effect_H */
