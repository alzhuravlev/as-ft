#ifndef ft_game_screen_H
#define ft_game_screen_H

#include "engine.h"
#include "texture_atlas_loader.h"
#include "ft_tower.h"
#include "ft_tower_loader.h"
#include "anim.h"
#include "touch_helper.h"
#include "ft_controls_layer.h"
#include "ft_pause_layer.h"
#include "ft_complete_layer.h"
#include "ft_ads_layer.h"
#include "fte_math.h"
#include "ft_box_loader.h"

#include "ft_base_screen.h"

#include <string>

using namespace fte;

namespace ft {

const int PAUSE_LAYER_ID = 1;
const int COMPLETE_LAYER_ID = 2;
const int ADS_LAYER_ID = 3;

class GameScreen: public BaseScreen {
private:

	PerspectiveCamera camera;
	Tower tower;

	BoxItem box_item;
	int level_index;

	float last_y;
	float last_angle;

	void do_resize(FTE_ENV env) {
		float width = get_width();
		float height = get_height();

		camera.update(width, height, M_PI * 0.2f);
		tower.resize(width, height);
	}

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		tower.init_renderer(command, env);
	}

	void do_init(FTE_ENV env) {
		float width = get_width();
		float height = get_height();

		tower.init(width, height, env);
	}

	void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		std::string file_name = box_item.format_tower_file_name(level_index);
		tower.load_tower(TOWER_DATA_QUALITY_LEVEL_MED, file_name, env);

		float width = get_width();
		float height = get_height();

		tower.init_once(width, height, get_updater()->get_audio(), env);
		tower.reset_items_to_default();
		tower.resize(width, height);
		tower.update(0.0f, camera);

		fade_in();

		tower.change_state(TOWER_STATE_IN_GAME_READY_GO);
	}

	void do_release(FTE_ENV env) {
		BaseScreen::do_release(env);
		tower.release();
	}

	void do_update_once(RenderCommand* command) {
		tower.render_once(command);
	}

	void do_update(float delta, RenderCommand* command) {

		tower.update_camera(delta, camera);

		switch (tower.get_state()) {

		case TOWER_STATE_IN_GAME_READY_GO:
		case TOWER_STATE_IN_GAME:
		case TOWER_STATE_FINISHED:
		case TOWER_STATE_COMPLETE:
			tower.update(delta, camera);
			break;

		case TOWER_STATE_NEXT_LEVEL_REQUEST:
			fade_out();
			tower.change_state(TOWER_STATE_NEXT_LEVEL_PRECESS);
			break;

		case TOWER_STATE_COMPLETE_REQUEST:
			tower.save_to_preferences(get_pref());
			show_layer(COMPLETE_LAYER_ID);
			tower.change_state(TOWER_STATE_COMPLETE);
			break;

		case TOWER_STATE_PAUSE_REQUEST:
			hide_all_layers();
			show_layer(PAUSE_LAYER_ID);
			tower.change_state(TOWER_STATE_PAUSE);
			break;

		case TOWER_STATE_PAUSE:
			break;

		case TOWER_STATE_UNPAUSE_REQUEST:
			hide_layer();
			tower.change_state(TOWER_STATE_IN_GAME);
			break;

		case TOWER_STATE_RESTART_REQUEST:
			fade_out();
			tower.change_state(TOWER_STATE_RESTART_PROCESS);
			break;

		case TOWER_STATE_MENU_REQUERT:
			fade_out();
			tower.change_state(TOWER_STATE_MENU_PROCESS);
			break;

		case TOWER_STATE_MENU_PROCESS:

			switch (get_state()) {
			case SCREEN_STATE_FADE_OUT_DONE:
				do_open_level_screen();
				break;

			case SCREEN_STATE_FADE_IN_DONE:
				break;
			}

			break;

		case TOWER_STATE_RESTART_PROCESS:

			switch (get_state()) {
			case SCREEN_STATE_FADE_OUT_DONE:
				hide_layer();
				tower.reset_items_to_default();
				tower.update(delta, camera);
				if (tower.should_show_ads())
					tower.change_state(TOWER_STATE_ADS_REQUEST);
				else
					tower.change_state(TOWER_STATE_IN_GAME_READY_GO);
				fade_in();
				break;

			case SCREEN_STATE_FADE_IN_DONE:
				break;
			}

			break;

		case TOWER_STATE_NEXT_LEVEL_PRECESS:

			tower.update_camera(delta, camera);

			switch (get_state()) {
			case SCREEN_STATE_FADE_OUT_DONE:
				do_open_next_level();
				break;

			case SCREEN_STATE_FADE_IN_DONE:
				break;
			}

			break;

		case TOWER_STATE_ADS_REQUEST:
			show_layer(ADS_LAYER_ID);
			tower.change_state(TOWER_STATE_IN_ADS);
			break;

		case TOWER_STATE_IN_ADS:
			break;
		}

		tower.render(delta, command, camera);

		BaseScreen::do_update(delta, command);
	}

	void do_open_level_screen();
	void do_open_next_level();

public:
	GameScreen(const BoxItem & box_item, int level_index) {

		this->box_item = box_item;
		this->level_index = level_index;

		this->last_y = 0.0f;
		this->last_angle = 0.0f;

		this->add_active_layer(new ControlsLayer(&tower));
		this->add_layer(PAUSE_LAYER_ID, new PauseLayer(&tower));
		this->add_layer(COMPLETE_LAYER_ID, new CompleteLayer(&tower));
		this->add_layer(ADS_LAYER_ID, new AdsLayer(&tower));
	}

	~GameScreen() {
	}
}
;
} // namespace ft

#endif /* ft_game_screen_H */
