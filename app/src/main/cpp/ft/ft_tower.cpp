#include "fte_utils.h"
#include "fte_utils_3d.h"
#include "texture_atlas_loader.h"
#include "polygon_builder.h"

#include "ft_tower.h"
#include "ft_tower_preferences.h"
#include "ft_tower_item.h"
#include "ft_step_item.h"
#include "ft_step_falsy_item.h"
#include "ft_step_slippy_item.h"
#include "ft_door_item.h"
#include "ft_finish_door_item.h"
#include "ft_lift_item.h"
#include "ft_pipe_item.h"
#include "ft_eye_item.h"
#include "ft_ball_item.h"
#include "ft_block_item.h"
#include "ft_pogo_item.h"
#include "ft_vagon_item.h"
#include "ft_mosquito_item.h"
#include "ft_door_with_padlock_item.h"
#include "ft_key_item.h"
#include "ft_padlock_item.h"
#include "ft_button_for_platform_item.h"
#include "ft_step_toggler_item.h"
#include "ft_ground_item.h"

namespace ft {

const static int _PARAM_TOWER_SURFACE_POW = 20;

float _PARAM_Y_AMPL;
float _PARAM_Y_SIN_COEF;
float _PARAM_Y_COS_COEF;
float _PARAM_BASE_RADIUS;
float _PARAM_TOWER_HEIGHT;
float _PARAM_TOWER_HEIGHT_2;
float _PARAM_ANGLE_AMPL;
float _PARAM_ANGLE_SIN_ANGLE_COEF;
float _PARAM_ANGLE_SIN_Y_COEF;

float _CONST_RADIUS_FUNC_TOWER_SURFACE;

float _TOWER_SURFACE_OFFSET;
float _PLATFORM_DEPTH_2;
float _PLATFORM_GAP;

float radius_func_fixed(float y, float angle) {
	return BASE_RADIUS;
}

float radius_derivative_func_fixed(float y, float angle) {
	return 0.0f;
}

float radius_func_fixed_platform_inner_radius(float y, float angle) {
	return BASE_RADIUS - _PLATFORM_DEPTH_2;
}

float radius_func_platform_inner_radius(float y, float angle) {
	float r = radius_func(y, angle);
	r -= _PLATFORM_DEPTH_2;
	return r;
}

float radius_func_platform_inner_radius_ignore_angle(float y, float angle) {
	float r = radius_func(y, 0.0f);
	r -= _PLATFORM_DEPTH_2;
	return r;
}

float radius_func_tower_surface(float y, float angle) {
	float r = radius_func(y, angle);
	r -= _CONST_RADIUS_FUNC_TOWER_SURFACE * std::pow(std::abs(y - _PARAM_TOWER_HEIGHT_2), _PARAM_TOWER_SURFACE_POW);
	r -= _TOWER_SURFACE_OFFSET;
	r = r < 0.0f ? 0.0f : r;
	return r;
}

float radius_func_for_door_outer(float y, float angle) {
	float r = radius_func_tower_surface(y, angle);
	return r;
}

float radius_func_for_door_inner(float y, float angle) {
	float r = radius_func_tower_surface(y, angle);
	return r * 1.3f;
}

float radius_func_ignore_angle(float y, float angle) {
	float r = radius_func(y, 0.0f);
	return r;
}

float radius_func(float y, float angle) {
	float r = _PARAM_BASE_RADIUS + //
			_PARAM_Y_AMPL * (1.0f + sin_fast(_PARAM_Y_SIN_COEF * y) - cos_fast(_PARAM_Y_COS_COEF * y)) + // for Y distortion
			_PARAM_ANGLE_AMPL * sin_fast(_PARAM_ANGLE_SIN_Y_COEF * y + _PARAM_ANGLE_SIN_ANGLE_COEF * angle); // for ANGLE distortion
	return r;
}

float radius_derivative_func_y(float y, float angle) {
	return //_PARAM_BASE_RADIUS * _PARAM_START_COEF * y / ((_PARAM_START_COEF + y) * (_PARAM_START_COEF + y)) + //
	_PARAM_Y_AMPL * (_PARAM_Y_SIN_COEF * cos_fast(_PARAM_Y_SIN_COEF * y) + _PARAM_Y_COS_COEF * sin_fast(_PARAM_Y_COS_COEF * y)) + //
			_PARAM_ANGLE_SIN_Y_COEF * _PARAM_ANGLE_AMPL * cos_fast(_PARAM_ANGLE_SIN_Y_COEF * y + _PARAM_ANGLE_SIN_ANGLE_COEF * angle);
}

float radius_derivative_func_angle(float y, float angle) {
	return _PARAM_ANGLE_SIN_ANGLE_COEF * _PARAM_ANGLE_AMPL * cos_fast(_PARAM_ANGLE_SIN_Y_COEF * y + _PARAM_ANGLE_SIN_ANGLE_COEF * angle);
}

const std::string Tower::BASE_NAME_TILE = "tower_tile";

Tower::Tower() {

	tower_brick_height = 0.0f;

	map_width = 0.0f;
	map_width_2 = 0.0f;
	map_width_4 = 0.0f;
	map_full_width = 0.0f;

	key_color1 = false;
	key_color2 = false;
	key_color3 = false;

	button_color1 = false;
	button_color2 = false;
	button_color3 = false;

	mosquito_collected = 0;
	mosquito_count = 0;
	mosquito_remains = 0;

	ball_collected = 0;
	ball_count = 0;
	ball_remains = 0;

	main_character_item = NULL;
	main_character_angle = 0.0f;

	default_distance_from_base_radius = 0.0f;

	tower_batches_count = 0;

	bricks_in_cache = 0;

	scale_factor_x = 0.0f;
	scale_factor_y = 0.0f;

	world = NULL;

	time_left = 0.0f;

	vertex_vibration_freq = 0.0f;

	main_character_y = 0.0f;
	screen_aspect = 1.0f;

	ground_item = NULL;
	finish_item = NULL;

	shader_state_time = 0.0f;

	retry_counter = 0;
}

Tower::~Tower() {
	release_polygons();
	delete_vector_elements<TowerItem*>(items);
	main_character_item = NULL;

//	audio.destroy_music(m_id);
//	audio.destroy_sound(crump_sound_id);
//	audio.destroy_sound(mosquito_collected_sound_id);
//	audio.destroy_sound(lift_arrived_sound_id);
//	audio.destroy_sound(monster_hit_sound_id);
//	audio.destroy_sound(monster_punch_sound_id);

	DELETE(world);
}

void Tower::create_polygons(FTE_ENV env) {
	// tower

	create_caches_simple(env);

	// create item's polygons

	for (std::vector<TowerItem*>::const_iterator it = items.begin(); it != items.end(); ++it) {
		TowerItem* item = *it;
		item->create_polygons(select_or_create_cache_for_xy(item->get_col(), item->get_row()), env);
	}
}

void Tower::release_polygons() {

	delete_map_values<TYPE_STATIC_BATCH_ID, PolygonCache*>(tower_batches);

	for (std::vector<TowerItem*>::const_iterator it = items.begin(); it != items.end(); ++it) {
		TowerItem* item = *it;
		item->release_polygons();
	}
}

void Tower::create_caches_simple(FTE_ENV env) {
	TextureAtlasLoader* tower_atlas = get_tower_atlas(env);

	Sprite* tile = PolygonBuiler::create_sprite(*tower_atlas, tower_data.sprite_for_tower_tile, true, true);
	Sprite* tile_nm = PolygonBuiler::create_sprite(*tower_atlas, tower_data.sprite_for_tower_tile_nm, false, false);

	const int tiles_in_cache = get_tiles_in_cache();
	const float tile_height = tower_brick_height * tower_data.bricks_in_tile;
	const float tower_height = tower_brick_height * tower_data.rows;

//	std::vector<std::vector<Vec3> > holes;
//	build_doors_holes(holes, true, get_door_frame_smoothness());

	for (int i = 0; i != tower_batches_count; i++) {

		PolygonCache* pc = select_or_create_cache(START_TOWER_BATCH_ID + i);

		for (int j = 0; j != tiles_in_cache; j++) {

			float translate_y = (i * tiles_in_cache + j) * tile_height;
			float exceed_y = (translate_y + tile_height) - tower_height;
			float v_scale = exceed_y > 0.0f ? (tile_height - exceed_y) / tile_height : 1.0f;
			float height = exceed_y > 0.0f ? (tile_height - exceed_y) : tile_height;

			if (F_GR(height, 0.0f))
				create_cylinder(pc, tile, tile_nm, 1, get_tower_num_edges(), get_tower_num_edges_for_texture(), tower_data.tower_step_vertical, height, v_scale, radius_func_tower_surface, radius_derivative_func_y, 0.0f, translate_y, 0.0f);
		}
	}

	DELETE(tile);

	LOGD("fte: tower has %d caches", tower_batches_count);
}

PolygonCache* Tower::select_or_create_cache_for_xy(float col, float row) {
	int idx = (int) (row / bricks_in_cache);
	return select_or_create_cache(START_TOWER_BATCH_ID + idx);
}

PolygonCache* Tower::select_or_create_cache(TYPE_STATIC_BATCH_ID batch_id) {
	std::map<TYPE_STATIC_BATCH_ID, PolygonCache*>::iterator it = tower_batches.find(batch_id);
	if (it == tower_batches.end()) {
		PolygonCache* pc = new PolygonCache;
		tower_batches[batch_id] = pc;
		return pc;
	}
	return it->second;
}

TowerItem* Tower::create_item_from_params(const TowerDataItem & data) {
	const std::string & type = data.params[0];

	TowerItem* item = NULL;
	if (type == "StepItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: StepItem must have 1 int params!");
		} else {
			std::string param1 = data.params[1];
			if (param1 == "DEFAULT") {
				item = new StepItem(this, data.col, data.row);
			} else if (param1 == "FALSY") {
				item = new StepFalsyItem(this, data.col, data.row);
			} else if (param1 == "RIGHT" || param1 == "LEFT") {
				item = new StepSlippyItem(this, data.col, data.row, param1 == "RIGHT" ? STEP_SLIPPY_DIRECTION_RIGHT : STEP_SLIPPY_DIRECTION_LEFT);
			}
		}

	} else if (type == "BlockItem") {

//		item = new BlockItem(this, data.col, data.row);

	} else if (type == "BallItem") {

		int key_color = data.params.size() > 1 ? validate_key_color(atoi(data.params[1].c_str())) : 0;
		int aggressive_level = data.params.size() > 2 ? atoi(data.params[2].c_str()) : MONSTER_AGGRESSIVE_LEVEL_HIGH;
		item = new BallItem(this, data.col, data.row, key_color, aggressive_level);

	} else if (type == "EyeItem") {

		if (data.params.size() < 3) {
			LOGE("fte: Tower: create_item_from_params: EyeItem must have at least 2 params!");
		} else {
			int vel_x = atoi(data.params[1].c_str());
			int vel_y = atoi(data.params[2].c_str());
			int key_color = data.params.size() > 3 ? validate_key_color(atoi(data.params[3].c_str())) : 0;
			int aggressive_level = data.params.size() > 4 ? atoi(data.params[4].c_str()) : MONSTER_AGGRESSIVE_LEVEL_HIGH;
			item = new EyeItem(this, data.col, data.row, vel_x, vel_y, key_color, aggressive_level);
		}

	} else if (type == "DoorItem") {

		item = new DoorItem(this, data.col, data.row);

	} else if (type == "DoorWithPadLockItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: DoorWithPadlockItem must have 1 int params!");
		} else {
			int color = atoi(data.params[1].c_str());
			item = new DoorWithPadLockItem(this, data.col, data.row, color);
		}

	} else if (type == "KeyItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: KeyItem must have 1 int params!");
		} else {
			int color = atoi(data.params[1].c_str());
			item = new KeyItem(this, data.col, data.row, color);
		}

	} else if (type == "FinishDoorItem") {

		finish_item = item = new FinishDoorItem(this, data.col, data.row);

	} else if (type == "PogoItem") {

		item = new PogoItem(this, data.col, data.row);

	} else if (type == "VagonItem") {

		item = new VagonItem(this, data.col, data.row);

	} else if (type == "PipeItem") {

		item = new PipeItem(this, data.col, data.row);

	} else if (type == "LiftItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: LiftItem must have at least 1 float params!");
		} else {
			float move_up_by = (float) atof(data.params[1].c_str());
			float delay_down = data.params.size() > 2 ? (float) atof(data.params[2].c_str()) : 0.0f;
			float delay_up = data.params.size() > 3 ? (float) atof(data.params[3].c_str()) : 0.0f;
			bool controllable = data.params.size() > 4 ? data.params[4] != "false" : true;
			item = new LiftItem(this, data.col, data.row, delay_up, delay_down, move_up_by, controllable);
		}

	} else if (type == "MosquitoItem") {

		item = new MosquitoItem(this, data.col, data.row);

	} else if (type == "ButtonForPlatformItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: ButtonForPlatformItem must have at least 1 param!");
		} else {
			int color = atoi(data.params[1].c_str());
			item = new ButtonForPlatformItem(this, data.col, data.row, color);
		}

	} else if (type == "StepTogglerItem") {

		if (data.params.size() < 2) {
			LOGE("fte: Tower: create_item_from_params: StepTogglerItem must have at least 1 param!");
		} else {
			int color = atoi(data.params[1].c_str());
			item = new StepTogglerItem(this, data.col, data.row, color);
		}

	} else if (type == "GroundItem") {

		ground_item = item = new GroundItem(this);
	}

	if (item && item->get_contact_type() == TYPE_MAIN_CHARACTER)
		main_character_item = item;

	return item;
}

bool items_comparator(TowerItem* i1, TowerItem* i2) {
	return (i1->get_contact_type() < i2->get_contact_type());
}

void Tower::build_door_hole(const Rect & door_rect, std::vector<std::vector<Vec3> > & holes, bool able_to_split, int smoothness, bool loop) {

	std::vector<Vec3> hole;

	const float da = M_2xPI / smoothness;

	// apply smoothness
	float w = (door_rect.x2 - door_rect.x1) * 0.5f;
	float h = door_rect.y2 - door_rect.y1;

	float center_x = door_rect.x1 + w;

	float prev_vx = door_rect.x1;

	const int c = loop ? smoothness + 1 : smoothness;
	for (int i = 0; i != c; i++) {

		float cur_a = M_PI - da * i;

		float x = w * cos_fast(cur_a);
		float sin_a = sin_fast(cur_a);
		float y = sin_a > 0.0f ? h * sin_a : 0.25f * h * sin_a;

		Vec3 v;
		v.x = door_rect.x1 + w + x;
		v.y = door_rect.y1 + y;

		if (able_to_split) {

			if (prev_vx < 0.0f && v.x > 0.0f) {
				float edge_x = center_x;
				float edge_y = sqrtf(h * h * (1.0f - edge_x * edge_x / (w * w)));

				hole.push_back(Vec3(M_2xPI, door_rect.y1 + edge_y, 0.0f));
				hole.push_back(Vec3(M_2xPI, door_rect.y1, 0.0f));
				holes.push_back(hole);

				hole.clear();

				hole.push_back(Vec3(0.0f, door_rect.y1, 0.0f));
				hole.push_back(Vec3(0.0f, door_rect.y1 + edge_y, 0.0f));

			} else if (prev_vx > 0.0f && v.x < 0.0f) {
				float edge_x = center_x;
				float edge_y = 0.25f * sqrtf(h * h * (1.0f - edge_x * edge_x / (w * w)));

				hole.push_back(Vec3(0.0f, door_rect.y1 - edge_y, 0.0f));
				hole.push_back(Vec3(0.0f, door_rect.y1, 0.0f));
				holes.push_back(hole);

				hole.clear();

				hole.push_back(Vec3(M_2xPI, door_rect.y1, 0.0f));
				hole.push_back(Vec3(M_2xPI, door_rect.y1 - edge_y, 0.0f));

			} else if (prev_vx < M_2xPI && v.x > M_2xPI) {
				float edge_x = M_2xPI - center_x;
				float edge_y = sqrtf(h * h * (1.0f - edge_x * edge_x / (w * w)));

				hole.push_back(Vec3(M_2xPI, door_rect.y1 + edge_y, 0.0f));
				hole.push_back(Vec3(M_2xPI, door_rect.y1, 0.0f));
				holes.push_back(hole);

				hole.clear();

				hole.push_back(Vec3(0.0f, door_rect.y1, 0.0f));
				hole.push_back(Vec3(0.0f, door_rect.y1 + edge_y, 0.0f));
			} else if (prev_vx > M_2xPI && v.x < M_2xPI) {
				float edge_x = M_2xPI - center_x;
				float edge_y = 0.25f * sqrtf(h * h * (1.0f - edge_x * edge_x / (w * w)));

				hole.push_back(Vec3(0.0f, door_rect.y1 - edge_y, 0.0f));
				hole.push_back(Vec3(0.0f, door_rect.y1, 0.0f));
				holes.push_back(hole);

				hole.clear();

				hole.push_back(Vec3(M_2xPI, door_rect.y1, 0.0f));
				hole.push_back(Vec3(M_2xPI, door_rect.y1 - edge_y, 0.0f));
			}
		}

		if (able_to_split) {
			prev_vx = v.x;

			if (v.x < 0.0f)
				v.x += M_2xPI;
			else if (v.x > M_2xPI)
				v.x -= M_2xPI;
		}

		hole.push_back(v);
	}

	holes.push_back(hole);
}

void Tower::build_door_rect(float col, float row, Rect & door_rect, float scale_x, float scale_y) {
	door_rect.x1 = 0.0f;
	door_rect.x2 = scale_factor_x;

	door_rect.y1 = 0.0f;
	door_rect.y2 = scale_factor_y;

	door_rect.translate(-scale_factor_x * 0.5f, 0.0f);

	door_rect.mul(get_door_scale_x() * scale_x, get_door_scale_y() * scale_y);

	door_rect.translate(scale_factor_x * (col + 0.5f), row * get_brick_height_scale() * scale_factor_y);
}

void Tower::build_doors_holes(std::vector<std::vector<Vec3> > & holes, bool able_to_split, int smoothness) {

	if (smoothness == 0)
		return;

	Rect door_rect;

	for (std::vector<TowerDataItem>::const_iterator it = tower_data.items.begin(); it != tower_data.items.end(); ++it) {
		TowerDataItem data_item = *it;
		std::string type = data_item.params[0];

		if (type == "DoorItem" || type == "DoorWithPadLockItem" || type == "FinishDoorItem") {

			build_door_rect(data_item.col, data_item.row, door_rect);
			build_door_hole(door_rect, holes, able_to_split, smoothness);

			if (type != "FinishDoorItem") {
				float opposite_col = get_opposite_col(data_item.col);
				build_door_rect(opposite_col, data_item.row, door_rect);
				build_door_hole(door_rect, holes, able_to_split, smoothness);
			}
		}
	}
}

void Tower::create_items() {
	for (std::vector<TowerDataItem>::const_iterator it = tower_data.items.begin(); it != tower_data.items.end(); ++it) {

		const TowerDataItem & data_item = *it;
		TowerItem* item = create_item_from_params(data_item);
		if (item != NULL) {
			items.push_back(item);
			item->create_child_items(items);
		}
	}

	// This sort defines a draw order
	std::sort(items.begin(), items.end(), items_comparator);

	uint32_t idx = 0;

	for (std::vector<TowerItem*>::const_iterator it = items.begin(); it != items.end(); ++it) {

		TowerItem* item = *it;

		if (item->is_need_to_update())
			items_to_update.push_back(item);

		if (item->is_need_to_render_toon())
			items_to_render_toon.push_back(item);
		else if (item->is_need_to_render_no_toon())
			items_to_render_no_toon.push_back(item);

		if (item->is_need_to_render_effects())
			items_to_render_effects.push_back(item);

		if (item->is_need_to_render_tails())
			items_to_render_tails.push_back(item);

		if (item->is_need_to_render_standalone())
			items_to_render_standalone.push_back(item);
	}

	LOGD("fte: Tower: ===================================");
	LOGD("items=%d", items.size());
	LOGD("items_to_update=%d", items_to_update.size());
	LOGD("items_to_render=%d", items_to_render_toon.size() + items_to_render_no_toon.size());
	LOGD("items_to_render_standalone=%d", items_to_render_standalone.size());
	LOGD("fte: Tower: ===================================");
}

void Tower::load_tower(int quality_level, const std::string & file_name, FTE_ENV env) {

	TowerLoader tl(file_name, env);

	tower_file_name = file_name;

	tower_data = tl.get_tower_data();
	tower_data.set_quality_level(quality_level);

	recalc_vertex_vibration();

	default_distance_from_base_radius = 0.0f;

	scale_factor_x = M_2xPI / tower_data.cols;
	scale_factor_y = BASE_RADIUS * scale_factor_x;

	map_width = tower_data.cols;
	map_width_2 = map_width * 0.5f;
	map_width_4 = map_width * 0.25f;
	map_full_width = map_width * (1 + tower_data.max_cols / tower_data.cols);

	tower_brick_height = tower_data.brick_height_scale * scale_factor_y;

	bricks_in_cache = tower_data.tiles_in_cache * tower_data.bricks_in_tile;

	tower_batches_count = (int) (std::ceil(((float) tower_data.rows / bricks_in_cache)));

	_PARAM_Y_AMPL = tower_data.tower_surface_y_amplitude;
	_PARAM_Y_SIN_COEF = tower_data.tower_surface_y_sin_coef;
	_PARAM_Y_COS_COEF = tower_data.tower_surface_y_cos_coef;
	_PARAM_BASE_RADIUS = BASE_RADIUS;
	_PARAM_TOWER_HEIGHT = tower_data.rows * tower_brick_height;
	_PARAM_TOWER_HEIGHT_2 = _PARAM_TOWER_HEIGHT * 0.5f;
	_PARAM_ANGLE_AMPL = tower_data.tower_surface_angle_amplitude;
	_PARAM_ANGLE_SIN_ANGLE_COEF = tower_data.tower_surface_angle_sin_angle_coef;
	_PARAM_ANGLE_SIN_Y_COEF = tower_data.tower_surface_angle_sin_y_coef;

	_CONST_RADIUS_FUNC_TOWER_SURFACE = (_PARAM_BASE_RADIUS / std::pow(_PARAM_TOWER_HEIGHT_2, _PARAM_TOWER_SURFACE_POW));

	_PLATFORM_DEPTH_2 = tower_data.platform_depth * 0.5f;
	_PLATFORM_GAP = tower_data.platform_gap;

	_TOWER_SURFACE_OFFSET = _PLATFORM_GAP + _PLATFORM_DEPTH_2;
}

void Tower::render_once(RenderCommand* command) {

//	for (std::vector<TowerItem*>::iterator it = items_to_render_toon.begin(), end = items_to_render_toon.end(); it != end; ++it) {
//		TowerItem* item = *it;
//		item->render_once(command);
//	}
//
//	for (std::vector<TowerItem*>::iterator it = items_to_render_no_toon.begin(), end = items_to_render_no_toon.end(); it != end; ++it) {
//		TowerItem* item = *it;
//		item->render_once(command);
//	}

	for (std::map<TYPE_STATIC_BATCH_ID, PolygonCache*>::iterator it = tower_batches.begin(), end = tower_batches.end(); it != end; ++it) {
		TYPE_STATIC_BATCH_ID batch_id = it->first;
		PolygonCache* pc = it->second;
		command->begin_end_static_batch(pc, batch_id);
	}
}

void Tower::render_tower_batches(float delta, RenderCommand* command, const Camera & camera) {
	if (!main_character_item)
		return;

	const b2Vec2 & pos = main_character_item->get_map_position();

	int idx = ((int) (pos.y / get_brick_height_scale())) / bricks_in_cache;

	command->begin_end_static_batch_nodata(START_TOWER_BATCH_ID + idx, get_shader_id_nm(), SHADER_ID_TOON, true, true, camera.get_view_proj_matrix());
	if (idx > 0)
		command->begin_end_static_batch_nodata(START_TOWER_BATCH_ID + idx - 1, get_shader_id_nm(), SHADER_ID_TOON, true, true, camera.get_view_proj_matrix());
	if (idx < tower_batches_count - 1)
		command->begin_end_static_batch_nodata(START_TOWER_BATCH_ID + idx + 1, get_shader_id_nm(), SHADER_ID_TOON, true, true, camera.get_view_proj_matrix());
}

void Tower::render(float delta, RenderCommand* command, const Camera & camera) {

	shader_state_time += delta;
	if (shader_state_time > M_2xPI)
		shader_state_time -= M_2xPI;
	command->set_state_time(shader_state_time);

	command->set_toon_fog_color(tower_data.toon_fog_color_r, tower_data.toon_fog_color_g, tower_data.toon_fog_color_b);
	command->set_toon_fog_alpha_threshold(tower_data.toon_fog_alpha_threshold);

	command->set_toon_weight(TOON_WEIGHT);
	command->set_toon_weight_delta(TOON_WEIGHT_DELTA);

	command->set_vertex_vibration_freq(vertex_vibration_freq);
	command->set_vertex_vibration_amplitude(tower_data.vertex_vibration_amplitude);

	render_tower_batches(delta, command, camera);

//	int vc1 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

//	int vc2 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

	const std::vector<TowerItem*>::iterator begin_standalone = items_to_render_standalone.begin(), end_standalone = items_to_render_standalone.end();
	std::vector<TowerItem*>::iterator it_standalone;
	for (it_standalone = begin_standalone; it_standalone != end_standalone; ++it_standalone) {
		TowerItem* item = *it_standalone;
		item->render_standalone(delta, command, camera);
	}

	const std::vector<TowerItem*>::iterator begin_toon = items_to_render_toon.begin(), end_toon = items_to_render_toon.end();
	std::vector<TowerItem*>::iterator it_toon;
	command->begin_batch(get_shader_id(), SHADER_ID_TOON, true, true, camera.get_view_proj_matrix());
	for (it_toon = begin_toon; it_toon != end_toon; ++it_toon) {
		TowerItem* item = *it_toon;
		item->render(delta, command, camera);
	}
	command->end_batch();

	const std::vector<TowerItem*>::iterator begin_no_toon = items_to_render_no_toon.begin(), end_no_toon = items_to_render_no_toon.end();
	std::vector<TowerItem*>::iterator it_no_toon;
	command->begin_batch(get_shader_id(), 0, true, true, camera.get_view_proj_matrix());
	for (it_no_toon = begin_no_toon; it_no_toon != end_no_toon; ++it_no_toon) {
		TowerItem* item = *it_no_toon;
		item->render(delta, command, camera);
	}
	command->end_batch();

	const std::vector<TowerItem*>::iterator begin_effects = items_to_render_effects.begin(), end_effects = items_to_render_effects.end();
	std::vector<TowerItem*>::iterator it_effects;
	command->begin_batch(SHADER_ID_PARTICLES, 0, false, false, camera.get_view_proj_matrix());
	for (it_effects = begin_effects; it_effects != end_effects; ++it_effects) {
		TowerItem* item = *it_effects;
		item->render_effects(delta, command, camera);
	}
	command->end_batch();

	const std::vector<TowerItem*>::iterator begin_tails = items_to_render_tails.begin(), end_tails = items_to_render_tails.end();
	std::vector<TowerItem*>::iterator it_tails;
	command->begin_batch(SHADER_ID_PARTICLES, 0, true, false, camera.get_view_proj_matrix());
	for (it_tails = begin_tails; it_tails != end_tails; ++it_tails) {
		TowerItem* item = *it_tails;
		item->render_tails(delta, command, camera);
	}
	command->end_batch();

//	int vc3 = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;

//	LOGD("vc(items) = %d; vc(particles) = %d; total=%d", vc2 - vc1, vc3 - vc2, vc3 - vc1);
}

void Tower::update_items(float delta, const Camera & camera) {
	std::vector<TowerItem*>::iterator begin = items_to_update.begin(), end = items_to_update.end(), it;
	for (it = begin; it != end; ++it) {
		TowerItem* item = *it;
		item->update(delta, camera);
	}
}

void Tower::update_camera(float delta, Camera & camera) {
	if (main_character_item) {
		main_character_item->get_world_angle_and_y(main_character_y, main_character_angle);
		camera_state.update(delta, camera, main_character_y, main_character_angle);
	}
}

void Tower::update(float delta, const Camera & camera) {
	update_state(delta);

	switch (get_state()) {
	case TOWER_STATE_IN_GAME:
		time_left -= delta;
		if (F_LE(time_left, 0.0f))
			main_character_item->change_state(MC_STATE_DYING);
		break;
	}

	world->Step(delta, 1, 1);
	update_items(delta, camera);
}

void Tower::init_renderer(InitRendererCommand* command, FTE_ENV env) {

	TextureAtlasLoader* tower_atlas = get_tower_atlas(env);
	TextureAtlasLoader* items_atlas = get_items_atlas(env);
	TextureAtlasLoader* particles_atlas = get_particles_atlas(env);

	tower_atlas->add_texture_descriptors(command);
	items_atlas->add_texture_descriptors(command);
	particles_atlas->add_texture_descriptors(command);

	command->add(ShaderDescriptor(SHADER_ID_PL, "shaders/pl/vs.glsl", "shaders/pl/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_PL_NM, "shaders/pl/nm/vs.glsl", "shaders/pl/nm/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_PL_MODEL, "shaders/pl/model/vs.glsl", "shaders/pl/model/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_DL, "shaders/dl/vs.glsl", "shaders/dl/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_DL_NM, "shaders/dl/nm/vs.glsl", "shaders/dl/nm/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_SL, "shaders/sl/vs.glsl", "shaders/sl/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_SL_NM, "shaders/sl/nm/vs.glsl", "shaders/sl/nm/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_NL, "shaders/nl/vs.glsl", "shaders/nl/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_NL_MODEL, "shaders/nl/model/vs.glsl", "shaders/nl/model/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_PARTICLES, "shaders/particles/vs.glsl", "shaders/particles/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_TOON, "shaders/toon/vs.glsl", "shaders/toon/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_TOON_MODEL, "shaders/toon/model/vs.glsl", "shaders/toon/model/fs.glsl"));

	command->add(ShaderDescriptor(SHADER_ID_DOOR_HOLE, "shaders/door_hole/vs.glsl", "shaders/door_hole/fs.glsl"));
}

void Tower::init_once(int w, int h, const AudioManager & audio, FTE_ENV env) {
	world = new b2World(b2Vec2(0.0f, -10.0f));
	world->SetContactListener(this);

	this->audio = audio;

	m_id = audio.create_music("sfx/preview.mp3");
	crump_sound_id = audio.create_sound("sfx/crump.ogg");
	mosquito_collected_sound_id = audio.create_sound("sfx/hit9.ogg");
	lift_arrived_sound_id = audio.create_sound("sfx/lift.ogg");
	monster_hit_sound_id = audio.create_sound("sfx/hit5.ogg");
	monster_punch_sound_id = audio.create_sound("sfx/fall.ogg");

	audio.play_music(m_id, true);

	create_items();
	create_items_body(env);
	create_polygons(env);
}

void Tower::create_items_body(FTE_ENV env) {

	Box2dLoader* box2d_loader = get_items_shapes(env);

	for (std::vector<TowerItem*>::iterator it = items.begin(), end = items.end(); it != end; ++it) {
		TowerItem* item = *it;
		item->create_body(*box2d_loader);
	}
}

void Tower::do_change_state(int state) {
	switch (state) {
	case TOWER_STATE_RESTART_REQUEST:
		retry_counter++;
		break;
	}
}

void Tower::reset_items_to_default() {
	mosquito_collected = 0;
	mosquito_remains = mosquito_count;

	ball_collected = 0;
	ball_remains = ball_count;

	key_color1 = false;
	key_color2 = false;
	key_color3 = false;

	button_color1 = tower_data.active_at_start_button1;
	button_color2 = tower_data.active_at_start_button2;
	button_color3 = tower_data.active_at_start_button3;

	camera_state.start();

	time_left = tower_data.time_limit;

	for (std::vector<TowerItem*>::iterator it = items.begin(), end = items.end(); it != end; ++it) {
		TowerItem* item = *it;
		item->reset_to_default();
	}
}

void Tower::init(int w, int h, FTE_ENV env) {
	this->env = env;
}

void Tower::release() {
//	release_polygons();
}

void Tower::resize(int w, int h) {
	float radius = BASE_RADIUS + get_platform_depth() * 0.5f;
	camera_state.set_default_scale(std::min(w, h) / (2.0f * radius));
	screen_aspect = (float) w / h;
	recalc_vertex_vibration();
}

void Tower::control_inactive() {
	if (main_character_item)
		main_character_item->control_inactive();
}

void Tower::control_left_pressed() {
	if (main_character_item)
		main_character_item->control_left_pressed();
}

void Tower::control_upper_left_pressed() {
	if (main_character_item)
		main_character_item->control_upper_left_pressed();
}

void Tower::control_right_pressed() {
	if (main_character_item)
		main_character_item->control_right_pressed();
}

void Tower::control_upper_right_pressed() {
	if (main_character_item)
		main_character_item->control_upper_right_pressed();
}

void Tower::control_down_pressed() {
	if (main_character_item)
		main_character_item->control_down_pressed();
}

void Tower::control_tap() {
	if (main_character_item)
		main_character_item->control_tap();
}

int Tower::get_control_type() {
	if (main_character_item)
		return main_character_item->get_control_type();
	return CONTROL_TYPE_NONE;
}

float Tower::get_tower_height() {
	return tower_data.rows * tower_brick_height;
}

void Tower::get_main_character_map_position(b2Vec2 & pos) {
	if (main_character_item)
		pos = main_character_item->get_map_position();
	else
		pos.SetZero();
}

void Tower::get_main_character_world_position(float & x, float & y, float & z, float & angle) {
	if (main_character_item)
		main_character_item->get_world_position_from_map(x, y, z, angle);
	else
		x = y = z = angle = 0.0f;
}

void Tower::get_main_character_world_position(Vec3 & pos) {
	if (main_character_item) {
		float angle;
		main_character_item->get_world_position_from_map(pos.x, pos.y, pos.z, angle);
	} else
		pos.set_to_zero();
}

float Tower::get_main_character_angle_y() {
	if (main_character_item)
		return main_character_item->get_angle_y();
	return 0.0f;
}

bool Tower::has_key(int color) {
	switch (color) {
	case KEY_COLOR_1:
		return key_color1;
	case KEY_COLOR_2:
		return key_color2;
	case KEY_COLOR_3:
		return key_color3;
	}
	return false;
}

void Tower::collect_key(int color) {
	switch (color) {
	case KEY_COLOR_1:
		key_color1 = true;
		break;
	case KEY_COLOR_2:
		key_color2 = true;
		break;
	case KEY_COLOR_3:
		key_color3 = true;
		break;
	}
}

bool Tower::is_button_active(int color) {
	switch (color) {
	case BUTTON_COLOR_1:
		return button_color1;
	case BUTTON_COLOR_2:
		return button_color2;
	case BUTTON_COLOR_3:
		return button_color3;
	}
	return false;
}

void Tower::set_button_active(int color, bool active) {
	switch (color) {
	case BUTTON_COLOR_1:
		button_color1 = active;
		break;
	case BUTTON_COLOR_2:
		button_color2 = active;
		break;
	case BUTTON_COLOR_3:
		button_color3 = active;
		break;
	}
}

float Tower::get_angle_y_to_look_at_main_character(float my_angle) const {
	float target_angle = main_character_angle - my_angle;
	if (target_angle > M_PI) {
		target_angle = -(M_2xPI - target_angle);
	} else if (target_angle < -M_PI) {
		target_angle = M_2xPI + target_angle;
	}

	if (target_angle < 0.0f)
		return M_PI - target_angle * 0.5f;
	return -target_angle * 0.5f;
}

void Tower::restart() {
	change_state(TOWER_STATE_RESTART_REQUEST);
}

void Tower::pause() {
	switch (get_state()) {
	case TOWER_STATE_IN_ADS:
	case TOWER_STATE_IN_GAME:
		change_state(TOWER_STATE_PAUSE_REQUEST);
		break;
	default:
		break;
	}
}

void Tower::unpause() {
	switch (get_state()) {
	case TOWER_STATE_PAUSE:
		change_state(TOWER_STATE_UNPAUSE_REQUEST);
		break;
	default:
		break;
	}
}

void Tower::quit_to_menu() {
	switch (get_state()) {
	case TOWER_STATE_PAUSE:
	case TOWER_STATE_COMPLETE:
		change_state(TOWER_STATE_MENU_REQUERT);
		break;
	default:
		break;
	}
}

void Tower::next_level() {
	switch (get_state()) {
	case TOWER_STATE_COMPLETE:
		change_state(TOWER_STATE_NEXT_LEVEL_REQUEST);
		break;
	default:
		break;
	}
}

void Tower::tower_finished() {
	change_state(TOWER_STATE_FINISHED);
	for (std::vector<TowerItem*>::iterator it = items.begin(), end = items.end(); it != end; ++it) {
		TowerItem* item = *it;
		item->tower_finished();
	}
}

void Tower::tower_request_complete_screen() {
	change_state(TOWER_STATE_COMPLETE_REQUEST);
}

bool Tower::is_sun_earned(int sun_type) {
	switch (sun_type) {
	case SUN_TYPE_PASSED:
		return true;
	case SUN_TYPE_ALL_MOSQUITOS:
		return mosquito_count > 0 && mosquito_remains == 0;
	case SUN_TYPE_ALL_BALLS:
		return ball_count > 0 && ball_remains == 0;
	}
	return false;
}

bool Tower::is_sun_exists(int sun_type) {
	switch (sun_type) {
	case SUN_TYPE_PASSED:
		return true;
	case SUN_TYPE_ALL_MOSQUITOS:
		return mosquito_count > 0;
	case SUN_TYPE_ALL_BALLS:
		return ball_count > 0;
	}
	return false;
}

int Tower::get_sun_count() {
	int c = 1;
	if (mosquito_count > 0)
		c++;
	if (ball_count > 0)
		c++;
	return c;
}

void Tower::recalc_vertex_vibration() {
	vertex_vibration_freq = tower_data.vertex_vibration_freq / screen_aspect;
}

void Tower::save_to_preferences(Preferences & pref) {
	float prev_score = TowerPreferences::get_score(pref, tower_file_name);
	float curr_score = time_left;
	if (prev_score < curr_score)
		TowerPreferences::set_score(pref, tower_file_name, curr_score);
}

TowerItem* Tower::get_nearest_item(const b2Vec2 & pos, float radius, CONTACT_TYPE filter) {
	float min = 1000.0f;
	TowerItem* res = NULL;

	for (std::vector<TowerItem*>::iterator it = items.begin(), end = items.end(); it != end; ++it) {
		TowerItem* item = *it;
		if (item->is_active() && item->is_in_frustrum())
			if (item->get_body())
				if ((item->get_contact_type() & filter) != 0) {
					const b2Vec2 & item_pos = item->get_map_position();

					float dx = std::abs(pos.x - item_pos.x);
					float dy = std::abs(pos.y - item_pos.y);

					if (dx > map_width_2)
						dx = map_width - dx;

					float d = std::sqrt(dx * dx + dy * dy);
					if (d < radius && min > d) {
						res = item;
						min = d;
					}
				}
	}
	return res;
}

TowerItem* Tower::get_item_at(float col, float row, CONTACT_TYPE filter) {
	for (std::vector<TowerItem*>::iterator it = items.begin(), end = items.end(); it != end; ++it) {
		TowerItem* item = *it;
		if ((item->get_contact_type() & filter) != 0)
			if (F_EQ_EPS(item->get_col(), col, 1e-3) && F_EQ(item->get_row(), row))
				return item;
	}

	return NULL;
}

TowerItem* Tower::get_item_at_left(float col, float row, CONTACT_TYPE filter) {
	col -= 1.0f;
	col = col < 0.0f ? map_width - 1.0f : col;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_right(float col, float row, CONTACT_TYPE filter) {
	col += 1.0f;
	col = col >= map_width ? 0.0f : col;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_top(float col, float row, CONTACT_TYPE filter) {
	row += 1.0f;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_bottom(float col, float row, CONTACT_TYPE filter) {
	row -= 1.0f;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_left_top(float col, float row, CONTACT_TYPE filter) {
	row += 1.0f;
	col -= 1.0f;
	col = col < 0.0f ? map_width - 1.0f : col;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_left_bottom(float col, float row, CONTACT_TYPE filter) {
	row -= 1.0f;
	col -= 1.0f;
	col = col < 0.0f ? map_width - 1.0f : col;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_right_top(float col, float row, CONTACT_TYPE filter) {
	row += 1.0f;
	col += 1.0f;
	col = col >= map_width ? 0.0f : col;
	return get_item_at(col, row, filter);
}

TowerItem* Tower::get_item_at_right_bottom(float col, float row, CONTACT_TYPE filter) {
	row -= 1.0f;
	col += 1.0f;
	col = col >= map_width ? 0 : col;
	return get_item_at(col, row, filter);
}

void Tower::BeginContact(b2Contact* contact) {
	b2Fixture* fA = contact->GetFixtureA();
	b2Fixture* fB = contact->GetFixtureB();
	b2Body* bA = fA->GetBody();
	b2Body* bB = fB->GetBody();
	void* udA = bA->GetUserData();
	void* udB = bB->GetUserData();
	TowerItem* itemA = (TowerItem*) udA;
	TowerItem* itemB = (TowerItem*) udB;
	if (itemA)
		itemA->begin_contact(fA, fB, contact);
	if (itemB)
		itemB->begin_contact(fB, fA, contact);
}

void Tower::EndContact(b2Contact* contact) {
	b2Fixture* fA = contact->GetFixtureA();
	b2Fixture* fB = contact->GetFixtureB();
	b2Body* bA = fA->GetBody();
	b2Body* bB = fB->GetBody();
	void* udA = bA->GetUserData();
	void* udB = bB->GetUserData();
	TowerItem* itemA = (TowerItem*) udA;
	TowerItem* itemB = (TowerItem*) udB;
	if (itemA)
		itemA->end_contact(fA, fB, contact);
	if (itemB)
		itemB->end_contact(fB, fA, contact);
}

void Tower::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
	b2Fixture* fA = contact->GetFixtureA();
	b2Fixture* fB = contact->GetFixtureB();
	b2Body* bA = fA->GetBody();
	b2Body* bB = fB->GetBody();
	void* udA = bA->GetUserData();
	void* udB = bB->GetUserData();
	TowerItem* itemA = (TowerItem*) udA;
	TowerItem* itemB = (TowerItem*) udB;
	if (itemA)
		itemA->pre_solve_contact(fA, fB, contact, 1.);
	if (itemB)
		itemB->pre_solve_contact(fB, fA, contact, -1.);
}

void Tower::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
//	LOGD("PostSolve");
	b2Fixture* fA = contact->GetFixtureA();
	b2Fixture* fB = contact->GetFixtureB();
	b2Body* bA = fA->GetBody();
	b2Body* bB = fB->GetBody();
	void* udA = bA->GetUserData();
	void* udB = bB->GetUserData();
	TowerItem* itemA = (TowerItem*) udA;
	TowerItem* itemB = (TowerItem*) udB;
	if (itemA)
		itemA->post_solve_contact(fA, fB, contact, 1.);
	if (itemB)
		itemB->post_solve_contact(fB, fA, contact, -1.);
}

} // namespace ft
