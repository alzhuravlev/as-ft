#ifndef ft_ads_layer_H
#define ft_ads_layer_H

#include "engine.h"
#include "command.h"
#include "touch_helper.h"
#include "fte_utils.h"
#include "anim.h"
#include "polygon_builder.h"
#include "ft_button.h"
#include "ft_base_screen.h"
#include "ft_tower_item.h"

using namespace fte;

namespace ft {

class AdsLayer: public BaseScreen {
private:

	Tower* tower;

	Button buy_premium;
	Button show_ad;

	Polygon* bg;

	FTE_ENV env;

protected:

	virtual void do_resize(FTE_ENV env) {

		float big_w = get_screen_ratio() * 0.9f;
		float big_h = get_screen_ratio() * 0.3f;
		float big_shift_y = big_h * 0.5f;

		float small_w = get_screen_ratio() * 0.8f;
		float small_h = get_screen_ratio() * 0.2f;
		float small_shift_y = -small_h * 0.9f;

		buy_premium.set_width(big_w);
		buy_premium.set_height(big_h);
		buy_premium.set_translate_xy(0.0f, big_shift_y);
		buy_premium.update_rect();

		show_ad.set_width(small_w);
		show_ad.set_height(small_h);
		show_ad.set_translate_xy(0.0f, small_shift_y);
		show_ad.update_rect();

		Rect bg_rect;
		bg_rect.set(-0.5f * get_screen_width(), -0.5f * get_screen_height(), 0.5f * get_screen_width(), 0.5f * get_screen_height());

		bg->set_bounds(bg_rect);
	}

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader* atlas = tower->get_controls_atlas(env);
		atlas->add_texture_descriptors(command);
	}

	virtual void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader* atlas = tower->get_controls_atlas(env);

		FontLoader font_loader("fonts/default_font.fnt", env);
		Sprite* font_sprite = PolygonBuiler::create_sprite(*atlas, "default_font", true, false);

		std::string buy_premium_title = "Buy Premium";
		std::string title_noads = Iap_get_info_title(env, "noads");
		if (title_noads.size() > 0)
			buy_premium_title = title_noads;

		std::string price_noads = Iap_get_info_price(env, "noads");
		if (price_noads.size() > 0)
			buy_premium_title += " " + price_noads;

		buy_premium.init_with_text(atlas, &font_loader, "buy_premium;t:" + buy_premium_title, "buy_premium_active;t:" + buy_premium_title, font_sprite);
		show_ad.init_with_text(atlas, &font_loader, "show_ad;t:Show Ad", "show_ad_active;t:Show Ad", font_sprite);

		bg = PolygonBuiler::create_sprite(*atlas, "bg", true, false);
	}

	virtual void do_init(FTE_ENV env) {
		this->env = env;
	}

	virtual void do_show_as_layer() {
	}

	virtual void do_update(float delta, RenderCommand* command) {

		const TouchHelper & th = get_touch_helper();
		const Camera & camera = get_ortho_camera();

		float width = get_width();
		float height = get_height();

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, camera.get_view_proj_matrix());
		{
			command->add(bg);
			buy_premium.render_button(delta, command, th, camera, width, height);
			show_ad.render_button(delta, command, th, camera, width, height);
		}
		command->end_batch();

		if (Iap_is_purchased(env, "noads")) {
			tower->pause();
		} else if (buy_premium.is_button_tapped(th, camera, width, height)) {
			Iap_purchase(env, "noads");
		} else if (show_ad.is_button_tapped(th, camera, width, height)) {
			Ads_show(env, "Default");
			tower->pause();
		}

		BaseScreen::do_update(delta, command);
	}

	virtual void do_update_once(RenderCommand* command) {
	}

public:

	AdsLayer(Tower* tower) {
		this->tower = tower;
		this->bg = NULL;
	}

	~AdsLayer() {
		DELETE(bg);
	}

};

} // namespace ft

#endif /* ft_ads_layer_H */
