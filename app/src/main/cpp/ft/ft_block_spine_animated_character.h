#ifndef ft_block_spine_animated_character_H
#define ft_block_spine_animated_character_H

#include "ft_spine_animated_character.h"

namespace ft {

class BlockSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

protected:

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {

		std::string base_name = "block";
		std::string texture_name = base_name;

		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/" + base_name + ".obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

		std::string anim_filename = "items/" + base_name + ".json";
		std::string atlas_filename = "items/" + base_name + ".atlas";
		float json_scale = item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y();

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, atlas_filename, anim_filename, texture_name, json_scale, true, true, false, false, false, env);

		spine_data->set_mix("sleeping", "waking", 0.3f);
		spine_data->set_mix("waking", "active", 0.3f);

		spine_data->set_mix("active", "dying", 0.3f);
		spine_data->set_mix("active", "punch", 0.3f);

		spine_data->set_mix("punch", "active", 0.3f);

		spine = new SpineRenderer(spine_data);
	}

};

} // namespace ft

#endif /* ft_block_spine_animated_character_H */
