#ifndef ft_box_loader_H
#define ft_box_loader_H

#include "engine.h"
#include "external.h"
#include "fte_utils.h"

#include <string>
#include <vector>
#include <map>

using namespace fte;

namespace ft {

struct BoxItem {
	int id;
	std::string image;
	std::string folder;
	std::vector<std::string> levels;

	std::string format_tower_file_name(int level_index) {
		std::string level_file = levels[level_index];
		std::string file_name = folder + "/" + level_file + ".txt";
		return file_name;
	}
};

class BoxLoader {

private:
	std::vector<BoxItem> items;

	void load(std::string file_name, FTE_ENV env) {

		std::vector<std::string> lines;
		read_resource_lines(file_name, env, lines);

		int current_id = -1;

		std::map<int, BoxItem> tmp;

		for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::string line = *it;

			remove_chars(line, "\n\r");

			if (line.length() == 0)
				continue;

			std::vector<std::string> el;
			split(line, ':', el);

			if (el.size() == 0)
				continue;

			if (el[0] == "box") {
				current_id = atoi(el[1].c_str());
				tmp[current_id].id = current_id;
			} else if (current_id != -1 && el.size() > 1) {
				if (el[0] == "image") {
					tmp[current_id].image = el[1];
				} else if (el[0] == "folder") {
					tmp[current_id].folder = el[1];
				} else if (el[0] == "levels") {
					split(el[1], ',', tmp[current_id].levels);
				}
			}
		}

		for (std::map<int, BoxItem>::const_iterator it = tmp.begin(); it != tmp.end(); ++it) {
			items.push_back(it->second);
		}

		LOGD("Boxes: %s", file_name.c_str());
		LOGD("Boxes.size: %d", items.size());
	}

public:
	BoxLoader(std::string file_name, FTE_ENV env) {
		load(file_name, env);
	}

	void get_items(std::vector<BoxItem> & out) {
		std::copy(items.begin(), items.end(), std::back_inserter(out));
	}
}
;

} // namespace ft

#endif /* ft_box_loader_H */
