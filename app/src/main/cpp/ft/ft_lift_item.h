#ifndef ft_lift_item_H
#define ft_lift_item_H

#include "fte_utils_3d.h"
#include "ft_tower.h"
#include "ft_tower_item.h"
#include "ft_lift_pipe_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "anim.h"
#include "polygon_builder.h"
#include "ring_fade_anim.h"
#include "ft_lift_spine_animated_character.h"

namespace ft {

#define LIFT_CAN_CONTACT_WITH 		TYPE_MAIN_CHARACTER | TYPE_MONSTER | TYPE_PLATFORMS_FILTER

const static int LIFT_STATE_DOWN = 0;
const static int LIFT_STATE_UP = 1;
const static int LIFT_STATE_MOVING_UP = 2;
const static int LIFT_STATE_MOVING_DOWN = 3;

// Move tuning
const static float LIFT_MOTOR_SPEED_FROM = 7.0f;
const static float LIFT_MOTOR_SPEED_TO = 3.0f;
const static float LIFT_MOTOR_FORCE_FROM = 250.0f;
const static float LIFT_MOTOR_FORCE_TO = 110.0f;

class LiftItem: public TowerItem {
private:

	bool controllable;
	bool auto_move_up;
	bool auto_move_down;
	float delay_up;
	float delay_down;
	float move_up_by;

	Interpolator interpolator;

	BaseAnimatedCharacter* animated_character;

	b2PrismaticJoint* joint;

	void create_body_col_row(Box2dLoader & box2d_loader, int col, int row) {
		b2BodyDef def;
		def.type = b2_dynamicBody;
		def.gravityScale = 0.;
		def.fixedRotation = true;
		def.position = b2Vec2(col_to_map_x(col), row_to_map_y(row));
		body = get_tower()->get_world()->CreateBody(&def);
		body->SetUserData(this);

		b2FixtureDef fd;
		fd.density = 3.0f;
		fd.friction = 0.0f;
		fd.restitution = 0.0f;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		Box2dItem* item = box2d_loader.find_item("lift");
		attach_fixture_to_body(body, &fd, item, 1.0f, get_scale_factor());

		b2PrismaticJointDef pjd;

		// Bouncy limit
		b2Vec2 axis(0.0f, 1.0f);
		axis.Normalize();
		pjd.Initialize(get_tower()->get_ground_item()->get_body(), body, b2Vec2(col_to_map_x(col), row_to_map_y(row)), axis);

//		pjd.maxMotorForce = LIFT_MOTOR_FORCE;
		pjd.lowerTranslation = 0.0f;
		pjd.upperTranslation = move_up_by * get_tower()->get_brick_height_scale();
		pjd.enableLimit = true;
		pjd.enableMotor = true;

		joint = (b2PrismaticJoint*) get_tower()->get_world()->CreateJoint(&pjd);
	}

protected:

	virtual void do_release_polygons() {
		DELETE(animated_character);
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		int cnt = move_up_by * get_tower()->get_pipe_smoonthness();
		float scale = 1.0f / get_tower()->get_pipe_smoonthness();
		for (int i = 0; i < cnt; i++) {
			float row = get_row() + i * scale;
			items.push_back(new LiftPipeItem(get_tower(), get_col(), row, (i % get_tower()->get_pipe_smoonthness()) * scale, scale));
		}
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		animated_character = new LiftSpineAnimatedCharacter;
		animated_character->init(this, env);
	}

	virtual void do_reset_to_default() {
		body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
		change_state(LIFT_STATE_DOWN, true);
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_body_col_row(box2d_loader, get_col(), get_row());
	}

	virtual void do_update(float delta, const Camera & camera) {
		switch (get_state()) {

		case LIFT_STATE_DOWN: {
			if (auto_move_up && get_state_time() > delay_up)
				change_state(LIFT_STATE_MOVING_UP);
		}
			break;

		case LIFT_STATE_UP: {
			if (auto_move_down && get_state_time() > delay_down)
				change_state(LIFT_STATE_MOVING_DOWN);
		}
			break;

		case LIFT_STATE_MOVING_UP: {
			if (auto_move_down && get_state_time() > delay_down)
				change_state(LIFT_STATE_MOVING_DOWN);
			else {
				float k = joint->GetJointTranslation() / joint->GetUpperLimit();
				k = interpolator.interpolate(k);

				if (F_GE(k, 1.0f)) {
					change_state(LIFT_STATE_UP);
				} else {
					float speed = LIFT_MOTOR_SPEED_FROM + (LIFT_MOTOR_SPEED_TO - LIFT_MOTOR_SPEED_FROM) * k;
					float force = LIFT_MOTOR_FORCE_FROM + (LIFT_MOTOR_FORCE_TO - LIFT_MOTOR_FORCE_FROM) * k;
					joint->SetMotorSpeed(speed);
					joint->SetMaxMotorForce(force);
				}
			}
		}
			break;

		case LIFT_STATE_MOVING_DOWN: {
			if (auto_move_up && get_state_time() > delay_up)
				change_state(LIFT_STATE_MOVING_UP);
			else {
				float k = 1.0f - joint->GetJointTranslation() / joint->GetUpperLimit();
				k = interpolator.interpolate(k);

				if (F_GE(k, 1.0f)) {
					change_state(LIFT_STATE_DOWN);
				} else {
					float speed = -(LIFT_MOTOR_SPEED_FROM + (LIFT_MOTOR_SPEED_TO - LIFT_MOTOR_SPEED_FROM) * k);
					float force = LIFT_MOTOR_FORCE_FROM + (LIFT_MOTOR_FORCE_TO - LIFT_MOTOR_FORCE_FROM) * k;
					joint->SetMotorSpeed(speed);
					joint->SetMaxMotorForce(force);
				}
			}
		}
			break;
		}

		animated_character->update(this, delta, camera);
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
//		spine->render_batches(command, get_tower()->get_shader_id_model(), SHADER_ID_TOON_MODEL, true, true, camera);
		animated_character->render(this, delta, command, camera);
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
	}

	void do_change_state(int state) {

		switch (state) {

		case LIFT_STATE_UP:
			joint->SetMotorSpeed(0.0f);
			get_tower()->play_lift_arrired();
			break;

		case LIFT_STATE_DOWN:
			joint->SetMaxMotorForce(10000.0f);
			joint->SetMotorSpeed(0.0f);
			break;

		case LIFT_STATE_MOVING_DOWN:
			break;

		case LIFT_STATE_MOVING_UP:
			break;
		}
	}

public:
	LiftItem(Tower* tower, float col, float row, float delay_up, float delay_down, float move_up_by, bool controllable) :
			TowerItem(tower, col, row, TYPE_LIFT, LIFT_CAN_CONTACT_WITH, tower->get_brick_height_scale()) {

		this->controllable = controllable;

		this->delay_up = delay_up;
		this->delay_down = delay_down;

		this->auto_move_up = F_NEQ(delay_up, 0.0f);
		this->auto_move_down = F_NEQ(delay_down, 0.0f);

		this->move_up_by = move_up_by + 0.5f;

		this->animated_character = NULL;
		this->joint = NULL;
	}

	~LiftItem() {
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}

	bool is_moving() {
		return get_state() == LIFT_STATE_MOVING_DOWN || get_state() == LIFT_STATE_MOVING_UP;
	}

	bool is_moving_up() {
		return get_state() == LIFT_STATE_MOVING_UP;
	}

	bool is_moving_down() {
		return get_state() == LIFT_STATE_MOVING_DOWN;
	}

	void toggle_lift() {

		if (!controllable)
			return;

		if (get_state_time() < 0.2f)
			return;

		switch (get_state()) {

		case LIFT_STATE_DOWN:
		case LIFT_STATE_MOVING_DOWN:
			change_state(LIFT_STATE_MOVING_UP);
			break;

		case LIFT_STATE_UP:
		case LIFT_STATE_MOVING_UP:
			change_state(LIFT_STATE_MOVING_DOWN);
			break;
		}
	}

	int get_platform_temporary_group() {
		return -1;
	}

	void platform_contact_from_top_begin() {
		animated_character->play("running1", false);
		animated_character->play_next("running2", true);
	}

	void platform_contact_from_top_end() {
		animated_character->play("running3", false);
		animated_character->play_next("idle", true);
	}

	virtual void pre_solve_contact(b2Body* my_body, b2Fixture* my_fixture, TowerItem* other_item, b2Contact* contact, float f) {
	}

	virtual void post_solve_contact(b2Body* my_body, b2Fixture* my_fixture, TowerItem* other_item, b2Contact* contact, float f) {
	}
}
;

} // namespace ft

#endif /* ft_lift_item_H */
