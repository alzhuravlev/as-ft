#ifndef ft_tower_H
#define ft_tower_H

#include "engine.h"
#include "camera.h"
#include "command.h"
#include "polygon_cache.h"
#include "ft_tower_loader.h"
#include "audio_manager.h"
#include "statable.h"
#include "ft_camera_state.h"
#include "ft_tower_item_consts.h"
#include "ft_shaders_consts.h"
#include "loaders.h"
#include "preferences.h"
#include "frame_anim_polygon_cache.h"
#include "spine_data.h"

#include "Box2D/Box2D.h"

using namespace fte;

/////////////////////////////////////////////////////////////////////////////

/**
 * Tower presentation params
 */
namespace ft {

const static float TIME_STEP = 1.0f / 60.0f;

const static float TOON_WEIGHT = BASE_RADIUS * 0.0020f;
const static float TOON_WEIGHT_DELTA = TOON_WEIGHT * 10.0f;
const static float SPINE_SCALE = 0.01f;

const static TYPE_STATIC_BATCH_ID START_TOWER_BATCH_ID = 1;
const static TYPE_STATIC_BATCH_ID START_EYE_BATCH_ID = 100;
const static TYPE_STATIC_BATCH_ID START_BALL_BATCH_ID = 200;
const static TYPE_STATIC_BATCH_ID START_MC_BATCH_ID = 300;

const static float TOWER_PIPE_RADIUS_COEF = 0.2f;

const static float TOWER_PIPE_NUM_EDGES = 4.0f;
const static float TOWER_PIPE_NUM_EDGES_FOR_TEXTURE = 2.0f;

/////////////////////////////////////////////////////////////////////////////

class TowerItem;

const int TOWER_STATE_IN_GAME_READY_GO = STATE_DEFAULT;
const int TOWER_STATE_IN_GAME = 1;
const int TOWER_STATE_PAUSE = 2;
const int TOWER_STATE_COMPLETE = 3;

const int TOWER_STATE_PAUSE_REQUEST = 4;

const int TOWER_STATE_RESTART_REQUEST = 5;
const int TOWER_STATE_UNPAUSE_REQUEST = 6;
const int TOWER_STATE_MENU_REQUERT = 7;

const int TOWER_STATE_RESTART_PROCESS = 8;
const int TOWER_STATE_MENU_PROCESS = 9;
const int TOWER_STATE_NEXT_LEVEL_PRECESS = 10;

const int TOWER_STATE_COMPLETE_REQUEST = 11;
const int TOWER_STATE_NEXT_LEVEL_REQUEST = 12;

const int TOWER_STATE_FINISHED = 13;

const int TOWER_STATE_ADS_REQUEST = 14;
const int TOWER_STATE_IN_ADS = 15;

float radius_func(float y, float angle);
float radius_derivative_func_y(float y, float angle);
float radius_derivative_func_angle(float y, float angle);

float radius_func_fixed(float y, float angle);
float radius_func_fixed_platform_inner_radius(float y, float angle);

float radius_func_platform_inner_radius(float y, float angle);
float radius_func_platform_inner_radius_ignore_angle(float y, float angle);

float radius_func_petal_surface(float y, float angle);
float radius_func_tower_surface(float y, float angle);
float radius_func_for_door_inner(float y, float angle);
float radius_func_for_door_outer(float y, float angle);

float radius_func_ignore_angle(float y, float angle);

//struct TowerItemPosition {
//	float map_x;
//	uint32_t idx;
//};
//
//struct TowerItemPositionCompare {
//	bool operator()(const TowerItemPosition& a, const TowerItemPosition& b) const {
//		if (a.map_x < b.map_x)
//			return true;
//		else if (a.map_x == b.map_x)
//			if (a.idx < b.idx)
//				return true;
//		return false;
//	}
//};

class Tower: public b2ContactListener, public Statable {
private:

	const static std::string BASE_NAME_TILE;

	Loaders loaders;

	std::map<TYPE_STATIC_BATCH_ID, PolygonCache*> tower_batches;

	std::vector<TowerItem*> items;
	std::vector<TowerItem*> items_to_update;

	std::vector<TowerItem*> items_to_render_effects;
	std::vector<TowerItem*> items_to_render_tails;
	std::vector<TowerItem*> items_to_render_toon;
	std::vector<TowerItem*> items_to_render_no_toon;
	std::vector<TowerItem*> items_to_render_standalone;

	TowerData tower_data;

	std::string tower_file_name;

	b2World* world;

	int retry_counter;

	FTE_ENV env;

	AudioManager audio;

	float tower_brick_height;
	float scale_factor_x;
	float scale_factor_y;
	float map_width;
	float map_width_2;
	float map_width_4;
	float map_full_width;
	float default_distance_from_base_radius;
	int tower_batches_count;
	int bricks_in_cache;

	TowerItem* main_character_item;
	float main_character_angle;
	float main_character_y;

	TowerItem* ground_item;
	TowerItem* finish_item;

	int mosquito_count;
	int mosquito_collected;
	int mosquito_remains;

	int ball_count;
	int ball_collected;
	int ball_remains;

	bool key_color1;
	bool key_color2;
	bool key_color3;

	bool button_color1;
	bool button_color2;
	bool button_color3;

	float screen_aspect;
	float vertex_vibration_freq;

	float shader_state_time;

	float time_left;

	CameraState camera_state;

	FrameAnimationPolygonCache frame_animation_polygon_cache;

	MUSIC_ID m_id;
	SOUND_ID crump_sound_id;
	SOUND_ID mosquito_collected_sound_id;
	SOUND_ID lift_arrived_sound_id;
	SOUND_ID monster_hit_sound_id;
	SOUND_ID monster_punch_sound_id;

	void create_polygons(FTE_ENV env);
	void release_polygons();

	void create_caches_simple(FTE_ENV env);

	TowerItem* create_item_from_params(const TowerDataItem & data);
	void create_items();
	void update_items(float delta, const Camera & camera);
	void create_items_body(FTE_ENV env);

	PolygonCache* select_or_create_cache(TYPE_STATIC_BATCH_ID cache_id);
	PolygonCache* select_or_create_cache_for_xy(float row, float col);

	void recalc_vertex_vibration();

protected:

	virtual void do_change_state(int state);

public:

	Tower();

	~Tower();

	void load_tower(int quality_level, const std::string & file_name, FTE_ENV env);

	void init_renderer(InitRendererCommand* command, FTE_ENV env);

	void init_once(int w, int h, const AudioManager & audio, FTE_ENV env);

	void init(int w, int h, FTE_ENV env);

	void resize(int w, int h);

	void release();

	void reset_items_to_default();

	void control_inactive();

	void control_left_pressed();

	void control_upper_left_pressed();

	void control_right_pressed();

	void control_upper_right_pressed();

	void control_down_pressed();

	void control_tap();

	int get_control_type();

	void update_camera(float delta, Camera & camera);

	void update(float delta, const Camera & camera);

	void render_once(RenderCommand* command);

	void render_tower_batches(float delta, RenderCommand* command, const Camera & camera);

	void render(float delta, RenderCommand* command, const Camera & camera);

	float get_tower_height();

	inline float get_tower_brick_height() const {
		return tower_brick_height;
	}

	inline float get_scale_factor_x() const {
		return scale_factor_x;
	}

	inline float get_scale_factor_y() const {
		return scale_factor_y;
	}

	inline float get_map_width() const {
		return map_width;
	}

	inline float get_map_width_2() const {
		return map_width_2;
	}

	inline float get_default_distance_from_base_radius() const {
		return default_distance_from_base_radius;
	}

	inline Loaders & get_loaders() {
		return loaders;
	}

	inline FontLoader* get_default_font_loader(FTE_ENV env) {
		return loaders.get_font_loader("fonts/default_font.fnt", env);
	}

	inline FontLoader* get_particles_font_loader(FTE_ENV env) {
		return loaders.get_font_loader("fonts/particles_font.fnt", env);
	}

	inline TextureAtlasLoader* get_controls_atlas(FTE_ENV env) {
		return loaders.get_texture_atlas_loader("screens/controls.atlas", env);
	}

	inline TextureAtlasLoader* get_items_atlas(FTE_ENV env) {
		return loaders.get_texture_atlas_loader("game/items.atlas", env);
	}

	inline TextureAtlasLoader* get_tower_atlas(FTE_ENV env) {
		return loaders.get_texture_atlas_loader("game/tower.atlas", env);
	}

	inline TextureAtlasLoader* get_particles_atlas(FTE_ENV env) {
		return loaders.get_texture_atlas_loader("particles/particles_game.atlas", env);
	}

	inline Box2dLoader* get_particles_shape_loader(FTE_ENV env) {
		return loaders.get_box2d_loader("particles/particle.json", env);
	}

	inline Box2dLoader* get_items_shapes(FTE_ENV env) {
		return loaders.get_box2d_loader("items/items_shapes.json", env);
	}

	inline FrameAnimationPolygonCache & get_frame_animation_polygon_cache() {
		return frame_animation_polygon_cache;
	}

	inline b2World* get_world() const {
		return world;
	}

	inline TowerItem* get_ground_item() const {
		return ground_item;
	}

	inline TowerItem* get_finish_item() const {
		return finish_item;
	}

	inline CameraState & get_camera_state() {
		return camera_state;
	}

	inline const AudioManager & get_audio() const {
		return audio;
	}

	inline const TowerData & get_tower_data() {
		return tower_data;
	}

	inline int get_mosquito_count() const {
		return mosquito_count;
	}

	inline int get_mosquito_collected() const {
		return mosquito_collected;
	}

	inline int get_mosquito_remains() const {
		return mosquito_remains;
	}

	inline int get_ball_count() const {
		return ball_count;
	}

	inline int get_ball_collected() const {
		return ball_collected;
	}

	inline int get_ball_remains() const {
		return ball_remains;
	}

	inline void catch_mosquito() {
		if (mosquito_collected < mosquito_count) {
			mosquito_collected++;
			mosquito_remains = mosquito_count - mosquito_collected;
		}
	}

	inline void add_mosquito() {
		mosquito_count++;
	}

	inline void catch_ball() {
		if (ball_collected < ball_count) {
			ball_collected++;
			ball_remains = ball_count - ball_collected;
		}
	}

	inline void add_ball() {
		ball_count++;
	}

	void get_main_character_map_position(b2Vec2 & pos);

	void get_main_character_world_position(float & x, float & y, float & z, float & angle);

	void get_main_character_world_position(Vec3 & pos);

	float get_main_character_angle_y();

	bool has_key(int color);

	void collect_key(int color);

	bool is_button_active(int color);
	void set_button_active(int color, bool active);

	inline int get_cols() {
		return tower_data.cols;
	}

	inline int get_rows() {
		return tower_data.rows;
	}

	inline bool get_platform_follow_tower_surface_distortion_angle() {
		return tower_data.platform_follow_tower_surface_distortion_angle;
	}

	inline bool get_platform_follow_tower_surface_distortion() {
		return tower_data.platform_follow_tower_surface_distortion;
	}

	inline float get_platform_gap() {
		return tower_data.platform_gap;
	}

	inline float get_platform_smoothness() {
		return tower_data.platform_smoothness;
	}

	inline float get_platform_amplitude() {
		return tower_data.platform_amplitude;
	}

	inline float get_platform_cycles() {
		return tower_data.platform_cycles;
	}

	inline float get_platform_incline_amplitude() {
		return tower_data.platform_incline_amplitude;
	}

	inline float get_platform_incline_cycles() {
		return tower_data.platform_incline_cycles;
	}

	inline const std::string & get_platform_shape_name() {
		return tower_data.platform_shape_name;
	}

	inline int get_bricks_in_tile() {
		return tower_data.bricks_in_tile;
	}

	inline int get_tiles_in_cache() {
		return tower_data.tiles_in_cache;
	}

	inline float get_brick_height_scale() {
		return tower_data.brick_height_scale;
	}

	inline float get_platform_height_scale() {
		return tower_data.platform_height_scale;
	}

	inline float get_door_scale_x() {
		return tower_data.door_scale_x;
	}

	inline float get_door_scale_y() {
		return tower_data.door_scale_y;
	}

	inline int get_door_frame_smoothness() {
		return tower_data.door_frame_smoothness;
	}

	inline int get_door_frame_roundness() {
		return tower_data.door_frame_roundness;
	}

	inline float get_door_frame_radius() {
		return tower_data.door_frame_radius;
	}

	inline int get_pipe_smoonthness() {
		return tower_data.pipe_smoonthness;
	}

	inline int get_pipe_roundness() {
		return tower_data.pipe_roundness;
	}

	inline float get_pipe_radius() {
		return tower_data.pipe_radius;
	}

	inline int get_lift_pipe_roundness() {
		return tower_data.lift_pipe_roundness;
	}

	inline float get_lift_pipe_radius() {
		return tower_data.lift_pipe_radius;
	}

	inline float get_pipe_radius_amplitude() {
		return tower_data.pipe_radius_amplitude;
	}

	inline float get_pipe_radius_cycles() {
		return tower_data.pipe_radius_cycles;
	}

	inline float get_lift_pipe_radius_amplitude() {
		return tower_data.lift_pipe_radius_amplitude;
	}

	inline float get_lift_pipe_radius_cycles() {
		return tower_data.lift_pipe_radius_cycles;
	}

	inline float get_pogo_scale() {
		return tower_data.pogo_scale;
	}

	inline float get_lift_scale() {
		return tower_data.lift_scale;
	}

	inline float get_mosquito_scale() {
		return tower_data.mosquito_scale;
	}

	inline float get_pogo_move_vel_x() {
		return tower_data.pogo_move_vel_x;
	}

	inline float get_pogo_move_accel_x() {
		return tower_data.pogo_move_accel_x;
	}

	inline float get_pogo_jump_vel_x() {
		return tower_data.pogo_jump_vel_x;
	}

	inline float get_pogo_jump_height() {
		return tower_data.pogo_jump_height;
	}

	inline float get_vagon_scale() {
		return tower_data.vagon_scale;
	}

	inline float get_eye_scale() {
		return tower_data.eye_scale;
	}

	inline float get_ball_scale() {
		return tower_data.ball_scale;
	}

	inline float get_block_scale() {
		return tower_data.block_scale;
	}

	inline float get_zipperfly_scale() {
		return tower_data.zipperfly_scale;
	}

	inline std::string get_sprite_for_tower_tile() {
		return tower_data.sprite_for_tower_tile;
	}

	inline std::string get_sprite_for_tower_tile_nm() {
		return tower_data.sprite_for_tower_tile_nm;
	}

	inline std::string get_sprite_for_door() {
		return tower_data.sprite_for_door;
	}

	inline std::string get_sprite_for_step() {
		return tower_data.sprite_for_step;
	}

	inline std::string get_sprite_for_step_nm() {
		return tower_data.sprite_for_step_nm;
	}

	inline std::string get_sprite_for_stepside() {
		return tower_data.sprite_for_stepside;
	}

	inline std::string get_sprite_for_pipe() {
		return tower_data.sprite_for_pipe;
	}

	inline std::string get_sprite_for_pipe_nm() {
		return tower_data.sprite_for_pipe_nm;
	}

	inline std::string get_sprite_for_stepside_nm() {
		return tower_data.sprite_for_stepside_nm;
	}

	inline int get_tower_num_edges() {
		return tower_data.tower_num_edges;
	}

	inline int get_tower_num_edges_for_texture() {
		return tower_data.tower_num_edges_for_texture;
	}

	inline float get_platform_depth() {
		return tower_data.platform_depth;
	}

	inline float get_light_ambient() {
		return tower_data.light_ambient;
	}
	inline float get_light_specular_exp() {
		return tower_data.light_specular_exp;
	}
	inline float get_light_spot_exp() {
		return tower_data.light_spot_exp;
	}
	inline float get_light_spot_cos_cutoff() {
		return tower_data.light_spot_cos_cutoff;
	}
	inline float get_light_attenuation() {
		return tower_data.light_attenuation;
	}

	inline float get_zipperfly_distance_from_base_radius() {
		return tower_data.zipperfly_distance_from_base_radius;
	}
	inline float get_zipperfly_movement_alpha() {
		return tower_data.zipperfly_movement_alpha;
	}
	inline float get_zipperfly_distance_y() {
		return tower_data.zipperfly_distance_y;
	}

	inline void get_bg_color(float & r, float & g, float & b) {
		r = tower_data.bg_color_r;
		g = tower_data.bg_color_g;
		b = tower_data.bg_color_b;
	}

	inline uint32_t get_bg_color_i() {
		uint32_t c;
		pack_color_f(c, tower_data.bg_color_r, tower_data.bg_color_g, tower_data.bg_color_b, 1.0f);
		return c;
	}

	inline void get_fog_color(float & r, float & g, float & b) {
		r = tower_data.fog_color_r;
		g = tower_data.fog_color_g;
		b = tower_data.fog_color_b;
	}

	inline uint32_t get_fog_color_i() {
		uint32_t c;
		pack_color_f(c, tower_data.fog_color_r, tower_data.fog_color_g, tower_data.fog_color_b, 1.0f);
		return c;
	}

	inline void get_bubble_color(float & r, float & g, float & b) {
		r = tower_data.bubble_color_r;
		g = tower_data.bubble_color_g;
		b = tower_data.bubble_color_b;
	}

	inline uint32_t get_bubble_color_i() {
		uint32_t c;
		pack_color_f(c, tower_data.bubble_color_r, tower_data.bubble_color_g, tower_data.bubble_color_b, 1.0f);
		return c;
	}

	inline void get_toon_fog_color(float & r, float & g, float & b) {
		r = tower_data.toon_fog_color_r;
		g = tower_data.toon_fog_color_g;
		b = tower_data.toon_fog_color_b;
	}

	inline uint32_t get_toon_fog_color_i() {
		uint32_t c;
		pack_color_f(c, tower_data.toon_fog_color_r, tower_data.toon_fog_color_g, tower_data.toon_fog_color_b, 1.0f);
		return c;
	}

	inline float get_fog_a() {
		return tower_data.fog_a;
	}

	inline float get_fog_b() {
		return tower_data.fog_b;
	}

	inline void inc_time_left(float v) {
		time_left += v;
	}

	inline float get_time_left() {
		return time_left;
	}

	inline int get_time_left_i() {
		return (int) time_left;
	}

	inline int get_light_type() {
		return tower_data.light_type;
	}

	inline int get_shader_id_nm() {
		int shader_id;

		switch (get_light_type()) {
		case LIGHT_TYPE_PL:
			return SHADER_ID_PL_NM;
			break;

		case LIGHT_TYPE_SL:
			return SHADER_ID_SL_NM;
			break;

		case LIGHT_TYPE_NL:
			return SHADER_ID_NL;
			break;
		}

		return SHADER_ID_DL_NM;
	}

	inline int get_shader_id_model() {
		int shader_id;

		switch (get_light_type()) {
		case LIGHT_TYPE_PL:
			return SHADER_ID_PL_MODEL;
			break;

		case LIGHT_TYPE_SL:
			return SHADER_ID_SL_MODEL;
			break;

		case LIGHT_TYPE_NL:
			return SHADER_ID_NL_MODEL;
			break;
		}

		return SHADER_ID_DL;
	}

	inline int get_shader_id() {
		int shader_id;

		switch (get_light_type()) {
		case LIGHT_TYPE_PL:
			return SHADER_ID_PL;
			break;

		case LIGHT_TYPE_SL:
			return SHADER_ID_SL;
			break;

		case LIGHT_TYPE_NL:
			return SHADER_ID_NL;
			break;
		}

		return SHADER_ID_DL;
	}

	inline float get_opposite_col(float col) {
		float opposite_col = col + map_width_2;
		while (F_GE(opposite_col, map_width))
			opposite_col -= map_width;
		return opposite_col;
	}

	inline float get_main_character_angle() const {
		return main_character_angle;
	}

	inline float get_main_character_y() const {
		return main_character_y;
	}

	float get_angle_y_to_look_at_main_character(float my_angle) const;

	void restart();
	void pause();
	void unpause();
	void quit_to_menu();
	void next_level();

	void tower_finished();
	void tower_request_complete_screen();

	bool is_sun_earned(int sun_type);
	bool is_sun_exists(int sun_type);
	int get_sun_count();

	void save_to_preferences(Preferences & pref);

	Polygon* get_reusable_polygon(const std::string & key);
	Polygon* put_reusable_polygon(const std::string & key, Polygon* polygon);

	TowerItem* get_nearest_item(const b2Vec2 & pos, float radius, CONTACT_TYPE filter);

	TowerItem* get_item_at(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_left(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_right(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_top(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_bottom(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_left_top(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_left_bottom(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_right_top(float col, float row, CONTACT_TYPE filter);
	TowerItem* get_item_at_right_bottom(float col, float row, CONTACT_TYPE filter);

	void build_door_hole(const Rect & door_rect, std::vector<std::vector<Vec3> > & holes, bool able_to_split, int smoothness, bool loop = true);
	void build_door_rect(float col, float row, Rect & door_rect, float scale_x = 1.0f, float scale_y = 1.0f);
	void build_doors_holes(std::vector<std::vector<Vec3> > & holes, bool able_to_split, int smoothness);

	// Box2D contacts
	virtual void BeginContact(b2Contact* contact);

	virtual void EndContact(b2Contact* contact);

	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);

	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

	// sfx
	inline void play_crump() {
		audio.play_sound(crump_sound_id);
	}

	inline void play_mosquito_collected() {
		audio.play_sound(mosquito_collected_sound_id);
	}

	inline void play_lift_arrired() {
		audio.play_sound(lift_arrived_sound_id);
	}

	inline void play_monster_hit() {
		audio.play_sound(monster_hit_sound_id);
	}

	inline void play_monster_punch() {
		audio.play_sound(monster_punch_sound_id);
	}

	//

	inline bool should_show_ads() {
		if (retry_counter > 5)
			if (!Iap_is_purchased(env, "noads"))
				if (Ads_is_ready_to_show(env, "Default")) {
					retry_counter = 0;
					return true;
				}
		return false;
	}
};

} // namespace ft

#endif /* ft_tower_H */
