#ifndef ft_controls_H
#define ft_controls_H

#include "engine.h"
#include "command.h"
#include "touch_helper.h"
#include "fte_utils.h"
#include "anim.h"
#include "polygon_builder.h"
#include "ft_button.h"
#include "ft_base_screen.h"
#include "ft_tower_item.h"
#include "font_loader.h"
#include "font_renderer.h"
#include "spine_renderer.h"
#include "ft_tower.h"
#include "spine_data.h"

using namespace fte;

namespace ft {

    const static float CONTROL_MOVE_VEL_X_THRESHOLD = 200.0f;
    const static float CONTROL_JUMP_VEL_X_THRESHOLD = 300.0f;
    const static float CONTROL_JUMP_VEL_Y_THRESHOLD = 800.0f;

    const static int CONTROL_LAST_PRESSED_NONE = 0;
    const static int CONTROL_LAST_PRESSED_LEFT = 1;
    const static int CONTROL_LAST_PRESSED_RIGHT = 2;

    class ControlsLayer : public BaseScreen {
    private:

        Tower *tower;

        Button pause_button;

        int last_pressed;
        int last_time_left;

        std::string last_time_left_s;

        FontRenderer font_renderer;

        Vec3 score_base_position;
        Vec3 score_position;
        Vec3 score_scale;

        Rect left_side;
        Rect right_side;

        SpineRenderer *spine_text_ready_go;
        SpineData *spine_text_ready_go_data;
        Vec3 spine_text_position;
        Vec3 spine_text_scale;

    protected:

        virtual void do_resize(FTE_ENV env) {

            BaseScreen::do_resize(env);

            float pw = get_screen_ratio() * 0.2f;
            float padding = get_screen_ratio() * 0.05f;

            float px = -0.5f * get_screen_width() + padding + pw * 0.5f;
            float py = 0.5f * get_screen_height() - padding - pw * 0.5f;

            pause_button.set_width(pw);
            pause_button.set_height(pw);
            pause_button.set_alpha(0.4f);
            pause_button.set_translate_xy(px, py);
            pause_button.update_rect();

            left_side.set(-0.5f * get_screen_width(), -0.5f * get_screen_height(), 0.0f, 0.5f * get_screen_height());
            right_side.set(0.0f, -0.5f * get_screen_height(), 0.5f * get_screen_width(), 0.5f * get_screen_height());

            score_scale.set(pw);
            score_base_position.set(0.5f * get_screen_width() - padding, 0.5f * get_screen_height() - padding - pw, 0.0f);
            score_position = score_base_position;

            spine_text_position.set(0.0f);
            spine_text_scale.set(pw * 1.5f);
        }

        void do_init_renderer(InitRendererCommand *command, FTE_ENV env) {
            TextureAtlasLoader *atlas = tower->get_controls_atlas(env);
            atlas->add_texture_descriptors(command);
        }

        virtual void do_init_once(FTE_ENV env) {
            BaseScreen::do_init_once(env);

            TextureAtlasLoader *atlas = tower->get_controls_atlas(env);
            pause_button.init(atlas, "pause", "pause_active");

            font_renderer.init(*atlas, *tower->get_default_font_loader(env), "default_font");

            std::wstring ws_ready;
            convert_string_to_wstring("Ready", ws_ready);

            std::wstring ws_go;
            convert_string_to_wstring("Go!", ws_go);

            std::map<std::string, Polygon *> polygons_map;

            Polygon *p_ready = PolygonBuiler::create_static_text(*atlas, *tower->get_default_font_loader(env), "default_font", ws_ready);
            polygons_map["ready"] = p_ready;

            Polygon *p_go = PolygonBuiler::create_static_text(*atlas, *tower->get_default_font_loader(env), "default_font", ws_go);
            polygons_map["go"] = p_go;

            spine_text_ready_go_data = new SpineData();
            spine_text_ready_go_data->initialize_custom("items/text_ready_go.atlas", "items/text_ready_go.json", SPINE_SCALE * tower->get_scale_factor_y(), false, polygons_map, env);
            spine_text_ready_go_data->set_save_original_colors(false);

            spine_text_ready_go = new SpineRenderer(spine_text_ready_go_data);
        }

        virtual void do_init(FTE_ENV env) {
        }

        bool process_pause_button(float delta, RenderCommand *command) {
            const TouchHelper &th = get_touch_helper();
            const Camera &camera = get_ortho_camera();

            bool button_active = pause_button.render_button(delta, command, th, camera, get_width(), get_height());
            if (th.is_back_pressed() || pause_button.is_button_tapped(th, camera, get_width(), get_height())) {
                tower->pause();
                button_active = true;
            }

            return button_active;
        }

        void process_game_controls() {
            const TouchHelper &th = get_touch_helper();
            const Camera &camera = get_ortho_camera();

            if (th.is_tap()) {
                tower->control_tap();
            } else if (th.is_down()) {

                switch (tower->get_control_type()) {
                    case CONTROL_TYPE_SLIDE: {
                        const Vec3 &v = th.get_vel();

                        if (v.x > CONTROL_JUMP_VEL_X_THRESHOLD && v.y < -CONTROL_JUMP_VEL_Y_THRESHOLD) {
                            tower->control_upper_right_pressed();
                        } else if (v.x < -CONTROL_JUMP_VEL_X_THRESHOLD && v.y < -CONTROL_JUMP_VEL_Y_THRESHOLD) {
                            tower->control_upper_left_pressed();
                        } else if (v.x > CONTROL_MOVE_VEL_X_THRESHOLD) {
                            tower->control_right_pressed();
                            last_pressed = CONTROL_LAST_PRESSED_RIGHT;
                        } else if (v.x < -CONTROL_MOVE_VEL_X_THRESHOLD) {
                            tower->control_left_pressed();
                            last_pressed = CONTROL_LAST_PRESSED_LEFT;
                        } else if (last_pressed == CONTROL_LAST_PRESSED_RIGHT) {
                            tower->control_right_pressed();
                        } else if (last_pressed == CONTROL_LAST_PRESSED_LEFT) {
                            tower->control_left_pressed();
                        }
                    }
                        break;

                    case CONTROL_TYPE_SCREEN_SIDES:
                        if (th.is_button_active(left_side, camera, get_width(), get_height()))
                            tower->control_left_pressed();
                        else if (th.is_button_active(right_side, camera, get_width(), get_height()))
                            tower->control_right_pressed();
                        break;
                }

            } else {
                tower->control_inactive();
                last_pressed = CONTROL_LAST_PRESSED_NONE;
            }
        }

        void render_time(float delta, RenderCommand *command) {

            bool changed = last_time_left != tower->get_time_left_i();
            if (changed) {
                last_time_left = tower->get_time_left_i();
                int m = last_time_left / 60;
                int s = last_time_left % 60;
                if (s < 0)
                    s = 0;
                last_time_left_s = to_string(m) + (s < 10 ? ":0" : ":") + to_string(s);

                Vec3 dim;
                font_renderer.measure_string(last_time_left_s, dim);
                score_position.x = score_base_position.x - dim.x * score_scale.x;
            }

            font_renderer.render_string(last_time_left_s, *command, score_position, score_scale);
        }

        virtual void do_update(float delta, RenderCommand *command) {

            const Camera &camera = get_ortho_camera();

            if (tower->get_state() != TOWER_STATE_FINISHED) {

                command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, camera.get_view_proj_matrix());
                {

                    if (tower->get_state() == TOWER_STATE_IN_GAME_READY_GO) {
                        if (spine_text_ready_go->is_animation_complete())
                            spine_text_ready_go->set_animation("active", false);
                        spine_text_ready_go->update(delta, spine_text_position, VEC3_ZERO, spine_text_scale);
                        spine_text_ready_go->render(command);
                        if (spine_text_ready_go->is_animation_complete())
                            tower->change_state(TOWER_STATE_IN_GAME);
                    } else {
                        render_time(delta, command);

                        bool user_input_processed = process_pause_button(delta, command);
                        if (!user_input_processed)
                            process_game_controls();
                    }
                }
                command->end_batch();
            }

            BaseScreen::do_update(delta, command);
        }

        virtual void do_update_once(RenderCommand *command) {
        }

    public:

        ControlsLayer(Tower *tower) {
            this->tower = tower;
            this->last_pressed = CONTROL_LAST_PRESSED_NONE;
            this->last_time_left = -1;
            this->spine_text_ready_go = NULL;
        }

        ~ControlsLayer() {
            DELETE(spine_text_ready_go);
            DELETE(spine_text_ready_go_data);
        }

    };

}
// namespace ft

#endif /* ft_controls_H */
