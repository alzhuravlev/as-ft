#ifndef ft_test_anim_screen_H
#define ft_test_anim_screen_H

#include "engine.h"
#include "texture_atlas_loader.h"
#include "anim.h"
#include "frame_anim_polygon_cache.h"
#include "touch_helper.h"
#include "fte_math.h"

#include "ft_camera_state.h"
#include <string>

#include "ft_button.h"

using namespace fte;

namespace ft {

class TestAnimScreen: public Screen {
private:

	PerspectiveCamera camera;

	TouchHelper th;

	FrameAnimationPolygonCache cache;
	FrameAnimationExt frame_animation;

	Button duration_inc;
	Button duration_dec;

	Button button1;
	Button button2;
	Button button3;
	Button button4;
	Button button5;

	std::string animation1;
	std::string animation2;
	std::string animation3;
	std::string animation4;
	std::string animation5;

	FontRenderer font_renderer;

	std::string current_animation;
	std::string base_name;

	Vec3 text_scale;

	Vec3 saved_angles;
	float down_duration;
	float down_delay;

	void do_touch(TouchEvent events[], int size) {
		th.process(events, size);
	}

	void do_resize(FTE_ENV env) {
		float width = get_width();
		float height = get_height();

		camera.update(width, height, M_PI * 0.2f);
		camera.set_scale(std::min(width, height) / (2.0f * BASE_RADIUS));

		float bw = get_screen_ratio() * 0.2f;

		text_scale.set(bw * 0.5f);

		float lx = -get_screen_width() * 0.5f + bw * 0.5f;
		float rx = get_screen_width() * 0.5f - bw * 0.5f;
		float y = get_screen_height() * 0.5f - bw * 0.5f;

		duration_dec.set_width(bw);
		duration_dec.set_height(bw);
		duration_dec.set_translate_xy(lx, y);
		duration_dec.update_rect();

		duration_inc.set_width(bw);
		duration_inc.set_height(bw);
		duration_inc.set_translate_xy(rx, y);
		duration_inc.update_rect();

		y -= bw;

		button1.set_width(bw);
		button1.set_height(bw);
		button1.set_translate_xy(lx, y);
		button1.update_rect();

		y -= bw;

		button2.set_width(bw);
		button2.set_height(bw);
		button2.set_translate_xy(lx, y);
		button2.update_rect();

		y -= bw;

		button3.set_width(bw);
		button3.set_height(bw);
		button3.set_translate_xy(lx, y);
		button3.update_rect();

		y -= bw;

		button4.set_width(bw);
		button4.set_height(bw);
		button4.set_translate_xy(lx, y);
		button4.update_rect();

		y -= bw;

		button5.set_width(bw);
		button5.set_height(bw);
		button5.set_translate_xy(lx, y);
		button5.update_rect();

	}

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader atlas("game/items.atlas", env);
		atlas.add_texture_descriptors(command);
		command->add(ShaderDescriptor(SHADER_ID_PL, "shaders/pl/vs.glsl", "shaders/pl/fs.glsl"));
	}

	void do_init(FTE_ENV env) {
		float width = get_width();
		float height = get_height();
	}

	void init_micro_cat(FTE_ENV env) {

		base_name = "micro_cat";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 2, 151, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping_in", 2, 10, 0.2f));
		frame_animation.add_data(FrameAnimationExtData("sleeping", 11, 50, 1.5f));
		frame_animation.add_data(FrameAnimationExtData("sleeping_out", 51, 59, 0.2f));

		frame_animation.add_data(FrameAnimationExtData("waking", 120, 151, 0.9f));

		frame_animation.add_data(FrameAnimationExtData("active_in", 2, 10, 0.2f));
		frame_animation.add_data(FrameAnimationExtData("active", 11, 50, 0.8f));
		frame_animation.add_data(FrameAnimationExtData("active_out", 51, 59, 0.2f));

		frame_animation.add_data(FrameAnimationExtData("punch", 59, 76, 0.7f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 76, 120, 1.4f, true));

	}

	void init_micro_cow(FTE_ENV env) {

		base_name = "micro_cow";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "idle";
		animation2 = "move";
		animation3 = "jump";
		animation4 = "hit";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 2, 151, env);

		frame_animation.add_data(FrameAnimationExtData("idle_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("idle", 11, 50, 1.5f, true));
		frame_animation.add_data(FrameAnimationExtData("idle_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("move_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("move", 11, 50, 0.5f, true));
		frame_animation.add_data(FrameAnimationExtData("move_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("jump", 120, 151, 1.0f, true));
		frame_animation.add_data(FrameAnimationExtData("hit", 80, 109, 2.0f, true));
		frame_animation.add_data(FrameAnimationExtData("dying", 80, 109, 2.0f, true));

	}

	void init_micro_pig(FTE_ENV env) {

		base_name = "micro_pig";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 2, 151, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping_in", 2, 10, 0.2f));
		frame_animation.add_data(FrameAnimationExtData("sleeping", 11, 50, 1.5f));
		frame_animation.add_data(FrameAnimationExtData("sleeping_out", 51, 59, 0.2f));

		frame_animation.add_data(FrameAnimationExtData("waking", 120, 151, 0.9f));

		frame_animation.add_data(FrameAnimationExtData("active_in", 2, 10, 0.2f));
		frame_animation.add_data(FrameAnimationExtData("active", 11, 50, 0.8f));
		frame_animation.add_data(FrameAnimationExtData("active_out", 51, 59, 0.2f));

		frame_animation.add_data(FrameAnimationExtData("punch", 59, 76, 0.7f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 76, 120, 1.4f, true));

	}

	void init_micro_rooster(FTE_ENV env) {

		base_name = "micro_rooster";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "idle";
		animation2 = "move";
		animation3 = "jump";
		animation4 = "hit";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 2, 151, env);

		frame_animation.add_data(FrameAnimationExtData("idle_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("idle", 11, 50, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("idle_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("move_in", 2, 10, 0.1f));
		frame_animation.add_data(FrameAnimationExtData("move", 11, 50, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("move_out", 51, 59, 0.1f));

		frame_animation.add_data(FrameAnimationExtData("jump", 120, 151, 1.0f));

		frame_animation.add_data(FrameAnimationExtData("hit", 80, 109, 1.0f, true));
		frame_animation.add_data(FrameAnimationExtData("dying", 80, 109, 1.0f, true));

	}

	void init_micro_demon(FTE_ENV env) {

		base_name = "micro_demon";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 418, 517, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 642, 665, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 11, 65, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 151, 170, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 241, 276, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 418, 517, 4.5f));
		frame_animation.add_data(FrameAnimationExtData("waking", 642, 665, 1.0f, true));
		frame_animation.add_data(FrameAnimationExtData("active", 11, 65, 2.0f));
		frame_animation.add_data(FrameAnimationExtData("punch", 151, 170, 0.8f, true));
		frame_animation.add_data(FrameAnimationExtData("dying", 241, 276, 1.5f, true));

	}

	void init_micro_dragon(FTE_ENV env) {

		base_name = "micro_dragon";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 7, 60, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 311, 346, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 61, 80, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 141, 210, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 560, 650, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 560, 650, 3.5f));

		frame_animation.add_data(FrameAnimationExtData("waking", 141, 210, 2.0f));

		frame_animation.add_data(FrameAnimationExtData("active", 7, 60, 2.0f));

		frame_animation.add_data(FrameAnimationExtData("punch", 61, 80, 0.8f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 311, 346, 1.5f, true));

	}

	void init_micro_druid(FTE_ENV env) {

		base_name = "micro_druid";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 11, 70, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 110, 130, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 200, 230, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 290, 338, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 561, 584, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 11, 70, 2.3f));

		frame_animation.add_data(FrameAnimationExtData("waking", 110, 130, 2.0f));

		frame_animation.add_data(FrameAnimationExtData("active", 561, 584, 0.9f));

		frame_animation.add_data(FrameAnimationExtData("punch", 200, 230, 0.6f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 290, 338, 1.5f, true));

	}

	void init_micro_ghost(FTE_ENV env) {

		base_name = "micro_ghost";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 834, 866, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 318, 387, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 62, 138, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 238, 257, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 258, 277, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 834, 866, 0.8f));

		frame_animation.add_data(FrameAnimationExtData("waking", 318, 387, 2.5f));

		frame_animation.add_data(FrameAnimationExtData("active", 62, 138, 1.45f));

		frame_animation.add_data(FrameAnimationExtData("punch", 238, 257, 0.6f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 258, 277, 0.7f, true));

	}

	void init_micro_ghoul(FTE_ENV env) {

		base_name = "micro_ghoul";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 120, 239, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 542, 601, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 762, 802, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 444, 483, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 851, 894, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 120, 239, 4.0f));

		frame_animation.add_data(FrameAnimationExtData("waking", 542, 601, 2.5f));

		frame_animation.add_data(FrameAnimationExtData("active", 762, 802, 1.2f));

		frame_animation.add_data(FrameAnimationExtData("punch", 444, 483, 0.9f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 851, 894, 1.5f, true));

	}

	void init_micro_knight(FTE_ENV env) {

		base_name = "micro_knight";
		TextureAtlasLoader atlas_loader("game/items.atlas", env);

		animation1 = "sleeping";
		animation2 = "waking";
		animation3 = "active";
		animation4 = "punch";
		animation5 = "dying";

		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 11, 70, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 241, 288, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 573, 596, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 200, 230, env);
		cache.add_to_cache_polygon_direct_load("items/" + base_name + "/", atlas_loader, base_name, base_name, 290, 338, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 11, 70, 3.0f));

		frame_animation.add_data(FrameAnimationExtData("waking", 241, 288, 2.22f));

		frame_animation.add_data(FrameAnimationExtData("active", 573, 596, 0.9f));

		frame_animation.add_data(FrameAnimationExtData("punch", 200, 230, 0.9f, true));

		frame_animation.add_data(FrameAnimationExtData("dying", 290, 338, 2.0f, true));

	}

	void do_init_once(FTE_ENV env) {
		float width = get_width();
		float height = get_height();

		FontLoader font_loader("fonts/default_font.fnt", env);

		font_renderer.init(font_loader);

		duration_dec.init_with_text(NULL, &font_loader, "t:-:0.75", "t:-:0.99");
		duration_inc.init_with_text(NULL, &font_loader, "t:+:0.75", "t:+:0.99");

		current_animation = "";

//		init_micro_cat(env);
		init_micro_cow(env);
//		init_micro_pig(env);
//		init_micro_rooster(env);

//		init_micro_demon(env);
//		init_micro_dragon(env);
//		init_micro_druid(env);
//		init_micro_ghost(env);
//		init_micro_ghoul(env);
//		init_micro_knight(env);

		button1.init_with_text(NULL, &font_loader, "t:" + animation1 + ":0.75", "t:" + animation1 + ":0.99");
		button2.init_with_text(NULL, &font_loader, "t:" + animation2 + ":0.75", "t:" + animation2 + ":0.99");
		button3.init_with_text(NULL, &font_loader, "t:" + animation3 + ":0.75", "t:" + animation3 + ":0.99");
		button4.init_with_text(NULL, &font_loader, "t:" + animation4 + ":0.75", "t:" + animation4 + ":0.99");
		button5.init_with_text(NULL, &font_loader, "t:" + animation5 + ":0.75", "t:" + animation5 + ":0.99");
	}

	void do_release(FTE_ENV env) {
	}

	void do_update_once(RenderCommand* command) {
	}

	void do_update(float delta, RenderCommand* command) {
		float width = get_width();
		float height = get_height();

		camera.update_view_proj_matrix();

		command->set_clear_color(0.8f, 0.7f, 1.0f, 1.0f);
		command->set_light_fog_color(Vec3(0.8f, 0.7f, 1.0f));
		command->set_light_position(Vec3(2.0f, 2.0f, 2.0f));
		command->set_light_ambient_light(0.2f);
		command->set_light_eye(Vec3(-camera.get_Z().x, -camera.get_Z().y, -camera.get_Z().z));
		command->set_light_specular_exp(256.0f);
		command->set_light_fog_a(1.0f);
		command->set_light_fog_b(1.0f);

		bool active = frame_animation.is_animation_playing();

		frame_animation.update(delta);
		Polygon* p = cache.get(base_name, frame_animation.get_current_frame());

		FrameAnimationExtData* current_animation_data = frame_animation.get_current_animation_data();

		if (current_animation_data)
			if (duration_dec.is_button_tapped(th, get_ortho_camera(), width, height)) {
				current_animation_data->duration -= 0.01f;
			} else if (duration_inc.is_button_tapped(th, get_ortho_camera(), width, height)) {
				current_animation_data->duration += 0.01f;
			}

		if (button1.is_button_tapped(th, get_ortho_camera(), width, height)) {
			frame_animation.play(animation1, true);
		} else if (button2.is_button_tapped(th, get_ortho_camera(), width, height)) {
			frame_animation.play(animation2, true);
		} else if (button3.is_button_tapped(th, get_ortho_camera(), width, height)) {
			frame_animation.play(animation3, false);
		} else if (button4.is_button_tapped(th, get_ortho_camera(), width, height)) {
			frame_animation.play(animation4, false);
		} else if (button5.is_button_tapped(th, get_ortho_camera(), width, height)) {
			frame_animation.play(animation5, false);
		}

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, get_ortho_camera().get_view_proj_matrix());
		{
			button1.render_button(delta, command, th, get_ortho_camera(), width, height);
			button2.render_button(delta, command, th, get_ortho_camera(), width, height);
			button3.render_button(delta, command, th, get_ortho_camera(), width, height);
			button4.render_button(delta, command, th, get_ortho_camera(), width, height);
			button5.render_button(delta, command, th, get_ortho_camera(), width, height);

			bool duration_dec_active = duration_dec.render_button(delta, command, th, get_ortho_camera(), width, height);
			bool duration_inc_active = duration_inc.render_button(delta, command, th, get_ortho_camera(), width, height);

			if (current_animation_data)
				if (duration_dec_active || duration_inc_active) {
					down_duration += delta;
					if (down_delay > 0.1f)
						down_delay -= delta;
					if (down_duration > down_delay) {
						down_duration = 0.0f;
						if (duration_dec_active)
							current_animation_data->duration -= 0.01f;
						else if (duration_inc_active)
							current_animation_data->duration += 0.01f;
					}
				}

			if (current_animation_data) {

				Vec3 tb, tp;

				std::string text = to_stringf(current_animation_data->duration);
				font_renderer.measure_string(text, tb);
				tb.mul(text_scale);
				tp.set(-tb.x * 0.5f, get_screen_height() * 0.5f - tb.y, 0.0f);
				font_renderer.render_string(text, *command, tp, text_scale);

				font_renderer.measure_string(current_animation_data->id, tb);
				tb.mul(text_scale);
				tp.set(-tb.x * 0.5f, get_screen_height() * 0.5f - tb.y * 2.1f, 0.0f);
				font_renderer.render_string(current_animation_data->id, *command, tp, text_scale);
			}

			Vec3 tb, tp;
			std::string active_str = active ? "true" : "false";
			font_renderer.measure_string(active_str, tb);
			tb.mul(text_scale);
			tp.set(-tb.x + get_screen_width() * 0.5f, -get_screen_height() * 0.5f, 0.0f);
			font_renderer.render_string(active_str, *command, tp, text_scale);

		}
		command->end_batch();

		command->begin_batch(SHADER_ID_PL, 0, true, true, camera.get_view_proj_matrix());
		{
			command->add(p);
		}
		command->end_batch();

		if (th.is_back_pressed()) {
		} else if (th.is_down()) {
			camera.set_angle_x(saved_angles.x + th.get_offset_y(width) * M_PI);
			camera.set_angle_y(saved_angles.y + th.get_offset_x(width) * M_PI);
		} else {
			saved_angles.x = camera.get_angle_x();
			saved_angles.y = camera.get_angle_y();
			down_duration = 0.0f;
			down_delay = 1.0f;
		}
		th.clear_state();
	}

public:
	TestAnimScreen() {
	}

	~TestAnimScreen() {
	}
}
;
} // namespace ft

#endif /* ft_test_anim_screen_H */
