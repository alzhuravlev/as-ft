#ifndef ft_tower_item_consts_H
#define ft_tower_item_consts_H

namespace ft {

typedef uint16_t CONTACT_TYPE;

const CONTACT_TYPE TYPE_GROUND = (1 << 0);

const CONTACT_TYPE TYPE_COLLECTABLE = (1 << 1);
const CONTACT_TYPE TYPE_BUTTON = (1 << 2);

const CONTACT_TYPE TYPE_LIFT = (1 << 3);
const CONTACT_TYPE TYPE_PIPE = (1 << 4);
const CONTACT_TYPE TYPE_LIFT_PIPE = (1 << 5);
const CONTACT_TYPE TYPE_DOOR = (1 << 6);
const CONTACT_TYPE TYPE_ZIPPER_FLY = (1 << 7);
const CONTACT_TYPE TYPE_STEP = (1 << 8);
const CONTACT_TYPE TYPE_STEP_SLIPPY = (1 << 9);
const CONTACT_TYPE TYPE_MONSTER = (1 << 10);
const CONTACT_TYPE TYPE_MAIN_CHARACTER = (1 << 11);
const CONTACT_TYPE TYPE_STEP_TOGGLER = (1 << 12);

const CONTACT_TYPE TYPE_STEPS_FILTER = (TYPE_LIFT | TYPE_STEP | TYPE_STEP_SLIPPY | TYPE_STEP_TOGGLER);
const CONTACT_TYPE TYPE_PLATFORMS_FILTER = (TYPE_STEPS_FILTER | TYPE_PIPE | TYPE_LIFT_PIPE);
const CONTACT_TYPE TYPE_MONSTERS_FILTER = (TYPE_MONSTER);

const CONTACT_TYPE TYPE_ALL_FILTER = 0xffff;

const int STEP_POINT_UP = 1;
const int STEP_POINT_MID = 2;
const int STEP_POINT_DOWN = 3;

const int CONTROL_UP_NONE = 0;
const int CONTROL_UP_DOOR = 1;
const int CONTROL_UP_LIFT = 2;

const static int KEY_COLOR_1 = 1;
const static int KEY_COLOR_2 = 2;
const static int KEY_COLOR_3 = 3;

const static int BUTTON_COLOR_1 = 1;
const static int BUTTON_COLOR_2 = 2;
const static int BUTTON_COLOR_3 = 3;

const static int CONTROL_TYPE_NONE = 0;
const static int CONTROL_TYPE_SLIDE = 1;
const static int CONTROL_TYPE_SCREEN_SIDES = 2;

const static int SUN_TYPE_PASSED = 0;
const static int SUN_TYPE_ALL_MOSQUITOS = 1;
const static int SUN_TYPE_ALL_BALLS = 2;

const static int STEP_SLIPPY_DIRECTION_NONE = 0;
const static int STEP_SLIPPY_DIRECTION_LEFT = 1;
const static int STEP_SLIPPY_DIRECTION_RIGHT = 2;

inline int validate_key_color(int key_color) {
	switch (key_color) {
	case KEY_COLOR_1:
	case KEY_COLOR_2:
	case KEY_COLOR_3:
		return key_color;
	}
	LOGE("fte: invalid key color %d!", key_color);
	return 0;
}

inline int validate_button_color(int button_color) {
	switch (button_color) {
	case BUTTON_COLOR_1:
	case BUTTON_COLOR_2:
	case BUTTON_COLOR_3:
		return button_color;
	}
	LOGE("fte: invalid button color %d!", button_color);
	return 0;
}

inline const std::string get_key_texture_name(int key_color) {
	switch (key_color) {
	case KEY_COLOR_1:
		return "key1";
	case KEY_COLOR_2:
		return "key2";
	case KEY_COLOR_3:
		return "key3";
	}
	return "unknown";
}

inline const std::string get_button_texture_name(int button_color) {
	switch (button_color) {
	case BUTTON_COLOR_1:
		return "button1";
	case BUTTON_COLOR_2:
		return "button2";
	case BUTTON_COLOR_3:
		return "button3";
	}
	return "unknown";
}

inline const std::string get_step_toggler_texture_name(int button_color) {
	switch (button_color) {
	case BUTTON_COLOR_1:
		return "steptoggler1";
	case BUTTON_COLOR_2:
		return "steptoggler2";
	case BUTTON_COLOR_3:
		return "steptoggler3";
	}
	return "unknown";
}

inline const std::string get_step_toggler_texture_name_side(int button_color) {
	switch (button_color) {
	case BUTTON_COLOR_1:
		return "steptogglerside1";
	case BUTTON_COLOR_2:
		return "steptogglerside2";
	case BUTTON_COLOR_3:
		return "steptogglerside3";
	}
	return "unknown";
}

} // namespace ft

#endif /* ft_tower_item_consts_H */
