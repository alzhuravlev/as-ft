#ifndef ft_base_door_item_H
#define ft_base_door_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "anim.h"
#include "ring_fade_anim.h"
#include "ft_door_effect.h"

namespace ft {

#define DOOR_CAN_CONTACT_WITH 		TYPE_MAIN_CHARACTER

const int DOOR_STATE_CLOSE = 0;
const int DOOR_STATE_OPEN = 1;
const int DOOR_STATE_OPENING = 2;
const int DOOR_STATE_CLOSING = 3;
const int DOOR_STATE_FINISHING = 4;
const int DOOR_STATE_FINISHED = 5;

// tuning

const float DOOR_PULSE_TIME = 0.7f;
const float DOOR_OPEN_TIME = 0.25f;
const float DOOR_CLOSE_TIME = 0.4f;

class BaseDoorItem: public TowerItem {
private:
	BaseDoorItem* opposite_door_item;

	AccelerateDecelerateInterpolator interpolator;

	FrameAnimation animation;

	DoorEffect door_effect;

	std::vector<Polygon*> pc;
	Polygon* pc_hole;

	Vec3 position_saved;
	float cull_radius;

	void create_body_col_row(Box2dLoader & box2d_loader, int col, int row) {
		b2BodyDef def;
		body = get_tower()->get_world()->CreateBody(&def);
		body->SetUserData(this);

		b2FixtureDef fd;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = DOOR_CAN_CONTACT_WITH;

		fd.isSensor = true;

		Box2dItem* item = box2d_loader.find_item("door_sensor");
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

		body->SetTransform(b2Vec2(col_to_map_x(col), row_to_map_y(row)), 0.0f);
	}

protected:

	virtual void do_release_polygons() {
		delete_vector_elements(pc);
		DELETE(pc_hole);
		door_effect.release();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
	}

	virtual void create_door_polygons(FTE_ENV env, const std::string & texture_name) {

		TextureAtlasLoader* atlas = get_tower()->get_items_atlas(env);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		position_saved.set(x, y, z);
		cull_radius = get_onscreen_dim() * 1.5f;

		//
		Sprite* hole_sprite = PolygonBuiler::create_sprite(*atlas, "door_hole", true, true);
		Sprite* frame_sprite = PolygonBuiler::create_sprite(*atlas, texture_name, true, true);

		const int smoothness = get_tower()->get_door_frame_smoothness();
		const int roundness = get_tower()->get_door_frame_roundness();
		const float radius = get_tower()->get_door_frame_radius();

		Rect door_rect1;
		get_tower()->build_door_rect(get_col(), get_row(), door_rect1);

		std::vector<std::vector<Vec3> > holes1;
		get_tower()->build_door_hole(door_rect1, holes1, false, smoothness, false);

		const std::vector<Vec3> & hole1 = holes1.front();

		const int STEPS = 120;

		float scale_from = 0.8f;
		float scale_to = 0.1f;
		const float STEP = (scale_to - scale_from) / STEPS;

		PolygonCache tmp;
		for (int i = 0; i < STEPS; i++) {

			float scale = scale_from + STEP * (i + 1);

			Rect door_rect2;
			get_tower()->build_door_rect(get_col(), get_row(), door_rect2, scale, scale);

			std::vector<std::vector<Vec3> > holes2;
			get_tower()->build_door_hole(door_rect2, holes2, false, smoothness, false);

			const std::vector<Vec3> & hole2 = holes2.front();

			create_semi_tube_around_path(&tmp, frame_sprite, roundness, radius_func_for_door_outer, radius_func_for_door_inner, hole1, hole2, true);

			Polygon* p = tmp.create_composite();
			p->set_preserve_vert_transform(true);
			tmp.reset();

			pc.push_back(p);

			if (!pc_hole) {
				create_shape(&tmp, hole_sprite, radius_func_for_door_inner, hole2);
				pc_hole = tmp.create_composite();
				pc_hole->set_preserve_vert_transform(true);
				tmp.reset();
			}

//			LOGD("created door (%d) with %d vertices", i, p->get_vert_count());
		}

		DELETE(frame_sprite);
		DELETE(hole_sprite);

		door_effect.set_params_color(get_door_effect_color());
		door_effect.set_params_xyz(x, y + get_onscreen_dim_2(), z);
		door_effect.set_params_scale(get_scale_for_particles());
		door_effect.init(get_tower()->get_particles_atlas(env), env);
	}

	virtual void do_reset_to_default() {
		change_state(DOOR_STATE_CLOSE, true);
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		create_body_col_row(box2d_loader, get_col(), get_row());
	}

	virtual void do_change_state(int state) {
		switch (state) {
		case DOOR_STATE_CLOSE:

			animation.set_pingpong(true);
			animation.set_frame_count(pc.size(), pc.size() - pc.size() / 10, pc.size() - 1);
			animation.set_duration(DOOR_PULSE_TIME);
			animation.start(false);
			break;

		case DOOR_STATE_OPEN:

			animation.set_pingpong(true);
			animation.set_frame_count(pc.size(), 0, pc.size() / 10);
			animation.set_duration(DOOR_PULSE_TIME);
			animation.start(true);
			break;

		case DOOR_STATE_CLOSING:

			animation.set_cycle(false);
			animation.set_pingpong(false);
			animation.set_frame_count(pc.size());
			animation.set_duration(DOOR_CLOSE_TIME);
			animation.start(true);

			door_effect.stop();
			break;

		case DOOR_STATE_OPENING:

			animation.set_cycle(false);
			animation.set_pingpong(false);
			animation.set_frame_count(pc.size());
			animation.set_duration(DOOR_OPEN_TIME);
			animation.start(false);

			door_effect.start();
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {

		animation.update(delta);
		door_effect.update(delta, camera);

		switch (get_state()) {
		case DOOR_STATE_CLOSE:
			break;
		case DOOR_STATE_OPEN:
			break;
		case DOOR_STATE_CLOSING:
			if (get_state_time() > DOOR_CLOSE_TIME)
				change_state(DOOR_STATE_CLOSE);
			break;
		case DOOR_STATE_OPENING:
			if (get_state_time() > DOOR_OPEN_TIME)
				change_state(DOOR_STATE_OPEN);
			break;
		}

		in_frustrum = camera.sphere_in_frustrum(position_saved.x, position_saved.y, position_saved.z, cull_radius);
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
		Polygon* p = pc[animation.get_current_frame()];
		command->add(p);

	}

	virtual void do_render_standalone(float delta, RenderCommand* command, const Camera & camera) {
		switch (get_state()) {
		case DOOR_STATE_OPEN:
		case DOOR_STATE_CLOSING:
		case DOOR_STATE_OPENING:
			command->begin_batch(SHADER_ID_DOOR_HOLE, 0, true, true, camera.get_view_proj_matrix());
			command->add(pc_hole);
			command->end_batch();
			break;
		}
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		door_effect.render(command);
	}

	virtual uint32_t get_door_effect_color() {
		return get_tower()->get_bubble_color_i();
	}

public:
	BaseDoorItem(Tower* tower, float col, float row, CONTACT_TYPE contact_type, float scale_factor = 1.0f) :
			TowerItem(tower, col, row, contact_type, DOOR_CAN_CONTACT_WITH, scale_factor) {
		opposite_door_item = NULL;
		pc_hole = NULL;
		float d = get_tower()->get_platform_gap() + get_tower()->get_platform_depth() * 0.5f;
		set_distance_from_base_radius(-d);
	}

	~BaseDoorItem() {
	}

	virtual void open_door() {
		switch (get_state()) {
		case DOOR_STATE_CLOSE: {

			float k = 1.0f - (float) animation.get_current_frame() / animation.get_frame_count();

			change_state(DOOR_STATE_OPENING);

			animation.update(DOOR_OPEN_TIME * k);
			set_state_time(DOOR_OPEN_TIME * k);
		}
			break;

		case DOOR_STATE_CLOSING: {

			float k = 1.0f - (float) animation.get_current_frame() / animation.get_frame_count();

			change_state(DOOR_STATE_OPENING);

			animation.update(DOOR_OPEN_TIME * k);
			set_state_time(DOOR_OPEN_TIME * k);
		}
			break;
		}
	}

	virtual void close_door() {
		switch (get_state()) {
		case DOOR_STATE_OPEN: {

			float k = (float) animation.get_current_frame() / animation.get_frame_count();

			change_state(DOOR_STATE_CLOSING);

			animation.update(DOOR_CLOSE_TIME * k);
			set_state_time(DOOR_CLOSE_TIME * k);
		}
			break;

		case DOOR_STATE_OPENING: {

			float k = (float) animation.get_current_frame() / animation.get_frame_count();

			change_state(DOOR_STATE_CLOSING);

			animation.update(DOOR_CLOSE_TIME * k);
			set_state_time(DOOR_CLOSE_TIME * k);
		}
			break;
		}
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}

	virtual bool is_need_to_render_standalone() {
		return true;
	}

	BaseDoorItem* get_opposite_door_item() {
		return opposite_door_item;
	}

	void set_opposite_door_item(BaseDoorItem* item) {
		this->opposite_door_item = item;
	}
};

}
// namespace ft

#endif /* ft_base_door_item_H */
