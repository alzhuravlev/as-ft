#ifndef ft_base_monster_item_H
#define ft_base_monster_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "ft_zzz_effect.h"
#include "ft_key_item.h"
#include "ft_monster_die_effect.h"
#include "ring_anim.h"
#include "ft_base_animated_character.h"

namespace ft {

#define MONSTER_CAN_CONTACT_WITH 		TYPE_GROUND | TYPE_MAIN_CHARACTER | TYPE_MONSTER | TYPE_PLATFORMS_FILTER

const static int MONSTER_STATE_DEFAULT = 0;
const static int MONSTER_STATE_SLEEPING = 1;
const static int MONSTER_STATE_WAKING = 2;
const static int MONSTER_STATE_ACTIVE = 3;
const static int MONSTER_STATE_PUNCH = 4;
const static int MONSTER_STATE_DYING = 6;
const static int MONSTER_STATE_DIED = 7;
const static int MONSTER_STATE_INACTIVE = 8;

const static int MONSTER_AGGRESSIVE_LEVEL_DEFAULT = 0;
const static int MONSTER_AGGRESSIVE_LEVEL_LOW = 1;
const static int MONSTER_AGGRESSIVE_LEVEL_MED = 2;
const static int MONSTER_AGGRESSIVE_LEVEL_HIGH = 3;

const static float MONSTER_PUNCH_INC_TIME_LEFT = -10.0f;
const static float MONSTER_DYING_INC_TIME_LEFT = 5.0f;

// tuning

const static float MONSTER_DISTANCE_FROM_POGO_TO_WAKEUP = 3.0f;

class BaseMonsterItem: public TowerItem {
private:
	int key_color;
	KeyItem* key_item;

	LinearInterpolatedValue angle_y;

	b2Body* mirror_body;

	int aggressive_level;

	ZzzEffect zzz_effect;
	MonsterDieEffect monster_die_effect;

	RingAnim punch_anim;

//	SpineRenderer* spine_text_punch;
//	SpineRenderer* spine_text_dying;

	Vec3 spine_text_position;

protected:

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		if (key_color != 0) {
			key_item = new KeyItem(get_tower(), 0, 0, key_color, true);
			items.push_back(key_item);
		}
	}

	virtual void do_release_polygons() {
//		DELETE(spine_text_punch);
//		DELETE(spine_text_dying);
		zzz_effect.release();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		zzz_effect.set_params_scale(get_scale_for_particles());
		zzz_effect.init(get_tower()->get_particles_atlas(env), env);
		zzz_effect.set_params_color(get_particle_color());

		const uint32_t pcolor = 0xff3600ff;

		monster_die_effect.set_params_scale(get_scale_for_particles());
		monster_die_effect.set_params_color(pcolor);
		monster_die_effect.init(get_tower()->get_particles_atlas(env), env);

		RingAnimConfig cfg;
		cfg.cycle = false;
		cfg.antialiasing = 0.0f;
		cfg.amplitude = 0.35f;
		cfg.waves_count = 1;
		cfg.point_count = 32;
		cfg.inner_radius = 0.2f;
		cfg.outer_radius = 1.4f;

		cfg.scale_start = get_tower()->get_scale_factor_y() * get_scale_factor() * 0.8f;
		cfg.scale_end = cfg.scale_start * 3.6f;

		cfg.alpha_start = 1.0f;
		cfg.alpha_end = 0.0f;

		punch_anim.init_with_texture(cfg, 0.7f, *get_tower()->get_particles_atlas(env), "ring", pcolor);

//		spine_text_punch = create_spine_text(to_string(int(MONSTER_PUNCH_INC_TIME_LEFT), true), 0x770000ff, env);
//		spine_text_dying = create_spine_text(to_string(int(MONSTER_DYING_INC_TIME_LEFT), true), 0x7700ff00, env);
	}

	virtual void do_reset_to_default() {
		change_state(MONSTER_STATE_SLEEPING, true);
//		spine_text_punch->clear_animation();
//		spine_text_dying->clear_animation();
	}

	void create_monster_body(Box2dLoader & box2d_loader, const std::string & shape_name, float gravity_scale, float density, float friction, float restitution) {
		b2BodyDef def;
		def.fixedRotation = true;
		def.type = b2_dynamicBody;
		def.gravityScale = gravity_scale;
		def.position = b2Vec2(col_to_map_x(), row_to_map_y());
		def.userData = this;
		body = get_tower()->get_world()->CreateBody(&def);
		mirror_body = get_tower()->get_world()->CreateBody(&def);

		b2FixtureDef fd;
		fd.density = density;
		fd.friction = friction;
		fd.restitution = restitution;
		fd.filter.categoryBits = contact_type;
		fd.filter.maskBits = contact_mask;

		Box2dItem* item = box2d_loader.find_item(shape_name);
		attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());
		attach_fixture_to_body(mirror_body, &fd, item, get_scale_factor(), get_scale_factor());
	}

	virtual void do_change_state(int state) {
		switch (state) {
		case MONSTER_STATE_SLEEPING:

			visible = true;
			body_active = true;

			body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
			body->SetLinearVelocity(b2Vec2_zero);
			body->SetLinearDamping(1000.0f);

			zzz_effect.set_params_count(3, 15);
			zzz_effect.set_params_vel(0.7f, 0.9f);
			zzz_effect.set_params_epsilon_angle(M_PI_32);
			zzz_effect.set_params_vector(Vec3(0.0f, 1.0f, 0.0f));
			zzz_effect.start();

			play_animation("sleeping", true);

			break;

		case MONSTER_STATE_WAKING:

//			reset_contact_mask(MONSTER_CAN_CONTACT_WITH);

			visible = true;
			body_active = true;

			body->SetLinearDamping(0.0f);

			zzz_effect.set_params_count(10, 20);
			zzz_effect.set_params_vel(1.3f, 3.5f);
			zzz_effect.set_params_epsilon_angle(M_PI_4);
			zzz_effect.set_params_vector(Vec3(0.0f, 1.0f, 0.0f));

			play_animation("waking", false);

			break;

		case MONSTER_STATE_ACTIVE:

//			reset_contact_mask(MONSTER_CAN_CONTACT_WITH);

			body->SetLinearDamping(0.0f);

			visible = true;
			body_active = true;

			zzz_effect.set_params_count(0, 0);
			zzz_effect.set_params_vel(1.3f, 2.5f);
			zzz_effect.set_params_epsilon_angle(M_PI_32);
			zzz_effect.set_params_vector(Vec3(0.0f, 1.0f, 0.0f));

			play_animation("active", true);

			break;

		case MONSTER_STATE_PUNCH: {

			body->SetLinearDamping(1.0f);

//			reset_contact_mask((TYPE_GROUND) | ((MONSTER_CAN_CONTACT_WITH) & ~((TYPE_MONSTERS_FILTER) | (TYPE_MAIN_CHARACTER))));

			play_animation("punch", false);

			punch_anim.start();

			get_tower()->inc_time_left(MONSTER_PUNCH_INC_TIME_LEFT);
//			spine_text_punch->clear_animation();
//			spine_text_punch->set_animation("active", false);
			float a;
			get_tower()->get_main_character_world_position(spine_text_position.x, spine_text_position.y, spine_text_position.z, a);

			get_tower()->play_monster_punch();
		}
			break;

		case MONSTER_STATE_DYING: {

//			reset_contact_mask((TYPE_GROUND) | ((MONSTER_CAN_CONTACT_WITH) & ~((TYPE_MONSTERS_FILTER) | (TYPE_MAIN_CHARACTER))));

			body->SetLinearDamping(1.0f);

			play_animation("dying", false);

			zzz_effect.set_params_count(5, 40);
			zzz_effect.set_params_vel(1.0f, 4.0f);
			zzz_effect.set_params_epsilon_angle(M_PI_32);
			zzz_effect.set_params_vector(Vec3(0.0f, 1.0f, 0.0f));
			zzz_effect.start(true);

			monster_die_effect.start(true);

			get_tower()->inc_time_left(MONSTER_DYING_INC_TIME_LEFT);
//			spine_text_dying->clear_animation();
//			spine_text_dying->set_animation("active", false);
			float a;
			get_tower()->get_main_character_world_position(spine_text_position.x, spine_text_position.y, spine_text_position.z, a);

			get_tower()->play_monster_hit();
		}
			break;

		case MONSTER_STATE_DIED:
			zzz_effect.stop();
			body_active = false;
			if (key_item) {
				const b2Vec2 & pos = get_map_position();
				key_item->show_at(pos.x, pos.y);
			}
			break;

		case MONSTER_STATE_INACTIVE:
			visible = false;
			active = false;
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		angle_y.update(delta);

		correct_body_pos(mirror_body);

//		if (!spine_text_punch->is_animation_complete())
//			spine_text_punch->update(delta, spine_text_position, Vec3(camera.get_angle_x(), camera.get_angle_y(), camera.get_angle_z()), Vec3(0.6f, 0.6f, 0.6f));
//
//		if (!spine_text_dying->is_animation_complete())
//			spine_text_dying->update(delta, spine_text_position, Vec3(camera.get_angle_x(), camera.get_angle_y(), camera.get_angle_z()), Vec3(0.6f, 0.6f, 0.6f));

		if (zzz_effect.is_active()) {
			zzz_effect.set_params_xyz(x, y + get_onscreen_dim(), z);
			zzz_effect.update(delta, camera);
		}

		if (punch_anim.is_active()) {
			punch_anim.update_polygon_rotation(Vec3(camera.get_angle_x(), camera.get_angle_y(), camera.get_angle_z()));
			punch_anim.update_polygon_position(Vec3(x, y + get_onscreen_dim_2(), z));
			punch_anim.update(delta);
		}

		if (monster_die_effect.is_active()) {
			monster_die_effect.set_params_xyz(x, y + get_onscreen_dim_2(), z);
			monster_die_effect.update(delta, camera);
		}

		switch (get_state()) {
		case MONSTER_STATE_SLEEPING: {
			float a = get_tower()->get_angle_y_to_look_at_main_character(angle);
			angle_y.set_alpha(4.0f);
			angle_y.set_target(a);

			b2Vec2 pos;
			get_tower()->get_main_character_map_position(pos);
			float main_character_item_map_y = pos.y;
			float me_map_y = get_map_position().y;

			if (std::abs(main_character_item_map_y - me_map_y) < MONSTER_DISTANCE_FROM_POGO_TO_WAKEUP)
				change_state(MONSTER_STATE_WAKING);
		}
			break;

		case MONSTER_STATE_WAKING: {
			float a = get_tower()->get_angle_y_to_look_at_main_character(angle);
			angle_y.set_alpha(4.0f);
			angle_y.set_target(a);

			if (!is_animation_playing())
				change_state(MONSTER_STATE_ACTIVE);
		}
			break;

		case MONSTER_STATE_ACTIVE: {
			float a = get_tower()->get_angle_y_to_look_at_main_character(angle);
			angle_y.set_alpha(4.0f);
			angle_y.set_target(a);
		}
			break;

		case MONSTER_STATE_PUNCH:
			if (!is_animation_playing())
				change_state(MONSTER_STATE_ACTIVE);
			break;

		case MONSTER_STATE_DYING:
			if (!is_animation_playing())
				change_state(MONSTER_STATE_DIED);
			break;

		case MONSTER_STATE_DIED:
			if (!zzz_effect.is_active() && !monster_die_effect.is_active() /*&& spine_text_dying->is_animation_complete()*/)
				change_state(MONSTER_STATE_INACTIVE);
			break;

		case MONSTER_STATE_INACTIVE:
			break;
		}
	}

	virtual void do_render_once(RenderCommand* command) {
	}

	void render_monster_once(TYPE_STATIC_BATCH_ID start_batch_id, RenderCommand* command) {
//		spine->render_static_batch(command, start_batch_id);
	}

	void render_monster(TYPE_STATIC_BATCH_ID start_batch_id, float delta, RenderCommand* command, const Camera & camera) {
//		spine->render_static_batch_no_data(command, start_batch_id, get_tower()->get_shader_id_model(), SHADER_ID_TOON_MODEL, true, true, camera);
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		zzz_effect.render(command);
		monster_die_effect.render(command);
		punch_anim.render(command);

//		if (!spine_text_punch->is_animation_complete()) {
//			spine_text_punch->render(command);
//		}
//
//		if (!spine_text_dying->is_animation_complete())
//			spine_text_dying->render(command);
	}

	virtual uint32_t get_particle_color() {
		return get_tower()->get_bubble_color_i();
	}

	float get_angle_y() {
		return angle_y.get_value();
	}

public:
	BaseMonsterItem(Tower* tower, float col, float row, int key_color = 0, float scale_factor = 1.0f, int aggressive_level = MONSTER_AGGRESSIVE_LEVEL_DEFAULT) :
			TowerItem(tower, col, row, TYPE_MONSTER, MONSTER_CAN_CONTACT_WITH, scale_factor) {
		this->mirror_body = NULL;
		this->key_color = key_color;
		this->key_item = NULL;
		this->aggressive_level = aggressive_level;
//		this->spine_text_punch = NULL;
//		this->spine_text_dying = NULL;
	}

	~BaseMonsterItem() {
	}

	virtual void monster_die() {
		switch (get_state()) {
		case MONSTER_STATE_ACTIVE:
		case MONSTER_STATE_WAKING:
			change_state(MONSTER_STATE_DYING);
			break;
		}
	}

	virtual void monster_punch() {
		switch (get_state()) {
		case MONSTER_STATE_ACTIVE:
		case MONSTER_STATE_WAKING:
			change_state(MONSTER_STATE_PUNCH);
			break;
		}
	}

	virtual bool is_monster_active() {
		switch (get_state()) {
		case MONSTER_STATE_DEFAULT:
		case MONSTER_STATE_SLEEPING:
		case MONSTER_STATE_WAKING:
		case MONSTER_STATE_ACTIVE:
		case MONSTER_STATE_PUNCH:
			return true;
			break;
		}
		return false;
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}

	virtual bool is_need_to_render_effects() {
		return true;
	}

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		if (my_fixture->GetBody() != this->body)
			return;

		if (other_fixture->GetFilterData().categoryBits == TYPE_GROUND) {
			monster_die();
			return;
		}
	}

	inline int get_aggressive_level() const {
		return aggressive_level;
	}
}
;

} // namespace ft

#endif /* ft_base_monster_item_H */
