#ifndef ft_micro_demon_keyframe_animated_character_H
#define ft_micro_demon_keyframe_animated_character_H

#include "ft_keyframe_animated_character.h"
#include "texture_atlas_loader.h"

namespace ft {

class MicroDemonKeyframeAnimatedCharacter: public KeyframeAnimatedCharacter {
private:
	std::string base_name;
protected:

	virtual const std::string & get_base_name() {
		return base_name;

	}

	virtual void init_frame_animation(FrameAnimationExt & frame_animation, TowerItem* item, FTE_ENV env) {
		base_name = "micro_demon";

		TextureAtlasLoader* atlas_loader = item->get_tower()->get_items_atlas(env);
		item->get_tower()->get_frame_animation_polygon_cache().add_to_cache_polygon_direct_load("items/micro_demon/", *atlas_loader, "micro_demon", "micro_demon", 11, 65, env);
		item->get_tower()->get_frame_animation_polygon_cache().add_to_cache_polygon_direct_load("items/micro_demon/", *atlas_loader, "micro_demon", "micro_demon", 172, 240, env);
		item->get_tower()->get_frame_animation_polygon_cache().add_to_cache_polygon_direct_load("items/micro_demon/", *atlas_loader, "micro_demon", "micro_demon", 152, 171, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping", 11, 65, 4.0f));
		frame_animation.add_data(FrameAnimationExtData("waking", 172, 240, 1.1f, true));
		frame_animation.add_data(FrameAnimationExtData("active", 11, 65, 2.0f));
		frame_animation.add_data(FrameAnimationExtData("punch", 152, 171, 0.9f, true));
		frame_animation.add_data(FrameAnimationExtData("dying", 172, 240, 1.5f, true));
	}

};

} // namespace ft

#endif /* ft_micro_demon_keyframe_animated_character_H */
