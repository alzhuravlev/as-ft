#ifndef ft_step_slippy_item_H
#define ft_step_slippy_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "ft_base_step_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"

namespace ft {

class StepSlippyItem: public BaseStepItem {
private:
	int dir;

protected:

	virtual void do_release_polygons() {
		BaseStepItem::do_release_polygons();
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseStepItem::do_create_polygons(cache_lighting, env);

		TextureAtlasLoader* atlas = get_tower()->get_tower_atlas(env);

		create_step_polygon(cache_lighting, *atlas, "stepslippy", "stepslippyside", "stepslippy_nm", "stepslippyside_nm", env, true, true);
	}

public:
	StepSlippyItem(Tower* tower, float col, float row, int dir) :
			BaseStepItem(tower, col, row, TYPE_STEP_SLIPPY) {
		this->dir = dir;
	}

	virtual bool is_need_to_update() {
		return false;
	}
};

} // namespace ft

#endif /* ft_step_slippy_item_H */
