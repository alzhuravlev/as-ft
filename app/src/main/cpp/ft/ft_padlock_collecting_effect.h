#ifndef ft_padlock_collecting_effect_H
#define ft_padlock_collecting_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower_item_consts.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class PadlockCollectingEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleStartingLinearVelocityStrategy_AroundVector particleStartingLinearVelocityStrategy_AroundVector;
	ParticleStartingColorResolverStrategy_Randomize particleColorResolverStrategy_Randomize;
	ParticleScaleResolverStrategy_AppearDisapear particleScaleResolverStrategy_AppearDisapear;
	ParticleLinearVelocityResolverStrategy_WithGravity particleLinearVelocityResolverStrategy_WithGravity;

	ObjModelLoader* model_loader;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 5;
		config.max_count = 5;

		config.min_delay = 0.0f;
		config.max_delay = 0.1f;

		config.min_life_time = 1.2;
		config.max_life_time = 1.5;

		particleScaleResolverStrategy_AppearDisapear.set_normal_scale(BASE_RADIUS * 0.07f);
		particleStartingLinearVelocityStrategy_AroundVector.set_scalar(BASE_RADIUS * 0.9f, BASE_RADIUS * 0.95f);
		particleLinearVelocityResolverStrategy_WithGravity.set_gravity(BASE_RADIUS * 3.0f);

		particleStartingLinearVelocityStrategy_AroundVector.set_epsilon_angle(M_PI_2);
		particleStartingLinearVelocityStrategy_AroundVector.set_vector(Vec3(0.0f, 1.0f, 0.0f));

		particleColorResolverStrategy_Randomize.set_color(0xff000000, 0xffffffff);

		pe.add_initial_strategy(&particleColorResolverStrategy_Randomize);
		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleStartingLinearVelocityStrategy_AroundVector);

		pe.add_strategy(&particleScaleResolverStrategy_AppearDisapear);
		pe.add_strategy(&particleLinearVelocityResolverStrategy_WithGravity);

		pe.init_with_model(config, *model_loader, "body");

		emitters.push_back(&pe);
	}

public:

	void init(ObjModelLoader* model_loader, FTE_ENV env) {
		this->model_loader = model_loader;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleStartingPositionStrategy_Sphere.set_radius(scale);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
	}
};

} // namespace ft

#endif /* ft_padlock_collecting_effect_H */
