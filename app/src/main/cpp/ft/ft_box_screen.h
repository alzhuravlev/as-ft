#ifndef ft_box_screen_H
#define ft_box_screen_H

#include "engine.h"
#include "command.h"
#include "camera.h"
#include "updater.h"
#include "screen.h"
#include "texture_atlas_loader.h"
#include "polygon_builder.h"
#include "ft_button.h"
#include "ft_box_loader.h"

#include "ft_base_screen.h"

#include <string>
#include <map>

using namespace fte;

namespace ft {

class BoxScreen: public BaseScreen {
private:

	const static std::string BoxScreen_current_box_index;

	Polygon* bg;

	Button back;

	std::vector<BoxItem> boxes;

	MUSIC_ID m_id;
	SOUND_ID s_id;

	std::vector<Polygon*> polygons;

	float target_offset;
	float down_offset;
	float current_offset;

	float box_width;
	float box_gap;

	float current_scale;
	float target_scale;

	int current_box_index;
	bool current_active;

	Rect box_rect;

	bool open_level;
	bool open_back;

protected:

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader texture_atlas(std::string("screens/box.atlas"), env);
		texture_atlas.add_texture_descriptors(command);
	}

	void do_init_once(FTE_ENV env) {
		BaseScreen::do_init_once(env);

		TextureAtlasLoader atlas_loader("screens/box.atlas", env);

		box_width = get_screen_ratio() * 0.7f;
		box_gap = box_width * 0.15f;
		box_rect.set(-box_width * 0.5f, -box_width * 0.5f, box_width * 0.5f, box_width * 0.5f);

		bg = PolygonBuiler::create_sprite(atlas_loader, "bg", true, false);
		bg->set_origin_to(ORIGIN_CENTER);

		back.init(&atlas_loader, "back_bg;back_icon:0.75", "back_bg_active;back_icon_active:0.75");

		const AudioManager & audio = get_updater()->get_audio();
		m_id = audio.create_music("sfx/congratulations.ogg");
		s_id = audio.create_sound("sfx/crump.ogg");
//		audio.play_music(m_id);

		BoxLoader bl("boxes.txt", env);
		bl.get_items(boxes);

		for (std::vector<BoxItem>::const_iterator it = boxes.begin(); it != boxes.end(); ++it) {
			const BoxItem & item = *it;

			Polygon* p = PolygonBuiler::create_sprite(atlas_loader, item.image, true, false);
			p->set_origin_to(ORIGIN_CENTER);
			p->set_scale(box_width);
			polygons.push_back(p);
		}

		open_level = false;
		open_back = false;

		current_box_index = get_pref().get_int_value(BoxScreen_current_box_index, 0);

		float w = box_width + box_gap;

		current_offset = -current_box_index * w;
		target_offset = current_offset;
		down_offset = current_offset;

		fade_in();
	}

	void do_init(FTE_ENV env) {
		current_active = false;
	}

	void do_resize(FTE_ENV env) {
		BaseScreen::do_resize(env);

		float width = get_width();
		float height = get_height();

		float w = get_screen_ratio() * 0.2f;
		float x = width > height ? -0.5f + w * 0.5f : -get_screen_ratio() * 0.5f + w * 0.5f;
		float y = width > height ? get_screen_ratio() * 0.5f - w * 0.5f : 0.5f - w * 0.5f;

		back.set_width(w);
		back.set_height(w);
		back.set_translate_xy(x, y);
		back.update_rect();
	}

	virtual void do_fade_out_done() {
		if (open_back)
			do_open_back();
		else if (open_level)
			do_open_levels();
	}

	void update_polygons(float delta) {
		float width = get_width();
		float height = get_height();

		float w = box_width + box_gap;

		const TouchHelper & th = get_touch_helper();

		if (get_state() == SCREEN_STATE_DEFAULT)
			if (th.is_down()) {
				current_active = !th.is_moved() && th.is_button_active(box_rect, get_ortho_camera(), width, height);
				float d = th.get_offset_x(width) * get_screen_ratio() * 3.4f;
				d = CLAMP(d, -w, w);
				target_offset = down_offset + d;
				target_offset = CLAMP(target_offset, -w * (polygons.size() - 1) - w * 0.3f, w * 0.3f);
			} else {
				current_active = false;
				down_offset = current_offset;
				current_box_index = (int) -roundf(target_offset / w);
				target_offset = -current_box_index * w;
			}

		current_offset = current_offset + (target_offset - current_offset) * delta * 8.0f;
		float x = current_offset;
		for (int i = 0; i < polygons.size(); i++) {
			Polygon* p = polygons[i];
			p->set_translate_x(x);
			float scale = (1.0f - fabsf(x) / w) * 0.2f + 1.0f;
			scale = CLAMP(scale, 0.8f, 1.2f);
			p->set_scale(scale * box_width);

			x += w;
		}
	}

	void do_update(float delta, RenderCommand* command) {
		const TouchHelper & th = get_touch_helper();
		const AudioManager & audio = get_updater()->get_audio();

		float width = get_width();
		float height = get_height();

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, get_ortho_camera().get_view_proj_matrix());
		{
			command->add(bg);

			back.render_button(delta, command, th, get_ortho_camera(), width, height);

			update_polygons(delta);
			command->add(polygons);
		}
		command->end_batch();

		if (get_state() == SCREEN_STATE_DEFAULT) {
			if (th.is_back_pressed() || back.is_button_tapped(th, get_ortho_camera(), width, height)) {
				audio.play_sound(s_id);
				open_back = true;
				fade_out();
			} else if (!th.is_moved() && th.is_button_tapped(box_rect, get_ortho_camera(), width, height)) {
				audio.play_sound(s_id);
				open_level = true;
				fade_out();
			}
		}

		BaseScreen::do_update(delta, command);
	}

	void do_release(FTE_ENV env) {
		get_pref().set_int_value(BoxScreen_current_box_index, current_box_index);
		BaseScreen::do_release(env);
	}

	void do_open_back();
	void do_open_levels();

public:
	BoxScreen() {
		bg = NULL;
	}

	~BoxScreen() {
		DELETE(bg);
		delete_vector_elements<Polygon*>(polygons);
	}
}
;

} // namespace ft

#endif /* ft_box_screen_H */
