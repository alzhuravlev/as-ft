#ifndef ft_pogo_spine_animated_character_H
#define ft_pogo_spine_animated_character_H

#include "ft_spine_animated_character.h"

namespace ft {

class PogoSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

protected:

	virtual void init_spine(TowerItem* item, SpineRenderer* spine, FTE_ENV env) {

		std::string base_name = "pogo";

		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/" + base_name + ".obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

		spine->initialize_textured(*items_atlas, *model_loader, "items/" + base_name + ".atlas", "items/" + base_name + ".json", base_name, item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y(), true, true, false, false, false,
				env);

		spine->set_mix("idle", "move", 0.3f);
		spine->set_mix("idle", "jump", 0.3f);
		spine->set_mix("idle", "hit", 0.3f);

		spine->set_mix("move", "idle", 0.3f);
		spine->set_mix("move", "jump", 0.3f);
		spine->set_mix("move", "hit", 0.3f);

		spine->set_mix("jump", "idle", 0.3f);
		spine->set_mix("jump", "move", 0.3f);
		spine->set_mix("jump", "hit", 0.3f);

		spine->set_mix("hit", "idle", 0.3f);
		spine->set_mix("hit", "hit", 0.3f);
	}

};

} // namespace ft

#endif /* ft_pogo_spine_animated_character_H */
