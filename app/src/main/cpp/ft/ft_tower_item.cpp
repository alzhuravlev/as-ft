#include "ft_tower.h"
#include "ft_tower_item.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"

namespace ft {

TowerItem::TowerItem(Tower* tower, float col, float row, CONTACT_TYPE contact_type, CONTACT_TYPE contact_mask, float scale_factor) {

	this->active = true;

	this->tower = tower;
	this->col = col;
	this->row = row;

	this->parent = NULL;
	this->body = NULL;

	this->body_active = true;
	this->visible = true;
	this->in_frustrum = true;

	this->animated_character = NULL;

	this->contact_type = contact_type;
	this->contact_mask = contact_mask;

	this->scale_factor = scale_factor;
	this->onscreen_dim = scale_factor * tower->get_scale_factor_y();
	this->onscreen_dim_2 = onscreen_dim * 0.5f;

	this->distance_from_base_radius_saved = 0.0f;
	this->map_position_saved.Set(-1000.0f, -1000.0f);
	this->world_position_saved.set(0.0f);
	this->world_position_angle_saved = 0.0f;

	this->distance_from_base_radius = tower->get_default_distance_from_base_radius();
}

TowerItem::~TowerItem() {
}

void TowerItem::do_update(float delta, const Camera & camera) {
}

void TowerItem::do_render_once(RenderCommand* command) {
}

void TowerItem::do_render(float delta, RenderCommand* command, const Camera & camera) {
}

void TowerItem::do_render_standalone(float delta, RenderCommand* command, const Camera & camera) {
}

void TowerItem::do_render_effects(float delta, RenderCommand* command) {
}

void TowerItem::do_render_tails(float delta, RenderCommand* command) {
}

float TowerItem::get_angle_x_relative_to_tower_surface() {
	float x, y, z, angle;
	get_world_position_from_map(x, y, z, angle);
	float tan = radius_derivative_func_y(y, angle);
	return -atanf(tan);
}

float TowerItem::get_angle_x() {
	return 0.0f;
}

float TowerItem::get_angle_y() {
	return 0.0f;
}

float TowerItem::get_angle_z() {
	return body ? body->GetAngle() : 0.0f;
}

BaseAnimatedCharacter* TowerItem::create_animated_character() {
	return NULL;
}

void TowerItem::do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
}

void TowerItem::do_release_polygons() {
}

void TowerItem::do_create_child_items(std::vector<TowerItem*> & items) {
}

void TowerItem::do_create_body(Box2dLoader & box2d_loader) {
}

void TowerItem::do_reset_to_default() {
}

void TowerItem::do_change_state(int state) {
}

void TowerItem::create_child_items(std::vector<TowerItem*> & items) {

	std::vector<TowerItem*> tmp;

	do_create_child_items(tmp);

	for (std::vector<TowerItem*>::const_iterator it = tmp.begin(); it != tmp.end(); ++it) {
		TowerItem* child = *it;
		child->parent = this;
	}

	children.insert(children.end(), tmp.begin(), tmp.end());
	items.insert(items.end(), tmp.begin(), tmp.end());
}

void TowerItem::create_body(Box2dLoader & box2d_loader) {
	do_create_body(box2d_loader);
}

void TowerItem::reset_to_default() {
	active = true;

	if (animated_character)
		animated_character->reset();

	do_reset_to_default();
}

void TowerItem::create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
	do_create_polygons(cache_lighting, env);

	animated_character = create_animated_character();
	if (animated_character)
		animated_character->init(this, env);
}

void TowerItem::release_polygons() {
	do_release_polygons();
	DELETE(animated_character);
}

void TowerItem::update(float delta, const Camera & camera) {
	if (body)
		body->SetActive(body_active);

	if (!active)
		return;

	update_state(delta);

	if (visible && (is_need_to_render_toon() || is_need_to_render_toon()))
		update_in_frustrum(camera);

	do_update(delta, camera);

	if (animated_character && in_frustrum && visible)
		animated_character->update(this, delta, camera);
}

bool TowerItem::is_need_to_update() {
	return true;
}

bool TowerItem::is_need_to_render_toon() {
	return false;
}

bool TowerItem::is_need_to_render_no_toon() {
	return false;
}

bool TowerItem::is_need_to_render_standalone() {
	return false;
}

bool TowerItem::is_need_to_render_effects() {
	return false;
}

bool TowerItem::is_need_to_render_tails() {
	return false;
}

const b2Vec2 & TowerItem::get_map_position() {
	if (body)
		return body->GetPosition();
	return b2Vec2_zero;
}

void TowerItem::monster_die() {
}

void TowerItem::monster_wakeup() {
}

void TowerItem::monster_punch() {
}

bool TowerItem::is_monster_active() {
	return false;
}

void TowerItem::control_inactive() {
}

void TowerItem::control_left_pressed() {
}

void TowerItem::control_upper_left_pressed() {
}

void TowerItem::control_right_pressed() {
}

void TowerItem::control_upper_right_pressed() {
}

void TowerItem::control_down_pressed() {
}

void TowerItem::control_tap() {
}

int TowerItem::get_control_type() {
	return CONTROL_TYPE_NONE;
}

void TowerItem::open_door() {
}

void TowerItem::close_door() {
}

void TowerItem::button_contact_begin() {
}

void TowerItem::button_contact_end() {
}

void TowerItem::toggle_button() {
}

bool TowerItem::can_walk_through_door() {
	return true;
}

int TowerItem::get_platform_temporary_group() {
	return 0;
}

void TowerItem::platform_contact_from_top_begin() {
}

void TowerItem::platform_contact_from_top_end() {
}

void TowerItem::toggle_lift() {
}

void TowerItem::tower_finished() {
}

void TowerItem::begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
}

void TowerItem::end_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
}

void TowerItem::pre_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
}

void TowerItem::post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
}

SpineRenderer* TowerItem::create_spine_text(const std::string & text, uint32_t color, FTE_ENV env) {
//	std::wstring ws;
//	convert_string_to_wstring(text, ws);
//
//	std::map<std::string, Polygon*> polygons_map;
//	Polygon* p_body = PolygonBuiler::create_static_text(*get_tower()->get_particles_atlas(env), *get_tower()->get_particles_font_loader(env), "particles_font", ws);
//	p_body->set_color(color);
//	polygons_map["body"] = p_body;
//
//	SpineRenderer* spine_text = new SpineRenderer;
//	spine_text->initialize_custom("items/text.atlas", "items/text.json", SPINE_SCALE * get_tower()->get_scale_factor_y(), false, polygons_map, env);
//
//	return spine_text;
	return NULL;
}

} // namespace ft
