#ifndef ft_door_effect_H
#define ft_door_effect_H

#include "anim.h"
#include "particle_effect.h"
#include "particle_strategy.h"
#include "ft_tower.h"

using namespace fte;

namespace ft {

class DoorEffect: public ParticleEffect {
private:

	ParticleEmitter pe;

	ParticleStartingPositionStrategy_Sphere particleStartingPositionStrategy_Sphere;
	ParticleStartingRotationStrategy_RandomizeAngularVelocity particleStartingRotationStrategy_RandomizeAngularVelocity;
	ParticleStartingLinearVelocityStrategy_AroundVector particleStartingLinearVelocityStrategy_AroundVector;
	ParticleStartingColorResolverStrategy_Simple particleStartingColorResolverStrategy_Simple;
	ParticleLinearVelocityResolverStrategy_Simple particleLinearVelocityResolverStrategy_Simple;
	ParticleRotationResolverStrategy_FaceToLinearVelocity particleRotationResolverStrategy_FaceToLinearVelocity;
	ParticleScaleXYResolverStrategy_WithInterpolator<DecelerateInterpolator> particleScaleXYResolverStrategy_WithInterpolator;
	ParticleAlphaResolverStrategy_AppearDisapearAndPulse particleAlphaResolverStrategy_AppearDisapearAndPulse;

	TextureAtlasLoader* atlas;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) {

		ParticleEmitterConfig config;

		config.min_count = 0;
		config.max_count = 50;

		config.min_delay = 0.0f;
		config.max_delay = 0.8f;

		config.min_life_time = 0.8f;
		config.max_life_time = 0.8f;

		particleStartingRotationStrategy_RandomizeAngularVelocity.set_angular_velocity(-M_PI, M_PI);

		particleStartingLinearVelocityStrategy_AroundVector.set_epsilon_angle(M_PI_16);
		particleStartingLinearVelocityStrategy_AroundVector.set_scalar(6.0f, 10.0f);

		particleLinearVelocityResolverStrategy_Simple.set_damping(2.0f);

		pe.add_initial_strategy(&particleStartingPositionStrategy_Sphere);
		pe.add_initial_strategy(&particleStartingRotationStrategy_RandomizeAngularVelocity);
		pe.add_initial_strategy(&particleStartingLinearVelocityStrategy_AroundVector);
		pe.add_initial_strategy(&particleStartingColorResolverStrategy_Simple);

		particleAlphaResolverStrategy_AppearDisapearAndPulse.set_time(0.4f, 0.2f);

		pe.add_strategy(&particleLinearVelocityResolverStrategy_Simple);
		pe.add_strategy(&particleScaleXYResolverStrategy_WithInterpolator);
		pe.add_strategy(&particleAlphaResolverStrategy_AppearDisapearAndPulse);
		pe.add_strategy(&particleRotationResolverStrategy_FaceToLinearVelocity);

		pe.init_with_sprite(config, *atlas, "ray", ORIGIN_BOTTOM_CENTER);

		emitters.push_back(&pe);
	}

public:

	void init(TextureAtlasLoader* atlas, FTE_ENV env) {
		this->atlas = atlas;
		ParticleEffect::init(env);
	}

	void set_params_scale(float scale) {
		particleScaleXYResolverStrategy_WithInterpolator.set_scale(scale * 0.05f, scale * 0.2f, scale * 0.8f, scale * 3.1f);
		particleStartingPositionStrategy_Sphere.set_radius(scale * 0.0f);
	}

	void set_params_xyz(float x, float y, float z) {
		Vec3 centroid(x, y, z);
		particleStartingPositionStrategy_Sphere.set_centroid(centroid);
		particleStartingLinearVelocityStrategy_AroundVector.set_vector(Vec3(x, 0.0f, z));
	}

	void set_params_color(uint32_t color) {
		particleStartingColorResolverStrategy_Simple.set_color(color);
	}
};

} // namespace ft

#endif /* ft_door_effect_H */
