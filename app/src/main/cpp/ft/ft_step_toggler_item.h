#ifndef ft_step_toggler_item_H
#define ft_step_toggler_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "ft_step_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "anim.h"

namespace ft {

const static int STATE_STEP_TOGGLER_ON = 0;
const static int STATE_STEP_TOGGLER_OFF = 1;
const static int STATE_STEP_TOGGLER_TURNING_ON = 2;
const static int STATE_STEP_TOGGLER_TURNING_OFF = 3;

const static float STEP_TOGGLER_TURNING_ON_DURATION = 0.33f;
const static float STEP_TOGGLER_TURNING_OFF_DURATION = 0.33f;

const static float STEP_TOGGLER_ON_ALPHA = 1.0f;
const static float STEP_TOGGLER_OFF_ALPHA = 0.3f;

class StepTogglerItem: public BaseStepItem {
private:
	Polygon* pc;
	int color;

	AlphaAnimation a_anim;

	void horror_blink() {
		float st = get_state_time();
		if (st > 4.0f) {
			st -= 4.0f;
			set_state_time(st);
		}
		bool off = (1.0f < st && st < 1.4f) || (1.7f < st && st < 1.75f) || (1.8f < st && st < 1.85f) || (1.9f < st && st < 1.95f) || (2.9f < st && st < 3.7f);
		pc->set_alpha(off ? 0.0f : STEP_TOGGLER_OFF_ALPHA);
	}

protected:

	virtual void do_release_polygons() {
		DELETE(pc);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		TextureAtlasLoader* items_atlas = get_tower()->get_items_atlas(env);

		std::string texture_name = get_step_toggler_texture_name(color);
		std::string texture_name_side = get_step_toggler_texture_name_side(color);

		PolygonCache tmp;
		create_step_polygon(&tmp, *items_atlas, texture_name, texture_name_side, "", "", env, true, true);
		pc = tmp.create_composite();
	}

	virtual void do_reset_to_default() {
		change_state(get_tower()->is_button_active(color) ? STATE_STEP_TOGGLER_ON : STATE_STEP_TOGGLER_OFF, true);
	}

	virtual void do_change_state(int state) {
		switch (state) {
		case STATE_STEP_TOGGLER_ON:
			pc->set_alpha(STEP_TOGGLER_ON_ALPHA);
			body_active = true;
			break;

		case STATE_STEP_TOGGLER_OFF:
			pc->set_alpha(STEP_TOGGLER_OFF_ALPHA);
			body_active = false;
			break;

		case STATE_STEP_TOGGLER_TURNING_ON:
			a_anim.set_duration(STEP_TOGGLER_TURNING_ON_DURATION);
			a_anim.set_alpha(pc->get_alpha(), STEP_TOGGLER_ON_ALPHA);
			a_anim.start();
			body_active = true;
			break;

		case STATE_STEP_TOGGLER_TURNING_OFF:
			a_anim.set_duration(STEP_TOGGLER_TURNING_OFF_DURATION);
			a_anim.set_alpha(pc->get_alpha(), STEP_TOGGLER_OFF_ALPHA);
			a_anim.start();
			body_active = false;
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		switch (get_state()) {
		case STATE_STEP_TOGGLER_ON:
			if (!get_tower()->is_button_active(color)) {
				change_state(STATE_STEP_TOGGLER_TURNING_OFF);
			}
			break;
		case STATE_STEP_TOGGLER_OFF:
			if (get_tower()->is_button_active(color)) {
				change_state(STATE_STEP_TOGGLER_TURNING_ON);
			}

			horror_blink();
			break;
		case STATE_STEP_TOGGLER_TURNING_ON:
			a_anim.update(pc, delta);
			if (!a_anim.is_active())
				change_state(STATE_STEP_TOGGLER_ON);
			break;
		case STATE_STEP_TOGGLER_TURNING_OFF:
			a_anim.update(pc, delta);
			if (!a_anim.is_active())
				change_state(STATE_STEP_TOGGLER_OFF);
			break;
		}
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
//		command->add(pc);
	}

public:
	StepTogglerItem(Tower* tower, float col, float row, int color) :
			BaseStepItem(tower, col, row, TYPE_STEP_TOGGLER) {
		this->color = color;
		this->pc = NULL;
	}

	virtual bool is_need_to_render_no_toon() {
		return true;
	}

	int get_platform_temporary_group() {
		return color + 1;
	}
};

} // namespace ft

#endif /* ft_step_toggler_item_H */
