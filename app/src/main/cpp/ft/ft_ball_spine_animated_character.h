#ifndef ft_ball_spine_animated_character_H
#define ft_ball_spine_animated_character_H

#include "ft_spine_animated_character.h"
#include "comet_tail.h"

namespace ft {

class BallSpineAnimatedCharacter: public SpineAnimatedCharacter {
private:

	int aggressive_level;
//	CometTail tail;

protected:

	void do_release() {
		SpineAnimatedCharacter::do_release();
//		tail.release();
	}

	void do_reset() {
		SpineAnimatedCharacter::do_reset();
//		tail.reset();
	}

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) {

		std::string base_name = "ball";

		ObjModelLoader* model_loader = item->get_tower()->get_loaders().get_obj_model_loader("items/" + base_name + ".obj", env);
		TextureAtlasLoader* items_atlas = item->get_tower()->get_items_atlas(env);

//		tail.init_textured(*item->get_tower()->get_particles_atlas(env), "tail", 16, get_tail_alpha(), item->get_onscreen_dim() * 0.01f, item->get_onscreen_dim() * 0.85f, 0xff0000ff);

		std::string texture_name;
		switch (aggressive_level) {
		case MONSTER_AGGRESSIVE_LEVEL_DEFAULT:
		case MONSTER_AGGRESSIVE_LEVEL_LOW:
			texture_name = base_name + "_low";
			break;
		case MONSTER_AGGRESSIVE_LEVEL_MED:
			texture_name = base_name + "_med";
			break;
		case MONSTER_AGGRESSIVE_LEVEL_HIGH:
			texture_name = base_name + "_high";
			break;
		}

		std::string anim_filename = "items/" + base_name + ".json";
		std::string atlas_filename = "items/" + base_name + ".atlas";
		float json_scale = item->get_scale_factor() * SPINE_SCALE * item->get_tower()->get_scale_factor_y();

		spine_data = new SpineData();
		spine_data->initialize_textured(*items_atlas, *model_loader, atlas_filename, anim_filename, texture_name, json_scale, true, true, false, false, false, env);

		spine_data->set_mix("sleeping", "waking", 0.3f);
		spine_data->set_mix("waking", "active", 0.3f);

		spine_data->set_mix("active", "dying", 0.3f);
		spine_data->set_mix("active", "punch", 0.3f);

		spine_data->set_mix("punch", "active", 0.3f);

		spine = new SpineRenderer(spine_data);
	}

	virtual void do_render_tails(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		switch (item->get_state()) {
		case MONSTER_STATE_ACTIVE:
			float x, y, z, angle;
			item->get_world_position_from_map(x, y, z, angle);
//			tail.update(delta, Vec3(x, y + item->get_onscreen_dim_2(), z), camera);
//			tail.render(command);
			break;
		}
	}

	float get_tail_alpha() {
		switch (aggressive_level) {
		case MONSTER_AGGRESSIVE_LEVEL_DEFAULT:
		case MONSTER_AGGRESSIVE_LEVEL_LOW:
			return 6.0f;
		case MONSTER_AGGRESSIVE_LEVEL_MED:
			return 8.0f;
		case MONSTER_AGGRESSIVE_LEVEL_HIGH:
			return 12.0f;
		}
		return 1.0f;
	}

public:

	BallSpineAnimatedCharacter(int aggressive_level) {
		this->aggressive_level = aggressive_level;
	}

};

} // namespace ft

#endif /* ft_ball_spine_animated_character_H */
