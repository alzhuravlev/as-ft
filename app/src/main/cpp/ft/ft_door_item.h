#ifndef ft_door_item_H
#define ft_door_item_H

#include "ft_tower.h"
#include "ft_base_door_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "polygon_builder.h"
#include "anim.h"

namespace ft {

class DoorItem: public BaseDoorItem {
private:

protected:

	virtual void do_release_polygons() {
		BaseDoorItem::do_release_polygons();
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		BaseDoorItem::do_create_child_items(items);

		float opposite_col = get_tower()->get_opposite_col(get_col());

		DoorItem* opposite_door_item = new DoorItem(get_tower(), opposite_col, get_row());

		items.push_back(opposite_door_item);

		set_opposite_door_item(opposite_door_item);
		opposite_door_item->set_opposite_door_item(this);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseDoorItem::do_create_polygons(cache_lighting, env);
		create_door_polygons(env, get_tower()->get_sprite_for_door());
	}

	virtual void do_change_state(int state) {
		BaseDoorItem::do_change_state(state);

		switch (state) {
		case DOOR_STATE_CLOSE:
			break;
		case DOOR_STATE_OPEN:
			break;
		case DOOR_STATE_CLOSING:
			break;
		case DOOR_STATE_OPENING:
			break;
		}
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseDoorItem::do_update(delta, camera);

	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		BaseDoorItem::do_render_effects(delta, command);
	}

public:
	DoorItem(Tower* tower, float col, float row) :
			BaseDoorItem(tower, col, row, TYPE_DOOR, tower->get_door_scale_y()) {
	}

	~DoorItem() {
	}
};

}
// namespace ft

#endif /* ft_door_item_H */
