#ifndef ft_micro_bat_keyframe_animated_character_H
#define ft_micro_bat_keyframe_animated_character_H

#include "ft_keyframe_animated_character.h"
#include "texture_atlas_loader.h"

namespace ft {

class MicroBatKeyframeAnimatedCharacter: public KeyframeAnimatedCharacter {
private:
	std::string base_name;
protected:

	virtual const std::string & get_base_name() {
		return base_name;

	}

	virtual void init_frame_animation(FrameAnimationExt & frame_animation, TowerItem* item, FTE_ENV env) {
		base_name = "micro_cat";

		TextureAtlasLoader* atlas_loader = item->get_tower()->get_items_atlas(env);
		item->get_tower()->get_frame_animation_polygon_cache().add_to_cache_polygon_direct_load("items/micro_cat/", *atlas_loader, "micro_cat", "micro_cat", 1, 151, env);

		frame_animation.add_data(FrameAnimationExtData("sleeping_in", 2, 10, 0.3f));
		frame_animation.add_data(FrameAnimationExtData("sleeping", 10, 49, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("sleeping_out", 49, 59, 0.3f));
		frame_animation.add_data(FrameAnimationExtData("waking", 120, 151, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("active_in", 2, 10, 0.3f));
		frame_animation.add_data(FrameAnimationExtData("active", 10, 49, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("active_out", 49, 59, 0.3f));
		frame_animation.add_data(FrameAnimationExtData("punch", 60, 76, 1.0f));
		frame_animation.add_data(FrameAnimationExtData("dying", 76, 120, 2.0f));
	}

};

} // namespace ft

#endif /* ft_micro_bat_keyframe_animated_character_H */
