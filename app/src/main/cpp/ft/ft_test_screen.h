#ifndef ft_test_screen_H
#define ft_test_screen_H

#include "engine.h"
#include "texture_atlas_loader.h"
#include "anim.h"
#include "touch_helper.h"
#include "fte_math.h"

#include "ft_camera_state.h"
#include "comet_tail.h"
#include <string>

using namespace fte;

namespace ft {

class TestScreen: public Screen {
private:

	PerspectiveCamera camera;

	TouchHelper th;

	FTE_ENV env;

	CometTail tail;

	LinearInterpolatedVec3 pos;

	void do_touch(TouchEvent events[], int size) {
		th.process(events, size);
	}

	void do_resize(FTE_ENV env) {
		float w = get_width();
		float h = get_height();

		camera.update(w, h, M_PI * 0.2f);
		camera.set_scale(std::min(w, h) / (2.0f * BASE_RADIUS));
	}

	void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
		TextureAtlasLoader atlas("particles/particles_game.atlas", env);
		atlas.add_texture_descriptors(command);
	}

	void do_init(FTE_ENV env) {
		this->env = env;

		float width = get_width();
		float height = get_height();
	}

	void do_init_once(FTE_ENV env) {

		float width = get_width();
		float height = get_height();

		TextureAtlasLoader atlas("particles/particles_game.atlas", env);

		tail.init_textured(atlas, "tail", 6);
		tail.update_height(0.1f, 0.4f);
		tail.update_alpha(1.0f);

		pos.set_target(Vec3(0.0f, 10.0f, 0.0f));
	}

	void do_release(FTE_ENV env) {
	}

	void do_update_once(RenderCommand* command) {
	}

	void do_update(float delta, RenderCommand* command) {
//		camera.set_axis_rotation(true);
//		camera.set_axis(0.0f, 1.0f, 0.0f, camera.get_axis_angle() + delta);
//		camera.set_angle_x(M_PI_8);
//		camera.set_angle_y(camera.get_angle_y() + delta);
		camera.update_view_proj_matrix();

		pos.update(delta);
		tail.update(delta, pos.get_current(), camera);

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, camera.get_view_proj_matrix());
		{
			tail.render(command);
		}
		command->end_batch();

		if (th.is_back_pressed())
			finish_application(env);
		else if (th.is_tap()) {
			float x, y, z;
			z = 0.5f;
			th.get_curr_xy(x, y);
			camera.unproject(x, y, z, get_width(), get_height());
			LOGD("%f %f %f", x, y, z);
			pos.set_target(Vec3(x, y, 0.0f));
		}
		th.clear_state();
	}

public:
	TestScreen() {
	}

	~TestScreen() {
	}
}
;
} // namespace ft

#endif /* ft_test_screen_H */
