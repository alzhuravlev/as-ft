#ifndef ft_base_button_item_H
#define ft_base_button_item_H

#include "polygon_cache.h"
#include "camera.h"
#include "command.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "ft_tower_item.h"
#include "ring_fade_anim.h"
#include "ft_spine_animated_character.h"

namespace ft {

#define BUTTON_CAN_CONTACT_WITH        TYPE_MAIN_CHARACTER

    const static int BUTTON_STATE_ON = 1;
    const static int BUTTON_STATE_OFF = 2;
    const static int BUTTON_STATE_TURNING_ON = 3;
    const static int BUTTON_STATE_TURNING_OFF = 4;

    class BaseButtonItem : public TowerItem {
    private:

        //SpineAnimatedCharacter spine;

        SpineData *spine_data;

    protected:
        SpineRenderer *spine;

        void create_button_body(Box2dLoader &box2d_loader, const std::string &shape_name) {
            b2BodyDef def;
            def.type = b2_staticBody;
            body = get_tower()->get_world()->CreateBody(&def);

            body->SetUserData(this);

            b2FixtureDef fd;
            fd.density = 10.0f;
            fd.friction = 0.0f;
            fd.restitution = 1.0f;
            fd.filter.categoryBits = contact_type;
            fd.filter.maskBits = contact_mask;
            fd.isSensor = true;

            Box2dItem *item = box2d_loader.find_item(shape_name);
            attach_fixture_to_body(body, &fd, item, get_scale_factor(), get_scale_factor());

            body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
        }

        virtual void do_create_polygons(PolygonCache *cache_lighting, FTE_ENV env) {
            Box2dLoader *particle_shape_loader = get_tower()->get_particles_shape_loader(env);

            float x, y, z, angle;
            get_world_position_from_map(x, y, z, angle);
        }

        void create_button_polygons(const std::string &base_name, FTE_ENV env) {
            create_button_polygons(base_name, base_name, base_name, env);
        }

        void create_button_polygons(const std::string &model_name, const std::string &spine_name, const std::string &texture_name, FTE_ENV env) {
            ObjModelLoader *model_loader = get_tower()->get_loaders().get_obj_model_loader("items/" + model_name + ".obj", env);
            TextureAtlasLoader *items_atlas = get_tower()->get_items_atlas(env);

            spine_data = new SpineData();
            spine_data->initialize_textured(*items_atlas, *model_loader, "items/" + spine_name + ".atlas", "items/" + spine_name + ".json", texture_name, get_scale_factor() * SPINE_SCALE * get_tower()->get_scale_factor_y(), true, true, false, false,
                                            false, env);

            spine_data->set_mix("on", "turn_off", 0.3f);
            spine_data->set_mix("off", "turn_on", 0.3f);
            spine_data->set_mix("turn_on", "on", 0.3f);
            spine_data->set_mix("turn_off", "off", 0.3f);

            spine = new SpineRenderer(spine_data);
        }

        virtual void do_release_polygons() {
            DELETE(spine);
            DELETE(spine_data);
        }

        virtual void do_reset_to_default() {
            body->SetTransform(b2Vec2(col_to_map_x(), row_to_map_y()), 0.0f);
        }

        virtual void do_change_state(int state) {
            switch (state) {
                case BUTTON_STATE_ON:
                    spine->set_animation("on", true);
                    break;
                case BUTTON_STATE_OFF:
                    spine->set_animation("off", true);
                    break;

                case BUTTON_STATE_TURNING_ON:
                    spine->set_animation("turn_on", false);
                    break;
                case BUTTON_STATE_TURNING_OFF:
                    spine->set_animation("turn_off", false);
                    break;
            }
        }

        virtual void do_update(float delta, const Camera &camera) {
//		update_spine_from_body(delta, spine, &camera, false);

            switch (get_state()) {
                case BUTTON_STATE_ON:
                    break;
                case BUTTON_STATE_OFF:
                    break;

                case BUTTON_STATE_TURNING_ON:
                    if (spine->is_animation_complete())
                        change_state(BUTTON_STATE_ON);
                    break;
                case BUTTON_STATE_TURNING_OFF:
                    if (spine->is_animation_complete())
                        change_state(BUTTON_STATE_OFF);
                    break;
            }
        }

        virtual void do_render(float delta, RenderCommand *command, const Camera &camera) {
            spine->render(command);
        }

        virtual void do_render_effects(float delta, RenderCommand *command) {
        }

    public:
        BaseButtonItem(Tower *tower, float col, float row, float scale_factor = 1.0f) :
                TowerItem(tower, col, row, TYPE_BUTTON, BUTTON_CAN_CONTACT_WITH, scale_factor) {
            this->set_distance_from_base_radius(0.0f);
            this->spine = NULL;
            this->spine_data = NULL;
        }

        virtual void button_contact_begin() {
        }

        virtual void button_contact_end() {
        }

        virtual void toggle_button() {
            switch (get_state()) {
                case BUTTON_STATE_ON:
                    change_state(BUTTON_STATE_TURNING_OFF);
                    break;
                case BUTTON_STATE_OFF:
                    change_state(BUTTON_STATE_TURNING_ON);
                    break;
            }
        }

        virtual bool is_need_to_render_toon() {
            return true;
        }
    };

} // namespace ft

#endif /* ft_base_button_item_H */
