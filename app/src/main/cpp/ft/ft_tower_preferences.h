#ifndef ft_tower_preferences_H
#define ft_tower_preferences_H

#include "preferences.h"

namespace ft {

class TowerPreferences {
private:

	static int get_int_value(const Preferences & pref, const std::string & tower_file_name, const std::string & key) {
		return pref.get_int_value(tower_file_name + "$$$" + key);
	}

	static float get_float_value(const Preferences & pref, const std::string & tower_file_name, const std::string & key, float default_value = 0.0f) {
		return pref.get_float_value(tower_file_name + "$$$" + key, default_value);
	}

	static std::string get_string_value(const Preferences & pref, const std::string & tower_file_name, const std::string & key) {
		return pref.get_string_value(tower_file_name + "$$$" + key);
	}

	static void set_int_value(Preferences & pref, const std::string & tower_file_name, const std::string & key, int value) {
		pref.set_int_value(tower_file_name + "$$$" + key, value);
	}

	static void set_float_value(Preferences & pref, const std::string & tower_file_name, const std::string & key, float value) {
		pref.set_float_value(tower_file_name + "$$$" + key, value);
	}

	static void set_string_value(Preferences & pref, const std::string & tower_file_name, const std::string & key, const std::string & value) {
		pref.set_string_value(tower_file_name + "$$$" + key, value);
	}

public:

	static float get_score(const Preferences & pref, const std::string & tower_file_name) {
		return get_float_value(pref, tower_file_name, "score", -1.0f);
	}

	static void set_score(Preferences & pref, const std::string & tower_file_name, float value) {
		set_float_value(pref, tower_file_name, "score", value);
	}

	static int get_box_state(Preferences & pref, int box_id) {
		return pref.get_int_value("box_state_" + to_string(box_id));
	}

	static void set_box_state(Preferences & pref, int box_id, int box_state) {
		return pref.set_int_value("box_state_" + to_string(box_id), box_state);
	}

	static int get_level_state(Preferences & pref, int box_id, int level_id) {
//		if (box_id == 1 && level_id == 0)
		return 1;
		return pref.get_int_value("level_state_" + to_string(box_id) + "_" + to_string(level_id));
	}

	static void set_level_state(Preferences & pref, int box_id, int level_id, int level_state) {
		return pref.set_int_value("level_state_" + to_string(box_id) + "_" + to_string(level_id), level_state);
	}
};

} // namespace ft

#endif /* ft_tower_preferences_H */
