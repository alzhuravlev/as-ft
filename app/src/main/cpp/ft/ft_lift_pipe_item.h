#ifndef ft_lift_pipe_item_H
#define ft_lift_pipe_item_H

#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "ft_base_pipe_item.h"

namespace ft {

class LiftPipeItem: public BasePipeItem {
private:
	std::vector<Polygon*> polygons;

protected:

	virtual void do_release_polygons() {
		BasePipeItem::do_release_polygons();
		delete_vector_elements<Polygon*>(polygons);
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BasePipeItem::do_create_polygons(cache_lighting, env);

		TextureAtlasLoader* items_atlas = get_tower()->get_items_atlas(env);
		create_pipe_polygons(items_atlas, "liftpipe", "", get_tower()->get_lift_pipe_roundness(), get_tower()->get_lift_pipe_radius(), get_tower()->get_lift_pipe_radius_amplitude(), get_tower()->get_lift_pipe_radius_cycles(), true, polygons);
	}

	virtual void do_reset_to_default() {
		BasePipeItem::do_reset_to_default();
//		visible = false;
//		body_active = false;
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BasePipeItem::do_create_body(box2d_loader);
		create_pipe_body(box2d_loader, "lift_pipe");
	}

	virtual void do_update(float delta, const Camera & camera) {
		BasePipeItem::do_update(delta, camera);

		float lift_y, my_y;
		parent->get_world_y(lift_y);
		get_world_y(my_y);

		float dy = lift_y - my_y;

//		LOGD("lift_y=%f my_y=%f dy=%f", lift_y, my_y, dy);

		visible = F_GR(dy, 0.0f);
		body_active = visible;

//		if (visible) {
//			if (dy < height) {
//				begin = polygons.begin();
//				end = polygons.begin();
//
//				while (F_GR(dy, 0.0f)) {
//					++end;
//					dy -= step;
//				}
//			}
//		}

//		if (visible) {
//			Polygon* p = polygons.front();
//			glm::mat4 m = glm::make_mat4(camera.get_view_proj_matrix2());
//			p->print_verts(&m);
//		}
	}

	virtual void do_render(float delta, RenderCommand* command, const Camera & camera) {
		command->add(polygons);
	}

public:
	LiftPipeItem(Tower* tower, float col, float row, float v1_scale, float v_step) :
			BasePipeItem(tower, col, row, 1, 1.0f / tower->get_pipe_smoonthness(), v1_scale, v_step, TYPE_LIFT_PIPE) {
	}

	~LiftPipeItem() {
	}

	virtual bool is_need_to_render_toon() {
		return true;
	}
};

} // namespace ft

#endif /* ft_lift_pipe_item_H */
