#ifndef ft_base_screen_H
#define ft_base_screen_H

#include "engine.h"
#include "command.h"
#include "screen.h"
#include "touch_helper.h"
#include "ft_fade_in.h"
#include "ft_fade_out.h"
#include "preferences.h"

#include <string>

using namespace fte;

namespace ft {

class BaseScreen: public Screen {
private:

	TouchHelper th;

	FadeIn fade_in_transition;
	FadeOut fade_out_transition;

	Preferences pref;

protected:

	void do_touch(TouchEvent events[], int size) {
		th.process(events, size);
	}

	void do_update(float delta, RenderCommand* command) {
		th.clear_state();
	}

	virtual void do_init_once(FTE_ENV env) {
		pref.load(env);
	}

	void do_release(FTE_ENV env) {
		pref.save(env);
	}

public:
	BaseScreen() :
			pref("prefs.txt") {
		set_fade_in(&fade_in_transition);
		set_fade_out(&fade_out_transition);
	}

	virtual ~BaseScreen() {
	}

	inline const TouchHelper & get_touch_helper() const {
		return th;
	}

	inline Preferences & get_pref() {
		return pref;
	}

};

} // namespace ft

#endif /* ft_base_screen_H */
