#ifndef ft_base_animated_character_H
#define ft_base_animated_character_H

#include "ft_tower_item.h"
#include "camera.h"
#include "fte_math.h"
#include "command.h"

namespace ft {

class BaseAnimatedCharacter {
protected:

	virtual void do_init(TowerItem* item, FTE_ENV env) = 0;
	virtual void do_release() = 0;
	virtual void do_reset() = 0;
	virtual void do_update(TowerItem* item, float delta, const Camera & camera) = 0;
	virtual void do_render(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) = 0;
	virtual void do_render_effects(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) = 0;
	virtual void do_render_tails(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) = 0;
	virtual bool do_is_animation_playing(const std::string & name) = 0;
	virtual void do_play(const std::string & name, bool loop) = 0;
	virtual void do_play_next(const std::string & name, bool loop) = 0;
	virtual void do_stop(bool immediate) = 0;

public:

	virtual ~BaseAnimatedCharacter() {
	}

	void init(TowerItem* item, FTE_ENV env) {
		do_init(item, env);
	}

	void release() {
		do_release();
	}

	void reset() {
		do_reset();
	}

	void update(TowerItem* item, float delta, const Camera & camera) {
		do_update(item, delta, camera);
	}

	void render(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		do_render(item, delta, command, camera);
	}

	void render_effects(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		do_render_effects(item, delta, command, camera);
	}

	void render_tails(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		do_render_tails(item, delta, command, camera);
	}

	bool is_animation_playing(const std::string & name) {
		return do_is_animation_playing(name);
	}

	bool is_animation_playing() {
		return is_animation_playing("");
	}

	void play(const std::string & name, bool loop) {
		do_play(name, loop);
	}

	void play_next(const std::string & name, bool loop) {
		do_play_next(name, loop);
	}

	void stop(bool immediate = false) {
		do_stop(immediate);
	}
};

} // namespace ft

#endif /* ft_base_animated_character_H */
