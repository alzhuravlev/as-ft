#ifndef ft_pogo_item_H
#define ft_pogo_item_H

#include "ft_tower.h"
#include "ft_tower_item.h"
#include "texture_atlas_loader.h"
#include "polygon_cache.h"
#include "fte_utils_3d.h"
#include "touch_helper.h"
#include "ft_door_item.h"
#include "polygon_builder.h"
#include "ft_pogo_walk_effect.h"
#include "ft_zipper_fly_item.h"
#include "ft_base_mc_item.h"
#include "ft_micro_rooster_keyframe_animated_character.h"
#include "ft_micro_cat_keyframe_animated_character.h"
#include "ft_micro_cow_keyframe_animated_character.h"
#include "ft_micro_pig_keyframe_animated_character.h"

namespace ft {

class PogoItem: public BaseMainCharacterItem {
private:

	SOUND_ID walk_sound_id;
	SOUND_ID jump_sound_id;
	STREAM_ID walk_stream_id;

	PogoWalkEffect pogo_walk_effect;

protected:

	virtual void do_release_polygons() {
		BaseMainCharacterItem::do_release_polygons();
		pogo_walk_effect.release();
	}

	virtual BaseAnimatedCharacter* create_animated_character() {
		return new MicroCowKeyframeAnimatedCharacter;
	}

	virtual void do_create_polygons(PolygonCache* cache_lighting, FTE_ENV env) {
		BaseMainCharacterItem::do_create_polygons(cache_lighting, env);
		pogo_walk_effect.set_params_scale(get_scale_for_particles());
		pogo_walk_effect.set_params_color(get_tower()->get_bubble_color_i());
		pogo_walk_effect.init(get_tower()->get_particles_atlas(env), env);
	}

	virtual void do_reset_to_default() {
		BaseMainCharacterItem::do_reset_to_default();
	}

	virtual void do_create_child_items(std::vector<TowerItem*> & items) {
		BaseMainCharacterItem::do_create_child_items(items);
	}

	virtual void do_create_body(Box2dLoader & box2d_loader) {
		BaseMainCharacterItem::do_create_body(box2d_loader);

		create_mc_body(box2d_loader, "mc_pogo");
	}

	virtual void do_update(float delta, const Camera & camera) {
		BaseMainCharacterItem::do_update(delta, camera);

		float x, y, z, angle;
		get_world_position_from_map(x, y, z, angle);

		int min_count;
		int max_count;

		bool is_jump = get_state() == MC_STATE_JUMP_LEFT || get_state() == MC_STATE_JUMP_RIGHT;
		if (is_jump) {
			min_count = 3;
			max_count = 30;
		} else if (get_platform_on_bottom() > 0) {
			min_count = 3;
			max_count = 10;
		} else {
			min_count = 1;
			max_count = 5;
		}
		pogo_walk_effect.set_params_xyz(x, y, z);
		pogo_walk_effect.set_params_count(min_count, max_count);
		pogo_walk_effect.update(delta, camera);
	}

	virtual void do_render_effects(float delta, RenderCommand* command) {
		BaseMainCharacterItem::do_render_effects(delta, command);
		pogo_walk_effect.render(command);
	}

	virtual void do_change_state(int state) {
		BaseMainCharacterItem::do_change_state(state);

		switch (state) {
		case MC_STATE_JUMP_RIGHT:
		case MC_STATE_JUMP_LEFT:
		case MC_STATE_HIT:
			const b2Vec2 & vel = body->GetLinearVelocity();
			float jumpVel = calculateVerticalVelocityForHeight(get_tower()->get_pogo_jump_height()); //for 60fps
			body->SetLinearVelocity(b2Vec2(vel.x, jumpVel));
			break;
		}

		switch (state) {
		case MC_STATE_JUMP_RIGHT:
		case MC_STATE_JUMP_LEFT:
			get_tower()->get_audio().play_sound(jump_sound_id);
			break;
		}

		switch (state) {
		case MC_STATE_MOVE_LEFT:
		case MC_STATE_MOVE_RIGHT:
		case MC_STATE_JUMP_LEFT:
		case MC_STATE_JUMP_RIGHT:
		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
		case MC_STATE_WALK_INTO_DOOR:
		case MC_STATE_WALK_OUT_DOOR:
			pogo_walk_effect.start();
			break;
		default:
			pogo_walk_effect.stop();
			break;
		}

		switch (state) {
		case MC_STATE_MOVE_LEFT:
		case MC_STATE_MOVE_RIGHT:
		case MC_STATE_BEFORE_DOOR_MOVE_LEFT:
		case MC_STATE_BEFORE_DOOR_MOVE_RIGHT:
//			if (get_state_time() > 0.2f)
//				if (walk_stream_id == -1)
//					walk_stream_id = get_tower()->get_audio().play_sound(walk_sound_id, true);
			break;
		default:
//			if (walk_stream_id != -1) {
//				get_tower()->get_audio().stop_sound(walk_stream_id);
//				walk_stream_id = -1;
//			}
			break;
		}
	}

	virtual void change_vel(int dir) {
		const b2Vec2 & vel = body->GetLinearVelocity();
		float desired_vel_x;
		switch (dir) {
		case MC_CHANGE_VEL_LEFT:
//			desired_vel_x = b2Max(vel.x - get_tower()->get_pogo_move_accel_x(), get_platform_on_bottom() < 1 ? -get_tower()->get_pogo_jump_vel_x() : -get_tower()->get_pogo_move_vel_x());
			desired_vel_x = get_platform_on_bottom() < 1 ? -get_tower()->get_pogo_jump_vel_x() : -get_tower()->get_pogo_move_vel_x();
			break;
		case MC_CHANGE_VEL_RIGHT:
//			desired_vel_x = b2Min(vel.x + get_tower()->get_pogo_move_accel_x(), get_platform_on_bottom() < 1 ? get_tower()->get_pogo_jump_vel_x() : get_tower()->get_pogo_move_vel_x());
			desired_vel_x = get_platform_on_bottom() < 1 ? get_tower()->get_pogo_jump_vel_x() : get_tower()->get_pogo_move_vel_x();
			break;
		case MC_CHANGE_VEL_STOP:
			desired_vel_x = 0.0f;
			break;
		}
		float vel_change = (desired_vel_x - vel.x);
		float impulse = body->GetMass() * vel_change;
		body->ApplyLinearImpulse(b2Vec2(impulse, 0.0f), body->GetWorldCenter());
	}

public:
	PogoItem(Tower* tower, float col, float row) :
			BaseMainCharacterItem(tower, col, row, tower->get_pogo_scale()) {
		walk_sound_id = tower->get_audio().create_sound("sfx/walk.ogg");
		jump_sound_id = tower->get_audio().create_sound("sfx/jump.ogg");
		walk_stream_id = -1;
	}

	~PogoItem() {
	}

	int get_control_type() {
		return CONTROL_TYPE_SLIDE;
	}

	void control_inactive() {
		BaseMainCharacterItem::control_inactive();
	}

	void control_tap() {
		BaseMainCharacterItem::control_tap();
	}

	void control_upper_left_pressed() {
		BaseMainCharacterItem::control_upper_left_pressed();
	}

	void control_left_pressed() {
		BaseMainCharacterItem::control_left_pressed();
	}

	void control_upper_right_pressed() {
		BaseMainCharacterItem::control_upper_right_pressed();
	}

	void control_right_pressed() {
		BaseMainCharacterItem::control_right_pressed();
	}

	void control_down_pressed() {
		BaseMainCharacterItem::control_down_pressed();
	}

	virtual void begin_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		BaseMainCharacterItem::begin_contact(my_fixture, other_fixture, contact);
	}

	virtual void end_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact) {
		BaseMainCharacterItem::end_contact(my_fixture, other_fixture, contact);
	}

	virtual void pre_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		BaseMainCharacterItem::pre_solve_contact(my_fixture, other_fixture, contact, f);
	}

	virtual void post_solve_contact(b2Fixture* my_fixture, b2Fixture* other_fixture, b2Contact* contact, float f) {
		BaseMainCharacterItem::post_solve_contact(my_fixture, other_fixture, contact, f);
	}
}
;

} // namespace ft

#endif /* ft_pogo_item_H */
