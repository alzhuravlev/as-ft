#ifndef ft_spine_animated_character_H
#define ft_spine_animated_character_H

#include "ft_base_animated_character.h"
#include "spine_renderer.h"

namespace ft {

class SpineAnimatedCharacter: public BaseAnimatedCharacter {
private:

	SpineRenderer* spine;
	SpineData* spine_data;

protected:

	virtual void init_spine(TowerItem* item, SpineRenderer* & spine, SpineData* & spine_data, FTE_ENV env) = 0;

	void do_init(TowerItem* item, FTE_ENV env) {
		init_spine(item, spine, spine_data, env);
	}

	void do_release() {
		DELETE(spine);
		DELETE(spine_data);
	}

	void do_reset() {
	}

	void do_update(TowerItem* item, float delta, const Camera & camera) {
		float x, y, z, angle;
		item->get_world_position_from_map(x, y, z, angle);

		Vec3 translate(x, y, z);
		Vec3 rotate;
		Vec3 scale;

		float ax, ay, az;
		ax = item->get_angle_x();
		ay = item->get_angle_y();
		az = item->get_angle_z();

		rotate.x = ax;
		rotate.y = -angle + ay;
		rotate.z = cos_fast(ay) > 0.0f ? az : -az;

		scale.x = scale.y = scale.z = item->get_onscreen_dim();

		spine->update(delta, translate, rotate, scale);
	}

	void do_render(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
		spine->render(command);
	}

	void do_render_effects(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
	}

	void do_render_tails(TowerItem* item, float delta, RenderCommand* command, const Camera & camera) {
	}

	bool do_is_animation_playing(const std::string & name) {
		return !spine->is_animation_complete();
	}

	void do_play(const std::string & name, bool loop) {
		spine->set_animation(name, loop);
	}

	void do_play_next(const std::string & name, bool loop) {
		spine->add_animation(name, loop);
	}

	void do_stop(bool immediate) {
		spine->clear_animation();
	}

};

} // namespace ft

#endif /* ft_spine_animated_character_H */
