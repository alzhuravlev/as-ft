#ifndef command_H
#define command_H

#include "buffer.h"
#include "render_commandable.h"
#include "polygon.h"
#include "texture_descriptor.h"
#include "shader_descriptor.h"
#include "fte_math.h"

#include <vector>
#include <map>
#include <set>

namespace fte {

static const int COMMAND_NONE = 0;
static const int COMMAND_RENDER = 1;
static const int COMMAND_INIT_RENDERER = 2;
static const int COMMAND_RESIZE = 3;

static const float DEFAULT_TOON_WEIGHT = 0.015f;
static const float DEFAULT_TOON_WEIGHT_DELTA = 0.030f;

static const int SHADER_ID_DEFAULT_COLORED = -1;
static const int SHADER_ID_DEFAULT_TEXTURED = -2;

class Command;

typedef int TYPE_STATIC_BATCH_ID;

static const int MAX_BATCH_COUNT = 128;

struct RenderBatch {
	TextureDescriptor texture_descriptor;
	TextureDescriptor render_to_texture_descriptor;
	int vertex_size;
	int index_size;
	int vertex_offset;
	int index_offset;
	int num_components;
	int color_component_offset;
	int uv_component_offset;
	int normal_component_offset;
	int nm_uv_component_offset;
	int tangent_component_offset;
	int bitangent_component_offset;
	TYPE_STATIC_BATCH_ID static_batch_id;
	bool enable_depth;
	bool enable_cull_face;
	int shader_id;
	int shader_toon_id;
	bool use_view_proj_matrix;
	bool use_model_matrix;
	Matrix4x4 view_proj_matrix;
	Matrix4x4 model_matrix;
	Vec4 extra_param1;
	Vec4 extra_param2;
	Vec4 extra_param3;
	Vec4 extra_param4;
	Vec4 extra_param5;
	Vec4 extra_param6;
};

class ResizeCommand {
	friend class Command;
private:
	int w;
	int h;
public:
	void set_wh(int w, int h) {
		this->w = w;
		this->h = h;
	}

	void get_wh(int &w, int &h) const {
		w = this->w;
		h = this->h;
	}
};

class RenderCommand {
	friend class Command;
	friend class Renderer;

private:
	Buffer vertex_buffer;
	Buffer index_buffer;

	int vertex_counter;
	int vertex_offset;
	int index_offset;

	int static_batch_count;
	int static_batch_nodata_count;

	bool in_batch;

	std::vector<RenderBatch> batches;

	float clear_color_r;
	float clear_color_g;
	float clear_color_b;
	float clear_color_a;

	float toon_fog_color_r;
	float toon_fog_color_g;
	float toon_fog_color_b;

	float toon_fog_alpha_threshold;

	float toon_weight;
	float toon_weight_delta;

	Vec3 light_eye;
	Vec3 light_position;
	Vec3 light_direction;
	float light_ambient_light;
	float light_specular_exp;
	float light_attenuation;

	Vec3 light_fog_color;
	float light_fog_a;
	float light_fog_b;

	float light_spot_exp;
	float light_spot_cos_cutoff;

	float state_time;
	float vertex_vibration_freq;
	float vertex_vibration_amplitude;

private:
	RenderCommand() :
			vertex_buffer(1024 * 1024 * 2), index_buffer(1024 * 512) {

		clear_color_r = 0.0f;
		clear_color_g = 0.0f;
		clear_color_b = 0.0f;
		clear_color_a = 0.0f;

		toon_fog_color_r = 0.0f;
		toon_fog_color_g = 0.0f;
		toon_fog_color_b = 0.0f;

		toon_fog_alpha_threshold = 0.1f;

		toon_weight = DEFAULT_TOON_WEIGHT;
		toon_weight_delta = DEFAULT_TOON_WEIGHT_DELTA;

		state_time = 0.0f;
		vertex_vibration_amplitude = 0.0f;

		vertex_counter = 0;
	}

	void reset() {
		batches.clear();

		vertex_counter = 0;
		vertex_offset = 0;
		index_offset = 0;

		static_batch_count = 0;
		static_batch_nodata_count = 0;

		index_buffer.reset();
		vertex_buffer.reset();

		in_batch = false;
	}

	void release() {
		index_buffer.release();
		vertex_buffer.release();
	}

public:

	inline int get_batch_count() const {
		return batches.size();
	}

	const RenderBatch & get_batch(int index) const {
		return batches[index];
	}

	void begin_end_static_batch_nodata(TYPE_STATIC_BATCH_ID static_batch_id, int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const float* view_proj_matrix, const float* model_matrix = NULL) {
#ifdef DEBUG
		if (in_batch) {
			LOGE("fte: RenderCommand::begin_end_static_mask_batch: previous batch not ended! Skipped");
			return;
		}
#endif

		batches.push_back(RenderBatch());

		RenderBatch & current_batch = batches.back();

		current_batch.static_batch_id = static_batch_id;

		current_batch.shader_id = shader_id;
		current_batch.shader_toon_id = shader_toon_id;

		current_batch.enable_depth = enable_depth;
		current_batch.enable_cull_face = enable_cull_face;

		current_batch.use_view_proj_matrix = view_proj_matrix != NULL;
		if (current_batch.use_view_proj_matrix)
			current_batch.view_proj_matrix = *((Matrix4x4*) view_proj_matrix);

		current_batch.use_model_matrix = model_matrix != NULL;
		if (current_batch.use_model_matrix)
			current_batch.model_matrix = *((Matrix4x4*) model_matrix);

		current_batch.vertex_size = 0;
		current_batch.index_size = 0;

		static_batch_nodata_count++;
	}

	void begin_end_static_batch(RenderCommandableFixedIndexes* commandable, TYPE_STATIC_BATCH_ID static_data_id) {
#ifdef DEBUG
		if (in_batch) {
			LOGE("fte: RenderCommand::begin_end_static_batch: previous batch not ended! Skipped");
			return;
		}

		if (!commandable) {
			LOGE("fte: RenderCommand::begin_end_static_batch: commandable == NULL");
			return;
		}

		if (!commandable->has_data()) {
			LOGE("fte: RenderCommand::begin_end_static_batch: commandable has no data");
			return;
		}
#endif
		batches.push_back(RenderBatch());

		RenderBatch & current_batch = batches.back();

		current_batch.num_components = commandable->get_num_components();
		current_batch.color_component_offset = commandable->get_color_component_offset();
		current_batch.uv_component_offset = commandable->get_uv_component_offset();
		current_batch.normal_component_offset = commandable->get_normal_component_offset();
		current_batch.nm_uv_component_offset = commandable->get_nm_uv_component_offset();
		current_batch.tangent_component_offset = commandable->get_tangent_component_offset();
		current_batch.bitangent_component_offset = commandable->get_bitangent_component_offset();
		current_batch.texture_descriptor = commandable->get_texture_descriptor();

		current_batch.vertex_offset = vertex_offset;
		current_batch.index_offset = index_offset;
		current_batch.static_batch_id = static_data_id;

		int vert_size;
		int ind_size;

		commandable->write_to_vert_buffer(&vertex_buffer, vert_size);
		commandable->write_to_index_buffer(&index_buffer, ind_size);

		current_batch.vertex_size = vert_size;
		current_batch.index_size = ind_size;

		vertex_offset += vert_size;
		index_offset += ind_size;

		static_batch_count++;
	}

	void begin_end_static_batch(RenderCommandableIncIndexes* commandable, TYPE_STATIC_BATCH_ID static_data_id) {
#ifdef DEBUG
		if (in_batch) {
			LOGE("fte: RenderCommand::begin_end_static_batch: previous batch not ended! Skipped");
			return;
		}

		if (!commandable) {
			LOGE("fte: RenderCommand::begin_end_static_batch: commandable == NULL");
			return;
		}

		if (commandable->get_vert_count() == 0) {
			LOGE("fte: RenderCommand::begin_end_static_batch: commandable->get_vert_count() == 0");
			return;
		}
#endif
		batches.push_back(RenderBatch());

		RenderBatch & current_batch = batches.back();

		current_batch.num_components = commandable->get_num_components();
		current_batch.color_component_offset = commandable->get_color_component_offset();
		current_batch.uv_component_offset = commandable->get_uv_component_offset();
		current_batch.normal_component_offset = commandable->get_normal_component_offset();
		current_batch.nm_uv_component_offset = commandable->get_nm_uv_component_offset();
		current_batch.tangent_component_offset = commandable->get_tangent_component_offset();
		current_batch.bitangent_component_offset = commandable->get_bitangent_component_offset();
		current_batch.texture_descriptor = commandable->get_texture_descriptor();

		current_batch.vertex_offset = vertex_offset;
		current_batch.index_offset = index_offset;
		current_batch.static_batch_id = static_data_id;

		int vert_size;
		int ind_size;

		commandable->write_to_vert_buffer(&vertex_buffer, vert_size);
		commandable->write_to_index_buffer(&index_buffer, 0, ind_size);

		current_batch.vertex_size = vert_size;
		current_batch.index_size = ind_size;

		vertex_offset += vert_size;
		index_offset += ind_size;

		static_batch_count++;
	}

	void begin_batch(int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const float* view_proj_matrix, const float* model_matrix = NULL) {

		if (in_batch) {
			LOGE("fte: RenderCommand::begin_batch: previous batch not ended! Skipped");
			return;
		}

		batches.push_back(RenderBatch());

		RenderBatch & current_batch = batches.back();

		current_batch.use_model_matrix = model_matrix != NULL;
		if (current_batch.use_model_matrix)
			current_batch.model_matrix = *((Matrix4x4*) model_matrix);

		current_batch.use_view_proj_matrix = view_proj_matrix != NULL;
		if (current_batch.use_view_proj_matrix)
			current_batch.view_proj_matrix = *((Matrix4x4*) view_proj_matrix);

		current_batch.enable_depth = enable_depth;
		current_batch.enable_cull_face = enable_cull_face;
		current_batch.shader_id = shader_id;
		current_batch.shader_toon_id = shader_toon_id;
		current_batch.static_batch_id = 0;

		current_batch.vertex_offset = vertex_offset;
		current_batch.index_offset = index_offset;
		current_batch.vertex_size = 0;
		current_batch.index_size = 0;

		vertex_counter = 0;

		in_batch = true;
	}

	void end_batch() {
		in_batch = false;
		RenderBatch & current_batch = batches.back();
		if (current_batch.vertex_size == 0)
			batches.pop_back();
	}

	void add(RenderCommandableIncIndexes** commandable, int size) {
		for (int i = 0; i != size; i++)
			add(commandable[i]);
	}

	void add(const std::vector<Polygon*>::iterator & begin, const std::vector<Polygon*>::iterator & end) {
		for (std::vector<Polygon*>::iterator it = begin; it != end; ++it) {
			Polygon* p = *it;
			add(p);
		}
	}

	void add(std::vector<Polygon*> & commandable) {
		for (std::vector<Polygon*>::iterator it = commandable.begin(), end = commandable.end(); it != end; ++it) {
			Polygon* p = *it;
			add(p);
		}
	}

	void set_render_to_texture_descriptor(const TextureDescriptor & render_to_texture_descriptor) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_render_to_texture_descriptor: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.render_to_texture_descriptor = render_to_texture_descriptor;
	}

	void set_extra_param1(const Vec4 & extra_param1) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param1: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param1 = extra_param1;
	}

	void set_extra_param2(const Vec4 & extra_param2) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param2: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param2 = extra_param2;
	}

	void set_extra_param3(const Vec4 & extra_param3) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param3: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param3 = extra_param3;
	}

	void set_extra_param4(const Vec4 & extra_param4) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param4: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param4 = extra_param4;
	}

	void set_extra_param5(const Vec4 & extra_param5) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param5: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param5 = extra_param5;
	}

	void set_extra_param6(const Vec4 & extra_param6) {
		if (!in_batch) {
			LOGE("fte: RenderCommand::set_extra_param6: batch not begins! Skipped");
			return;
		}
		RenderBatch & current_batch = batches.back();
		current_batch.extra_param6 = extra_param6;
	}

	void add(RenderCommandableIncIndexes* commandable) {

		if (!in_batch) {
			LOGE("fte: RenderCommand::add: batch not begins! Skipped");
			return;
		}

		if (!commandable) {
			return;
		}

		if (!commandable->is_enabled()) {
			return;
		}

		RenderBatch & current_batch = batches.back();

		if (current_batch.vertex_size == 0) {
			current_batch.num_components = commandable->get_num_components();
			current_batch.color_component_offset = commandable->get_color_component_offset();
			current_batch.uv_component_offset = commandable->get_uv_component_offset();
			current_batch.normal_component_offset = commandable->get_normal_component_offset();
			current_batch.texture_descriptor = commandable->get_texture_descriptor();
			current_batch.nm_uv_component_offset = commandable->get_nm_uv_component_offset();
			current_batch.tangent_component_offset = commandable->get_tangent_component_offset();
			current_batch.bitangent_component_offset = commandable->get_bitangent_component_offset();
		} else {
#ifdef DEBUG
			if (current_batch.num_components != commandable->get_num_components() || current_batch.color_component_offset != commandable->get_color_component_offset() || current_batch.uv_component_offset != commandable->get_uv_component_offset()
					|| current_batch.normal_component_offset != commandable->get_normal_component_offset() || current_batch.texture_descriptor != commandable->get_texture_descriptor()) {
				LOGE("fte: Command::add: unable to add to batch! Skip!");
				return;
			}
#endif
		}

		int vert_size;
		int ind_size;

		commandable->write_to_vert_buffer(&vertex_buffer, vert_size);
		commandable->write_to_index_buffer(&index_buffer, vertex_counter, ind_size);

		current_batch.vertex_size += vert_size;
		current_batch.index_size += ind_size;

		vertex_offset += vert_size;
		index_offset += ind_size;

		vertex_counter += commandable->get_vert_count();
	}

	inline void* get_vert_ptr(const RenderBatch & batch) const {
		char* c = (char*) vertex_buffer.get_data();
		c += batch.vertex_offset;
		return c;
	}

	inline void* get_ind_ptr(const RenderBatch & batch) const {
		char* c = (char*) index_buffer.get_data();
		c += batch.index_offset;
		return c;
	}

	inline void* get_vert_ptr() const {
		return vertex_buffer.get_data();
	}

	inline void* get_ind_ptr() const {
		return index_buffer.get_data();
	}

	inline uint32_t get_vert_size() const {
		return vertex_buffer.get_size();
	}

	inline uint32_t get_ind_size() const {
		return index_buffer.get_size();
	}

	inline int get_static_batch_count() const {
		return static_batch_count;
	}

	inline int get_static_batch_nodata_count() const {
		return static_batch_nodata_count;
	}

	inline void get_clear_color(float & r, float & g, float & b, float & a) const {
		r = clear_color_r;
		g = clear_color_g;
		b = clear_color_b;
		a = clear_color_a;
	}

	inline void set_clear_color(float r, float g, float b, float a) {
		clear_color_r = r;
		clear_color_g = g;
		clear_color_b = b;
		clear_color_a = a;
	}

	inline void get_toon_fog_color(float & r, float & g, float & b) const {
		r = toon_fog_color_r;
		g = toon_fog_color_g;
		b = toon_fog_color_b;
	}

	inline void set_toon_fog_color(float r, float g, float b) {
		toon_fog_color_r = r;
		toon_fog_color_g = g;
		toon_fog_color_b = b;
	}

	inline float get_toon_fog_alpha_threshold() const {
		return toon_fog_alpha_threshold;
	}

	inline void set_toon_fog_alpha_threshold(float toon_fog_alpha_threshold) {
		this->toon_fog_alpha_threshold = toon_fog_alpha_threshold;
	}

	inline void set_state_time(float state_time) {
		this->state_time = state_time;
	}

	inline float get_state_time() const {
		return this->state_time;
	}

	inline float get_light_spot_cos_cutoff() const {
		return this->light_spot_cos_cutoff;
	}

	inline void set_light_spot_cos_cutoff(float light_spot_cos_cutoff) {
		this->light_spot_cos_cutoff = light_spot_cos_cutoff;
	}

	inline float get_light_spot_exp() const {
		return this->light_spot_exp;
	}

	inline void set_light_spot_exp(float light_spot_exp) {
		this->light_spot_exp = light_spot_exp;
	}

	inline float get_light_fog_a() const {
		return this->light_fog_a;
	}

	inline void set_light_fog_a(float light_fog_a) {
		this->light_fog_a = light_fog_a;
	}

	inline float get_light_fog_b() const {
		return this->light_fog_b;
	}

	inline void set_light_fog_b(float light_fog_b) {
		this->light_fog_b = light_fog_b;
	}

	inline float get_light_attenuation() const {
		return this->light_attenuation;
	}

	inline void set_light_attenuation(float light_attenuation) {
		this->light_attenuation = light_attenuation;
	}

	inline float get_light_specular_exp() const {
		return this->light_specular_exp;
	}

	inline void set_light_specular_exp(float light_specular_exp) {
		this->light_specular_exp = light_specular_exp;
	}

	inline float get_light_ambient_light() const {
		return this->light_ambient_light;
	}

	inline void set_light_ambient_light(float light_ambient_light) {
		this->light_ambient_light = light_ambient_light;
	}

	inline const Vec3 & get_light_fog_color() const {
		return this->light_fog_color;
	}

	inline void set_light_fog_color(const Vec3 & light_fog_color) {
		this->light_fog_color = light_fog_color;
	}

	inline const Vec3 & get_light_direction() const {
		return this->light_direction;
	}

	inline void set_light_direction(const Vec3 & light_direction) {
		this->light_direction = light_direction;
	}

	inline const Vec3 & get_light_position() const {
		return this->light_position;
	}

	inline void set_light_position(const Vec3 & light_position) {
		this->light_position = light_position;
	}

	inline const Vec3 & get_light_eye() const {
		return this->light_eye;
	}

	inline void set_light_eye(const Vec3 & light_eye) {
		this->light_eye = light_eye;
	}

	inline void set_vertex_vibration_freq(float vertex_vibration_freq) {
		this->vertex_vibration_freq = vertex_vibration_freq;
	}

	inline float get_vertex_vibration_freq() const {
		return this->vertex_vibration_freq;
	}

	inline void set_vertex_vibration_amplitude(float vertex_vibration_amplitude) {
		this->vertex_vibration_amplitude = vertex_vibration_amplitude;
	}

	inline float get_vertex_vibration_amplitude() const {
		return this->vertex_vibration_amplitude;
	}

	inline void set_toon_weight(float toon_weight) {
		this->toon_weight = toon_weight;
	}

	inline void set_toon_weight_delta(float toon_weight_delta) {
		this->toon_weight_delta = toon_weight_delta;
	}

	inline float get_toon_weight() const {
		return this->toon_weight;
	}

	inline float get_toon_weight_delta() const {
		return this->toon_weight_delta;
	}
};

class InitRendererCommand {
	friend class Command;
private:

	std::set<TextureDescriptor> texture_descriptors;
	std::vector<ShaderDescriptor> shader_descriptors;

	InitRendererCommand() {
	}

	void reset() {
		texture_descriptors.clear();
		shader_descriptors.clear();
	}

	void release() {
		texture_descriptors.clear();
		shader_descriptors.clear();
	}

public:

	void get_texture_descriptors(std::vector<TextureDescriptor> & out) const {
		std::copy(texture_descriptors.begin(), texture_descriptors.end(), std::back_inserter(out));
	}

	void get_shader_descriptors(std::vector<ShaderDescriptor> & out) const {
		std::copy(shader_descriptors.begin(), shader_descriptors.end(), std::back_inserter(out));
	}

	void add(const TextureDescriptor & td) {
		texture_descriptors.insert(td);
	}

	void add(const ShaderDescriptor & sd) {
		shader_descriptors.push_back(sd);
	}
};

class Command {
	friend class RenderCommand;
	friend class InitRendererCommand;
private:
	RenderCommand render_command;
	InitRendererCommand init_renderer_command;
	ResizeCommand resize_command;
	int cmd;

public:

	Command() :
			cmd(COMMAND_NONE) {
	}

	~Command() {
		release();
	}

	void reset() {
		cmd = COMMAND_NONE;
		init_renderer_command.reset();
		render_command.reset();
	}

	ResizeCommand* produce_resize_command() {
		reset();
		cmd = COMMAND_RESIZE;
		return &resize_command;
	}

	const ResizeCommand* consume_resize_command() const {
		if (cmd != COMMAND_RESIZE) {
			LOGI("illegal usage of the resize command!");
			return NULL;
		}
		return &resize_command;
	}

	RenderCommand* produce_render_command() {
		reset();
		cmd = COMMAND_RENDER;
		return &render_command;
	}

	const RenderCommand* consume_render_command() const {
		if (cmd != COMMAND_RENDER) {
			LOGI("illegal usage of the render command!");
			return NULL;
		}
		return &render_command;
	}

	InitRendererCommand* produce_init_renderer_command() {
		reset();
		cmd = COMMAND_INIT_RENDERER;
		return &init_renderer_command;
	}

	const InitRendererCommand* consume_init_renderer_command() const {
		if (cmd != COMMAND_INIT_RENDERER) {
			LOGI("illegal usage of the init renderer command!");
			return NULL;
		}
		return &init_renderer_command;
	}

	int get_cmd_type() const {
		return cmd;
	}

	void release() {
		init_renderer_command.release();
		render_command.release();
	}

};

} // namespace fte

#endif // command_H
