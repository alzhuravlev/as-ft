#include "statable.h"

namespace fte {

Statable::~Statable() {
}

Statable::Statable() {
	this->state = STATE_DEFAULT;
	this->pending_state = STATE_DEFAULT;
	this->state_time = 0.0f;
	this->transition_time = 0.0f;
	this->in_transition = false;
}

void Statable::change_state(int state, bool force) {
	if (this->state != state || force) {
		do_change_state(state);
		this->state_time = 0.0f;
		this->state = state;
		this->transition_time = 0.0f;
		this->in_transition = false;
		this->pending_state = STATE_DEFAULT;
	}
}

void Statable::change_state_transition(int state, float transition_time) {
	this->transition_time = transition_time;
	this->in_transition = true;
	this->state_time = 0.0f;
	this->pending_state = state;
}

void Statable::update_state(float delta) {
	state_time += delta;

	if (in_transition)
		if (state_time > transition_time)
			change_state(pending_state);
}

} // namespace fte
