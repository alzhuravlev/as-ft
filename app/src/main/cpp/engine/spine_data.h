#ifndef spine_data_H
#define spine_data_H

#include "spine/spine.h"
#include "spine/extension.h"

#include "texture_atlas_loader.h"
#include "obj_model_loader.h"

#include <vector>
#include <map>

namespace fte {

class SpineRenderer;

class SpineData {
	friend class SpineRenderer;
private:

	spSkeletonData* skeleton_data;
	spSkeletonJson* skeleton_json;
	spAtlas* atlas;
	spAnimationStateData* animation_state_data;

	std::vector<Polygon*> polygons;

	bool save_original_colors;

public:
	SpineData();
	~SpineData();

	inline void set_save_original_colors(bool b) {
		this->save_original_colors = b;
	}

	inline bool get_save_original_colors() {
		return this->save_original_colors;
	}

	void initialize_textured(const TextureAtlasLoader & atlas_loader, const ObjModelLoader & model_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, const std::string texture_name, float json_scale,
			bool use_color, bool use_normal, bool invert_normals, bool face_normal, bool preserve_vert_transform, FTE_ENV env);
	void initialize(const ObjModelLoader & model_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, bool use_normal, bool invert_normals, bool face_normal, bool preserve_vert_transform,
			FTE_ENV env);

	void initialize_custom(const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, bool use_normal, const std::map<std::string, Polygon*> polygons_map, FTE_ENV env);

	void initialize_sprite(const TextureAtlasLoader & atlas_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, FTE_ENV env);

	void set_mix(const std::string & animation1, const std::string & animation2, float duration);

	float get_animation_duration(const std::string & animation);

	inline const std::vector<Polygon*> & get_polygons() const {
		return polygons;
	}
};

} // namespace fte

#endif /* spine_data_H */
