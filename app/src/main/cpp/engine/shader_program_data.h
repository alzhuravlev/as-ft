#ifndef shader_program_data_H
#define shader_program_data_H

#include "fte_gl.h"
#include "command.h"

namespace fte {

class ShaderProgramData {
protected:
	Matrix4x4 view_proj_matrix;
	Matrix4x4 model_matrix;

	bool use_view_proj_matrix;
	bool use_model_matrix;

	int stride;

	GLenum texture_unit;

	GLvoid* position_component_ptr;
	GLvoid* color_component_ptr;
	GLvoid* uv_component_ptr;
	GLvoid* normal_component_ptr;
	GLvoid* nm_uv_component_ptr;
	GLvoid* tangent_component_ptr;
	GLvoid* bitangent_component_ptr;

	GLvoid* index_ptr;
	GLsizei index_count;

	bool enable_depth;
	bool enable_cull_face;

	int shader_id;
	int shader_toon_id;

	Vec4 extra_param1;
	Vec4 extra_param2;
	Vec4 extra_param3;
	Vec4 extra_param4;
	Vec4 extra_param5;
	Vec4 extra_param6;

public:

	virtual ~ShaderProgramData() {
	}

	inline bool is_enable_depth() const {
		return enable_depth;
	}

	inline bool is_enable_cull_face() const {
		return enable_cull_face;
	}

	inline GLenum get_texture_unit() const {
		return texture_unit;
	}

	inline GLfloat* get_view_proj_matrix() const {
		return (GLfloat*) view_proj_matrix.m;
	}

	inline GLfloat* get_model_matrix() const {
		return (GLfloat*) model_matrix.m;
	}

	inline bool is_use_view_proj_matrix() const {
		return use_view_proj_matrix;
	}

	inline bool is_use_model_matrix() const {
		return use_model_matrix;
	}

	inline GLint get_stride() const {
		return stride;
	}

	inline GLvoid* get_position_component_ptr() const {
		return position_component_ptr;
	}

	inline GLvoid* get_color_component_ptr() const {
		return color_component_ptr;
	}

	inline GLvoid* get_uv_component_ptr() const {
		return uv_component_ptr;
	}

	inline GLvoid* get_normal_component_ptr() const {
		return normal_component_ptr;
	}

	inline GLvoid* get_nm_uv_component_ptr() const {
		return nm_uv_component_ptr;
	}

	inline GLvoid* get_tangent_component_ptr() const {
		return tangent_component_ptr;
	}

	inline GLvoid* get_bitangent_component_ptr() const {
		return bitangent_component_ptr;
	}

	inline GLvoid* get_index_ptr() const {
		return index_ptr;
	}

	inline GLsizei get_index_count() const {
		return index_count;
	}

	inline int get_shader_id() const {
		return shader_id;
	}

	inline int get_shader_toon_id() const {
		return shader_toon_id;
	}

	inline const Vec4 & get_extra_param1() const {
		return extra_param1;
	}

	inline const Vec4 & get_extra_param2() const {
		return extra_param2;
	}

	inline const Vec4 & get_extra_param3() const {
		return extra_param3;
	}

	inline const Vec4 & get_extra_param4() const {
		return extra_param4;
	}

	inline const Vec4 & get_extra_param5() const {
		return extra_param5;
	}

	inline const Vec4 & get_extra_param6() const {
		return extra_param6;
	}

	virtual void bind() const {
	}

	virtual void unbind() const {
	}

	virtual void init() {
	}

	virtual void release() {
	}
};

class ShaderProgramStaticData: public ShaderProgramData {
private:

public:

	void wrap(const RenderBatch & batch, const RenderCommand & command, GLenum texture_unit) {

		if (batch.vertex_size) {

			this->texture_unit = texture_unit;

			stride = batch.num_components * sizeof(float);

			uintptr_t vert_offset = batch.vertex_offset;
			uintptr_t ind_offset = batch.index_offset;

			position_component_ptr = (GLvoid*) (vert_offset + 0);
			color_component_ptr = !batch.color_component_offset ? 0 : (GLvoid*) (vert_offset + batch.color_component_offset * sizeof(float));
			uv_component_ptr = !batch.uv_component_offset ? 0 : (GLvoid*) (vert_offset + batch.uv_component_offset * sizeof(float));
			normal_component_ptr = !batch.normal_component_offset ? 0 : (GLvoid*) (vert_offset + batch.normal_component_offset * sizeof(float));
			nm_uv_component_ptr = !batch.nm_uv_component_offset ? 0 : (GLvoid*) (vert_offset + batch.nm_uv_component_offset * sizeof(float));
			tangent_component_ptr = !batch.tangent_component_offset ? 0 : (GLvoid*) (vert_offset + batch.tangent_component_offset * sizeof(float));
			bitangent_component_ptr = !batch.bitangent_component_offset ? 0 : (GLvoid*) (vert_offset + batch.bitangent_component_offset * sizeof(float));

			index_count = batch.index_size / sizeof(short);
			index_ptr = (GLvoid*) (ind_offset + 0);

			enable_depth = batch.enable_depth;
			enable_cull_face = batch.enable_cull_face;
		}

		enable_depth = batch.enable_depth;
		enable_cull_face = batch.enable_cull_face;
		shader_id = batch.shader_id;
		shader_toon_id = batch.shader_toon_id;

		use_view_proj_matrix = batch.use_view_proj_matrix;
		if (use_view_proj_matrix)
			view_proj_matrix = batch.view_proj_matrix;

		use_model_matrix = batch.use_model_matrix;
		if (use_model_matrix)
			model_matrix = batch.model_matrix;

		extra_param1 = batch.extra_param1;
		extra_param2 = batch.extra_param2;
		extra_param3 = batch.extra_param3;
		extra_param4 = batch.extra_param4;
		extra_param5 = batch.extra_param5;
		extra_param6 = batch.extra_param6;
	}

	void release() {
	}

	virtual void bind() const {
	}

	virtual void unbind() const {
	}
};

class ShaderProgramDynamicData: public ShaderProgramData {
public:

	void pre(const RenderCommand & command) {
	}

	void wrap(const RenderBatch & batch, const RenderCommand & command, GLenum texture_unit) {

		this->texture_unit = texture_unit;

		enable_depth = batch.enable_depth;
		enable_cull_face = batch.enable_cull_face;

		shader_id = batch.shader_id;
		shader_toon_id = batch.shader_toon_id;

		use_view_proj_matrix = batch.use_view_proj_matrix;
		if (use_view_proj_matrix)
			view_proj_matrix = batch.view_proj_matrix;

		use_model_matrix = batch.use_model_matrix;
		if (use_model_matrix)
			model_matrix = batch.model_matrix;

		stride = batch.num_components * sizeof(float);

		char* ptr = (char*) command.get_vert_ptr(batch);

		position_component_ptr = (GLvoid*) ptr;
		color_component_ptr = !batch.color_component_offset ? 0 : (GLvoid*) (ptr + batch.color_component_offset * sizeof(float));
		uv_component_ptr = !batch.uv_component_offset ? 0 : (GLvoid*) (ptr + batch.uv_component_offset * sizeof(float));
		normal_component_ptr = !batch.normal_component_offset ? 0 : (GLvoid*) (ptr + batch.normal_component_offset * sizeof(float));
		nm_uv_component_ptr = !batch.nm_uv_component_offset ? 0 : (GLvoid*) (ptr + batch.nm_uv_component_offset * sizeof(float));
		tangent_component_ptr = !batch.tangent_component_offset ? 0 : (GLvoid*) (ptr + batch.tangent_component_offset * sizeof(float));
		bitangent_component_ptr = !batch.bitangent_component_offset ? 0 : (GLvoid*) (ptr + batch.bitangent_component_offset * sizeof(float));

		index_count = batch.index_size / sizeof(short);
		index_ptr = command.get_ind_ptr(batch);

		extra_param1 = batch.extra_param1;
		extra_param2 = batch.extra_param2;
		extra_param3 = batch.extra_param3;
		extra_param4 = batch.extra_param4;
		extra_param5 = batch.extra_param5;
		extra_param6 = batch.extra_param6;
	}
};

class ShaderProgramDynamicVBOData: public ShaderProgramData {
private:

	GLuint buffers[2];

public:

	ShaderProgramDynamicVBOData() {
		buffers[0] = 0;
		buffers[1] = 0;
	}

	void pre(const RenderCommand & command) {
		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);

//		void* v_ptr = glMapBufferOES(GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
//		void* i_ptr = glMapBufferOES(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY_OES);
//
//		memcpy(v_ptr, command.get_vert_ptr(), command.get_vert_size());
//		memcpy(i_ptr, command.get_ind_ptr(), command.get_ind_size());
//
//		glUnmapBufferOES(GL_ARRAY_BUFFER);
//		glUnmapBufferOES(GL_ELEMENT_ARRAY_BUFFER);

//		glBufferSubData(GL_ARRAY_BUFFER, 0, command.get_vert_size(), command.get_vert_ptr());
//		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, command.get_ind_size(), command.get_ind_ptr());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	void wrap(const RenderBatch & batch, const RenderCommand & command, GLenum texture_unit) {

		this->texture_unit = texture_unit;

		shader_id = batch.shader_id;
		shader_toon_id = batch.shader_toon_id;

		enable_depth = batch.enable_depth;
		enable_cull_face = batch.enable_cull_face;

		use_view_proj_matrix = batch.use_view_proj_matrix;
		if (use_view_proj_matrix)
			view_proj_matrix = batch.view_proj_matrix;

		use_model_matrix = batch.use_model_matrix;
		if (use_model_matrix)
			model_matrix = batch.model_matrix;

		stride = batch.num_components * sizeof(float);

		position_component_ptr = (GLvoid*) ((uintptr_t) batch.vertex_offset + 0);
		color_component_ptr = !batch.color_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.color_component_offset * sizeof(float));
		uv_component_ptr = !batch.uv_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.uv_component_offset * sizeof(float));
		normal_component_ptr = !batch.normal_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.normal_component_offset * sizeof(float));
		nm_uv_component_ptr = !batch.nm_uv_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.nm_uv_component_offset * sizeof(float));
		tangent_component_ptr = !batch.tangent_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.tangent_component_offset * sizeof(float));
		bitangent_component_ptr = !batch.bitangent_component_offset ? 0 : (GLvoid*) (batch.vertex_offset + batch.bitangent_component_offset * sizeof(float));

		index_count = batch.index_size / sizeof(short);
		index_ptr = (GLvoid*) ((uintptr_t) batch.index_offset + 0);

		extra_param1 = batch.extra_param1;
		extra_param2 = batch.extra_param2;
		extra_param3 = batch.extra_param3;
		extra_param4 = batch.extra_param4;
		extra_param5 = batch.extra_param5;
		extra_param6 = batch.extra_param6;
	}

	virtual void init() {
		glGenBuffers(2, buffers);

		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);

		glBufferData(GL_ARRAY_BUFFER, 1024 * 1024 * 2, NULL, GL_DYNAMIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 1024 * 1024, NULL, GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	virtual void release() {
		glDeleteBuffers(2, buffers);
		buffers[0] = 0;
		buffers[1] = 0;
	}

	virtual void bind() const {
		glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
	}

	virtual void unbind() const {
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
};

} // namespace fte

#endif /* shader_program_data_H */
