#include "gjk.h"
#include "math.h"
#include "vector"
#include "fte_math.h"
#include "stdlib.h"
#include "external.h"

namespace fte {

/// <summary>
/// Implements the Gilbert-Johnson-Keerthi algorithm for collision detection in 3D, as described in the video lecture at http://mollyrocket.com/849
/// See also http://www.cse.ttu.edu.tw/~jmchen/compg/fall04/students-presentation/GJK.pdf
/// </summary>

//to prevent infinite loops - if an intersection is not found in 20 rounds, consider there is no intersection
const int MaxIterations = 20;

/// <summary>
/// Finds the farthest point along a given direction of a convex polyhedron
/// </summary>
/// <param name="shape"></param>
/// <param name="direction"></param>
/// <returns></returns>
void MaxPointAlongDirection(float shape[], int vert_count, int stride, const Vec3 & direction, Vec3 & max) {
	max = *((Vec3*) &shape[0]);
	float max_dot = vec_dot(max, direction);

	Vec3 point;
	for (int i = 0; i < vert_count; i++) {
		point = *((Vec3*) &shape[i * stride]);
		float point_dot = vec_dot(point, direction);
		if (max_dot < point_dot) {
			max = point;
			max_dot = point_dot;
		}
	}
}

/// <summary>
/// Finds the farthest point along a given direction of the Minkowski difference of two convex polyhedra.
/// Called Support in the video lecture: max(D.Ai) - max(-D.Bj)
/// </summary>
void MaxPointInMinkDiffAlongDir(float shape1[], int vert_count1, int stride1, float shape2[], int vert_count2, int stride2, const Vec3 & direction, Vec3 & res) {
	Vec3 max1, max2, neg_direction;

	neg_direction = direction;
	vec_neg(neg_direction);

	MaxPointAlongDirection(shape1, vert_count1, stride1, direction, max1);
	MaxPointAlongDirection(shape2, vert_count2, stride2, neg_direction, max2);

	vec_sub(max1, max2, res);
}

/// <summary>
/// Updates the current simplex and the direction in which to look for the origin. Called DoSimplex in the video lecture.
/// </summary>
/// <param name="simplex">A list of points in the current simplex. The last point in the list must be the last point added to the simplex</param>
/// <param name="direction"></param>
/// <returns></returns>
bool UpdateSimplexAndDirection(std::vector<Vec3> & simplex, Vec3 & direction) {

	//if the simplex is a line
	if (simplex.size() == 2) {

		//A is the point added last to the simplex
		Vec3 A = simplex[1];
		Vec3 B = simplex[0];

		Vec3 AB;
		vec_sub(B, A, AB);

		Vec3 AO;
		AO = A;
		vec_neg(AO);

		if (vec_dot(AB, AO) > 0) {
			vec_cross(AB, AO, direction);
			vec_cross(direction, AB, direction);
		} else {
			direction = AO;
		}
	}
	//if the simplex is a triangle
	else if (simplex.size() == 3) {

		//A is the point added last to the simplex
		Vec3 A = simplex[2];
		Vec3 B = simplex[1];
		Vec3 C = simplex[0];

		Vec3 AO;
		AO = A;
		vec_neg(AO);

		Vec3 AB;
		vec_sub(B, A, AB);

		Vec3 AC;
		vec_sub(C, A, AC);

		Vec3 ABC;
		vec_cross(AB, AC, ABC);

		Vec3 ABC_AC;
		vec_cross(ABC, AC, ABC_AC);

		if (vec_dot(ABC_AC, AO) > 0) {
			if (vec_dot(AC, AO) > 0) {
				simplex.clear();
				simplex.push_back(C);
				simplex.push_back(A);
				vec_cross(AC, AO, direction);
				vec_cross(direction, AC, direction);
			} else if (vec_dot(AB, AO) > 0) {
				simplex.clear();
				simplex.push_back(B);
				simplex.push_back(A);
				vec_cross(AB, AO, direction);
				vec_cross(direction, AB, direction);
			} else {
				simplex.clear();
				simplex.push_back(A);
				direction = AO;
			}
		} else {

			Vec3 AB_ABC;
			vec_cross(AB, ABC, AB_ABC);

			if (vec_dot(AB_ABC, AO) > 0) {
				if (vec_dot(AB, AO) > 0) {
					simplex.clear();
					simplex.push_back(B);
					simplex.push_back(A);
					vec_cross(AB, AO, direction);
					vec_cross(direction, AB, direction);
				} else {
					simplex.clear();
					simplex.push_back(A);
					direction = AO;
				}
			} else {
				if (vec_dot(ABC, AO) > 0) {
					//the simplex stays A, B, C
					direction = ABC;
				} else {
					simplex.clear();
					simplex.push_back(B);
					simplex.push_back(C);
					simplex.push_back(A);
					direction = ABC;
					vec_neg(direction);
				}
			}
		}
	}
	//if the simplex is a tetrahedron
	else //if (simplex.Count == 4)
	{
		//A is the point added last to the simplex
		Vec3 A = simplex[3];
		Vec3 B = simplex[2];
		Vec3 C = simplex[1];
		Vec3 D = simplex[0];

		Vec3 AO;
		AO = A;
		vec_neg(AO);

		Vec3 AB;
		vec_sub(B, A, AB);

		Vec3 AC;
		vec_sub(C, A, AC);

		Vec3 AD;
		vec_sub(D, A, AD);

		Vec3 ABC;
		vec_cross(AB, AC, ABC);

		Vec3 ACD;
		vec_cross(AC, AD, ACD);

		Vec3 ADB;
		vec_cross(AD, AB, ADB);

		//the side (positive or negative) of B, C and D relative to the planes of ACD, ADB and ABC respectively
		float BsideOnACD = vec_dot(ACD, AB);
		float CsideOnADB = vec_dot(ADB, AC);
		float DsideOnABC = vec_dot(ABC, AD);

		// the side relative origin

		float BsideOnAO = vec_dot(ACD, AO);
		float CsideOnAO = vec_dot(ADB, AO);
		float DsideOnAO = vec_dot(ABC, AO);

		//whether the origin is on the same side of ACD/ADB/ABC as B, C and D respectively
		bool ABsameAsOrigin = F_GR(BsideOnACD * BsideOnAO, 0.);
		bool ACsameAsOrigin = F_GR(CsideOnADB * CsideOnAO, 0.);
		bool ADsameAsOrigin = F_GR(DsideOnABC * DsideOnAO, 0.);

		//if the origin is on the same side as all B, C and D, the origin is inside the tetrahedron and thus there is a collision
		if (ABsameAsOrigin && ACsameAsOrigin && ADsameAsOrigin) {
//			LOGD("A = %f %f %f", A.v[0], A.v[1], A.v[2]);
//			LOGD("B = %f %f %f", B.v[0], B.v[1], B.v[2]);
//			LOGD("C = %f %f %f", C.v[0], C.v[1], C.v[2]);
//			LOGD("D = %f %f %f", D.v[0], D.v[1], D.v[2]);
//
//			LOGD("BsideOnACD = %f %f %f %f", vec_dot(ACD, AB), SIGN(vec_dot(ACD, AB)), vec_dot(ACD, AO), SIGN(vec_dot(ACD, AO)));
//			LOGD("CsideOnADB = %f %f %f %f", vec_dot(ADB, AC), SIGN(vec_dot(ADB, AC)), vec_dot(ADB, AO), SIGN(vec_dot(ADB, AO)));
//			LOGD("DsideOnABC = %f %f %f %f", vec_dot(ABC, AD), SIGN(vec_dot(ABC, AD)), vec_dot(ABC, AO), SIGN(vec_dot(ABC, AO)));
			return true;
		}
		//if the origin is not on the side of B relative to ACD
		else if (!ABsameAsOrigin) {
			//B is farthest from the origin among all of the tetrahedron's points, so remove it from the list and go on with the triangle case
			simplex.erase(simplex.begin() + 2); // B
			//the new direction is on the other side of ACD, relative to B
			vec_mul(ACD, -BsideOnACD);
			direction = ACD;
		}
		//if the origin is not on the side of C relative to ADB
		else if (!ACsameAsOrigin) {
			//C is farthest from the origin among all of the tetrahedron's points, so remove it from the list and go on with the triangle case
			simplex.erase(simplex.begin() + 1); // C
			//the new direction is on the other side of ADB, relative to C
			vec_mul(ADB, -CsideOnADB);
			direction = ADB;
		}
		//if the origin is not on the side of D relative to ABC
		else //if (!ADsameAsOrigin)
		{
			//D is farthest from the origin among all of the tetrahedron's points, so remove it from the list and go on with the triangle case
			simplex.erase(simplex.begin()); // D
			//the new direction is on the other side of ABC, relative to D
			vec_mul(ABC, -DsideOnABC);
			direction = ABC;
		}

		//go on with the triangle case
		//maybe we should restrict the depth of the recursion, just like we restricted the number of iterations in BodiesIntersect?
		return UpdateSimplexAndDirection(simplex, direction);

	}

	//no intersection found on this iteration
	return false;
}

/// <summary>
/// Given the vertices (in any order) of two convex 3D bodies, calculates whether they intersect
/// </summary>
bool is_polyhedron_intersects(float shape1[], int vert_count1, int stride1, float shape2[], int vert_count2, int stride2) {

	//for initial point, just take the difference between any two vertices (in this case - the first ones)
	Vec3 initialPoint;
	Vec3 s1 = *((Vec3*) &shape1[0]);
	Vec3 s2 = *((Vec3*) &shape2[0]);
	vec_sub(s1, s2, initialPoint);

	Vec3 S;
	MaxPointInMinkDiffAlongDir(shape1, vert_count1, stride1, shape2, vert_count2, stride2, initialPoint, S);

	Vec3 D;
	D = S;
	vec_neg(D);

	std::vector<Vec3> simplex;
	simplex.push_back(S);

	for (int i = 0; i < MaxIterations; i++) {
		Vec3 A;
		MaxPointInMinkDiffAlongDir(shape1, vert_count1, stride1, shape2, vert_count2, stride2, D, A);
		if (vec_dot(A, D) < 0) {
			return false;
		}

		simplex.push_back(A);

		if (UpdateSimplexAndDirection(simplex, D)) {
//			LOGD("*********************************");
//			LOGD("// i = %d", i);
//			LOGD("// D = (%f %f %f)", D.v[0], D.v[1], D.v[2]);
//			LOGD("int vert_count1 = %d;", vert_count1);
//			LOGD("int vert_count2 = %d;", vert_count2);
//			LOGD("int stride1 = %d;", stride1);
//			LOGD("int stride2 = %d;", stride2);
//			LOGD("float s1[%d];", vert_count1 * stride1);
//			LOGD("float s2[%d];", vert_count2 * stride2);
//			for (int i = 0; i < vert_count1 * stride1; i++) {
//				LOGD("s1[%d] = %.12f;", i, shape1[i]);
//			}
//
//			for (int i = 0; i < vert_count2 * stride2; i++)
//				LOGD("s2[%d] = %.18f;", i, shape2[i]);
//			LOGD("*********************************");
//			exit(0);
			return true;
		}
	}
	return false;
}

} // namespace fte
