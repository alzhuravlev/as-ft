#include "preferences.h"
#include "fte_utils.h"

fte::Preferences::Preferences(const std::string & file_name) {
	this->file_name = file_name;
	this->dirty = false;
}

fte::Preferences::~Preferences() {
}

int fte::Preferences::get_int_value(const std::string & key, int default_value) const {
	if (!root.isMember(key))
		return default_value;
	return root[key].asInt();
}

float fte::Preferences::get_float_value(const std::string & key, float default_value) const {
	if (!root.isMember(key))
		return default_value;
	return (float) root[key].asDouble();
}

std::string fte::Preferences::get_string_value(const std::string & key, const std::string & default_value) const {
	std::string s;
	if (!root.isMember(key))
		return default_value;
	return root[key].asString();
}

void fte::Preferences::set_int_value(const std::string & key, int value) {
	Json::Value v(value);
	root[key] = v;
	dirty = true;
}

void fte::Preferences::set_float_value(const std::string & key, float value) {
	Json::Value v(value);
	root[key] = v;
	dirty = true;
}

void fte::Preferences::set_string_value(const std::string & key, const std::string & value) {
	Json::Value v(value);
	root[key] = v;
	dirty = true;
}

void fte::Preferences::load(FTE_ENV env) {

	std::string s;
	bool b = read_internal_to_string(file_name, env, s);
	if (!b)
		return;

	Json::Reader reader;
	reader.parse(s, root, false);

	dirty = false;
}

void fte::Preferences::save(FTE_ENV env) {
	if (!dirty)
		return;
	Json::FastWriter writer;
	std::string s = writer.write(root);
	save_internal_file(env, file_name, s.data(), s.size());
	dirty = false;
}
