#ifndef screen_H
#define screen_H

#include "updater.h"
#include "engine.h"
#include "camera.h"
#include "command.h"
#include "external.h"
#include "fte_utils.h"
#include "statable.h"
#include "screen_transition.h"

#ifdef DEBUG
#include "font_renderer.h"
#endif

#include <map>
#include <vector>

namespace fte {

class Updater;

const int SCREEN_STATE_DEFAULT = STATE_DEFAULT;
const int SCREEN_STATE_FADING_IN = -1;
const int SCREEN_STATE_FADING_OUT = -2;
const int SCREEN_STATE_FADE_IN_DONE = -3;
const int SCREEN_STATE_FADE_OUT_DONE = -4;

class Screen: public Statable {
private:
	bool screen_size_initialized;
	bool initialized_once;
	bool initialized;
	bool renderer_initialized;
	bool updated_once;

	OrthographicCamera ortho_camera;

	float screen_ratio;
	float screen_min_dim;
	float screen_max_dim;
	float screen_width;
	float screen_height;

	bool portrait_orientation;

#ifdef DEBUG
	FontRenderer fps_font_renderer;
#endif

	std::vector<Screen*> active_layers;
	std::vector<Screen*> popup_layers;

	std::map<int, Screen*> layers;

	Updater* updater;

	ScreenTransition* fade_in_transition;
	ScreenTransition* fade_out_transition;

	float width;
	float height;

	Screen* parent;

	void update_screen_sizes(int w, int h);

	Screen* get_root_screen();

protected:

	virtual void do_resize(FTE_ENV env) {
	}

	virtual void do_touch(TouchEvent events[], int size) {
	}

	virtual void do_custom_command(int commands[], int size) {
	}

	virtual void do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
	}

	virtual void do_init_once(FTE_ENV env) {
	}

	virtual void do_init(FTE_ENV env) {
	}

	virtual void do_release(FTE_ENV env) {
	}

	virtual void do_update(float delta, RenderCommand* command) {
	}

	virtual void do_update_once(RenderCommand* command) {
	}

	virtual void do_change_state(int state);

	virtual void do_show_as_layer();

	virtual void do_fading_in();
	virtual void do_fading_out();
	virtual void do_fade_in_done();
	virtual void do_fade_out_done();

	void set_fade_in(ScreenTransition* fade_in_transition);

	void set_fade_out(ScreenTransition* fade_out_transition);

public:
	Screen(Screen* parent = NULL);

	virtual ~Screen();

	void resize(int w, int h, FTE_ENV env);

	void touch(TouchEvent events[], int size);

	void custom_command(int commands[], int size);

	void init_renderer(InitRendererCommand* command, FTE_ENV env);

	void init_once(int w, int h, FTE_ENV env);

	void init(int w, int h, FTE_ENV env);

	void release(FTE_ENV env);

	void update(float delta, RenderCommand* command);

	void update_once(RenderCommand* command);

	inline bool is_ready_for_render() const {
		return initialized && initialized_once && renderer_initialized && screen_size_initialized;
	}

	inline bool is_renderer_initialized() const {
		return renderer_initialized;
	}

	inline bool is_initilized_once() const {
		return initialized_once;
	}

	inline bool is_updated_once() const {
		return updated_once;
	}

	inline bool is_initilized() const {
		return initialized;
	}

	inline bool is_screen_size_initilized() const {
		return screen_size_initialized;
	}

	void set_updater(Updater* updater) {
		this->updater = updater;
	}

	inline Updater* get_updater() const {
		return this->updater;
	}

	Screen* get_layer(int layer_id);
	void add_layer(int layer_id, Screen* layer);
	void add_active_layer(Screen* layer);

	inline float get_width() const {
		return width;
	}

	inline float get_height() const {
		return height;
	}

	inline float get_screen_ratio() const {
		return screen_ratio;
	}

	inline float get_screen_max_dim() const {
		return screen_max_dim;
	}

	inline float get_screen_min_dim() const {
		return screen_min_dim;
	}

	inline float get_screen_width() const {
		return screen_width;
	}

	inline float get_screen_height() const {
		return screen_height;
	}

	inline bool is_portrait_orientation() const {
		return portrait_orientation;
	}

	inline const OrthographicCamera & get_ortho_camera() const {
		return ortho_camera;
	}

	bool show_layer(int layer_id);

	void hide_layer();
	void hide_all_layers();
	bool has_layer() const;

	void fade_in();
	void fade_out();
};

} // namespace fte

#endif /* screen_H */
