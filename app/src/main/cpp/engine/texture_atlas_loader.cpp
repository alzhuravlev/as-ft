#include "texture_atlas_loader.h"

namespace fte {

bool page_item_comparator(PageItem* i1, PageItem* i2) {
	return (i1->index < i2->index);
}

} // namespace fte
