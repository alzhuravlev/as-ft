#ifndef engine_H
#define engine_H

#include "stdint.h"

namespace fte {

enum {
	ATOUCH_DOWN, ATOUCH_UP, ATOUCH_MOVE, ATOUCH_BACK_PRESSED
};

struct ResizeEvent {
	int dummy;
};

struct TouchEvent {
	int32_t action;
	int32_t id;
	float x;
	float y;
	int64_t nano_time;
};

typedef void* FTE_ENGINE;
typedef void* FTE_SCREEN;
typedef void* FTE_ENV;

FTE_ENGINE fteCreateEngine(FTE_SCREEN screen, FTE_ENV env);
FTE_ENGINE fteCreateEngineNoThread(FTE_SCREEN screen, FTE_ENV env);

void fteDestroyEngine(FTE_ENGINE &engine);
void fteStart(FTE_ENGINE engine);
bool fteStep(FTE_ENGINE engine);
void fteStop(FTE_ENGINE engine);
void fteSendResize(FTE_ENGINE engine);
void fteSendTouch(FTE_ENGINE engine, const TouchEvent &event);
void fteSendCustomCommand(FTE_ENGINE engine, int command);

} // namespace fte

#endif /* engine_H */
