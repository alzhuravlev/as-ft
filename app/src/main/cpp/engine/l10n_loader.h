#ifndef l10n_loader_H
#define l10n_loader_H

#include "engine.h"
#include "external.h"
#include "fte_utils.h"

#include <string>
#include <map>
#include <vector>

#define DEFAULT_LOCALE "default"

namespace fte {

class LocalizationLoader {
private:
	std::map<std::string, std::map<std::string, std::wstring> > data;

	void load_file(const std::string & path, const std::string & locale, FTE_ENV env) {
		std::vector<std::wstring> lines;
		read_resource_lines_w(path + "/" + locale, env, lines);

		std::map<std::string, std::wstring> map;

		for (std::vector<std::wstring>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::wstring line = *it;
			std::string l;
			convert_wstring_to_string(line, l);

			size_t pos = line.find_first_of('=', 0);
			if (pos != std::wstring::npos) {

				std::wstring wcode = line.substr(0, pos);
				std::string code;
				convert_wstring_to_string(wcode, code);

				std::wstring wvalue = line.substr(pos + 1, std::wstring::npos);

				map[code] = wvalue;
			}
		}

		data[locale] = map;
	}

	bool get_local_string(const std::string & locale, const std::string & code, std::wstring & out) {
		std::map<std::string, std::map<std::string, std::wstring> >::const_iterator locale_it = data.find(locale);
		if (locale_it == data.end())
			return false;

		const std::map<std::string, std::wstring> & map = locale_it->second;
		std::map<std::string, std::wstring>::const_iterator code_it = map.find(code);

		if (code_it == map.end())
			return false;

		out = code_it->second;
		return true;
	}

public:
	LocalizationLoader() {
	}

	void load(const std::string & path, const std::string & locale, FTE_ENV env) {
		load_file(path, DEFAULT_LOCALE, env);
		load_file(path, locale, env);
	}

	bool resolve_local_string(const std::string & locale, const std::string & code, std::wstring & out) {
		if (get_local_string(locale, code, out))
			return true;

		if (get_local_string(DEFAULT_LOCALE, code, out))
			return true;

		wchar_t space[1];
		space[0] = ' ';

		out.clear();
		out.assign(space, 1);

		return false;
	}
};

} // namespace fte

#endif /* l10n_loader_H */
