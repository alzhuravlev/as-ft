#ifndef road_utils_H
#define road_utils_H

#include "fte_math.h"

#include <vector>

namespace fte {

void road_point_to_far_point(const Vec3 & world_point, float width, Vec3 & res);
void road_point_far_dir(const Vec3 & world_point, Vec3 & res);

} // namespace fte

#endif /* road_utils_H */
