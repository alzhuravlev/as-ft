#ifndef preferences_H
#define preferences_H

#include "external.h"
#include "json/json.h"

#include <map>

namespace fte {

class Preferences {
private:
	std::string file_name;

	Json::Value root;

	bool dirty;

public:
	Preferences(const std::string & file_name);
	~Preferences();

	int get_int_value(const std::string & key, int default_value = 0) const;
	float get_float_value(const std::string & key, float default_value = 0.0f) const;
	std::string get_string_value(const std::string & key, const std::string & default_value = "") const;

	void set_int_value(const std::string & key, int value);
	void set_float_value(const std::string & key, float value);
	void set_string_value(const std::string & key, const std::string & value);

	void load(FTE_ENV env);
	void save(FTE_ENV env);
};

} // namespace fte

#endif /* preferences_H */
