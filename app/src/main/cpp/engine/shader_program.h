#ifndef shader_program_H
#define shader_program_H

#include "command.h"
#include "shader_program_data.h"
#include "fte_gl.h"
#include "fte_utils_gl.h"

namespace fte {

class ShaderProgram {
private:

	std::string vs_file_name;
	std::string fs_file_name;

	GLuint g_program;

	GLint a_position_handle;
	GLint a_color_handle;
	GLint a_uv_handle;
	GLint a_normal_handle;
	GLint a_nm_uv_handle;
	GLint a_tangent_handle;
	GLint a_bitangent_handle;

	GLint u_toon_weight;
	GLint u_toon_weight_delta;
	GLint u_toon_eye;
	GLint u_toon_fog_color;
	GLint u_toon_fog_alpha_threshold;
	GLint u_toon_fog_a;
	GLint u_toon_fog_b;

	GLint u_state_time;
	GLint u_vertex_vibration_freq;
	GLint u_vertex_vibration_amplitude;

	GLint u_light__position;
	GLint u_light__direction;
	GLint u_light__ambient_light;
	GLint u_light__eye;
	GLint u_light__eye_f;
	GLint u_light__specular_exp;
	GLint u_light__attenuation;
	GLint u_light__fog_color;
	GLint u_light__fog_a;
	GLint u_light__fog_b;
	GLint u_light__spot_exp;
	GLint u_light__spot_cos_cutoff;

	GLint u_extra_param1;
	GLint u_extra_param2;
	GLint u_extra_param3;
	GLint u_extra_param4;
	GLint u_extra_param5;
	GLint u_extra_param6;

	GLint u_view_proj_handle;
	GLint u_model_handle;

	GLint u_texture_handle;

public:

	ShaderProgram(const std::string & vs_file_name, const std::string & fs_file_name) {
		this->vs_file_name = vs_file_name;
		this->fs_file_name = fs_file_name;
	}

	void init(FTE_ENV env) {

		std::string v_source;
		std::string f_source;
		read_resource_to_string(vs_file_name, env, v_source);
		read_resource_to_string(fs_file_name, env, f_source);

		g_program = createProgram(v_source.c_str(), f_source.c_str());

		if (!g_program) {
			LOGE("fte: ShaderProgram: Could not create program.");
			return;
		}

		a_position_handle = glGetAttribLocation(g_program, "a_position");
		a_color_handle = glGetAttribLocation(g_program, "a_color");
		a_uv_handle = glGetAttribLocation(g_program, "a_uv");
		a_normal_handle = glGetAttribLocation(g_program, "a_normal");
		a_nm_uv_handle = glGetAttribLocation(g_program, "a_nm_uv");
		a_tangent_handle = glGetAttribLocation(g_program, "a_tangent");
		a_bitangent_handle = glGetAttribLocation(g_program, "a_bitangent");

		u_view_proj_handle = glGetUniformLocation(g_program, "u_view_proj");
		u_model_handle = glGetUniformLocation(g_program, "u_model");

		u_state_time = glGetUniformLocation(g_program, "u_state_time");
		u_vertex_vibration_freq = glGetUniformLocation(g_program, "u_vertex_vibration_freq");
		u_vertex_vibration_amplitude = glGetUniformLocation(g_program, "u_vertex_vibration_amplitude");
		u_texture_handle = glGetUniformLocation(g_program, "u_texture");
		u_toon_weight = glGetUniformLocation(g_program, "u_toon_weight");
		u_toon_weight_delta = glGetUniformLocation(g_program, "u_toon_weight_delta");
		u_toon_eye = glGetUniformLocation(g_program, "u_toon_eye");
		u_toon_fog_color = glGetUniformLocation(g_program, "u_toon_fog_color");
		u_toon_fog_alpha_threshold = glGetUniformLocation(g_program, "u_toon_fog_alpha_threshold");
		u_toon_fog_a = glGetUniformLocation(g_program, "u_toon_fog_a");
		u_toon_fog_b = glGetUniformLocation(g_program, "u_toon_fog_b");

		u_light__position = glGetUniformLocation(g_program, "u_light_position");
		u_light__direction = glGetUniformLocation(g_program, "u_light_direction");
		u_light__ambient_light = glGetUniformLocation(g_program, "u_light_ambient_light");
		u_light__specular_exp = glGetUniformLocation(g_program, "u_light_specular_exp");
		u_light__attenuation = glGetUniformLocation(g_program, "u_light_attenuation");
		u_light__spot_exp = glGetUniformLocation(g_program, "u_light_spot_exp");
		u_light__spot_cos_cutoff = glGetUniformLocation(g_program, "u_light_spot_cos_cutoff");

		u_light__eye = glGetUniformLocation(g_program, "u_light_eye");
		u_light__eye_f = glGetUniformLocation(g_program, "u_light_eye_f");
		u_light__fog_color = glGetUniformLocation(g_program, "u_light_fog_color");
		u_light__fog_a = glGetUniformLocation(g_program, "u_light_fog_a");
		u_light__fog_b = glGetUniformLocation(g_program, "u_light_fog_b");

		u_extra_param1 = glGetUniformLocation(g_program, "u_extra_param1");
		u_extra_param2 = glGetUniformLocation(g_program, "u_extra_param2");
		u_extra_param3 = glGetUniformLocation(g_program, "u_extra_param3");
		u_extra_param4 = glGetUniformLocation(g_program, "u_extra_param4");
		u_extra_param5 = glGetUniformLocation(g_program, "u_extra_param5");
		u_extra_param6 = glGetUniformLocation(g_program, "u_extra_param6");
	}

	void release() {
		glDeleteProgram(g_program);
	}

	void render_data(const ShaderProgramData & data, const RenderCommand & command) {

		glUseProgram(g_program);
		CHECK_GL_ERROR("glUseProgram(g_program)");

		if (u_view_proj_handle != -1 && data.is_use_view_proj_matrix())
			glUniformMatrix4fv(u_view_proj_handle, 1, GL_FALSE, data.get_view_proj_matrix());

		if (u_model_handle != -1 && data.is_use_model_matrix())
			glUniformMatrix4fv(u_model_handle, 1, GL_FALSE, data.get_model_matrix());

		glEnableVertexAttribArray(a_position_handle);

		if (u_texture_handle != -1)
			glUniform1i(u_texture_handle, data.get_texture_unit());

		if (a_color_handle != -1)
			glEnableVertexAttribArray(a_color_handle);

		if (a_uv_handle != -1)
			glEnableVertexAttribArray(a_uv_handle);

		if (a_normal_handle != -1)
			glEnableVertexAttribArray(a_normal_handle);

		if (a_nm_uv_handle != -1)
			glEnableVertexAttribArray(a_nm_uv_handle);

		if (a_tangent_handle != -1)
			glEnableVertexAttribArray(a_tangent_handle);

		if (a_bitangent_handle != -1)
			glEnableVertexAttribArray(a_bitangent_handle);

		if (u_light__direction != -1) {
			const Vec3 & direction = command.get_light_direction();
			glUniform3f(u_light__direction, direction.x, direction.y, direction.z);
		}

		if (u_light__position != -1) {
			const Vec3 & position = command.get_light_position();
			glUniform3f(u_light__position, position.x, position.y, position.z);
		}

		if (u_light__ambient_light != -1)
			glUniform1f(u_light__ambient_light, command.get_light_ambient_light());

		if (u_light__specular_exp != -1)
			glUniform1f(u_light__specular_exp, command.get_light_specular_exp());

		if (u_light__attenuation != -1)
			glUniform1f(u_light__attenuation, command.get_light_attenuation());

		if (u_light__spot_exp != -1)
			glUniform1f(u_light__spot_exp, command.get_light_spot_exp());

		if (u_light__spot_cos_cutoff != -1)
			glUniform1f(u_light__spot_cos_cutoff, command.get_light_spot_cos_cutoff());

		const Vec3 & eye = command.get_light_eye();
		if (u_light__eye != -1)
			glUniform3f(u_light__eye, eye.x, eye.y, eye.z);

		if (u_light__eye_f != -1)
			glUniform3f(u_light__eye_f, eye.x, eye.y, eye.z);

		if (u_light__fog_color != -1) {
			const Vec3 & fog_color = command.get_light_fog_color();
			glUniform3f(u_light__fog_color, fog_color.x, fog_color.y, fog_color.z);
		}

		if (u_light__fog_a != -1)
			glUniform1f(u_light__fog_a, command.get_light_fog_a());

		if (u_light__fog_b != -1)
			glUniform1f(u_light__fog_b, command.get_light_fog_b());

		if (u_toon_weight != -1)
			glUniform1f(u_toon_weight, command.get_toon_weight());

		if (u_toon_weight_delta != -1)
			glUniform1f(u_toon_weight_delta, command.get_toon_weight_delta());

		if (u_toon_fog_color != -1) {
			float tfc_r, tfc_g, tfc_b;
			command.get_toon_fog_color(tfc_r, tfc_g, tfc_b);
			glUniform3f(u_toon_fog_color, tfc_r, tfc_g, tfc_b);
		}

		if (u_toon_fog_alpha_threshold != -1)
			glUniform1f(u_toon_fog_alpha_threshold, command.get_toon_fog_alpha_threshold());

		if (u_toon_eye != -1) {
			const Vec3 & eye = command.get_light_eye();
			glUniform3f(u_toon_eye, eye.x, eye.y, eye.z);
		}

		if (u_toon_fog_a != -1)
			glUniform1f(u_toon_fog_a, command.get_light_fog_a());

		if (u_toon_fog_b != -1)
			glUniform1f(u_toon_fog_b, command.get_light_fog_b());

		if (u_state_time != -1)
			glUniform1f(u_state_time, command.get_state_time());

		if (u_vertex_vibration_freq != -1)
			glUniform1f(u_vertex_vibration_freq, command.get_vertex_vibration_freq());

		if (u_vertex_vibration_amplitude != -1)
			glUniform1f(u_vertex_vibration_amplitude, command.get_vertex_vibration_amplitude());

		if (u_extra_param1 != -1)
			glUniform4f(u_extra_param1, data.get_extra_param1().x, data.get_extra_param1().y, data.get_extra_param1().z, data.get_extra_param1().w);

		if (u_extra_param2 != -1)
			glUniform4f(u_extra_param2, data.get_extra_param2().x, data.get_extra_param2().y, data.get_extra_param2().z, data.get_extra_param2().w);

		if (u_extra_param3 != -1)
			glUniform4f(u_extra_param3, data.get_extra_param3().x, data.get_extra_param3().y, data.get_extra_param3().z, data.get_extra_param3().w);

		if (u_extra_param4 != -1)
			glUniform4f(u_extra_param4, data.get_extra_param4().x, data.get_extra_param4().y, data.get_extra_param4().z, data.get_extra_param4().w);

		if (u_extra_param5 != -1)
			glUniform4f(u_extra_param5, data.get_extra_param5().x, data.get_extra_param5().y, data.get_extra_param5().z, data.get_extra_param5().w);

		if (u_extra_param6 != -1)
			glUniform4f(u_extra_param6, data.get_extra_param6().x, data.get_extra_param6().y, data.get_extra_param6().z, data.get_extra_param6().w);

		data.bind();

		int stride = data.get_stride();

		glVertexAttribPointer(a_position_handle, 3, GL_FLOAT, GL_FALSE, stride, data.get_position_component_ptr());
		CHECK_GL_ERROR("glVertexAttribPointer");

		if (a_color_handle != -1)
			glVertexAttribPointer(a_color_handle, 4, GL_UNSIGNED_BYTE, GL_TRUE, stride, data.get_color_component_ptr());

		if (a_uv_handle != -1)
			glVertexAttribPointer(a_uv_handle, 2, GL_FLOAT, GL_FALSE, stride, data.get_uv_component_ptr());

		if (a_normal_handle != -1)
			glVertexAttribPointer(a_normal_handle, 3, GL_FLOAT, GL_FALSE, stride, data.get_normal_component_ptr());

		if (a_nm_uv_handle != -1)
			glVertexAttribPointer(a_nm_uv_handle, 2, GL_FLOAT, GL_FALSE, stride, data.get_nm_uv_component_ptr());

		if (a_tangent_handle != -1)
			glVertexAttribPointer(a_tangent_handle, 3, GL_FLOAT, GL_FALSE, stride, data.get_tangent_component_ptr());

		if (a_bitangent_handle != -1)
			glVertexAttribPointer(a_bitangent_handle, 3, GL_FLOAT, GL_FALSE, stride, data.get_bitangent_component_ptr());

		// draw
		glDrawElements(GL_TRIANGLES, data.get_index_count(), GL_UNSIGNED_SHORT, data.get_index_ptr());
		CHECK_GL_ERROR("glDrawElements");

		// finalize
		glDisableVertexAttribArray(a_position_handle);

		if (a_color_handle != -1)
			glDisableVertexAttribArray(a_color_handle);

		if (a_uv_handle != -1)
			glDisableVertexAttribArray(a_uv_handle);

		if (a_normal_handle != -1)
			glDisableVertexAttribArray(a_normal_handle);

		if (a_nm_uv_handle != -1)
			glDisableVertexAttribArray(a_nm_uv_handle);

		if (a_tangent_handle != -1)
			glDisableVertexAttribArray(a_tangent_handle);

		if (a_bitangent_handle != -1)
			glDisableVertexAttribArray(a_bitangent_handle);

		data.unbind();

		glUseProgram(0);
	}
};

} // namespace fte

#endif /* shader_program_H */
