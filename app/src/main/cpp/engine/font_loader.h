#ifndef font_loader_H
#define font_loader_H

#include "engine.h"
#include "command.h"

#include <vector>
#include <map>
#include <string>

namespace fte {

struct FontChar {
	int id;
	float x;
	float y;
	float width;
	float height;
	float xoffset;
	float yoffset;
	float xadvance;
	int page;

	// pre_calc
	float u1, u2, v1, v2;
	float x1, x2, y1, y2, x3, y3, x4, y4;
	float w, h;
};

struct FontInfo {
	float padding_up;
	float padding_right;
	float padding_down;
	float padding_left;
	float spacing_h;
	float spacing_v;
};

struct FontCommon {
	float line_height;
	float base;
	float scale_w;
	float scale_h;
};

struct FontPage {
	int id;
	std::string texture_file_name;
};

/**
 * Load BMFont file format: http://www.angelcode.com/products/bmfont/doc/file_format.html
 */
class FontLoader {
private:

	std::string file_name;

	FontInfo font_info;
	FontCommon font_common;
	std::map<int, FontPage> pages;
	std::map<int, FontChar> chars;

	void load(FTE_ENV env) {

		std::vector<std::string> lines;
		read_resource_lines(file_name, env, lines);

		for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::string line = *it;

			remove_chars(line, "\n\r");

			if (line.length() == 0)
				continue;

			std::vector<std::string> el;
			split(line, ' ', el);

			if (el.size() < 2)
				continue;

			if (el[0] == "info") {
				font_info.padding_up = 0;
				font_info.padding_right = 0;
				font_info.padding_down = 0;
				font_info.padding_left = 0;
				font_info.spacing_h = 0;
				font_info.spacing_v = 0;

				for (int i = 1; i < el.size(); i++) {
					std::string s = el[i];
					if (starts_with(s, "padding=")) {
						std::string val = s.substr(8, std::string::npos);
						std::vector<std::string> vals;
						split(val, ',', vals);
						font_info.padding_up = vals.size() > 0 ? atoi(vals[0].c_str()) : 0;
						font_info.padding_right = vals.size() > 1 ? atoi(vals[1].c_str()) : 0;
						font_info.padding_down = vals.size() > 2 ? atoi(vals[2].c_str()) : 0;
						font_info.padding_left = vals.size() > 3 ? atoi(vals[3].c_str()) : 0;
					} else if (starts_with(s, "spacing=")) {
						std::string val = s.substr(8, std::string::npos);
						std::vector<std::string> vals;
						split(val, ',', vals);
						font_info.spacing_h = vals.size() > 0 ? atoi(vals[0].c_str()) : 0;
						font_info.spacing_v = vals.size() > 1 ? atoi(vals[1].c_str()) : 0;
					}
				}

			} else if (el[0] == "common") {

				for (int i = 1; i < el.size(); i++) {
					std::string s = el[i];
					if (starts_with(s, "lineHeight=")) {
						std::string val = s.substr(strlen("lineHeight="), std::string::npos);
						font_common.line_height = atoi(val.c_str());
					} else if (starts_with(s, "base=")) {
						std::string val = s.substr(strlen("base="), std::string::npos);
						font_common.base = atoi(val.c_str());
					} else if (starts_with(s, "scaleW=")) {
						std::string val = s.substr(strlen("scaleW="), std::string::npos);
						font_common.scale_w = atoi(val.c_str());
					} else if (starts_with(s, "scaleH=")) {
						std::string val = s.substr(strlen("scaleH="), std::string::npos);
						font_common.scale_h = atoi(val.c_str());
					}
				}

			} else if (el[0] == "page") {

				FontPage font_page;

				for (int i = 1; i < el.size(); i++) {
					std::string s = el[i];
					if (starts_with(s, "id=")) {
						std::string val = s.substr(strlen("id="), std::string::npos);
						font_page.id = atoi(val.c_str());
					} else if (starts_with(s, "file=")) {
						std::string val = s.substr(strlen("file="), std::string::npos);
						remove_chars(val, "\"");

						replace_file_name(file_name, val, val);
						font_page.texture_file_name = val;
					}
				}

				pages[font_page.id] = font_page;

			} else if (el[0] == "char") {

				FontChar font_char;

				for (int i = 1; i < el.size(); i++) {
					std::string s = el[i];
					if (starts_with(s, "id=")) {
						std::string val = s.substr(strlen("id="), std::string::npos);
						font_char.id = atoi(val.c_str());
					} else if (starts_with(s, "x=")) {
						std::string val = s.substr(strlen("x="), std::string::npos);
						font_char.x = atoi(val.c_str());
					} else if (starts_with(s, "y=")) {
						std::string val = s.substr(strlen("y="), std::string::npos);
						font_char.y = atoi(val.c_str());
					} else if (starts_with(s, "width=")) {
						std::string val = s.substr(strlen("width="), std::string::npos);
						font_char.width = atoi(val.c_str());
					} else if (starts_with(s, "height=")) {
						std::string val = s.substr(strlen("height="), std::string::npos);
						font_char.height = atoi(val.c_str());
					} else if (starts_with(s, "xoffset=")) {
						std::string val = s.substr(strlen("xoffset="), std::string::npos);
						font_char.xoffset = atoi(val.c_str());
					} else if (starts_with(s, "yoffset=")) {
						std::string val = s.substr(strlen("yoffset="), std::string::npos);
						font_char.yoffset = atoi(val.c_str());
					} else if (starts_with(s, "xadvance=")) {
						std::string val = s.substr(strlen("xadvance="), std::string::npos);
						font_char.xadvance = atoi(val.c_str());
					} else if (starts_with(s, "page=")) {
						std::string val = s.substr(strlen("page="), std::string::npos);
						font_char.page = atoi(val.c_str());
					}
				}

				chars[font_char.id] = font_char;
			}
		}

		LOGD("fte: Font loaded from file '%s'. char count: %d; pages: %d", file_name.c_str(), chars.size(), pages.size());
	}

	void pre_calc() {

		const float k = 1.0f / font_common.line_height;

		font_common.line_height = 1.0f;
		font_common.base *= k;

		const float w = 1.0f / font_common.scale_w;
		const float h = 1.0f / font_common.scale_h;

		for (std::map<int, FontChar>::iterator it = chars.begin(); it != chars.end(); ++it) {
			FontChar & font_char = it->second;

			font_char.xoffset *= k;
			font_char.yoffset *= k;

			font_char.xadvance *= k;

			font_char.u1 = w * font_char.x;
			font_char.u2 = font_char.u1 + w * font_char.width;

			font_char.v1 = 1.0f - h * (font_char.y + font_char.height);
			font_char.v2 = font_char.v1 + h * font_char.height;

			// imaging that font_common.line_height = 1
			font_char.w = font_char.width * k;
			font_char.h = font_char.height * k;

			font_char.x1 = font_char.xoffset;
			font_char.y1 = 1.0f - (font_char.yoffset + font_char.h);

			font_char.x2 = font_char.xoffset + font_char.w;
			font_char.y2 = font_char.y1;

			font_char.x3 = font_char.x2;
			font_char.y3 = 1.0f - font_char.yoffset;

			font_char.x4 = font_char.x1;
			font_char.y4 = font_char.y3;
		}
	}

public:
	FontLoader(std::string file_name, FTE_ENV env) {
		this->file_name = file_name;
		load(env);
		pre_calc();
	}

	const FontCommon & get_font_common() const {
		return font_common;
	}

	const FontInfo & get_font_info() const {
		return font_info;
	}

	bool get_font_page(int page_id, FontPage & out) const {
		std::map<int, FontPage>::const_iterator it = pages.find(page_id);
		if (it == pages.end())
			return false;
		out = it->second;
		return true;
	}

	void get_font_chars(std::map<int, FontChar> & out) const {
		out = chars;
	}

	bool get_font_char(int char_id, FontChar & out) const {
		std::map<int, FontChar>::const_iterator it = chars.find(char_id);
		if (it == chars.end())
			return false;
		out = it->second;
		return true;
	}

	void add_texture_descriptors(InitRendererCommand* command) const {
		for (std::map<int, FontPage>::const_iterator it = pages.begin(); it != pages.end(); it++) {
			const FontPage & font_page = it->second;
			command->add(TextureDescriptor(font_page.texture_file_name));

			if (TextureAtlas_VERBOSE)
				LOGD("fte: FontLoader: texture descriptor added to command: %s", font_page.texture_file_name.c_str());
		}
	}

	std::string get_file_name() const {
		return file_name;
	}
};

} // namespace fte

#endif /* font_loader_H */
