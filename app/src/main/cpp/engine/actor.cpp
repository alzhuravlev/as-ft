#include "actor.h"

namespace fte {

Actor::Actor(Scene* scene, int type, uint64_t boradcast_mask) {
	this->scene = scene;
	this->type = type;
	this->active = false;
	this->visible = true;
	this->broadcast_mask = boradcast_mask;
}

Actor::~Actor() {
}

void Actor::do_activate() {
}

void Actor::do_deactivate() {
}

void Actor::do_init(FTE_ENV env) {
}

void Actor::do_update(float delta, const Camera & camera) {
}

void Actor::do_render(float delta, RenderCommand* command, const Camera & camera, int id) {
}

void Actor::init(FTE_ENV env) {
	do_init(env);
}

void Actor::update(float delta, const Camera & camera) {
	if (!active)
		return;
	update_state(delta);
	do_update(delta, camera);
}

void Actor::render(float delta, RenderCommand* command, const Camera & camera, int id) {
	if (!active)
		return;
	if (!visible)
		return;
	do_render(delta, command, camera, id);
}

void Actor::activate() {
	active = true;
	scene->activate_actor(this);
	do_activate();
}

void Actor::deactivate() {
	this->active = false;
	scene->deactivate_actor(this);
	do_deactivate();
}

void Actor::send_broadcast(uint64_t type) {
	scene->send_broadcast(type);
}

void Actor::send_broadcast(uint64_t type, void* p) {
	scene->send_broadcast(type, p);
}

void Actor::do_receive_broadcast(uint64_t type, void* p) {
}

} // namespace fte
