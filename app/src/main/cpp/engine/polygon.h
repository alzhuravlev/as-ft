#ifndef polygon_H
#define polygon_H

#include "texture_descriptor.h"
#include "buffer.h"
#include "fte_utils.h"
#include "transformable.h"
#include "render_commandable.h"
#include "gjk.h"

#include <limits>

namespace fte {

const static uint16_t ORIGIN_TOP = (1 << 0);
const static uint16_t ORIGIN_BOTTOM = (1 << 1);
const static uint16_t ORIGIN_V_CENTER = (1 << 2);

const static uint16_t ORIGIN_LEFT = (1 << 3);
const static uint16_t ORIGIN_RIGHT = (1 << 4);
const static uint16_t ORIGIN_H_CENTER = (1 << 5);

const static uint16_t ORIGIN_NEAR = (1 << 6);
const static uint16_t ORIGIN_FAR = (1 << 7);
const static uint16_t ORIGIN_Z_CENTER = (1 << 8);

const static uint16_t ORIGIN_TOP_LEFT = ORIGIN_TOP | ORIGIN_LEFT;
const static uint16_t ORIGIN_TOP_RIGHT = ORIGIN_TOP | ORIGIN_RIGHT;
const static uint16_t ORIGIN_TOP_CENTER = ORIGIN_TOP | ORIGIN_H_CENTER;
const static uint16_t ORIGIN_CENTER = ORIGIN_V_CENTER | ORIGIN_H_CENTER;
const static uint16_t ORIGIN_BOTTOM_LEFT = ORIGIN_BOTTOM | ORIGIN_LEFT;
const static uint16_t ORIGIN_BOTTOM_RIGHT = ORIGIN_BOTTOM | ORIGIN_RIGHT;
const static uint16_t ORIGIN_BOTTOM_CENTER = ORIGIN_BOTTOM | ORIGIN_H_CENTER;

const static uint16_t ORIGIN_V_MASK = ORIGIN_TOP | ORIGIN_BOTTOM | ORIGIN_V_CENTER;
const static uint16_t ORIGIN_H_MASK = ORIGIN_LEFT | ORIGIN_RIGHT | ORIGIN_H_CENTER;
const static uint16_t ORIGIN_Z_MASK = ORIGIN_NEAR | ORIGIN_FAR | ORIGIN_Z_CENTER;

class Polygon: public Transformable, public RenderCommandableIncIndexes {
private:

	Rect verts_aabb;
	float dirty_aabb;

	bool clockwise_index_order;

	bool normals_rotation;
	bool fixed_rotation;

	bool preserve_vert_colors;
	bool preserve_vert_transform;

protected:

	int last_offset;

	int ind_count;
	int ind_size;

	short* inds;
	short* inds_tmp;

	int vert_count;
	int vert_size;

	float* verts;
	float* verts_calculated;

	inline void apply_transformation() {

		if (preserve_vert_transform)
			return;

		if (!dirty)
			return;

#ifdef DEBUG
		DEBUG_APPLY_TRANSFORMATIONS_COUNTER++;
		DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER += vert_count;
#endif

		memcpy(verts_calculated, verts, vert_size);

		float* p_verts_calculated = verts_calculated;
		float* p_verts_calculated_xyz;
		float* p_verts_calculated_normal;
		float* p_verts_calculated_tangent;
		float* p_verts_calculated_bitangent;

		const bool rotate_euler = euler_rotation; // && (F_NEQ(angle_x, 0.) || F_NEQ(angle_y, 0.) || F_NEQ(angle_z, 0.));
		const bool rotate_around_axis = axis_rotation && F_NEQ(axis_angle, 0.0f);
		const bool rotate_normals = normal_component_offset && normals_rotation && (rotate_euler || rotate_around_axis);

		for (int i = 0; i != vert_count; i++) {

			p_verts_calculated_xyz = p_verts_calculated;

			float & x_vert = *p_verts_calculated_xyz;
			float & y_vert = *(++p_verts_calculated_xyz);
			float & z_vert = *(++p_verts_calculated_xyz);

			x_vert -= origin_x;
			y_vert -= origin_y;
			z_vert -= origin_z;

			x_vert *= scale_x;
			y_vert *= scale_y;
			z_vert *= scale_z;

			if (!fixed_rotation) {

				if (rotate_euler)
					rotate_point_fast_fast(cos_x, sin_x, cos_y, sin_y, cos_z, sin_z, x_vert, y_vert, z_vert);

				if (rotate_around_axis)
					rotate_point_around_axis_fast(qw, qx, qy, qz, x_vert, y_vert, z_vert);

				if (rotate_normals) {

					p_verts_calculated_normal = p_verts_calculated + normal_component_offset;

					float & normal_x = *p_verts_calculated_normal;
					float & normal_y = *(++p_verts_calculated_normal);
					float & normal_z = *(++p_verts_calculated_normal);

					if (rotate_euler)
						rotate_point_fast_fast(cos_x, sin_x, cos_y, sin_y, cos_z, sin_z, normal_x, normal_y, normal_z);

					if (rotate_around_axis)
						rotate_point_around_axis_fast(qw, qx, qy, qz, normal_x, normal_y, normal_z);

					if (tangent_component_offset) {

						p_verts_calculated_tangent = p_verts_calculated + tangent_component_offset;

						float & t_x = *p_verts_calculated_tangent;
						float & t_y = *(++p_verts_calculated_tangent);
						float & t_z = *(++p_verts_calculated_tangent);

						if (rotate_euler)
							rotate_point_fast_fast(cos_x, sin_x, cos_y, sin_y, cos_z, sin_z, t_x, t_y, t_z);

						if (rotate_around_axis)
							rotate_point_around_axis_fast(qw, qx, qy, qz, t_x, t_y, t_z);
					}

					if (bitangent_component_offset) {

						p_verts_calculated_bitangent = p_verts_calculated + bitangent_component_offset;

						float & b_x = *p_verts_calculated_bitangent;
						float & b_y = *(++p_verts_calculated_bitangent);
						float & b_z = *(++p_verts_calculated_bitangent);

						if (rotate_euler)
							rotate_point_fast_fast(cos_x, sin_x, cos_y, sin_y, cos_z, sin_z, b_x, b_y, b_z);

						if (rotate_around_axis)
							rotate_point_around_axis_fast(qw, qx, qy, qz, b_x, b_y, b_z);
					}
				}
			}

			x_vert += x;
			y_vert += y;
			z_vert += z;

			p_verts_calculated += num_components;
		}

//		LOGD("fte: Polygon: apply_transformation. vert_count = %d", vert_count);
		dirty = false;
	}

	inline void apply_color() {
		if (preserve_vert_colors)
			return;

		if (!dirty_color)
			return;

		if (!color_component_offset)
			return;

		float* p_verts = verts;
		float* p_verts_calculated = verts_calculated;

		uint32_t c = color;
		apply_alpha(c, alpha);

		float colorf = PACK_COLOR_I(c);

		for (int i = 0; i != vert_count; i++) {
			*(p_verts + color_component_offset) = colorf;
			p_verts += num_components;

			*(p_verts_calculated + color_component_offset) = colorf;
			p_verts_calculated += num_components;
		}
		dirty_color = false;
	}

	inline void set_vert_component(int index, int component, float value) {
#ifdef DEBUG
		if (component >= num_components) {
			LOGE("fte: Polygon: set_vert_component: component >= num_components (%d >= %d)", component, num_components);
			return;
		}
#endif
		int idx = index * num_components + component;
		verts[idx] = value;
		verts_calculated[idx] = value;
		dirty_color = true;
		dirty = true;
	}

	inline float get_vert_component(int index, int component) const {
		if (component >= num_components) {
			LOGE("fte: Polygon: get_vert_component: component >= num_components (%d >= %d)", component, num_components);
			return 0.;
		}
		return verts[index * num_components + component];
	}

	void calc_verts_aabb() {
		if (dirty_aabb) {
			dirty_aabb = false;
			calculate_aabb(verts_aabb.x1, verts_aabb.x2, verts_aabb.y1, verts_aabb.y2);
		}
	}

public:

	Polygon(bool use_color, bool use_uv, bool use_normal, bool clockwise_index_order = false, bool use_nm_uv = false) :
			RenderCommandableIncIndexes() {

		this->vert_count = 0;
		this->vert_size = 0;
		this->verts = NULL;
		this->verts_calculated = NULL;

		this->ind_count = 0;
		this->ind_size = 0;
		this->last_offset = 0;
		this->inds = NULL;
		this->inds_tmp = NULL;

		this->dirty_aabb = true;

		this->fixed_rotation = false;
		this->normals_rotation = true;

		this->preserve_vert_colors = false;
		this->preserve_vert_transform = false;

		this->enabled = true;

		this->clockwise_index_order = clockwise_index_order;

		num_components = 3;

		int offset = 3;

		if (use_color) {
			num_components += 1;
			color_component_offset = offset;
			offset += 1;
		}

		if (use_uv) {
			num_components += 2;
			uv_component_offset = offset;
			offset += 2;
		}

		if (use_normal) {
			num_components += 3;
			normal_component_offset = offset;
			offset += 3;
		}

		if (use_nm_uv) {

			num_components += 2;
			nm_uv_component_offset = offset;
			offset += 2;

			num_components += 3;
			tangent_component_offset = offset;
			offset += 3;

			num_components += 3;
			bitangent_component_offset = offset;
			offset += 3;

		}
	}

	Polygon(const Polygon & p) :
			RenderCommandableIncIndexes(p), Transformable(p) {

		verts_aabb = p.verts_aabb;
		dirty_aabb = p.dirty_aabb;

		clockwise_index_order = p.clockwise_index_order;

		normals_rotation = p.normals_rotation;
		fixed_rotation = p.fixed_rotation;
		preserve_vert_colors = p.preserve_vert_colors;
		preserve_vert_transform = p.preserve_vert_transform;

		ind_count = p.ind_count;
		ind_size = p.ind_size;
		last_offset = p.last_offset;

		inds = new short[ind_count];
		inds_tmp = new short[ind_count];

		memcpy(inds, p.inds, ind_size);
		memcpy(inds_tmp, p.inds_tmp, ind_size);

		vert_count = p.vert_count;
		vert_size = p.vert_size;

		verts = new float[vert_count * num_components];
		verts_calculated = new float[vert_count * num_components];

		memcpy(verts, p.verts, vert_size);
		memcpy(verts_calculated, p.verts_calculated, vert_size);
	}

	Polygon & operator=(const Polygon & rhs) {

		RenderCommandableIncIndexes::operator=(rhs);
		Transformable::operator=(rhs);

		verts_aabb = rhs.verts_aabb;
		dirty_aabb = rhs.dirty_aabb;

		clockwise_index_order = rhs.clockwise_index_order;

		normals_rotation = rhs.normals_rotation;
		fixed_rotation = rhs.fixed_rotation;
		preserve_vert_colors = rhs.preserve_vert_colors;
		preserve_vert_transform = rhs.preserve_vert_transform;

		ind_count = rhs.ind_count;
		ind_size = rhs.ind_size;
		last_offset = rhs.last_offset;

		if (inds)
			free(inds);
		if (inds_tmp)
			free(inds_tmp);

		inds = new short[ind_count];
		inds_tmp = new short[ind_count];

		memcpy(inds, rhs.inds, ind_size);
		memcpy(inds_tmp, rhs.inds_tmp, ind_size);

		vert_count = rhs.vert_count;
		vert_size = rhs.vert_size;

		if (verts)
			free(verts);
		if (verts_calculated)
			free(verts_calculated);

		verts = new float[vert_count * num_components];
		verts_calculated = new float[vert_count * num_components];

		memcpy(verts, rhs.verts, vert_size);
		memcpy(verts_calculated, rhs.verts_calculated, vert_size);

		return *this;
	}

	virtual ~Polygon() {

		if (verts)
			free(verts);
		if (verts_calculated)
			free(verts_calculated);

		if (inds)
			free(inds);
		if (inds_tmp)
			free(inds_tmp);

	}

	float get_ratio() {
		calc_verts_aabb();
		float w = verts_aabb.x2 - verts_aabb.x1;
		float h = verts_aabb.y2 - verts_aabb.y1;
		return w / h;
	}

	bool is_normals_rotation() {
		return this->normals_rotation;
	}

	void set_normals_rotation(bool v) {
		this->normals_rotation = v;
	}

	bool get_clockwise_index_order() {
		return clockwise_index_order;
	}

	void set_clockwise_index_order(bool clockwise_index_order) {
		this->clockwise_index_order = clockwise_index_order;
	}

	virtual void set_vert_count(int vert_count, bool default_indexes = true) {

		if (verts)
			free(verts);
		if (verts_calculated)
			free(verts_calculated);

		this->vert_count = vert_count;
		this->vert_size = vert_count * num_components * sizeof(float);
		this->verts = (float*) malloc(vert_size);
		this->verts_calculated = (float*) malloc(vert_size);

		memset(this->verts, 0, vert_size);
		memset(this->verts_calculated, 0, vert_size);

		if (default_indexes)
			build_default_indexes();
	}

	virtual void set_ind_count(int ind_count) {

		if (inds)
			free(inds);
		if (inds_tmp)
			free(inds_tmp);

		this->last_offset = 0;

		this->ind_count = ind_count;
		this->ind_size = ind_count * sizeof(short);
		this->inds = (short*) malloc(ind_size);
		this->inds_tmp = (short*) malloc(ind_size);

		memset(this->inds, 0, ind_size);
		memset(this->inds_tmp, 0, ind_size);
	}

	void build_default_indexes() {
		set_ind_count((vert_count - 2) * 3);

		if (clockwise_index_order) {
			int idx = 0;
			for (int i = vert_count - 1; i > 1; i--) {
				inds[idx++] = 0;
				inds[idx++] = i;
				inds[idx++] = i - 1;
			}
		} else {
			int idx = 0;
			for (int i = 1, end = vert_count - 1; i < end; i++) {
				inds[idx++] = 0;
				inds[idx++] = i;
				inds[idx++] = i + 1;
			}
		}

		memcpy(inds_tmp, inds, ind_size);
	}

	void build_triangle_strip_swap_even_indexes() {
		set_ind_count((vert_count - 2) * 3);

		int idx = 0;
		for (int i = 0, end = vert_count - 2; i < end; i++) {
			bool even = i % 2 == 0;
			if ((!clockwise_index_order && even) || (clockwise_index_order && !even)) {
				inds[idx++] = i;
				inds[idx++] = i + 1;
				inds[idx++] = i + 2;
			} else {
				inds[idx++] = i;
				inds[idx++] = i + 2;
				inds[idx++] = i + 1;
			}
		}

		memcpy(inds_tmp, inds, ind_size);
	}

	void assign_data(void* verts, int vert_count, void* inds, int ind_count) {
		set_vert_count(vert_count, false);
		set_ind_count(ind_count);

		memcpy(this->verts, verts, vert_size);
		memcpy(this->verts_calculated, verts, vert_size);
		memcpy(this->inds, inds, ind_size);
		memcpy(this->inds_tmp, inds, ind_size);
	}

	void assign_ind_data(void* inds) {
		memcpy(this->inds, inds, ind_size);
		memcpy(this->inds_tmp, inds, ind_size);
	}

	void set_index_value(int index, int value) {
		if (index >= 0 && index < ind_count) {
			inds[index] = value;
			inds_tmp[index] = value;
		}
	}

	// RenderCommandable methods impl

	virtual void write_to_vert_buffer(Buffer* vert_buf, int &size) {
		apply_transformation();
		apply_color();
		vert_buf->write(verts_calculated, vert_size);
		size = vert_size;
	}

	virtual void write_to_index_buffer(Buffer* index_buf, int offset, int &size) {
		if (last_offset != offset) {
			short* p_inds_tmp = inds_tmp;
			short* p_inds = inds;
			for (int i = 0; i != ind_count; i++) {
				*p_inds_tmp = *p_inds + offset;
				p_inds_tmp++;
				p_inds++;
			}
			last_offset = offset;
		}
		index_buf->write(inds_tmp, ind_size);
		size = ind_size;
	}

	inline int get_vert_count() const {
		return vert_count;
	}

	int get_ind_count() const {
		return ind_count;
	}

	// intersection

	bool intersect_with(Polygon* other) {
		if (dirty)
			apply_transformation();

		if (other->dirty)
			other->apply_transformation();

		return is_polyhedron_intersects(verts_calculated, vert_count, num_components, other->verts_calculated, other->vert_count, other->num_components);
	}

	// verts

	void set_vert_normal(int index, float x, float y, float z) {
		if (!normal_component_offset) {
			LOGE("fte: Polygon: set_vert_normal. normals are not supported by this polygon. pass use_normal in constructor.");
			return;
		}
		set_vert_component(index, normal_component_offset, x);
		set_vert_component(index, normal_component_offset + 1, y);
		set_vert_component(index, normal_component_offset + 2, z);
	}

	void get_vert_normal(int index, float &x, float &y, float &z) {
		if (!normal_component_offset) {
			LOGE("fte: Polygon: get_vert_normal. normals are not supported by this polygon. pass use_normal in constructor.");
			return;
		}
		x = get_vert_component(index, normal_component_offset);
		y = get_vert_component(index, normal_component_offset + 1);
		z = get_vert_component(index, normal_component_offset + 2);
	}

	void set_vert_tangent(int index, float x, float y, float z) {
		if (!tangent_component_offset) {
			LOGE("fte: Polygon: set_vert_tangent. normals are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		set_vert_component(index, tangent_component_offset, x);
		set_vert_component(index, tangent_component_offset + 1, y);
		set_vert_component(index, tangent_component_offset + 2, z);
	}

	void get_vert_tangent(int index, float &x, float &y, float &z) {
		if (!tangent_component_offset) {
			LOGE("fte: Polygon: get_vert_tangent. normals are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		x = get_vert_component(index, tangent_component_offset);
		y = get_vert_component(index, tangent_component_offset + 1);
		z = get_vert_component(index, tangent_component_offset + 2);
	}

	void set_vert_bitangent(int index, float x, float y, float z) {
		if (!bitangent_component_offset) {
			LOGE("fte: Polygon: set_vert_bitangent. normals are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		set_vert_component(index, bitangent_component_offset, x);
		set_vert_component(index, bitangent_component_offset + 1, y);
		set_vert_component(index, bitangent_component_offset + 2, z);
	}

	void get_vert_bitangent(int index, float &x, float &y, float &z) {
		if (!bitangent_component_offset) {
			LOGE("fte: Polygon: get_vert_bitangent. normals are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		x = get_vert_component(index, bitangent_component_offset);
		y = get_vert_component(index, bitangent_component_offset + 1);
		z = get_vert_component(index, bitangent_component_offset + 2);
	}

	void set_vert_color(int index, uint32_t color) {
		if (!color_component_offset) {
			LOGE("fte: Polygon: set_vert_color. color are not supported by this polygon. pass use_color in constructor.");
			return;
		}
		set_vert_component(index, color_component_offset, PACK_COLOR_I(color));
	}

	uint32_t get_vert_color(int index) const {
		if (!color_component_offset) {
			LOGE("fte: Polygon: get_vert_color. color are not supported by this polygon. pass use_color in constructor.");
			return 0xffffffff;
		}
		float f = get_vert_component(index, color_component_offset);
		return UNPACK_COLOR_I(f);
	}

	inline void set_vert_xyz(int index, float x, float y, float z) {
		set_vert_component(index, 0, x);
		set_vert_component(index, 1, y);
		set_vert_component(index, 2, z);
		dirty_aabb = true;
	}

	inline void set_vert_xy(int index, float x, float y) {
		set_vert_component(index, 0, x);
		set_vert_component(index, 1, y);
		dirty_aabb = true;
	}

	inline void get_vert_xyz(int index, float &x, float &y, float &z) const {
		x = get_vert_component(index, 0);
		y = get_vert_component(index, 1);
		z = get_vert_component(index, 2);
	}

	float get_vert_u(int index) const {
		if (!uv_component_offset) {
			LOGE("fte: Polygon: get_vert_u. uv are not supported by this polygon. pass use_uv in constructor.");
			return 0.0f;
		}
		return get_vert_component(index, uv_component_offset);
	}

	float get_vert_v(int index) const {
		if (!uv_component_offset) {
			LOGE("fte: Polygon: get_vert_v. uv are not supported by this polygon. pass use_uv in constructor.");
			return 0.0f;
		}
		return get_vert_component(index, uv_component_offset + 1);
	}

	void set_vert_uv(int index, float u, float v) {
		if (!uv_component_offset) {
			LOGE("fte: Polygon: set_vert_uv. uv are not supported by this polygon. pass use_uv in constructor.");
			return;
		}
		set_vert_component(index, uv_component_offset, u);
		set_vert_component(index, uv_component_offset + 1, v);
	}

	void set_vert_u(int index, float u) {
		if (!uv_component_offset) {
			LOGE("fte: Polygon: set_vert_u. uv are not supported by this polygon. pass use_uv in constructor.");
			return;
		}
		set_vert_component(index, uv_component_offset, u);
	}

	void set_vert_v(int index, float v) {
		if (!uv_component_offset) {
			LOGE("fte: Polygon: set_vert_v. uv are not supported by this polygon. pass use_uv in constructor.");
			return;
		}
		set_vert_component(index, uv_component_offset + 1, v);
	}

	// normal map uv

	float get_vert_nm_u(int index) const {
		if (!nm_uv_component_offset) {
			LOGE("fte: Polygon: get_vert_nm_u. nm_uv are not supported by this polygon. pass use_nm_uv in constructor.");
			return 0.0f;
		}
		return get_vert_component(index, nm_uv_component_offset);
	}

	float get_vert_nm_v(int index) const {
		if (!nm_uv_component_offset) {
			LOGE("fte: Polygon: get_vert_nm_v. nm_uv are not supported by this polygon. pass use_nm_uv in constructor.");
			return 0.0f;
		}
		return get_vert_component(index, nm_uv_component_offset + 1);
	}

	void set_vert_nm_uv(int index, float u, float v) {
		if (!nm_uv_component_offset) {
			LOGE("fte: Polygon: set_vert_nm_uv. nm_uv are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		set_vert_component(index, nm_uv_component_offset, u);
		set_vert_component(index, nm_uv_component_offset + 1, v);
	}

	void set_vert_nm_u(int index, float u) {
		if (!nm_uv_component_offset) {
			LOGE("fte: Polygon: set_vert_nm_u. nm_uv are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		set_vert_component(index, nm_uv_component_offset, u);
	}

	void set_vert_nm_v(int index, float v) {
		if (!nm_uv_component_offset) {
			LOGE("fte: Polygon: set_vert_nm_v. nm_uv are not supported by this polygon. pass use_nm_uv in constructor.");
			return;
		}
		set_vert_component(index, nm_uv_component_offset + 1, v);
	}

	void set_vert_xyz_uv(int index, float x, float y, float z, float u, float v) {
		set_vert_xyz(index, x, y, z);
		set_vert_uv(index, u, v);
		dirty_aabb = true;
	}

	void set_origin_to(uint16_t alignment) {

		float min_x, max_x, min_y, max_y, min_z, max_z;
		calculate_aabb(min_x, max_x, min_y, max_y, min_z, max_z);

		switch (alignment & ORIGIN_H_MASK) {
		case ORIGIN_LEFT:
			set_origin_x(min_x);
			break;
		case ORIGIN_RIGHT:
			set_origin_x(max_x);
			break;
		case ORIGIN_H_CENTER:
			set_origin_x(min_x + (max_x - min_x) * 0.5f);
			break;
		}

		switch (alignment & ORIGIN_V_MASK) {
		case ORIGIN_TOP:
			set_origin_y(max_y);
			break;
		case ORIGIN_BOTTOM:
			set_origin_y(min_y);
			break;
		case ORIGIN_V_CENTER:
			set_origin_y(min_y + (max_y - min_y) * 0.5f);
			break;
		}

		switch (alignment & ORIGIN_Z_MASK) {
		case ORIGIN_NEAR:
			set_origin_z(max_z);
			break;
		case ORIGIN_FAR:
			set_origin_z(min_z);
			break;
		case ORIGIN_Z_CENTER:
			set_origin_z(min_z + (max_z - min_z) * 0.5f);
			break;
		}
	}

	inline void set_fixed_rotation(bool fixed_rotation) {
		this->fixed_rotation = fixed_rotation;
	}

	inline bool is_fixed_rotation() {
		return this->fixed_rotation;
	}

	inline bool is_preserve_vert_colors() {
		return preserve_vert_colors;
	}

	inline void set_preserve_vert_colors(bool preserve_vert_colors) {
		this->preserve_vert_colors = preserve_vert_colors;
	}

	inline bool is_preserve_vert_transform() {
		return preserve_vert_transform;
	}

	inline void set_preserve_vert_transform(bool preserve_vert_transform) {
		this->preserve_vert_transform = preserve_vert_transform;
	}

	// utilities to positioning polygon on screen

	void set_bounds(const Rect & r) {
		set_bounds(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1);
	}

	void set_bounds(float x, float y, float w, float h) {

		float sx, sy;

		calc_verts_aabb();

		sx = w / (verts_aabb.x2 - verts_aabb.x1);
		sy = h / (verts_aabb.y2 - verts_aabb.y1);

		set_scale_xy(sx, sy);

		set_translate_x(x - verts_aabb.x1 * sx + origin_x * sx);
		set_translate_y(y - verts_aabb.y1 * sy + origin_y * sy);
	}

	void fill_cell_inside_preserve_ratio(float x, float y, float w, float h, float row, float col, float rows, float cols, float pt = 0, float pr = 0, float pb = 0, float pl = 0) {
		float _w = w / cols;
		float _h = h / rows;
		float _x = x + _w * col;
		float _y = y + _h * row;
		set_bounds_inside_preserve_ratio(_x, _y, _w, _h, pt, pr, pb, pl);
	}

	void fill_cell_inside_preserve_ratio(const Rect & r, const Cell & c, const Padding & p) {
		fill_cell_inside_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1, c.row, c.col, c.rows, c.cols, p.t, p.r, p.b, p.l);
	}

	void fill_cell_inside_preserve_ratio(const Rect & r, const Cell & c) {
		fill_cell_inside_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1, c.row, c.col, c.rows, c.cols);
	}

	void fill_cell_crop_preserve_ratio(float x, float y, float w, float h, float row, float col, float rows, float cols) {
		float _w = w / cols;
		float _h = h / rows;
		float _x = x + _w * col;
		float _y = y + _h * row;
		set_bounds_crop_preserve_ratio(_x, _y, _w, _h);
	}

	void fill_cell_crop_preserve_ratio(const Rect & r, const Cell & c) {
		fill_cell_crop_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1, c.row, c.col, c.rows, c.cols);
	}

	void set_bounds_crop_preserve_ratio(const Rect & r) {
		set_bounds_crop_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1);
	}

	void set_bounds_crop_preserve_ratio(float x, float y, float w, float h) {

		float _w = w;
		float _h = h;
		float _x = x;
		float _y = y;

		float b_ratio = _w / _h;
		float ratio = get_ratio();

		if (ratio >= 1.0f && b_ratio >= 1.0f) {
			if (ratio > b_ratio) {
				_w = _h * ratio;
				_x = x + (w - _w) * 0.5f;
			} else {
				_h = _w / ratio;
				_y = y + (h - _h) * 0.5f;
			}
		} else if (ratio <= 1.0f && b_ratio <= 1.0f) {
			if (ratio > b_ratio) {
				_w = _h * ratio;
				_x = x + (w - _w) * 0.5f;
			} else {
				_h = _w / ratio;
				_y = y + (h - _h) * 0.5f;
			}
		} else if (ratio >= 1.0f && b_ratio <= 1.0f) {
			_w = _h * ratio;
			_x = x + (w - _w) * 0.5f;
		} else if (ratio <= 1.0f && b_ratio >= 1.0f) {
			_h = _w / ratio;
			_y = y + (h - _h) * 0.5f;
		}
		set_bounds(_x, _y, _w, _h);
	}

	void set_bounds_inside_preserve_ratio(const Rect & r, const Padding & p) {
		set_bounds_inside_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1, p.t, p.r, p.b, p.l);
	}

	void set_bounds_inside_preserve_ratio(const Rect & r) {
		set_bounds_inside_preserve_ratio(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1);
	}

	void set_bounds_inside_preserve_ratio(float x, float y, float w, float h, float pt = 0, float pl = 0, float pb = 0, float pr = 0) {

		x = x + w * pl;
		y = y + h * pb;
		w = w * (1.0f - pl - pr);
		h = h * (1.0f - pt - pb);

		float bw = w;
		float bh = h;
		float bx = x;
		float by = y;
		float bratio = bw / bh;

		calc_verts_aabb();
		float pw = verts_aabb.x2 - verts_aabb.x1;
		float ph = verts_aabb.y2 - verts_aabb.y1;
		float pratio = pw / ph;

		float scale = pratio > bratio ? bw / pw : bh / ph;

		float _w = pw * scale;
		float _h = ph * scale;
		float _x = x + 0.5f * bw - 0.5f * _w;
		float _y = y + 0.5f * bh - 0.5f * _h;

		set_bounds(_x, _y, _w, _h);

//		if (ratio >= 1.0f && b_ratio >= 1.0f) {
//			if (ratio > b_ratio) {
//				_h = _w / ratio;
//				_y = y + (h - _h) * 0.5f;
//			} else {
//				_w = _h * ratio;
//				_x = x + (w - _w) * 0.5f;
//			}
//		} else if (ratio <= 1.0f && b_ratio <= 1.0f) {
//			if (ratio > b_ratio) {
//				_h = _w / ratio;
//				_y = y + (h - _h) * 0.5f;
//			} else {
//				_w = _h * ratio;
//				_x = x + (w - _w) * 0.5f;
//			}
//		} else if (ratio >= 1.0f && b_ratio <= 1.0f) {
//			_h = _w / ratio;
//			_y = y + (h - _h) * 0.5f;
//		} else if (ratio <= 1.0f && b_ratio >= 1.0f) {
//			_w = _h * ratio;
//			_x = x + (w - _w) * 0.5f;
//		}

//		set_bounds(_x, _y, _w, _h);
	}

	void calculate_aabb(float & min_x, float & max_x, float & min_y, float & max_y) {
		float min_z, max_z;
		calculate_aabb(min_x, max_x, min_y, max_y, min_z, max_z);
	}

	void calculate_aabb(float & min_x, float & max_x, float & min_y, float & max_y, float & min_z, float & max_z) {

		min_x = std::numeric_limits<float>::max();
		max_x = -min_x;
		min_y = std::numeric_limits<float>::max();
		max_y = -min_y;
		min_z = std::numeric_limits<float>::max();
		max_z = -min_z;

		for (int i = 0; i < vert_count; i++) {

			int idx_base = i * num_components;
			int idx_x = idx_base++;
			int idx_y = idx_base++;
			int idx_z = idx_base++;

			float x_vert = verts[idx_x];
			float y_vert = verts[idx_y];
			float z_vert = verts[idx_z];

			if (min_x > x_vert)
				min_x = x_vert;

			if (max_x < x_vert)
				max_x = x_vert;

			if (min_y > y_vert)
				min_y = y_vert;

			if (max_y < y_vert)
				max_y = y_vert;

			if (min_z > z_vert)
				min_z = z_vert;

			if (max_z < z_vert)
				max_z = z_vert;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	// FOR DEBUG ONLY
	void get_vert_as_vec4(int index, glm::vec4 &v) {
		if (dirty)
			apply_transformation();
		float x = verts_calculated[index * num_components];
		float y = verts_calculated[index * num_components + 1];
		float z = verts_calculated[index * num_components + 2];
		v.x = x;
		v.y = y;
		v.z = z;
		v.w = 1.;
	}

	void get_vert_uv_as_vec4(int index, glm::vec4 &vec) {
		if (dirty)
			apply_transformation();
		float u = verts_calculated[index * num_components + 4];
		float v = verts_calculated[index * num_components + 5];
		vec.x = u;
		vec.y = v;
		vec.z = 1.;
		vec.w = 1.;
	}

	void print_verts(glm::mat4* m) {
		LOGD("==============================");
		for (int i = 0; i < vert_count; i++) {
			glm::vec4 v;
			glm::vec4 uv;
			get_vert_as_vec4(i, v);
			get_vert_uv_as_vec4(i, uv);
			if (m != NULL)
				v = *m * v;
			LOGD("%d: %f %f %f %f     (%f %f)", i, v.x, v.y, v.z, v.w, uv.x, uv.y);
			v = v / v.w;
			LOGD("%d: %f %f %f %f     (%f %f)", i, v.x, v.y, v.z, v.w, uv.x, uv.y);
		}
		LOGD("==============================");
	}
	// FOR DEBUG ONLY
	//////////////////////////////////////////////////////////////////////////////////
};

} // namespace fte

#endif // polygon_H
