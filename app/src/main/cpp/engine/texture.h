#ifndef texture_H
#define texture_H

#include "engine.h"
#include "stddef.h"
#include "external.h"
#include "png.h"
#include "fte_utils.h"
#include "texture_descriptor.h"

#include <string>

#include "fte_gl.h"
#include "fte_utils_gl.h"

namespace fte {

class Texture {
private:
	TextureDescriptor texture_descriptor;
	GLuint texture_id;
	GLenum texture_unit;
	GLsizei width;
	GLsizei height;
	GLsizei data_size;

	GLuint framebuffer_id;

	void* load_pkm(std::string file_name, FTE_ENV env) {

		void* buf;
		long length;

		if (!load_resource_file(env, file_name, buf, length))
			return NULL;

		data_size = length - sizeof(ETC1Header);
		width = swap_bytes(((ETC1Header*) buf)->texWidth);
		height = swap_bytes(((ETC1Header*) buf)->texHeight);

		LOGD("pkm %d %d %d", data_size, width, height);

		void* data = malloc(data_size);
		void* data_ptr = ((char*) buf) + sizeof(ETC1Header);
		memcpy(data, data_ptr, data_size);

		free(buf);
		buf = NULL;

		return data;
	}

	void* load_png(std::string file_name, FTE_ENV env) {

		void* data;
		read_in in;

		if (!in.init(file_name, env)) {
			LOGE("fte: Texture: unable to load texture. file not found: %s", file_name.c_str());
			return NULL;
		}

		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if (!png_ptr) {
			LOGE("fte error png_create_read_struct: %s", file_name.c_str());
			return NULL;
		}

		png_infop info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr) {
			LOGE("fte error png_create_info_struct: %s", file_name.c_str());
			png_destroy_read_struct(&png_ptr, NULL, NULL);
			return NULL;
		}

		png_set_read_fn(png_ptr, &in, my_read_png);

		png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

		png_uint_32 width, height;
		int color_type, interlace_type;
		int bit_depth;
		png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, NULL, NULL);

		this->width = width;
		this->height = height;

		unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
		data = malloc(row_bytes * height);

		png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);

		for (int i = 0; i < height; i++) {
			memcpy((char*) data + (row_bytes * (height - 1 - i)), row_pointers[i], row_bytes);
		}

		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

		return data;
	}

public:
	Texture(const TextureDescriptor & texture_descriptor) {
		this->texture_descriptor = texture_descriptor;
		this->texture_id = 0;
		this->texture_unit = GL_TEXTURE0;
		this->framebuffer_id = 0;
		this->width = 0;
		this->height = 0;
		this->data_size = 0;
	}

	~Texture() {
	}

	void init(FTE_ENV env, GLenum unit, int sw, int sh) {

		std::string file_name = texture_descriptor.to_string();
		texture_unit = unit;

		void* data = NULL;
		bool is_pkm = false;
		bool is_offscreen = false;

		if (ends_with(file_name, ".png"))
			data = load_png(file_name, env);
		else if (ends_with(file_name, ".pkm")) {
			data = load_pkm(file_name, env);
			is_pkm = true;
		} else {
			is_offscreen = true;
			width = sw;
			height = sh;
		}

		if (!is_offscreen && data == NULL) {
			LOGE("fte: Texture::init. Failed to load texture data from file '%s'", file_name.c_str());
			return;
		}

		if (is_offscreen) {
			glGenFramebuffers(1, &framebuffer_id);
			glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id);
		}

		glGenTextures(1, &texture_id);
		CHECK_GL_ERROR("glGenTextures");

		glActiveTexture(unit);

		glBindTexture(GL_TEXTURE_2D, texture_id);
		CHECK_GL_ERROR("glBindTexture");

		if (is_pkm)
			glCompressedTexImage2D(GL_TEXTURE_2D, 0, GL_ETC1_RGB8_OES, width, height, 0, data_size, data);
		else {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			CHECK_GL_ERROR("glTexImage2D");
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		if (is_offscreen) {
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture_id, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

//		glGenerateMipmap(GL_TEXTURE_2D);
//		CHECK_GL_ERROR("glGenerateMipmap");

		if (data) {
			free(data);
			data = NULL;
		}

		LOGI("fte: Texture: init %dx%d: %s", width, height, file_name.c_str());
	}

	void release() {
		LOGI("fte: Texture: release %dx%d (%s)", width, height, texture_descriptor.to_string().c_str());
		glDeleteTextures(1, &texture_id);
		texture_id = 0;
		texture_unit = GL_TEXTURE0;
		if (framebuffer_id) {
			glDeleteFramebuffers(1, &framebuffer_id);
			framebuffer_id = 0;
		}
		width = 0;
		height = 0;
	}

	inline const TextureDescriptor & get_texture_descriptor() const {
		return texture_descriptor;
	}

	inline GLuint get_texture_id() const {
		return texture_id;
	}

	inline GLuint get_framebuffer_id() const {
		return framebuffer_id;
	}

	inline GLenum get_texture_unit() const {
		return texture_unit - GL_TEXTURE0;
	}
};

} // namespace fte

#endif // texture_H
