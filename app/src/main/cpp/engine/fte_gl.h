#ifndef fte_gl_H
#define fte_gl_H

#ifdef PLATFORM_IOS
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#endif

#ifdef PLATFORM_ANDROID
#include "GLES2/gl2.h"
#include "GLES2/gl2ext.h"
#endif

#ifdef PLATFORM_WIN
#include "glew.h"
#endif

#endif /* fte_gl_H */
