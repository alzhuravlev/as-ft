#ifndef scene_H
#define scene_H

#include "screen.h"
#include "actor.h"
#include "frame_anim_polygon_cache.h"
#include "loaders.h"

#include <vector>
#include <list>

namespace fte {

class Actor;

bool actors_comparator(Actor* a1, Actor* a2);

class Scene: public Screen {

	friend class Actor;

private:

	std::vector<Actor*> actors;

	FrameAnimationPolygonCache frame_animation_polygon_cache;
	Loaders loaders;

	void activate_actor(Actor* actor);
	void deactivate_actor(Actor* actor);

	virtual void do_init_once(FTE_ENV env);
	virtual void do_init(FTE_ENV env);
	virtual void do_release(FTE_ENV env);

protected:

	virtual void do_touch(TouchEvent events[], int size);
	virtual void do_init_renderer(InitRendererCommand* command, FTE_ENV env);

	virtual void do_create_actors(std::vector<Actor*> & actors_to_add) = 0;

	virtual void do_start_scene(FTE_ENV env) = 0;
	virtual void do_stop_scene(FTE_ENV env) = 0;

	virtual void do_init_scene(FTE_ENV env) = 0;

	virtual void do_receive_broadcast(uint64_t type, void* p);

public:
	Scene();

	~Scene();

	void unproject_tap_ray(float win_x, float win_y, Vec3 & ray_origin, Vec3 & ray_direction, const Camera & camera);

	Actor* find_first_inactive(int type);

	Actor* find_first_active(int type);

	void find_all_active(int type, std::vector<Actor*> & out);

	void send_broadcast(uint64_t type, void* p = NULL);

	inline Loaders & get_loaders() {
		return loaders;
	}

	inline FrameAnimationPolygonCache & get_frame_animation_polygon_cache() {
		return frame_animation_polygon_cache;
	}

	virtual bool is_paused() const;
};

} // namespace fte

#endif /* scene_H */

