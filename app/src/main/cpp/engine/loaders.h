#ifndef loaders_H
#define loaders_H

#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "font_loader.h"
#include "obj_model_loader.h"

#include <map>
#include <string>

namespace fte {

class Loaders {
private:

	std::map<std::string, TextureAtlasLoader*> texture_atlas_loaders;
	std::map<std::string, Box2dLoader*> box2d_loaders;
	std::map<std::string, FontLoader*> font_loaders;
	std::map<std::string, ObjModelLoader*> obj_model_loaders;

public:

	~Loaders() {
		delete_map_values<std::string, TextureAtlasLoader*>(texture_atlas_loaders);
		delete_map_values<std::string, Box2dLoader*>(box2d_loaders);
		delete_map_values<std::string, FontLoader*>(font_loaders);
		delete_map_values<std::string, ObjModelLoader*>(obj_model_loaders);
	}

	TextureAtlasLoader* get_texture_atlas_loader(const std::string & file_name, FTE_ENV env) {
		std::map<std::string, TextureAtlasLoader*>::iterator it = texture_atlas_loaders.find(file_name);
		TextureAtlasLoader* loader = NULL;
		if (it == texture_atlas_loaders.end()) {
			loader = new TextureAtlasLoader(file_name, env);
			texture_atlas_loaders[file_name] = loader;
		} else {
			loader = it->second;
		}
		return loader;
	}

	Box2dLoader* get_box2d_loader(const std::string & file_name, FTE_ENV env) {
		std::map<std::string, Box2dLoader*>::iterator it = box2d_loaders.find(file_name);
		Box2dLoader* loader = NULL;
		if (it == box2d_loaders.end()) {
			loader = new Box2dLoader(file_name, env);
			box2d_loaders[file_name] = loader;
		} else {
			loader = it->second;
		}
		return loader;
	}

	FontLoader* get_font_loader(const std::string & file_name, FTE_ENV env) {
		std::map<std::string, FontLoader*>::iterator it = font_loaders.find(file_name);
		FontLoader* loader = NULL;
		if (it == font_loaders.end()) {
			loader = new FontLoader(file_name, env);
			font_loaders[file_name] = loader;
		} else {
			loader = it->second;
		}
		return loader;
	}

	ObjModelLoader* get_obj_model_loader(const std::string & file_name, FTE_ENV env) {
		std::map<std::string, ObjModelLoader*>::iterator it = obj_model_loaders.find(file_name);
		ObjModelLoader* loader = NULL;
		if (it == obj_model_loaders.end()) {
			loader = new ObjModelLoader(file_name, env);
			obj_model_loaders[file_name] = loader;
		} else {
			loader = it->second;
		}
		return loader;
	}

};

} // namespace fte
#endif /* loaders_H */
