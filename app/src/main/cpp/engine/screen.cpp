#include "screen.h"

namespace fte {

Screen::Screen(Screen* parent) {
	initialized_once = false;
	initialized = false;
	renderer_initialized = false;
	screen_size_initialized = false;
	updated_once = false;

	width = 0;
	height = 0;

	screen_ratio = 1.0f;
	screen_max_dim = 0.0f;
	screen_min_dim = 0.0f;
	screen_height = 0.0f;
	screen_width = 0.0f;

	portrait_orientation = false;

	fade_in_transition = NULL;
	fade_out_transition = NULL;

	updater = NULL;

	this->parent = parent;
}

Screen::~Screen() {
	delete_vector_elements<Screen*>(active_layers);
	delete_map_values<int, Screen*>(layers);
}

void Screen::update_screen_sizes(int w, int h) {
	width = w;
	height = h;

	portrait_orientation = width < height;

	screen_max_dim = std::max(width, height);
	screen_min_dim = std::min(width, height);

	screen_ratio = screen_min_dim / screen_max_dim;

	if (portrait_orientation) {
		screen_width = screen_ratio;
		screen_height = 1.0f;
	} else {
		screen_width = 1.0f;
		screen_height = screen_ratio;
	}
}

void Screen::resize(int w, int h, FTE_ENV env) {
	LOGI("fte: Screen: process RESIZE command %d x %d", w, h);

	screen_size_initialized = true;
	update_screen_sizes(w, h);

	// the screen becomes size of (1 x screen_ratio) 1 - height; screen_ratio - width
	ortho_camera.update(width, height);
	ortho_camera.set_scale(screen_max_dim);
	ortho_camera.update_view_proj_matrix();

	do_resize(env);

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->resize(w, h, env);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->resize(w, h, env);
	}

	if (fade_in_transition != NULL)
		fade_in_transition->resize(w, h);

	if (fade_out_transition != NULL)
		fade_out_transition->resize(w, h);
}

void Screen::init_renderer(InitRendererCommand* command, FTE_ENV env) {
	if (renderer_initialized)
		return;

	renderer_initialized = true;
	LOGI("fte: Screen: generate InitRendererCommand command");

#ifdef DEBUG
	FontLoader font_loader("fonts/default_font.fnt", env);
	font_loader.add_texture_descriptors(command);
#endif

	do_init_renderer(command, env);

	command->add(ShaderDescriptor(SHADER_ID_DEFAULT_COLORED, "shaders/default/colored/vs.glsl", "shaders/default/colored/fs.glsl"));
	command->add(ShaderDescriptor(SHADER_ID_DEFAULT_TEXTURED, "shaders/default/textured/vs.glsl", "shaders/default/textured/fs.glsl"));

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->init_renderer(command, env);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->init_renderer(command, env);
	}
}

void Screen::init_once(int w, int h, FTE_ENV env) {
	if (initialized_once)
		return;

	LOGI("fte: Screen: init_once %d x %d", w, h);
	initialized_once = true;

	update_screen_sizes(w, h);

	do_init_once(env);

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->init_once(w, h, env);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->init_once(w, h, env);
	}
}

void Screen::init(int w, int h, FTE_ENV env) {
	if (initialized)
		return;

	LOGI("fte: Screen: init %d x %d", w, h);
	update_screen_sizes(w, h);

	initialized = true;

#ifdef DEBUG
	FontLoader font_loader("fonts/default_font.fnt", env);
	fps_font_renderer.init(font_loader);
#endif

	do_init(env);

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->init(w, h, env);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->init(w, h, env);
	}
}

void Screen::release(FTE_ENV env) {
	initialized = false;
	renderer_initialized = false;
	screen_size_initialized = false;
	updated_once = false;

#ifdef DEBUG
	fps_font_renderer.release();
#endif

	do_release(env);

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->release(env);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->release(env);
	}
}

void Screen::update(float delta, RenderCommand* command) {
	update_state(delta);

	do_update(delta, command);

	if (active_layers.size() > 0) {
		for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
			Screen* active_layer = *it;
			active_layer->update(delta, command);
		}
	}

	if (popup_layers.size() > 0) {
		std::vector<Screen*>::iterator it = popup_layers.begin(), end = popup_layers.end();
		while (it != end) {
			Screen* popup_layer = *it;
			popup_layer->update(delta, command);
			++it;
		}
	}

	switch (get_state()) {
	case SCREEN_STATE_FADING_IN:
		fade_in_transition->update(delta, command, ortho_camera);
		if (fade_in_transition->get_state() == SCREEN_TRANS_STATE_DONE)
			change_state(SCREEN_STATE_FADE_IN_DONE);
		break;

	case SCREEN_STATE_FADING_OUT:
		fade_out_transition->update(delta, command, ortho_camera);
		if (fade_out_transition->get_state() == SCREEN_TRANS_STATE_DONE)
			change_state(SCREEN_STATE_FADE_OUT_DONE);
		break;

	case SCREEN_STATE_FADE_OUT_DONE:
		fade_out_transition->update(delta, command, ortho_camera);
		break;

	case SCREEN_STATE_FADE_IN_DONE:
		change_state(SCREEN_STATE_DEFAULT);
		break;
	}

#ifdef DEBUG
	if (!parent && updater) {
		Vec3 position(-0.5f * get_screen_width(), -0.5f * get_screen_height(), 0.0f);
		Vec3 scale(0.07f, 0.07f, 0.07f);

		std::string s = to_string(updater->____fps_flushed) + " " + to_string(updater->____trans_vert_avg) + " " + to_string(updater->____update_time_avg * 1e+6) + " " + to_string(updater->____batches_avg);

		command->begin_batch(SHADER_ID_DEFAULT_TEXTURED, 0, false, false, ortho_camera.get_view_proj_matrix());
		fps_font_renderer.render_string(s, *command, position, scale);
		command->end_batch();
	}
#endif

}

void Screen::update_once(RenderCommand* command) {
	if (updated_once)
		return;

	updated_once = true;

	do_update_once(command);

	for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
		Screen* layer = *it;
		layer->update_once(command);
	}

	for (std::map<int, Screen*>::iterator it = layers.begin(), end = layers.end(); it != end; ++it) {
		Screen* layer = it->second;
		layer->update_once(command);
	}
}

Screen* Screen::get_layer(int layer_id) {
	return layers[layer_id];
}

void Screen::add_layer(int layer_id, Screen* layer) {
	layers[layer_id] = layer;
}

void Screen::add_active_layer(Screen* layer) {
	active_layers.push_back(layer);
}

Screen* Screen::get_root_screen() {
	return parent == NULL ? this : parent->get_root_screen();
}

bool Screen::show_layer(int layer_id) {

	if (parent == NULL) {
		std::map<int, Screen*>::iterator it = layers.find(layer_id);
		if (it == layers.end())
			return false;

		Screen* layer_to_show = it->second;
		layer_to_show->do_show_as_layer();
		popup_layers.push_back(layer_to_show);
		return true;
	}

	return parent->show_layer(layer_id);
}

bool Screen::has_layer() const {
	if (parent)
		return parent->has_layer();
	return popup_layers.size() > 0;
}

void Screen::hide_layer() {
	if (!parent) {
		if (popup_layers.size() > 0)
			popup_layers.pop_back();
	} else
		parent->hide_layer();
}

void Screen::hide_all_layers() {
	if (!parent)
		popup_layers.clear();
	else
		parent->hide_layer();
}

void Screen::custom_command(int commands[], int size) {
	do_custom_command(commands, size);
}

void Screen::touch(TouchEvent events[], int size) {

	if (popup_layers.size() > 0) {
		Screen* current_popup_layer = popup_layers.back();
		current_popup_layer->touch(events, size);
	} else {
		do_touch(events, size);
		for (std::vector<Screen*>::iterator it = active_layers.begin(), end = active_layers.end(); it != end; ++it) {
			Screen* layer = *it;
			layer->touch(events, size);
		}
	}
}

void Screen::do_show_as_layer() {
}

void Screen::fade_in() {
	if (fade_in_transition == NULL)
		return;

	change_state(SCREEN_STATE_FADING_IN);
}

void Screen::fade_out() {
	if (fade_out_transition == NULL)
		return;

	change_state(SCREEN_STATE_FADING_OUT);
}

void Screen::do_change_state(int state) {
	switch (state) {
	case SCREEN_STATE_FADING_IN:
		fade_in_transition->restart();
		do_fading_in();
		break;

	case SCREEN_STATE_FADING_OUT:
		fade_out_transition->restart();
		do_fading_out();
		break;

	case SCREEN_STATE_FADE_IN_DONE:
		do_fade_in_done();
		break;

	case SCREEN_STATE_FADE_OUT_DONE:
		do_fade_out_done();
		break;
	}
}

void Screen::do_fading_in() {
}

void Screen::do_fading_out() {
}

void Screen::do_fade_in_done() {
}

void Screen::do_fade_out_done() {
}

void Screen::set_fade_out(ScreenTransition* fade_out_transition) {
	this->fade_out_transition = fade_out_transition;
}

void Screen::set_fade_in(ScreenTransition* fade_in_transition) {
	this->fade_in_transition = fade_in_transition;
}

} // namespace fte
