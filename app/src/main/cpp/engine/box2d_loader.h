#ifndef box2d_loader_H
#define box2d_loader_H

#include "json/json.h"
#include "engine.h"
#include "polygon.h"
#include "external.h"

#include <string>

namespace fte {

struct Box2dItemPolygon {
	std::vector<Vec3> polygon;
};

struct Box2dItemCyrcle {
	float x, y, r;
};

struct Vec3Compare {
	bool operator()(const Vec3 & a, const Vec3 & b) const {
		if (a.x < b.x)
			return true;
		else if (F_EQ(a.x, b.x))
			if (a.y < b.y)
				return true;
			else if (F_EQ(a.y, b.y))
				if (a.z < b.z)
					return true;
		return false;
	}
};

struct Box2dItem {
	std::string name;
	Vec3 origin;
	std::vector<Box2dItemPolygon> polygons;
	std::vector<Box2dItemCyrcle> cyrcles;
	std::vector<Vec3> polygon;

	void collapse_same_verts(std::map<Vec3, int, Vec3Compare> & m) {
		int idx = 0;

		int vert_count = polygon.size();

		for (std::vector<Vec3>::const_iterator it = polygon.begin(), end = polygon.end(); it != end; ++it) {
			const Vec3 & v = *it;
			std::map<Vec3, int, Vec3Compare>::const_iterator map_it = m.find(v);
			if (map_it == m.end()) {
				m[v] = idx++;
			}
		}

		LOGD("fte: box2d_loader: collapse_same_verts: m.size = %d; ind_count = %d", m.size());
	}

	void calculate_aabb(Rect & rect) {
		rect.x1 = std::numeric_limits<float>::max();
		rect.x2 = -rect.x1;
		rect.y1 = std::numeric_limits<float>::max();
		rect.y2 = -rect.y1;

		for (std::vector<Vec3>::const_iterator it = polygon.begin(), end = polygon.end(); it != end; ++it) {
			const Vec3 & v = *it;

			if (v.x < rect.x1)
				rect.x1 = v.x;
			if (v.x > rect.x2)
				rect.x2 = v.x;

			if (v.y < rect.y1)
				rect.y1 = v.y;
			if (v.y > rect.y2)
				rect.y2 = v.y;
		}
	}
};

/**
 * Loads file created with https://code.google.com/p/box2d-editor/
 */
class Box2dLoader {
private:

	std::vector<Box2dItem*> items;

	void load(const std::string & file_name, FTE_ENV env) {
		Json::Reader reader;

		void* buf;
		long size;

		if (!load_resource_file(env, file_name, buf, size)) {
			LOGE("fte: Sprite3dAtlas: can't load file: %s", file_name.c_str());
		} else {
			membuf mb((char*) buf, size);
			std::istream istr(&mb);

			Json::Value DEFVAL;
			Json::Value root;

			if (!reader.parse(istr, root, false)) {
				LOGE("fte: Sprite3d: unable parse json file: %s", file_name.c_str());
			} else {
				Json::Value rbs = root["rigidBodies"];
				for (int i = 0; i < rbs.size(); i++) {
					Json::Value rb = rbs[i];

					Box2dItem* item = new Box2dItem;

					items.push_back(item);

					item->name = rb["name"].asString();

					Json::Value origin = rb["origin"];

					item->origin.x = (float) origin["x"].asDouble();
					item->origin.y = (float) origin["y"].asDouble();
					item->origin.z = 0.;

					// shapes

					Json::Value shapes = rb["shapes"];

					for (int j = 0; j < shapes.size(); j++) {
						Json::Value shape = shapes[j];

						if (shape["type"].asString() == "POLYGON") {

							Json::Value vertices = shape["vertices"];

							for (int k = 0; k < vertices.size(); k++) {
								Json::Value vert = vertices[k];

								Vec3 v;
								v.x = (float) vert["x"].asDouble();
								v.y = (float) vert["y"].asDouble();
								v.z = 0.;

								item->polygon.push_back(v);
							}

							// import only first POLYGON
							break;
						}
					}

					// polygons
					Json::Value polygons = rb["polygons"];

					for (int j = 0; j < polygons.size(); j++) {
						Json::Value polygon = polygons[j];

						Box2dItemPolygon pi;

						for (int k = 0; k < polygon.size(); k++) {
							Json::Value vert = polygon[k];

							Vec3 v;
							v.x = (float) vert["x"].asDouble();
							v.y = (float) vert["y"].asDouble();
							v.z = 0.;

							pi.polygon.push_back(v);
						}

						item->polygons.push_back(pi);
					}

					// cyrcles
					Json::Value circles = rb["circles"];

					for (int j = 0; j < circles.size(); j++) {
						Json::Value cyrcle = circles[j];

						Box2dItemCyrcle ci;
						ci.x = (float) cyrcle["cx"].asDouble();
						ci.y = (float) cyrcle["cy"].asDouble();
						ci.r = (float) cyrcle["r"].asDouble();

						item->cyrcles.push_back(ci);
					}
				}
			}
		}

		free(buf);
	}

public:
	Box2dLoader(const std::string & file_name, FTE_ENV env) {
		load(file_name, env);
	}

	~Box2dLoader() {
		delete_vector_elements<Box2dItem*>(items);
	}

	Box2dItem* find_item(const std::string & name) const {
		for (std::vector<Box2dItem*>::const_iterator it = items.begin(); it != items.end(); it++) {
			Box2dItem* item = *it;
			if (strcmp(item->name.c_str(), name.c_str()) == 0) {
				return item;
			}
		}
		LOGE("fte: Box2dItem: '%s' not found!", name.c_str());
		return NULL;
	}
};

} // namespace fte

#endif /* box2d_loader_H */
