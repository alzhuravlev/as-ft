#ifndef actor_H
#define actor_H

#include "scene.h"
#include "statable.h"

namespace fte {

class Scene;

class Actor: public Statable {
private:
	Scene* scene;
	int type;
	bool active;
	bool visible;

	uint64_t broadcast_mask;

protected:

	virtual void do_init(FTE_ENV env);
	virtual void do_activate();
	virtual void do_deactivate();
	virtual void do_update(float delta, const Camera & camera);
	virtual void do_render(float delta, RenderCommand* command, const Camera & camera, int id);

	virtual void do_receive_broadcast(uint64_t type, void* p);

public:

	void init(FTE_ENV env);
	void update(float delta, const Camera & camera);
	void render(float delta, RenderCommand* command, const Camera & camera, int id);

	inline void receive_broadcast(uint64_t type, void* p) {
		if (active && (type & broadcast_mask))
			do_receive_broadcast(type, p);
	}

	void activate();
	void deactivate();

	Actor(Scene* scene, int type, uint64_t boradcast_mask = 0);
	virtual ~Actor();

	void send_broadcast(uint64_t type);
	void send_broadcast(uint64_t type, void* p);

	inline Scene* get_scene() const {
		return scene;
	}

	inline int get_type() const {
		return type;
	}

	inline bool is_active() const {
		return this->active;
	}

	inline bool is_visible() const {
		return this->visible;
	}

	inline void set_visible(bool visible) {
		this->visible = visible;
	}
};

} // namespace fte

#endif /* actor_H */

