#ifndef updater_H
#define updater_H

#include "command.h"
#include "fte_time.h"
#include "fte_utils.h"
#include "engine.h"
#include "external.h"
#include "audio_manager.h"

#include "unistd.h"

namespace fte {

class Screen;

const static float TIME_STEP = 1.0f / 60.0f;

/**
 * Updates screen state. Screens are responsible to prepare data for Renderer
 */
class Updater {
private:

	Screen* screen;
	Screen* pending_screen;
	fte_timespec frame_start;
	float smooth_delta;

	AudioManager audio;
	bool audio_initialized;

	void process_resize(const Command & command, FTE_ENV env);

	void update_screen(RenderCommand* command, FTE_ENV env);

	void process_render(Command & command, FTE_ENV env);

	void swap_screens(FTE_ENV env);

public:

#ifdef DEBUG
	float ____fps_time;
	int ____frame;
	int ____fps_flushed;
	float ____update_time_max;
	float ____update_time_min;
	float ____update_time_accum;
	int ____update_count;
	int ____batches_count;

	float ____update_time_avg;
	int ____trans_avg;
	int ____trans_vert_avg;
	int ____batches_avg;

#endif

	Updater() :
			pending_screen(NULL), screen(NULL), smooth_delta(0) {
		audio_initialized = false;
	}

	~Updater() {
		if (audio_initialized)
			audio.release();
	}

	void set_pending_screen(Screen* pending_screen);

	void init(FTE_ENV env);

	void release(FTE_ENV env);

	void process_touch(TouchEvent events[], int size);

	void process_custom_command(int commands[], int size);

	void process(Command & command, FTE_ENV env);

	inline const AudioManager & get_audio() {
		return audio;
	}
};

} // namespace fte

#endif // updater_H
