#include "fte_utils_3d.h"
#include "fte_utils.h"
#include "map_utils.h"
#include "float.h"

#include "gpc.h"

namespace fte {

float next_value_y(float x, float angle, float step, RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_y) {
	float k = radius_derivative_func_y(x, angle);
	return x + step * cos_fast(atanf(k));
}

float next_value_angle(float x, float angle, float step, RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_angle) {
	float k = radius_derivative_func_angle(x, angle);
	return angle + step * cos_fast(atanf(k));
}

void build_world_points_between_world_point(int count, const Vec3 & world_point1, const Vec3 & world_point2, std::vector<Vec3> & world_points, std::vector<Vec3> & normals) {

	Vec3 origin;
	vec_center(world_point1, world_point2, origin);

	float mid_y = world_point1.y + 0.5f * (world_point2.y - world_point1.y);
	Vec3 wp1(world_point1.x, world_point1.y - mid_y, world_point1.z);
	Vec3 wp2(world_point2.x, world_point2.y - mid_y, world_point2.z);

	Vec3 cp;
	vec_cross(wp1, wp2, cp);
	vec_normalize(cp);

	Vec3 wp;
	vec_sub(world_point1, origin, wp);

	float a = 0.0f;
	float step_a = M_PI / count;

	int c = count + 1;
	for (int i = 0; i != c; i++) {

		Vec3 tmp(wp);
		rotate_point_around_axis(cp.x, cp.y, cp.z, a, tmp.x, tmp.y, tmp.z);

		Vec3 n(tmp);
		vec_normalize(n);
		normals.push_back(n);

		vec_add(tmp, origin, tmp);
		world_points.push_back(tmp);

		a += step_a;
	}
}

void build_world_points_around_world_point(int count, float radius, float radius_amplitude, float radius_cycles, const Vec3 & world_point_prev, const Vec3 & world_point, const Vec3 & world_point_next, std::vector<Vec3> & world_points,
		std::vector<Vec3> & normals) {

	Vec3 axis;

	Vec3 v1;
	Vec3 v2;

	vec_sub(world_point_prev, world_point, v1);
	vec_sub(world_point_next, world_point, v2);

	vec_neg(v1);
	vec_normalize(v1);
	vec_normalize(v2);

	vec_add(v1, v2, axis);
	vec_normalize(axis);

	Vec3 point;

	float step_angle = M_2xPI / count;

	Vec3 wp(world_point.x, 0.0f, world_point.z);
	vec_cross(axis, wp, point);
	vec_normalize(point);

	float _cos = vec_dot(v1, point);
	float _sin = sqrtf(1.0f - _cos * _cos);
	float _len = (radius + radius * radius_amplitude * sin_fast(radius_cycles * world_point.y)) / _sin;
	vec_mul(point, _len);

	for (int i = 0; i != count; ++i) {

		Vec3 normal = point;
		Vec3 point2 = point;

		vec_add(world_point, point, point2);
		world_points.push_back(point2);

		// normal
		vec_normalize(normal);
		normals.push_back(normal);

		rotate_point_around_axis(axis.x, axis.y, axis.z, step_angle, point.x, point.y, point.z);
	}
}

void create_tube_around_path(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int num_edges, float frame_radius, float frame_radius_amplitude, float frame_radius_cycles, const std::vector<Vec3> & angular_points, RADIUS_FUNC radius_func,
		bool loop, float v1_scale, float v2_scale) {

	if (loop && angular_points.size() < 3) {
		LOGE("fte: create_tube_around_path: path must have at least 3 vertices for loop. actual: %d", angular_points.size());
		return;
	}

	if (!loop && angular_points.size() < 4) {
		LOGE("fte: create_tube_around_path: path must have at least 4 vertices for non loop. actual: %d", angular_points.size());
		return;
	}

	std::vector<Vec3> backbase_world_points;
	std::vector<std::vector<Vec3> > world_points;
	std::vector<std::vector<Vec3> > world_normals;

	angular_points_to_world_points(angular_points, backbase_world_points, radius_func, loop);

	const std::vector<Vec3>::const_iterator backbase_world_point_begin = backbase_world_points.begin();
	const std::vector<Vec3>::const_iterator backbase_world_point_end = backbase_world_points.end();

	std::vector<Vec3>::const_iterator backbase_world_point_prev_it = backbase_world_point_begin;

	std::vector<Vec3>::const_iterator backbase_world_point_curr_it = backbase_world_point_begin;
	++backbase_world_point_curr_it;

	std::vector<Vec3>::const_iterator backbase_world_point_next_it = backbase_world_point_begin;
	++backbase_world_point_next_it;
	++backbase_world_point_next_it;

	for (; backbase_world_point_next_it != backbase_world_point_end; ++backbase_world_point_prev_it, ++backbase_world_point_curr_it, ++backbase_world_point_next_it) {
		const Vec3 & world_point_prev = *backbase_world_point_prev_it;
		const Vec3 & world_point = *backbase_world_point_curr_it;
		const Vec3 & world_point_next = *backbase_world_point_next_it;

		std::vector<Vec3> points;
		std::vector<Vec3> normals;

		build_world_points_around_world_point(num_edges, frame_radius, frame_radius_amplitude, frame_radius_cycles, world_point_prev, world_point, world_point_next, points, normals);

		world_points.push_back(points);
		world_normals.push_back(normals);
	}

	const bool use_color = sprite->get_color_component_offset();
	const bool use_uv = sprite->get_uv_component_offset();
	const bool use_normal = sprite->get_normal_component_offset();

	const float u1 = sprite->get_u1();
	const float u_size = sprite->get_u2() - sprite->get_u1();

	const float v1 = sprite->get_v1() + (sprite->get_v2() - sprite->get_v1()) * v1_scale;
	const float v_size = (sprite->get_v2() - sprite->get_v1()) * (v2_scale - v1_scale);

	const bool use_uv_nm = use_normal && sprite_nm;

	float nm_u1, nm_v1, nm_u_size, nm_v_size;
	if (use_uv_nm) {
		nm_u1 = sprite_nm->get_u1();
		nm_u_size = sprite_nm->get_u2() - sprite_nm->get_u1();

		nm_v1 = sprite_nm->get_v1() + (sprite_nm->get_v2() - sprite_nm->get_v1()) * v1_scale;
		nm_v_size = (sprite_nm->get_v2() - sprite_nm->get_v1()) * (v2_scale - v1_scale);
	}

//

	const int v_count_in_row = num_edges + 1;
	const int v_count = (loop ? world_points.size() + 1 : world_points.size()) * v_count_in_row;
	const int i_count = (loop ? world_points.size() : world_points.size() - 1) * num_edges * 3 * 2;

	Polygon p(use_color, use_uv, use_normal, true, use_uv_nm);
	p.set_texture_descriptor(sprite->get_texture_descriptor());
	p.set_vert_count(v_count, false);
	p.set_ind_count(i_count);

	const int wp_count_v = loop ? world_points.size() + 1 : world_points.size();
	const int wp_size = world_points.size();

	int v_idx = 0;

	for (int i = 0; i != wp_count_v; ++i) {
		const std::vector<Vec3> & wp_curr = world_points[i % wp_size];
		const std::vector<Vec3> & wn_curr = world_normals[i % wp_size];

		for (int j = 0; j != v_count_in_row; ++j) {
			const Vec3 & wp = wp_curr[j % num_edges];
			const Vec3 & wn = wn_curr[j % num_edges];

			p.set_vert_xyz(v_idx, wp.x, wp.y, wp.z);

			const float u = u1 + u_size * j / num_edges;
			const float v = v1 + v_size * i / (wp_count_v - 1);
			p.set_vert_uv(v_idx, u, v);

			if (use_normal) {
				p.set_vert_normal(v_idx, wn.x, wn.y, wn.z);

				if (use_uv_nm) {

					const float nm_u = nm_u1 + nm_u_size * j / num_edges;
					const float nm_v = nm_v1 + nm_v_size * i / (wp_count_v - 1);
					p.set_vert_nm_uv(v_idx, nm_u, nm_v);

					Vec3 tangent;
					Vec3 bitangent;

					Vec3 wwp(wp.x, 0.0f, wp.z);

					vec_cross(wwp, wn, tangent);
					vec_normalize(tangent);

					vec_cross(tangent, wn, bitangent);
					vec_neg(bitangent);
					vec_normalize(bitangent);

					p.set_vert_tangent(v_idx, tangent.x, tangent.y, tangent.z);
					p.set_vert_bitangent(v_idx, bitangent.x, bitangent.y, bitangent.z);
				}
			}

			if (use_color)
				p.set_vert_color(v_idx, 0xffffffff);

			v_idx++;
		}
	}

	const int wp_count_i = loop ? world_points.size() : world_points.size() - 1;

	int i_idx = 0;

	for (int i = 0; i != wp_count_i; ++i) {

		for (int j = 0; j != num_edges; ++j) {

			int i_curr1 = j;
			int i_curr2 = j + 1;

			if (i_curr2 >= v_count_in_row) {
				i_curr1++;
				i_curr2++;
			}

			i_curr1 %= v_count_in_row;
			i_curr2 %= v_count_in_row;

			int i_next1 = j;
			int i_next2 = j + 1;

			if (i_next2 >= v_count_in_row) {
				i_next1++;
				i_next2++;
			}

			i_next1 %= v_count_in_row;
			i_next2 %= v_count_in_row;

			p.set_index_value(i_idx++, (i * v_count_in_row + i_curr1));
			p.set_index_value(i_idx++, (i * v_count_in_row + i_curr2));
			p.set_index_value(i_idx++, ((i + 1) * v_count_in_row + i_next2));

			p.set_index_value(i_idx++, (i * v_count_in_row + i_curr1));
			p.set_index_value(i_idx++, ((i + 1) * v_count_in_row + i_next2));
			p.set_index_value(i_idx++, ((i + 1) * v_count_in_row + i_next1));
		}
	}

	cache->add(&p);

}

void create_semi_tube_around_path(PolygonCache* cache, const Sprite* sprite, int num_edges, RADIUS_FUNC radius_func1, RADIUS_FUNC radius_func2, const std::vector<Vec3> & angular_points1, const std::vector<Vec3> & angular_points2, bool loop) {

	if (angular_points1.size() != angular_points2.size()) {
		LOGE("fte: create_semi_tube_around_path: all angular_points* must have same size: angular_points1=%d; angular_points2=%d", angular_points1.size(), angular_points2.size());
		return;
	}

	if (num_edges < 1) {
		LOGE("fte: create_semi_tube_around_path: num_edges must be greater then 1. actual: %d", num_edges);
		return;
	}

	std::vector<Vec3> backbase_world_points1;
	std::vector<Vec3> backbase_world_points2;

	angular_points_to_world_points(angular_points1, backbase_world_points1, radius_func1, loop);
	angular_points_to_world_points(angular_points2, backbase_world_points2, radius_func2, loop);

	int size = backbase_world_points1.size();

	std::vector<std::vector<Vec3> > world_points;
	std::vector<std::vector<Vec3> > world_normals;

	for (int i = 0; i < size; i++) {
		const Vec3 & bb_world_point1 = backbase_world_points1[i];
		const Vec3 & bb_world_point2 = backbase_world_points2[i];

		std::vector<Vec3> points;
		std::vector<Vec3> normals;

		build_world_points_between_world_point(num_edges, bb_world_point1, bb_world_point2, points, normals);

		world_points.push_back(points);
		world_normals.push_back(normals);
	}

	const bool use_color = sprite->get_color_component_offset();
	const bool use_uv = sprite->get_uv_component_offset();
	const bool use_normal = sprite->get_normal_component_offset();

	const float u1 = sprite->get_u1();
	const float u_size = sprite->get_u2() - sprite->get_u1();

	const float v1 = sprite->get_v1();
	const float v_size = sprite->get_v2() - sprite->get_v1();

	//

	const int v_count_in_row = num_edges + 1;
	const int v_count = size * v_count_in_row;
	const int i_count = (loop ? size - 1 : size) * num_edges * 3 * 2;

	Polygon p(use_color, use_uv, use_normal, true);
	p.set_preserve_vert_transform(true);
	p.set_preserve_vert_colors(true);
	p.set_texture_descriptor(sprite->get_texture_descriptor());
	p.set_vert_count(v_count, false);
	p.set_ind_count(i_count);

	int v_idx = 0;

	for (int i = 0; i != size; ++i) {
		const std::vector<Vec3> & wp_curr = world_points[i];
		const std::vector<Vec3> & wn_curr = world_normals[i];

		for (int j = 0; j != v_count_in_row; ++j) {
			const Vec3 & wp = wp_curr[j];
			const Vec3 & wn = wn_curr[j];

			p.set_vert_xyz(v_idx, wp.x, wp.y, wp.z);

			if (use_uv) {
				const float u = u1 + u_size * i / (size - 1);
				const float v = v1 + v_size * j / (v_count_in_row - 1);
				p.set_vert_uv(v_idx, u, v);
			}

			if (use_normal)
				p.set_vert_normal(v_idx, wn.x, wn.y, wn.z);

			if (use_color)
				p.set_vert_color(v_idx, 0xffffffff);

			v_idx++;
		}
	}

	int i_idx = 0;

	for (int i = 0; i != size; ++i) {

		int v_offset = i * v_count_in_row;

		for (int j = 0; j != num_edges; ++j) {

			int i0 = v_offset + j;
			int i1 = v_offset + j + 1;
			int i2 = v_offset + j + v_count_in_row;

			int i3 = v_offset + j + 1;
			int i4 = v_offset + j + v_count_in_row + 1;
			int i5 = v_offset + j + v_count_in_row;

			i0 %= v_count;
			i1 %= v_count;
			i2 %= v_count;
			i3 %= v_count;
			i4 %= v_count;
			i5 %= v_count;

			p.set_index_value(i_idx++, i0);
			p.set_index_value(i_idx++, i1);
			p.set_index_value(i_idx++, i2);

			p.set_index_value(i_idx++, i3);
			p.set_index_value(i_idx++, i4);
			p.set_index_value(i_idx++, i5);
		}
	}

	cache->add(&p);

}

void create_shape(PolygonCache* cache, const Sprite* sprite, RADIUS_FUNC radius_func, const std::vector<Vec3> & angular_points) {

	std::vector<Vec3> backbase_world_points;
	angular_points_to_world_points(angular_points, backbase_world_points, radius_func, false);

	const bool use_color = sprite->get_color_component_offset();
	const bool use_uv = sprite->get_uv_component_offset();
	const bool use_normal = sprite->get_normal_component_offset();

	const float u_size_2 = (sprite->get_u2() - sprite->get_u1()) * 0.5f;
	const float u_center = sprite->get_u1() + u_size_2;

	const float v_size_2 = (sprite->get_v2() - sprite->get_v1()) * 0.5f;
	const float v_center = sprite->get_v1() + v_size_2;

	LOGD("center %f %f", u_center, v_center);

	//

	const int v_count = backbase_world_points.size();

	Vec3 center;
	for (std::vector<Vec3>::const_iterator it = backbase_world_points.begin(), end = backbase_world_points.end(); it != end; ++it) {
		const Vec3 & wp = *it;
		center.x += wp.x;
		center.y += wp.y;
		center.z += wp.z;
	}
	center.x /= v_count;
	center.y /= v_count;
	center.z /= v_count;

	Vec3 normal(center.x, 0.0f, center.z);
	vec_normalize(normal);

	Vec3 cp;
	vec_cross(Vec3(0.0f, 1.0f, 0.0f), normal, cp);

	float DU = 0.0f;
	float DV = 0.0f;
	for (std::vector<Vec3>::const_iterator it = backbase_world_points.begin(), end = backbase_world_points.end(); it != end; ++it) {
		const Vec3 & wp = *it;
		Vec3 vv;
		vec_sub(wp, center, vv);

		float du = fabsf(vec_dot(vv, cp));
		float dv = fabsf(vec_dot(vv, Vec3(0.0f, 1.0f, 0.0f)));

		if (DU < du)
			DU = du;
		if (DV < dv)
			DV = dv;
	}

	Polygon p(use_color, use_uv, use_normal, true);
	p.set_preserve_vert_transform(true);
	p.set_preserve_vert_colors(true);
	p.set_texture_descriptor(sprite->get_texture_descriptor());
	p.set_vert_count(v_count);

	int v_idx = 0;

	for (std::vector<Vec3>::const_iterator it = backbase_world_points.begin(), end = backbase_world_points.end(); it != end; ++it) {
		const Vec3 & wp = *it;

		p.set_vert_xyz(v_idx, wp.x, wp.y, wp.z);

		if (use_uv) {
			Vec3 vv;
			vec_sub(wp, center, vv);

			float du = vec_dot(vv, cp);
			float dv = vec_dot(vv, Vec3(0.0f, 1.0f, 0.0f));

			const float u = u_center + u_size_2 * du / DU;
			const float v = v_center + v_size_2 * dv / DV;

			p.set_vert_uv(v_idx, u, v);
		}

		if (use_normal)
			p.set_vert_normal(v_idx, normal.x, normal.y, normal.z);

		if (use_color)
			p.set_vert_color(v_idx, 0xffffffff);

		v_idx++;
	}

	cache->add(&p);
}

void create_cylinder(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int repeat_y, int num_edges, int num_edges_for_texture, float step_v, float height, float v_scale, RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_y,
		float translate_x, float translate_y, float translate_z, float start_angle, float full_angle) {

	int cnt = num_edges * num_edges_for_texture;
	int cnt_uv = num_edges_for_texture;

	float step_angle = full_angle / cnt;

	bool use_color = sprite->get_color_component_offset();
	bool use_uv = sprite->get_uv_component_offset();
	bool use_normal = sprite->get_normal_component_offset();

	float u1, v1, u_size, v_size;
	if (use_uv) {
		u1 = sprite->get_u1();
		u_size = sprite->get_u_size() / cnt_uv;
		v1 = sprite->get_v1();
		v_size = sprite->get_v_size() * v_scale;
	}

	bool use_nm_uv = use_normal && sprite_nm;
	float nm_u1, nm_v1, nm_u_size, nm_v_size;
	if (use_nm_uv) {
		nm_u1 = sprite_nm->get_u1();
		nm_u_size = sprite_nm->get_u_size() / cnt_uv;
		nm_v1 = sprite_nm->get_v1();
		nm_v_size = sprite_nm->get_v_size() * v_scale;
	}

	const float STEP = height * step_v;

	for (int iy = 0; iy < repeat_y; iy++) {

		float y_start = iy * height + translate_y;
		float y_end = y_start + height;

		float y1 = y_start;

		float _v1;
		if (use_uv)
			_v1 = v1;

		float _nm_v1;
		if (use_nm_uv)
			_nm_v1 = nm_v1;

		bool operate = true;

		while (operate) {

			float y2 = next_value_y(y1, 0.0f, STEP, radius_func, radius_derivative_func_y);
			if (y2 > y_end) {
				y2 = y_end;
				operate = false;
			}

			float _v_size;
			if (use_uv)
				_v_size = v_size * (y2 - y1) / height;

			float _nm_v_size;
			if (use_nm_uv)
				_nm_v_size = nm_v_size * (y2 - y1) / height;

			for (int ia = 0; ia < cnt; ia++) {

				int uv_idx = ia % cnt_uv;

				float _u1;
				if (use_uv)
					_u1 = u1 + uv_idx * u_size;

				float _nm_u1;
				if (use_nm_uv)
					_nm_u1 = nm_u1 + uv_idx * nm_u_size;

				float a1 = step_angle * ia + start_angle;
				float a2 = a1 + step_angle;

				// create polygons

				Polygon p(use_color, use_uv, use_normal, false, use_nm_uv);
				p.set_texture_descriptor(sprite->get_texture_descriptor());
				p.set_vert_count(4);
				p.set_translate(translate_x, 0.0f, translate_z);

				Vec3 v[4];
				v[0].x = a1;
				v[0].y = y1;

				v[1].x = a2;
				v[1].y = y1;

				v[2].x = a2;
				v[2].y = y2;

				v[3].x = a1;
				v[3].y = y2;

				for (int m = 0; m < 4; m++) {
					float cur_a = v[m].x;
					float y = v[m].y;

					float vx, vy, vz;
					float radius = radius_func(y, cur_a);

					vx = radius * sinf(cur_a);
					vy = y;
					vz = radius * cosf(cur_a);

					if (use_uv) {
						float u, v;
						u = _u1 + u_size * (cur_a - a1) / step_angle;
						v = _v1 + _v_size * (y - y1) / (y2 - y1);
						p.set_vert_uv(m, u, v);
					}

					p.set_vert_xyz(m, vx, vy, vz);

					if (use_color)
						p.set_vert_color(m, 0xffffffff);

					if (use_normal) {
						Vec3 n(vx, 0.0f, vz);
						vec_normalize(n);
						p.set_vert_normal(m, n.x, n.y, n.z);

						if (use_nm_uv) {

							float nm_u, nm_v;
							nm_u = _nm_u1 + nm_u_size * (cur_a - a1) / step_angle;
							nm_v = _nm_v1 + _nm_v_size * (y - y1) / (y2 - y1);
							p.set_vert_nm_uv(m, nm_u, nm_v);

							Vec3 tangent;
							Vec3 bitangent;

							tangent = n;
							rotate_point_around_axis(0.0f, 1.0f, 0.0f, -M_PI_2, tangent.x, tangent.y, tangent.z);

							vec_cross(n, tangent, bitangent);
							vec_neg(bitangent);
							vec_normalize(bitangent);

							p.set_vert_tangent(m, tangent.x, tangent.y, tangent.z);
							p.set_vert_bitangent(m, bitangent.x, bitangent.y, bitangent.z);
						}
					}
				}

				cache->add(&p);
			}

			_v1 += _v_size;
			_nm_v1 += _nm_v_size;
			y1 = y2;
		}
	}

}

void create_cylinder_with_holes(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int repeat_y, int num_edges, int num_edges_for_texture, float step_v, float height, float v_scale, const std::vector<std::vector<Vec3> > & holes,
		RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_y, float translate_x, float translate_y, float translate_z, float start_angle, float full_angle) {

	int cnt = num_edges * num_edges_for_texture;
	int cnt_uv = num_edges_for_texture;

	float step_angle = full_angle / cnt;

	bool use_color = sprite->get_color_component_offset();
	bool use_uv = sprite->get_uv_component_offset();
	bool use_normal = sprite->get_normal_component_offset();

	float u1, v1, u_size, v_size;
	if (use_uv) {
		u1 = sprite->get_u1();
		u_size = sprite->get_u_size() / cnt_uv;
		v1 = sprite->get_v1();
		v_size = sprite->get_v_size() * v_scale;
	}

	bool use_nm_uv = use_normal && sprite_nm;
	float nm_u1, nm_v1, nm_u_size, nm_v_size;
	if (use_nm_uv) {
		nm_u1 = sprite_nm->get_u1();
		nm_u_size = sprite_nm->get_u_size() / cnt_uv;
		nm_v1 = sprite_nm->get_v1();
		nm_v_size = sprite_nm->get_v_size() * v_scale;
	}

	gpc_vertex v4[4];
	gpc_vertex_list vl;
	gpc_polygon p1, p2, result;

	p1.contour = NULL;
	p1.hole = NULL;
	p1.num_contours = 0;

	p2.contour = NULL;
	p2.hole = NULL;
	p2.num_contours = 0;

	result.contour = NULL;
	result.hole = NULL;
	result.num_contours = 0;

	const float STEP = height * step_v;

	for (int iy = 0; iy < repeat_y; iy++) {

		float y_start = iy * height + translate_y;
		float y_end = y_start + height;

		float y1 = y_start;

		float _v1;
		if (use_uv)
			_v1 = v1;

		float _nm_v1;
		if (use_nm_uv)
			_nm_v1 = nm_v1;

		bool operate = true;

		while (operate) {

			float y2 = next_value_y(y1, 0.0f, STEP, radius_func, radius_derivative_func_y);
			if (y2 > y_end) {
				y2 = y_end;
				operate = false;
			}

			float _v_size;
			if (use_uv)
				_v_size = v_size * (y2 - y1) / height;

			float _nm_v_size;
			if (use_nm_uv)
				_nm_v_size = nm_v_size * (y2 - y1) / height;

			for (int ia = 0; ia < cnt; ia++) {

				int uv_idx = ia % cnt_uv;

				float _u1;
				if (use_uv)
					_u1 = u1 + uv_idx * u_size;

				float _nm_u1;
				if (use_nm_uv)
					_nm_u1 = nm_u1 + uv_idx * nm_u_size;

				float a1 = step_angle * ia + start_angle;
				float a2 = a1 + step_angle;

				v4[0].x = a1;
				v4[0].y = y1;

				v4[1].x = a2;
				v4[1].y = y1;

				v4[2].x = a2;
				v4[2].y = y2;

				v4[3].x = a1;
				v4[3].y = y2;

				gpc_free_polygon(&p1);
				gpc_free_polygon(&result);

				vl.num_vertices = 4;
				vl.vertex = v4;

				gpc_add_contour(&p1, &vl, 0);

				for (std::vector<std::vector<Vec3> >::const_iterator it = holes.begin(), end = holes.end(); it != end; ++it) {
					const std::vector<Vec3> & hole_points = *it;

					gpc_free_polygon(&p2);

					const int hp_size = hole_points.size();
					gpc_vertex v_n[hp_size];
					vl.num_vertices = hole_points.size();
					vl.vertex = v_n;

					gpc_vertex* p_v_n = v_n;
					for (std::vector<Vec3>::const_iterator points_it = hole_points.begin(), points_end = hole_points.end(); points_it != points_end; ++points_it) {
						const Vec3 & hole_point = *points_it;
						p_v_n->x = hole_point.x;
						p_v_n->y = hole_point.y;
						p_v_n++;
					}

					gpc_add_contour(&p2, &vl, 0);

					gpc_polygon_clip(GPC_DIFF, &p1, &p2, &result);

					gpc_free_polygon(&p1);
					p1 = result;

					result.contour = NULL;
					result.hole = NULL;
					result.num_contours = 0;
				}

				// create polygons

				gpc_tristrip ts;
				gpc_polygon_to_tristrip(&p1, &ts);

				for (int k = 0; k < ts.num_strips; k++) {

					const gpc_vertex_list & strip = ts.strip[k];

					Polygon p(use_color, use_uv, use_normal, false, use_nm_uv);
					p.set_texture_descriptor(sprite->get_texture_descriptor());
					p.set_vert_count(strip.num_vertices, false);
					p.build_triangle_strip_swap_even_indexes();
					p.set_translate(translate_x, 0.0f, translate_z);

					for (int m = 0; m < strip.num_vertices; m++) {
						float cur_a = strip.vertex[m].x;
						float y = strip.vertex[m].y;

						float vx, vy, vz;
						float radius = radius_func(y, cur_a);

						vx = radius * sinf(cur_a);
						vy = y;
						vz = radius * cosf(cur_a);

						if (use_uv) {
							float u, v;
							u = _u1 + u_size * (cur_a - a1) / step_angle;
							v = _v1 + _v_size * (y - y1) / (y2 - y1);
							p.set_vert_uv(m, u, v);
						}

						p.set_vert_xyz(m, vx, vy, vz);

						if (use_color)
							p.set_vert_color(m, 0xffffffff);

						if (use_normal) {
							Vec3 n(vx, 0.0f, vz);
							vec_normalize(n);
							p.set_vert_normal(m, n.x, n.y, n.z);

							if (use_nm_uv) {

								float nm_u, nm_v;
								nm_u = _nm_u1 + nm_u_size * (cur_a - a1) / step_angle;
								nm_v = _nm_v1 + _nm_v_size * (y - y1) / (y2 - y1);
								p.set_vert_nm_uv(m, nm_u, nm_v);

								Vec3 tangent;
								Vec3 bitangent;

								tangent = n;
								rotate_point_around_axis(0.0f, 1.0f, 0.0f, -M_PI_2, tangent.x, tangent.y, tangent.z);

								vec_cross(n, tangent, bitangent);
								vec_neg(bitangent);
								vec_normalize(bitangent);

								p.set_vert_tangent(m, tangent.x, tangent.y, tangent.z);
								p.set_vert_bitangent(m, bitangent.x, bitangent.y, bitangent.z);
							}
						}
					}

					cache->add(&p);
				}

				gpc_free_tristrip(&ts);
			}

			_v1 += _v_size;
			_nm_v1 += _nm_v_size;
			y1 = y2;
		}
	}

	gpc_free_polygon(&p1);
	gpc_free_polygon(&p2);
	gpc_free_polygon(&result);
}

void create_cone(PolygonCache* cache, Sprite* sprite, int num_edges, int num_edges_for_texture, float radius_bottom, float radius_top, float height, float translate_x, float translate_y, float translate_z, bool vert_clockwise) {

	if (!sprite->get_uv_component_offset()) {
		LOGE("fte: create_cone: sprite must use UV");
		return;
	}

	int cnt = num_edges * num_edges_for_texture;
	int cnt_uv = num_edges_for_texture;

	float a = 2. * M_PI / cnt;
	float a_2 = a / 2.;
	float a_shift = M_PI / num_edges - a_2;

	float h = height;

	float u1 = sprite->get_u1();
	float u2 = sprite->get_u2();
	float v1 = sprite->get_v1();
	float v2 = sprite->get_v2();

	float u_size = sprite->get_u_size() / cnt_uv;

	int use_color = sprite->get_color_component_offset();
	int use_normal = sprite->get_normal_component_offset();

	for (int i = 0; i < cnt; i++) {

		int uv_idx = i % cnt_uv;

		float _u1 = u1 + uv_idx * u_size;
		float _u2 = _u1 + u_size;

		float cur_a1 = a * i - a_shift;
		float cur_a2 = a * (i + 1) - a_shift;

		float x_bot1 = radius_bottom * cosf(cur_a1);
		float y_bot1 = 0.;
		float z_bot1 = -radius_bottom * sinf(cur_a1);

		float x_bot2 = radius_bottom * cosf(cur_a2);
		float y_bot2 = 0.;
		float z_bot2 = -radius_bottom * sinf(cur_a2);

		float x_top1 = radius_top * cosf(cur_a1);
		float y_top1 = h;
		float z_top1 = -radius_top * sinf(cur_a1);

		float x_top2 = radius_top * cosf(cur_a2);
		float y_top2 = h;
		float z_top2 = -radius_top * sinf(cur_a2);

		Polygon p(use_color, true, use_normal, vert_clockwise);
		p.set_texture_descriptor(sprite->get_texture_descriptor());
		p.set_vert_count(4);

		p.set_vert_xyz(0, x_bot1, y_bot1, z_bot1);
		p.set_vert_xyz(1, x_bot2, y_bot2, z_bot2);
		p.set_vert_xyz(2, x_top2, y_top2, z_top2);
		p.set_vert_xyz(3, x_top1, y_top1, z_top1);

		if (use_color) {
			p.set_vert_color(0, 0xffffffff);
			p.set_vert_color(1, 0xffffffff);
			p.set_vert_color(2, 0xffffffff);
			p.set_vert_color(3, 0xffffffff);
		}

		p.set_vert_uv(0, _u1, v1);
		p.set_vert_uv(1, _u2, v1);
		p.set_vert_uv(2, _u2, v2);
		p.set_vert_uv(3, _u1, v2);

		p.set_translate(translate_x, translate_y, translate_z);

		cache->add(&p);
	}

}

void create_tiled_map(PolygonCache* cache, Sprite* sprite, int repeat_x, int repeat_y, const Transformable & transform) {
	float r = sprite->get_ratio();
	float dx = r / repeat_x;
	float dy = 1.0f / repeat_x;
	for (int x = 0; x != repeat_x; x++) {
		for (int y = 0; y != repeat_y; y++) {
			Polygon p(*sprite);
			float vx1, vx2, vy1, vy2;

			vx1 = x * dx;
			vy1 = y * dy;
			vx2 = vx1 + dx;
			vy2 = vy1 + dy;

			p.set_vert_xy(0, vx1, vy1);
			p.set_vert_xy(1, vx2, vy1);
			p.set_vert_xy(2, vx2, vy2);
			p.set_vert_xy(3, vx1, vy2);
			p.apply(transform);
			cache->add(&p);
		}
	}
}

void create_v_half_ellipse(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_floor, int num_edges, int num_edges_for_texture, float radius_x, float radius_y, float depth, float translate_x, float translate_y, float translate_z,
		float angle_y, bool vert_clockwise) {

	if (!sprite->get_uv_component_offset()) {
		LOGE("fte: create_v_half_ellipse: sprite must use UV");
		return;
	}

	int cnt = num_edges * num_edges_for_texture;
	int cnt_uv = num_edges_for_texture;

	float a = M_PI / cnt;
	float a_2 = a / 2.;

	float u1 = sprite->get_u1();
	float u2 = sprite->get_u2();
	float v1 = sprite->get_v1();
	float v2 = sprite->get_v2();

	float u_size = sprite->get_u_size() / cnt_uv;

	int use_color = sprite->get_color_component_offset();
	int use_normal = sprite->get_normal_component_offset();

	for (int i = 0; i < cnt; i++) {

		int uv_idx = i % cnt_uv;

		float _u1 = u1 + uv_idx * u_size;
		float _u2 = _u1 + u_size;

		float cur_a1 = a * i;
		float cur_a2 = a * (i + 1);

		float x_near1 = radius_x * cosf(cur_a1);
		float y_near1 = radius_y * sinf(cur_a1);
		float z_near1 = depth;

		float x_near2 = radius_x * cosf(cur_a2);
		float y_near2 = radius_y * sinf(cur_a2);
		float z_near2 = depth;

		float x_far1 = radius_x * cosf(cur_a1);
		float y_far1 = radius_y * sinf(cur_a1);
		float z_far1 = -depth;

		float x_far2 = radius_x * cosf(cur_a2);
		float y_far2 = radius_y * sinf(cur_a2);
		float z_far2 = -depth;

		Polygon p(use_color, true, use_normal, vert_clockwise);
		p.set_texture_descriptor(sprite->get_texture_descriptor());
		p.set_vert_count(4);

		p.set_vert_xyz(0, x_near1, y_near1, z_near1);
		p.set_vert_xyz(1, x_near2, y_near2, z_near2);
		p.set_vert_xyz(2, x_far2, y_far2, z_far2);
		p.set_vert_xyz(3, x_far1, y_far1, z_far1);

		if (use_color) {
			p.set_vert_color(0, 0xffffffff);
			p.set_vert_color(1, 0xffffffff);
			p.set_vert_color(2, 0xffffffff);
			p.set_vert_color(3, 0xffffffff);
		}

		if (use_normal) {
			float nx, ny, nz;
			nx = 1.;
			ny = 0.;
			nz = 0.;
			rotate_point_fast(0, 0, -cur_a1, nx, ny, nz);
			p.set_vert_normal(0, -nx, -ny, -nz);
			p.set_vert_normal(3, -nx, -ny, -nz);

			nx = 1.;
			ny = 0.;
			nz = 0.;
			rotate_point_fast(0, 0, -cur_a2, nx, ny, nz);
			p.set_vert_normal(1, -nx, -ny, -nz);
			p.set_vert_normal(2, -nx, -ny, -nz);
		}

		p.set_vert_uv(0, _u1, v1);
		p.set_vert_uv(1, _u2, v1);
		p.set_vert_uv(2, _u2, v2);
		p.set_vert_uv(3, _u1, v2);

		p.set_translate(translate_x, translate_y, translate_z);
		p.set_angle_y(angle_y);

		cache->add(&p);
	}

	if (sprite_floor) {
		Polygon p(use_color, true, use_normal, vert_clockwise);
		p.set_texture_descriptor(sprite_floor->get_texture_descriptor());
		p.set_vert_count(4);

		p.set_vert_xyz(0, -radius_x, 0., depth);
		p.set_vert_xyz(1, radius_x, 0., depth);
		p.set_vert_xyz(2, radius_x, 0., -depth);
		p.set_vert_xyz(3, -radius_x, 0., -depth);

		p.set_vert_uv(0, sprite_floor->get_u1(), sprite_floor->get_v1());
		p.set_vert_uv(1, sprite_floor->get_u2(), sprite_floor->get_v1());
		p.set_vert_uv(2, sprite_floor->get_u2(), sprite_floor->get_v2());
		p.set_vert_uv(3, sprite_floor->get_u1(), sprite_floor->get_v2());

		if (use_normal) {
			p.set_vert_normal(0, 0., 1., 0.);
			p.set_vert_normal(1, 0., 1., 0.);
			p.set_vert_normal(2, 0., 1., 0.);
			p.set_vert_normal(3, 0., 1., 0.);
		}

		if (use_color) {
			p.set_vert_color(0, 0xffffffff);
			p.set_vert_color(1, 0xffffffff);
			p.set_vert_color(2, 0xffffffff);
			p.set_vert_color(3, 0xffffffff);
		}

		p.set_translate(translate_x, translate_y, translate_z);
		p.set_angle_y(angle_y);

		cache->add(&p);
	}
}

void create_tetrahedron(PolygonCache* cache) {

	const float m_pi_6 = M_PI / 6.;
	const float m_pi_3 = M_PI / 3.;

	float x0 = 0.;
	float y0 = 1.;
	float z0 = 0.;

	float x1, y1, z1;
	float x2, y2, z2;
	float x3, y3, z3;

	x1 = x2 = x3 = x0;
	y1 = y2 = y3 = y0;
	z1 = z2 = z3 = z0;

	float a = acosf(-1. / 3.);
	float b = 2. * M_PI / 3.;

	rotate_point_fast(a, 0, 0, x1, y1, z1);
	rotate_point_fast(a, b, 0, x2, y2, z2);
	rotate_point_fast(a, -b, 0, x3, y3, z3);

	Polygon p(true, false, false);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);
	p.set_preserve_vert_transform(true);

	p.set_vert_color(0, 0xffffffff);
	p.set_vert_color(1, 0xffffffff);
	p.set_vert_color(2, 0xffffffff);

	p.set_vert_xyz(0, x1, y1, z1);
	p.set_vert_xyz(1, x3, y3, z3);
	p.set_vert_xyz(2, x2, y2, z2);

	cache->add(&p);

	p.set_vert_color(0, 0xff00ffff);
	p.set_vert_color(1, 0xff00ffff);
	p.set_vert_color(2, 0xff00ffff);

	p.set_vert_xyz(0, x1, y1, z1);
	p.set_vert_xyz(1, x2, y2, z2);
	p.set_vert_xyz(2, x0, y0, z0);

	cache->add(&p);

	p.set_vert_color(0, 0xffffff00);
	p.set_vert_color(1, 0xffffff00);
	p.set_vert_color(2, 0xffffff00);

	p.set_vert_xyz(0, x2, y2, z2);
	p.set_vert_xyz(1, x3, y3, z3);
	p.set_vert_xyz(2, x0, y0, z0);

	cache->add(&p);

	p.set_vert_color(0, 0xffff00ff);
	p.set_vert_color(1, 0xffff00ff);
	p.set_vert_color(2, 0xffff00ff);

	p.set_vert_xyz(0, x3, y3, z3);
	p.set_vert_xyz(1, x1, y1, z1);
	p.set_vert_xyz(2, x0, y0, z0);

	cache->add(&p);
}

void __calc_mid_vert__(const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = v1.x + (v2.x - v1.x) / 2.;
	res.y = v1.y + (v2.y - v1.y) / 2.;
	res.z = v1.z + (v2.z - v1.z) / 2.;
}

void __shift_vert__(float radius_min, float radius_max, Vec3 & v) {
	srand(get_time().tv_nsec);
	float radius = radius_min + (radius_max - radius_min) * ((rand() % 1000) / 1000.);
	float len = vec_len(v);
	float c = radius / len;
	v.x *= c;
	v.y *= c;
	v.z *= c;
}

void __calc_triangle_center__(const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = (v0.x + v1.x + v2.x) / 3.;
	res.y = (v0.y + v1.y + v2.y) / 3.;
	res.z = (v0.z + v1.z + v2.z) / 3.;
}

void __calc_triangle__(Polygon & p, const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, uint32_t color, bool use_normal) {
	p.set_vert_xyz(0, v0.x, v0.y, v0.z);
	p.set_vert_xyz(1, v1.x, v1.y, v1.z);
	p.set_vert_xyz(2, v2.x, v2.y, v2.z);

	p.set_vert_color(0, color);
	p.set_vert_color(1, color);
	p.set_vert_color(2, color);

	if (use_normal) {
		Vec3 n0;
		Vec3 n1;
		Vec3 n2;

		Vec3 v01;
		Vec3 v02;

		vec_sub(v0, v1, v01);
		vec_sub(v0, v2, v02);

		Vec3 v10;
		Vec3 v12;

		vec_sub(v1, v0, v10);
		vec_sub(v1, v2, v12);

		Vec3 v20;
		Vec3 v21;

		vec_sub(v2, v0, v20);
		vec_sub(v2, v1, v21);

		vec_cross(v02, v01, n0);
		vec_cross(v12, v10, n1);
		vec_cross(v21, v20, n2);

		vec_normalize(n0);
		vec_normalize(n1);
		vec_normalize(n2);

		p.set_vert_normal(0, n0.x, n0.y, n0.z);
		p.set_vert_normal(1, n1.x, n1.y, n1.z);
		p.set_vert_normal(2, n2.x, n2.y, n2.z);
	}
}

void create_sphere(PolygonCache* sphere_cache, float radius_min, float radius_max, std::vector<Polygon*>* debris, std::vector<Vec3>* directions) {

	Buffer verts_buffer;
	Buffer inds_buffer;
	int vert_size;
	int ind_size;

	sphere_cache->write_to_vert_buffer(&verts_buffer, vert_size);
	sphere_cache->write_to_index_buffer(&inds_buffer, ind_size);

	int num_components = sphere_cache->get_num_components();
	int color_component_offset = sphere_cache->get_color_component_offset();
	int cnt_ind = ind_size / sizeof(short);

	bool use_normal = sphere_cache->get_normal_component_offset() > 0;

	float* verts = (float*) verts_buffer.get_data();
	short* inds = (short*) inds_buffer.get_data();

	Polygon p(sphere_cache->get_color_component_offset(), sphere_cache->get_uv_component_offset(), sphere_cache->get_normal_component_offset(), true);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);
	p.set_preserve_vert_transform(true);

	sphere_cache->reset();

	PolygonCache tmp;

	for (int i = 0; i < cnt_ind; i += 3) {

		tmp.reset();

		int idx0 = i * num_components;
		int idx1 = (i + 1) * num_components;
		int idx2 = (i + 2) * num_components;

		Vec3 v0, v1, v2;
		v0 = *((Vec3*) &verts[idx0]);
		v1 = *((Vec3*) &verts[idx1]);
		v2 = *((Vec3*) &verts[idx2]);

		uint32_t v0_color = *((uint32_t*) &verts[idx0 + color_component_offset]);
		uint32_t v1_color = *((uint32_t*) &verts[idx1 + color_component_offset]);
		uint32_t v2_color = *((uint32_t*) &verts[idx2 + color_component_offset]);

		Vec3 v00;
		Vec3 v01;
		Vec3 v02;
		Vec3 v12;

		v00.x = v00.y = v00.z = 0.;

		__calc_mid_vert__(v0, v1, v01);
		__calc_mid_vert__(v0, v2, v02);
		__calc_mid_vert__(v1, v2, v12);

		__shift_vert__(radius_min, radius_max, v0);
		__shift_vert__(radius_min, radius_max, v1);
		__shift_vert__(radius_min, radius_max, v2);
		__shift_vert__(radius_min, radius_max, v01);
		__shift_vert__(radius_min, radius_max, v02);
		__shift_vert__(radius_min, radius_max, v12);

		// v0 v01 v02
		__calc_triangle__(p, v0, v01, v02, v0_color, use_normal);
		sphere_cache->add(&p);

		if (debris != NULL) {

			tmp.add(&p);

			__calc_triangle__(p, v00, v0, v02, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v02, v01, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v01, v0, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);
		}

		// v1 v01 v12
		__calc_triangle__(p, v01, v1, v12, v1_color, use_normal);
		sphere_cache->add(&p);

		if (debris != NULL) {

			tmp.add(&p);

			__calc_triangle__(p, v00, v01, v12, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v1, v01, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v12, v1, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);
		}

		// v2 v02 v12
		__calc_triangle__(p, v2, v02, v12, v2_color, use_normal);
		sphere_cache->add(&p);

		if (debris != NULL) {

			tmp.add(&p);

			__calc_triangle__(p, v00, v02, v2, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v12, v02, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v2, v12, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);
		}

		// v01 v02 v12
		__calc_triangle__(p, v02, v01, v12, v0_color, use_normal);
		sphere_cache->add(&p);

		if (debris != NULL) {

			tmp.add(&p);

			__calc_triangle__(p, v00, v02, v12, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v12, v01, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);

			__calc_triangle__(p, v00, v01, v02, v0_color, use_normal);
			tmp.add(&p);
			sphere_cache->add(&p);
		}

		if (debris != NULL) {
			debris->push_back(tmp.create_composite());
			Vec3 d;
			__calc_triangle_center__(v0, v1, v2, d);
			directions->push_back(d);
		}
	}
}

void create_ship(PolygonCache* cache) {

	Polygon p(true, false, false, true);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);

	uint32_t color = 0xff00ff0f;

	Vec3 v0, v1, v2, v3, v4;

	VEC3(v0, 0., 2., 0.);
	VEC3(v1, 0., 0., .5);
	VEC3(v2, -.5, 0., 0.);
	VEC3(v3, .5, 0., 0.);
	VEC3(v4, 0., -.3, 0.);

	__calc_triangle__(p, v0, v1, v2, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v0, v3, v1, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v3, v4, v1, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v1, v4, v2, color, cache->get_normal_component_offset());
	cache->add(&p);

	uint32_t wings_color = 0xfff0ffff;

	Vec3 wv0, wv1, wv2, wv3;

	VEC3(wv0, 0., 1.8, 0.);
	VEC3(wv1, 0., .1, 0.);
	VEC3(wv2, -1.1, 0., 0.);
	VEC3(wv3, 1.1, 0., 0.);

	__calc_triangle__(p, wv0, wv3, wv1, wings_color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, wv0, wv1, wv2, wings_color, cache->get_normal_component_offset());
	cache->add(&p);

}

void create_jet(PolygonCache* cache) {

	Polygon p(true, false, false, false);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);

	uint32_t color0 = 0xff0000ff;
	uint32_t color1 = 0x000000ff;
	uint32_t color2 = 0x000000ff;

	float v0[3] = { 0., .0, 0. };
	float v1[3] = { -.5, -.6, 0. };
	float v2[3] = { .5, -.6, 0. };

	p.set_vert_xyz(0, v0[0], v0[1], v0[2]);
	p.set_vert_xyz(1, v1[0], v1[1], v1[2]);
	p.set_vert_xyz(2, v2[0], v2[1], v2[2]);

	p.set_vert_color(0, color0);
	p.set_vert_color(1, color1);
	p.set_vert_color(2, color2);

	cache->add(&p);
}

void create_rocket(PolygonCache* cache) {

	Polygon p(true, false, false, true);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);

	uint32_t color = 0xfffff0ff;

	Vec3 v0, v1, v2, v3, v4, v5, v6, v7, v8;

	VEC3(v0, .05, .4, .05);
	VEC3(v1, -.05, .4, .05);
	VEC3(v2, .05, .4, -.05);
	VEC3(v4, -.05, .4, -.05);

	VEC3(v5, .05, -.4, .05);
	VEC3(v6, -.05, -.4, .05);
	VEC3(v7, .05, -.4, -.05);
	VEC3(v8, -.05, -.4, -.05);

	__calc_triangle__(p, v0, v5, v6, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v1, v0, v6, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v4, v1, v6, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v4, v6, v8, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v2, v7, v5, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v2, v5, v0, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v4, v8, v7, color, cache->get_normal_component_offset());
	cache->add(&p);

	__calc_triangle__(p, v4, v7, v2, color, cache->get_normal_component_offset());
	cache->add(&p);
}

void create_rocket_jet(PolygonCache* cache) {

	Polygon p(true, false, false, false);
	p.set_vert_count(3);
	p.set_preserve_vert_colors(true);

	uint32_t color0 = 0xff0000ff;
	uint32_t color1 = 0x000000ff;
	uint32_t color2 = 0x000000ff;

	float v0[3] = { 0., -.4, 0. };
	float v1[3] = { -.2, -1., 0. };
	float v2[3] = { .2, -1., 0. };

	p.set_vert_xyz(0, v0[0], v0[1], v0[2]);
	p.set_vert_xyz(1, v1[0], v1[1], v1[2]);
	p.set_vert_xyz(2, v2[0], v2[1], v2[2]);

	p.set_vert_color(0, color0);
	p.set_vert_color(1, color1);
	p.set_vert_color(2, color2);

	cache->add(&p);
}

void create_cube(PolygonCache* cache, bool use_normal) {
	Polygon* p = new Polygon(true, false, use_normal);

	p->set_vert_count(8, false);

	// bottom
	p->set_vert_xyz(0, 0.0f, 0.0f, 0.0f);
	p->set_vert_xyz(1, 1.0f, 0.0f, 0.0f);
	p->set_vert_xyz(2, 1.0f, 0.0f, 1.0f);
	p->set_vert_xyz(3, 0.0f, 0.0f, 1.0f);

	// top
	p->set_vert_xyz(4, 0.0f, 1.0f, 0.0f);
	p->set_vert_xyz(5, 1.0f, 1.0f, 0.0f);
	p->set_vert_xyz(6, 1.0f, 1.0f, 1.0f);
	p->set_vert_xyz(7, 0.0f, 1.0f, 1.0f);

	p->set_ind_count(36);

	// bottom
	p->set_index_value(0, 0);
	p->set_index_value(1, 1);
	p->set_index_value(2, 2);

	p->set_index_value(3, 0);
	p->set_index_value(4, 2);
	p->set_index_value(5, 3);

	// top
	p->set_index_value(6, 0);
	p->set_index_value(7, 0);
	p->set_index_value(8, 0);

	p->set_index_value(9, 0);
	p->set_index_value(10, 0);
	p->set_index_value(11, 0);

	// left
	p->set_index_value(12, 0);
	p->set_index_value(13, 0);
	p->set_index_value(14, 0);

	p->set_index_value(15, 0);
	p->set_index_value(16, 0);
	p->set_index_value(17, 0);

	// right
	p->set_index_value(18, 0);
	p->set_index_value(19, 0);
	p->set_index_value(20, 0);

	p->set_index_value(21, 0);
	p->set_index_value(22, 0);
	p->set_index_value(23, 0);

	// front
	p->set_index_value(24, 0);
	p->set_index_value(25, 0);
	p->set_index_value(26, 0);

	p->set_index_value(27, 0);
	p->set_index_value(28, 0);
	p->set_index_value(29, 0);

	// back
	p->set_index_value(30, 0);
	p->set_index_value(31, 0);
	p->set_index_value(32, 0);

	p->set_index_value(33, 0);
	p->set_index_value(34, 0);
	p->set_index_value(35, 0);
}

} // namespace fte
