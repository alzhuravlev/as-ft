#ifndef interpolate_utils_H
#define interpolate_utils_H

#include "fte_math.h"
#include "polygon_cache.h"

#include <vector>

namespace fte {

void linear_interpolate(const std::vector<Vec3> & map_points, std::vector<Vec3> & interpolated_map_points, float step);
void beizer_interpolate(const std::vector<Vec3> & map_points, const std::vector<Vec3> & support_points, std::vector<Vec3> & interpolated_map_points, float step);

} // namespace fte

#endif /* interpolate_utils_H */
