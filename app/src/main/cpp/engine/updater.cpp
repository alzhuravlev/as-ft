#include "updater.h"
#include "screen.h"

namespace fte {

void Updater::process_resize(const Command & command, FTE_ENV env) {

	if (screen == NULL)
		return;

	int w, h;
	command.consume_resize_command()->get_wh(w, h);

	screen->init_once(w, h, env);
	screen->init(w, h, env);
	screen->resize(w, h, env);

	smooth_delta = 0.0f;
	frame_start = get_time();

#ifdef DEBUG
	____fps_time = 0.;
	____frame = 0;
#endif
}

void Updater::update_screen(RenderCommand* command, FTE_ENV env) {

	fte_timespec now = get_time();
	float delta = get_time_diff(frame_start, now);
	frame_start = now;

	if (smooth_delta == 0.0f)
		smooth_delta = delta;
	smooth_delta = smooth_delta + (delta - smooth_delta) * 0.05f;
//	smooth_delta = 1.0f / 60.0f;

	if (!screen->is_updated_once())
		screen->update_once(command);

	screen->update(smooth_delta, command);

#ifdef DEBUG
	float update_time = get_time_diff(now, get_time());
	if (update_time > ____update_time_max)
		____update_time_max = update_time;
	if (update_time < ____update_time_min)
		____update_time_min = update_time;
	____update_time_accum += update_time;
	____update_count++;
	____batches_count += command->get_batch_count();

	// print fps and etc.
	____fps_time += delta;
	____frame++;

	if (____fps_time >= 1.0f) {
		____fps_flushed = ____frame;

		LOG_FPS(____fps_flushed);

		____fps_time = 0.0f;
		____frame = 0;

		if (____update_count > 0) {
			____update_time_avg = ____update_time_accum / ____update_count;
			____trans_avg = DEBUG_APPLY_TRANSFORMATIONS_COUNTER / ____update_count;
			____trans_vert_avg = DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER / ____update_count;
			____batches_avg = ____batches_count / ____update_count;

			LOGD("fte: UPDATER: AVG=%f; MAX=%f; MIN=%f; AVG_APPLY_TRANS=%d; AVG_APPLY_TRANS_VERTS=%d; AVG_BATCHES=%d", ____update_time_avg, ____update_time_max, ____update_time_min, ____trans_avg, ____trans_vert_avg, ____batches_avg);

			____update_time_accum = 0.0f;
			____update_count = 0;
			____update_time_max = 0.0f;
			____update_time_min = 1.0f;
			____batches_count = 0;
			DEBUG_APPLY_TRANSFORMATIONS_COUNTER = 0;
			DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER = 0;
		}
	}
#endif
}

void Updater::process_render(Command & command, FTE_ENV env) {
	if (screen == NULL)
		return;

	if (screen->is_ready_for_render()) {
		RenderCommand* render_command = command.produce_render_command();
		update_screen(render_command, env);
	} else if (!screen->is_renderer_initialized()) {
		InitRendererCommand* load_texture_command = command.produce_init_renderer_command();
		screen->init_renderer(load_texture_command, env);
	} else if (!screen->is_screen_size_initilized()) {
		LOGI("fte: UPDATER: detect that screen has not initialized yet, generates RESIZE command");
		command.produce_resize_command();
	}
}

void Updater::swap_screens(FTE_ENV env) {
	if (pending_screen == NULL)
		return;

	if (screen != NULL) {
		screen->release(env);
		DELETE(screen);
	}

	screen = pending_screen;
	pending_screen = NULL;
}

void Updater::set_pending_screen(Screen* pending_screen) {
	this->pending_screen = pending_screen;
	if (pending_screen)
		this->pending_screen->set_updater(this);
#ifdef DEBUG
	____update_time_accum = 0.;
	____update_count = 0;
	____update_time_max = 0.;
	____update_time_min = 1.;
	____batches_count = 0;
	DEBUG_APPLY_TRANSFORMATIONS_COUNTER = 0;
	DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER = 0;
#endif
}

void Updater::init(FTE_ENV env) {
	LOGI("fte: UPDATER: init");
	if (!audio_initialized) {
		audio.init(env);
		audio_initialized = true;
	} else
		audio.resume(env);
}

void Updater::release(FTE_ENV env) {
	LOGI("fte: UPDATER: release");
	if (screen)
		screen->release(env);

	audio.pause();
}

void Updater::process_custom_command(int commands[], int size) {
	if (screen)
		screen->custom_command(commands, size);
}

void Updater::process_touch(TouchEvent events[], int size) {
	if (screen)
		screen->touch(events, size);
}

void Updater::process(Command & command, FTE_ENV env) {

	if (pending_screen != NULL) {
		LOGW("fte: UPDATER: swap screens");
		swap_screens(env);
	}

	if (screen == NULL)
		return;

	switch (command.get_cmd_type()) {
	case COMMAND_RESIZE:
		process_resize(command, env);
		process_render(command, env);
		break;
	default:
		process_render(command, env);
		break;
	}
}

} // namespace fte
