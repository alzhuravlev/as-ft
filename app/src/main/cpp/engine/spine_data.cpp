#include "spine_data.h"
#include "polygon_builder.h"

static fte::FTE_ENV _env_;

void _spAtlasPage_createTexture(spAtlasPage* self, const char* path) {
	self->width = 128;
	self->height = 128;
	self->rendererObject = NULL;
}

void _spAtlasPage_disposeTexture(spAtlasPage* self) {
}

char* _spUtil_readFile(const char* path, int* length) {
	void* buf;
	long int size;
	load_resource_file(_env_, std::string(path), buf, size);

	*length = size;
	return (char*) buf;
}

namespace fte {

SpineData::SpineData() {
	skeleton_data = NULL;
	skeleton_json = NULL;
	atlas = NULL;
	animation_state_data = NULL;

	save_original_colors = true;
}

SpineData::~SpineData() {
	spSkeletonData_dispose(skeleton_data);
	spSkeletonJson_dispose(skeleton_json);
	spAtlas_dispose(atlas);
	spAnimationStateData_dispose(animation_state_data);

	delete_vector_elements<Polygon*>(polygons);
}

void SpineData::initialize_sprite(const TextureAtlasLoader & atlas_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, FTE_ENV env) {
	_env_ = env;

	atlas = spAtlas_createFromFile(spine_atlas_file_name.c_str(), NULL);

	skeleton_json = spSkeletonJson_create(atlas);
	skeleton_json->scale = json_scale;
	skeleton_data = spSkeletonJson_readSkeletonDataFile(skeleton_json, spine_model_file_name.c_str());
	if (!skeleton_data) {
		LOGE("fte: SpineRenderer: no skeleton data: %s: %s", spine_model_file_name.c_str(), skeleton_json->error);
	}
	animation_state_data = spAnimationStateData_create(skeleton_data);

	_env_ = NULL;

	spSkeleton* skeleton = spSkeleton_create(skeleton_data);

	// create polygons for every slot attachment
	int slot_count = skeleton->slotCount;
	for (int i = 0; i < slot_count; i++) {
		spSlot* slot = skeleton->slots[i];
		spRegionAttachment* ra = SUB_CAST(spRegionAttachment, slot->attachment);

		std::string attachment_name = std::string(ra->super.name);

		Polygon* p = PolygonBuiler::create_sprite(atlas_loader, attachment_name, true, false);

		if (p) {
			p->set_origin_to(ORIGIN_CENTER);
			p->set_enabled(false);
		}

		ra->scaleX *= ra->regionOriginalHeight * skeleton_json->scale;
		ra->scaleY *= ra->regionOriginalHeight * skeleton_json->scale;
		ra->rendererObject = p;

		polygons.push_back(p);
	}

	spSkeleton_dispose(skeleton);
}

void SpineData::initialize_textured(const TextureAtlasLoader & atlas_loader, const ObjModelLoader & model_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, const std::string texture_name, float json_scale,
		bool use_color, bool use_normal, bool invert_normals, bool face_normal, bool preserve_vert_transform, FTE_ENV env) {

	_env_ = env;

	atlas = spAtlas_createFromFile(spine_atlas_file_name.c_str(), NULL);

	skeleton_json = spSkeletonJson_create(atlas);
	skeleton_json->scale = json_scale;
	skeleton_data = spSkeletonJson_readSkeletonDataFile(skeleton_json, spine_model_file_name.c_str());
	if (!skeleton_data) {
		LOGE("fte: SpineRenderer: no skeleton data: %s: %s", spine_model_file_name.c_str(), skeleton_json->error);
	}
	animation_state_data = spAnimationStateData_create(skeleton_data);

	_env_ = NULL;

	spSkeleton* skeleton = spSkeleton_create(skeleton_data);

	// create polygons for every slot attachment
	int slot_count = skeleton->slotCount;
	for (int i = 0; i < slot_count; i++) {
		spSlot* slot = skeleton->slots[i];
		spRegionAttachment* ra = SUB_CAST(spRegionAttachment, slot->attachment);

		std::string attachment_name = std::string(ra->super.name);

		Polygon* polygon;
		if (face_normal)
			polygon = PolygonBuiler::create_textured_model_face_normal(model_loader, atlas_loader, attachment_name, texture_name, use_color, invert_normals);
		else
			polygon = PolygonBuiler::create_textured_model(model_loader, atlas_loader, attachment_name, texture_name, use_color, use_normal, invert_normals);

		polygon->set_preserve_vert_transform(preserve_vert_transform);
		polygon->set_enabled(false);

		float min_x, max_x, min_y, max_y, min_z, max_z;
		polygon->calculate_aabb(min_x, max_x, min_y, max_y, min_z, max_z);
		float ox, oy;
		ox = (max_x + min_x) * 0.5f;
		oy = (max_y + min_y) * 0.5f;
		polygon->set_origin_xyz(ox, oy, 0.0f);

		ra->rendererObject = polygon;

		polygons.push_back(polygon);
	}

	spSkeleton_dispose(skeleton);
}

void SpineData::initialize(const ObjModelLoader & model_loader, const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, bool use_normal, bool invert_normals, bool face_normal, bool preserve_vert_transform,
		FTE_ENV env) {

	_env_ = env;

	atlas = spAtlas_createFromFile(spine_atlas_file_name.c_str(), NULL);

	skeleton_json = spSkeletonJson_create(atlas);
	skeleton_json->scale = json_scale;
	skeleton_data = spSkeletonJson_readSkeletonDataFile(skeleton_json, spine_model_file_name.c_str());
	if (!skeleton_data) {
		LOGE("fte: SpineRenderer: no skeleton data: %s: %s", spine_model_file_name.c_str(), skeleton_json->error);
	}
	animation_state_data = spAnimationStateData_create(skeleton_data);

	_env_ = NULL;

	spSkeleton* skeleton = spSkeleton_create(skeleton_data);

	// create polygons for every slot attachment
	int slot_count = skeleton->slotCount;
	for (int i = 0; i < slot_count; i++) {
		spSlot* slot = skeleton->slots[i];
		spRegionAttachment* ra = SUB_CAST(spRegionAttachment, slot->attachment);

		std::string attachment_name = std::string(ra->super.name);

		Polygon* polygon = PolygonBuiler::create_model(model_loader, attachment_name, use_normal, invert_normals);

		polygon->set_preserve_vert_transform(preserve_vert_transform);
		polygon->set_enabled(false);

		float min_x, max_x, min_y, max_y, min_z, max_z;
		polygon->calculate_aabb(min_x, max_x, min_y, max_y, min_z, max_z);
		float ox, oy;
		ox = (max_x + min_x) * 0.5f;
		oy = (max_y + min_y) * 0.5f;
		polygon->set_origin_xyz(ox, oy, 0.0f);

		ra->rendererObject = polygon;

		polygons.push_back(polygon);
	}

	spSkeleton_dispose(skeleton);
}

void SpineData::initialize_custom(const std::string & spine_atlas_file_name, const std::string & spine_model_file_name, float json_scale, bool use_normal, const std::map<std::string, Polygon*> polygons_map, FTE_ENV env) {

	_env_ = env;

	atlas = spAtlas_createFromFile(spine_atlas_file_name.c_str(), NULL);

	skeleton_json = spSkeletonJson_create(atlas);
	skeleton_json->scale = json_scale;
	skeleton_data = spSkeletonJson_readSkeletonDataFile(skeleton_json, spine_model_file_name.c_str());
	if (!skeleton_data) {
		LOGE("fte: SpineRenderer: no skeleton data: %s: %s", spine_model_file_name.c_str(), skeleton_json->error);
	}
	animation_state_data = spAnimationStateData_create(skeleton_data);

	_env_ = NULL;

	spSkeleton* skeleton = spSkeleton_create(skeleton_data);

	// create polygons for every slot attachment
	int slot_count = skeleton->slotCount;
	for (int i = 0; i < slot_count; i++) {
		spSlot* slot = skeleton->slots[i];
		spRegionAttachment* ra = SUB_CAST(spRegionAttachment, slot->attachment);

		std::string attachment_name = std::string(ra->super.name);

		std::map<std::string, Polygon*>::const_iterator map_it = polygons_map.find(attachment_name);
		if (map_it == polygons_map.end()) {
			LOGE("fte: SpineRenderer: '%s' not found in polygons_map", attachment_name.c_str());
			continue;
		}

		Polygon* polygon = map_it->second;
		polygon->set_enabled(false);

		float min_x, max_x, min_y, max_y, min_z, max_z;
		polygon->calculate_aabb(min_x, max_x, min_y, max_y, min_z, max_z);
		float ox, oy;
		ox = (max_x + min_x) * 0.5f;
		oy = (max_y + min_y) * 0.5f;
		polygon->set_origin_xyz(ox, oy, 0.0f);

		ra->rendererObject = polygon;

		polygons.push_back(polygon);
	}

	spSkeleton_dispose(skeleton);
}

void SpineData::set_mix(const std::string & animation1, const std::string & animation2, float duration) {
	spAnimation* a1 = spSkeletonData_findAnimation(skeleton_data, animation1.c_str());
	spAnimation* a2 = spSkeletonData_findAnimation(skeleton_data, animation2.c_str());
	if (a1 && a2)
		spAnimationStateData_setMix(animation_state_data, a1, a2, duration);
}

float SpineData::get_animation_duration(const std::string & animation) {
	spAnimation* a = spSkeletonData_findAnimation(skeleton_data, animation.c_str());
	if (a)
		return a->duration;
	return 0.0f;
}

} // namespace fte

