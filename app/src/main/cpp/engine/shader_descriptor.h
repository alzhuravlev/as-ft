#ifndef shader_descriptor_H
#define shader_descriptor_H

#include <string>
#include "external.h"

namespace fte {

struct ShaderDescriptor {

	int id;
	std::string vs_file_name;
	std::string fs_file_name;

	ShaderDescriptor(int id, const std::string & vs_file_name, const std::string & fs_file_name) {
		this->id = id;
		this->vs_file_name = vs_file_name;
		this->fs_file_name = fs_file_name;
	}

};

} // namespace fte

#endif // shader_descriptor_H
