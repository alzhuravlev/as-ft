#include "spine_renderer.h"
#include "polygon_builder.h"

namespace fte {

void al(spAnimationState* state, int trackIndex, spEventType type, spEvent* event, int loopCount) {
}

SpineRenderer::SpineRenderer(SpineData* data) {
	this->data = data;
	animation_state = spAnimationState_create(data->animation_state_data);
	skeleton = spSkeleton_create(data->skeleton_data);

	animation_state->rendererObject = this;
	animation_state->listener = al;
}

SpineRenderer::~SpineRenderer() {
	spAnimationState_dispose(animation_state);
	spSkeleton_dispose(skeleton);
}

void SpineRenderer::reset_bones() {
	spSkeleton_setBonesToSetupPose(skeleton);
}

void SpineRenderer::set_animation(const std::string & animation, bool loop) {
	spAnimation* a = spSkeletonData_findAnimation(data->skeleton_data, animation.c_str());
	if (a) {
		spTrackEntry* current = spAnimationState_getCurrent(animation_state, 0);
		if (current && current->animation)
			if (std::string(current->animation->name) == animation && loop == current->loop)
				return;
		spAnimationState_setAnimation(animation_state, 0, a, loop);
	} else
		spAnimationState_clearTracks(animation_state);
}

void SpineRenderer::add_animation(const std::string & animation, bool loop) {
	spAnimation* a = spSkeletonData_findAnimation(data->skeleton_data, animation.c_str());
	if (a)
		spAnimationState_addAnimation(animation_state, 0, a, loop, 0.0f);
	else
		spAnimationState_clearTracks(animation_state);
}

void SpineRenderer::clear_animation() {
	spAnimationState_clearTracks(animation_state);
}

bool SpineRenderer::is_animation_complete() {
	spTrackEntry* current = spAnimationState_getCurrent(animation_state, 0);
	return !current;
}

const char* SpineRenderer::get_current_animation_name() {
	spTrackEntry* current = spAnimationState_getCurrent(animation_state, 0);
	if (current && current->animation)
		return current->animation->name;
	return NULL;
}

void SpineRenderer::update_animation(float delta) {
	spAnimationState_update(animation_state, delta);
	spAnimationState_apply(animation_state, skeleton);
	spSkeleton_updateWorldTransform(skeleton);
}

void SpineRenderer::update(float delta, const Vec3 & translate, const Vec3 & rotate, const Vec3 & scale) {
	spAnimationState_update(animation_state, delta);
	spAnimationState_apply(animation_state, skeleton);
	spSkeleton_updateWorldTransform(skeleton);

	float x, y, z;
	float rotate_x, rotate_y, rotate_z;
	float scale_x, scale_y, scale_z;

	int slot_count = skeleton->slotCount;
	spSlot** slot = skeleton->drawOrder;
	for (int i = 0; i != slot_count; i++) {
		spBone* bone = (*slot)->bone;
		spRegionAttachment* ra = SUB_CAST(spRegionAttachment, (*slot)->attachment);
		Polygon* p = (Polygon*) ra->rendererObject;
		p->set_enabled(false);

		scale_x = scale.x * bone->worldScaleX * ra->scaleX;
		if (F_EQ(scale_x, 0.0f))
			continue;

		scale_y = scale.y * bone->worldScaleY * ra->scaleY;
		if (F_EQ(scale_y, 0.0f))
			continue;

		scale_z = scale.z * bone->worldScaleX * ra->scaleX;
		if (F_EQ(scale_z, 0.0f))
			continue;

		x = bone->worldX;
		y = bone->worldY;
		z = 0.0f;

		x += ra->x * bone->m00 + ra->y * bone->m01;
		y += ra->x * bone->m10 + ra->y * bone->m11;

		x *= scale.x;
		y *= scale.y;

		rotate_point_fast(0.0f, rotate.y, rotate.z, x, y, z);

		x += translate.x;
		y += translate.y;
		z += translate.z;

		rotate_x = rotate.x;
		rotate_y = rotate.y;
		rotate_z = rotate.z + (bone->worldRotation + ra->rotation) * M_DEG_TO_RAD;

		p->set_translate(x, y, z);
		p->set_angle_xyz(rotate_x, rotate_y, rotate_z);
		p->set_scale_xyz(scale_x, scale_y, scale_z);

		if (!data->save_original_colors) {
			uint32_t color;
			pack_color_f(color, (*slot)->r, (*slot)->g, (*slot)->b, (*slot)->a);
			p->set_color(color);
		}

		p->set_enabled(true);

		slot++;
	}
}

void SpineRenderer::render(RenderCommand* command) {
	command->add(data->polygons);
}

void SpineRenderer::render_batches(RenderCommand * command, int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const Camera & camera) {
	for (std::vector<Polygon*>::iterator it = data->polygons.begin(), end = data->polygons.end(); it != end; ++it) {
		Polygon* p = *it;
		p->set_preserve_vert_transform(true);
		p->update_model_matrix();
		command->begin_batch(shader_id, shader_toon_id, enable_depth, enable_cull_face, camera.get_view_proj_matrix(), p->get_model_matrix());
		command->add(p);
		command->end_batch();
	}
}

void SpineRenderer::render_static_batch(RenderCommand * command, TYPE_STATIC_BATCH_ID start_batch_id) {
	for (std::vector<Polygon*>::iterator it = data->polygons.begin(), end = data->polygons.end(); it != end; ++it) {
		Polygon* p = *it;
		p->update_model_matrix();
		command->begin_end_static_batch(p, start_batch_id++);
	}
}

void SpineRenderer::render_static_batch_no_data(RenderCommand * command, TYPE_STATIC_BATCH_ID start_batch_id, int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const Camera & camera) {
	for (std::vector<Polygon*>::iterator it = data->polygons.begin(), end = data->polygons.end(); it != end; ++it) {
		Polygon* p = *it;
		p->update_model_matrix();
		command->begin_end_static_batch_nodata(start_batch_id++, shader_id, shader_toon_id, enable_depth, enable_cull_face, camera.get_view_proj_matrix(), p->get_model_matrix());
	}
}

void SpineRenderer::render_to_cache(PolygonCache* cache) {
	for (std::vector<Polygon*>::iterator it = data->polygons.begin(), end = data->polygons.end(); it != end; ++it) {
		Polygon* p = *it;
		cache->add(p);
	}
}

} // namespace fte

