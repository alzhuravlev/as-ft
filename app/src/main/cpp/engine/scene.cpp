#include "scene.h"
#include "actor.h"

namespace fte {

bool actors_comparator(Actor* a1, Actor* a2) {
	return (a1->get_type() < a2->get_type());
}

Scene::Scene() {
}

Scene::~Scene() {
	delete_vector_elements<Actor*>(actors);
}

void Scene::activate_actor(Actor* actor) {
}

void Scene::deactivate_actor(Actor* actor) {
}

void Scene::do_release(FTE_ENV env) {
	do_stop_scene(env);
}

void Scene::do_init(FTE_ENV env) {
	do_start_scene(env);
}

void Scene::do_init_once(FTE_ENV env) {
	do_init_scene(env);

	do_create_actors(actors);

	std::sort(actors.begin(), actors.end(), actors_comparator);

	for (std::vector<Actor*>::iterator it = actors.begin(); it != actors.end(); ++it) {
		Actor* actor = *it;
		actor->init(env);
	}
}

void Scene::do_init_renderer(InitRendererCommand* command, FTE_ENV env) {
}

void Scene::do_touch(TouchEvent events[], int size) {
}

Actor* Scene::find_first_inactive(int type) {
	for (std::vector<Actor*>::iterator it = actors.begin(), it_end = actors.end(); it != it_end; ++it) {
		Actor* actor = *it;
		if (actor->get_type() == type && !actor->is_active())
			return actor;
	}
	return NULL;
}

Actor* Scene::find_first_active(int type) {
	for (std::vector<Actor*>::iterator it = actors.begin(), it_end = actors.end(); it != it_end; ++it) {
		Actor* actor = *it;
		if (actor->get_type() == type && actor->is_active())
			return actor;
	}
	return NULL;
}

void Scene::find_all_active(int type, std::vector<Actor*> & out) {
	for (std::vector<Actor*>::iterator it = actors.begin(), it_end = actors.end(); it != it_end; ++it) {
		Actor* actor = *it;
		if (actor->get_type() == type && actor->is_active())
			out.push_back(actor);
	}
}

void Scene::do_receive_broadcast(uint64_t type, void* p) {
}

void Scene::send_broadcast(uint64_t type, void* p) {
	do_receive_broadcast(type, p);
	for (std::vector<Actor*>::iterator it = actors.begin(), it_end = actors.end(); it != it_end; ++it) {
		Actor* actor = *it;
		if (actor->is_active())
			actor->receive_broadcast(type, p);
	}
}

void Scene::unproject_tap_ray(float win_x, float win_y, Vec3 & ray_origin, Vec3 & ray_direction, const Camera & camera) {

	win_y = get_height() - win_y;

	Vec3 near(win_x, win_y, 0.0f);
	Vec3 far(win_x, win_y, 1.0f);

	camera.unproject(near.x, near.y, near.z, get_width(), get_height());
	camera.unproject(far.x, far.y, far.z, get_width(), get_height());

	ray_origin = camera.get_camera_position();
	vec_sub(far, near, ray_direction);
	vec_normalize(ray_direction);
}

bool Scene::is_paused() const {
	return false;
}

} // namespace fte
