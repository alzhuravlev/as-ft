#ifndef sprite_H
#define sprite_H

#include "polygon.h"

namespace fte {

class Sprite: public Polygon {
	friend class TextureAtlasLoader;
protected:

public:
	Sprite(bool use_color, bool use_normal, bool clockwise_index_order = false, bool use_nm_uv = false) :
			Polygon(use_color, true, use_normal, clockwise_index_order, use_nm_uv) {
	}

	Sprite(const Sprite & s) :
			Polygon(s) {
	}

	Sprite & operator=(const Sprite & rhs) {
		Polygon::operator =(rhs);
		return *this;
	}

	~Sprite() {
	}

	// uv

	inline float get_u_size() const {
		return get_u2() - get_u1();
	}

	inline float get_v_size() const {
		return get_v2() - get_v1();
	}

	inline float get_u1() const {
		return get_vert_u(0);
	}

	inline float get_u2() const {
		return get_vert_u(1);
	}

	inline float get_v1() const {
		return get_vert_v(0);
	}

	inline float get_v2() const {
		return get_vert_v(3);
	}

	void set_u1(float u1) {
		set_vert_u(0, u1);
		set_vert_u(3, u1);
	}

	void set_u2(float u2) {
		set_vert_u(1, u2);
		set_vert_u(2, u2);
	}

	void set_v1(float v1) {
		set_vert_v(0, v1);
		set_vert_v(1, v1);
	}

	void set_v2(float v2) {
		set_vert_v(2, v2);
		set_vert_v(3, v2);
	}

	// normal map uv

	inline float get_nm_u_size() const {
		return get_nm_u2() - get_nm_u1();
	}

	inline float get_nm_v_size() const {
		return get_nm_v2() - get_nm_v1();
	}

	inline float get_nm_u1() const {
		return get_vert_nm_u(0);
	}

	inline float get_nm_u2() const {
		return get_vert_nm_u(1);
	}

	inline float get_nm_v1() const {
		return get_vert_nm_v(0);
	}

	inline float get_nm_v2() const {
		return get_vert_nm_v(3);
	}

	void set_nm_u1(float u1) {
		set_vert_nm_u(0, u1);
		set_vert_nm_u(3, u1);
	}

	void set_nm_u2(float u2) {
		set_vert_nm_u(1, u2);
		set_vert_nm_u(2, u2);
	}

	void set_nm_v1(float v1) {
		set_vert_nm_v(0, v1);
		set_vert_nm_v(1, v1);
	}

	void set_nm_v2(float v2) {
		set_vert_nm_v(2, v2);
		set_vert_nm_v(3, v2);
	}
};

} // namespace fte

#endif // sprite_H
