#ifndef font_renderer_H
#define font_renderer_H

#include "command.h"
#include "fte_math.h"
#include "polygon.h"
#include "polygon_builder.h"
#include "font_loader.h"

#include <map>
#include <vector>

namespace fte {

class FontRenderer {
private:

	std::map<int, Polygon*> polygons;
	std::map<int, FontChar> chars;
	FontCommon font_common;
	std::vector<TextureDescriptor> texture_descriptors;

	void create_polygons(const FontLoader & loader, Sprite* sprite = NULL) {
		for (std::map<int, FontChar>::const_iterator it = chars.begin(), end = chars.end(); it != end; ++it) {
			int code = it->first;
			Polygon* p = PolygonBuiler::create_character(loader, code, sprite);
			polygons[code] = p;
		}
	}

	void render_glyph(int code, RenderCommand & command, const Vec3 & position, const Vec3 & scale, Vec3 & current_position) {

		switch (code) {
		case '\n':
		case '\r':
			current_position.x = position.x;
			current_position.y -= font_common.line_height * scale.y;
			break;

		default:
			std::map<int, FontChar>::const_iterator chars_it = chars.find(code);
			if (chars_it == chars.end()) {
				LOGW("fte: render_glyph: character with code %d not found!", code);
				return;
			}

			const FontChar & font_char = chars_it->second;

			std::map<int, Polygon*>::const_iterator polygons_it = polygons.find(code);
			if (polygons_it == polygons.end()) {
				LOGW("fte: render_glyph: polygon with code %d not found!", code);
				return;
			}

			Polygon* p = polygons_it->second;

			p->set_translate(current_position.x, current_position.y - (1.0f - font_common.base) * scale.y, current_position.z);
			p->set_scale_xyz(scale.x, scale.y, scale.z);

			current_position.x += font_char.xadvance * scale.x;

			command.add(p);
			break;
		}
	}

	void measure_glyph(int code, Vec3 & line, bool last) {
		switch (code) {
		case '\n':
		case '\r':
			line.y += font_common.line_height;
			line.x = 0.0f;
			break;

		default:

			std::map<int, FontChar>::const_iterator chars_it = chars.find(code);
			if (chars_it == chars.end()) {
				LOGW("fte: measure_glyph: character with code %d not found!", code);
				return;
			}

			const FontChar & font_char = chars_it->second;

			if (last)
				line.x += font_char.w;
			else
				line.x += font_char.xadvance;
			break;
		}
	}

public:

	FontRenderer() {
	}

	~FontRenderer() {
		release();
	}

	void init(const FontLoader & loader, Sprite* sprite = NULL) {
		loader.get_font_chars(chars);
		font_common = loader.get_font_common();
		create_polygons(loader, sprite);
	}

	void init(const TextureAtlasLoader & atlas, const FontLoader & loader, const std::string & sprite_name) {
		Sprite* s = PolygonBuiler::create_sprite(atlas, sprite_name, true, false);
		loader.get_font_chars(chars);
		font_common = loader.get_font_common();
		create_polygons(loader, s);
		DELETE(s);
	}

	void release() {
		delete_map_values<int, Polygon*>(polygons);
	}

	void measure_string(const std::string & value, Vec3 & result) {
		Vec3 line;
		line.y += font_common.base;
		const int l = value.size();
		const int last = l - 1;
		for (int i = 0; i != l; i++) {
			int code = value.at(i);
			measure_glyph(code, line, i == last);
			if (line.x > result.x)
				result.x = line.x;
		}
		result.y = line.y;
	}

	void render_string(const std::string & value, RenderCommand & command, const Vec3 & position, const Vec3 & scale) {
		Vec3 current_position = position;

		for (std::string::const_iterator it = value.begin(), end = value.end(); it != end; ++it) {
			int code = *it;
			render_glyph(code, command, position, scale, current_position);
		}
	}

	void render_int(int value, RenderCommand & command, const Vec3 & position, const Vec3 & scale) {
		std::string s;
		fast_itoa(value, s);
		render_string(s, command, position, scale);
	}
};

} // namespace fte

#endif /* font_renderer_H */
