#ifndef fte_time_H
#define fte_time_H

namespace fte {

typedef struct {
	long tv_sec;
	long tv_nsec;
} fte_timespec;

} // namespace fte

#endif /* fte_time_H */
