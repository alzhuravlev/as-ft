#include "particle_emitter.h"

namespace fte {

Particle* Particle::first_available = NULL;

Particle* Particle::create() {
	if (first_available == NULL) {
		Particle* p = new Particle;
		p->next = NULL;
		return p;
	}

	Particle * free_particle = first_available;
	first_available = free_particle->next;

	return free_particle;
}

void Particle::release(Particle* particle) {
	Particle* p = first_available;
	first_available = particle;
	first_available->next = p;
}

} // namespace fte
