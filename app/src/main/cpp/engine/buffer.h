#ifndef buffer_H
#define buffer_H

#include "external.h"
#include "stdint.h"
#include "fte_utils.h"

#include <string>

namespace fte {

/**
 * Reusable dynamic size buffer.
 * Accumulate and transfer data from update cycle to render cycle.
 */
class Buffer {
private:
	void* data;
	uint32_t initial_size;
	uint32_t capacity;
	uint32_t size;
	uint32_t read_index;

	void ceil_to_pow_2(uint32_t & v) {
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
	}

	inline void ensure_size(uint32_t reqired_size) {
		if (data == NULL) {
			data = malloc(initial_size);
			capacity = initial_size;
		}

		uint32_t new_size = size + reqired_size;
		if (capacity < new_size) {
			capacity = new_size;
			ceil_to_pow_2(capacity);
			data = realloc(data, capacity);
		}
	}

public:
	Buffer(uint32_t initial_size = 2048) {
		this->initial_size = initial_size;
		data = NULL;
		size = 0;
		capacity = 0;
		read_index = 0;
	}

	~Buffer() {
		free(data);
	}

	void release() {
		free(data);
		data = NULL;
		read_index = 0;
		size = 0;
	}

	inline void reset() {
		read_index = 0;
		size = 0;
	}

	void write(const void* buf, uint32_t length) {
		ensure_size(length);
		char* c = (char*) data;
		c += size;
		memcpy(c, buf, length);
		size += length;
	}

	void write_short(short val) {
		write(&val, sizeof(short));
	}

	void write_int(int val) {
		write(&val, sizeof(int));
	}

	void write_float(float val) {
		write(&val, sizeof(float));
	}

	void write_string(const std::string & val) {
		write(val.data(), val.size());
	}

	bool read(void* & buf, uint32_t length) {
		if (data == NULL || read_index + length > size)
			return false;
		char* ptr = (char*) data;
		ptr += read_index;
		read_index += length;
		buf = (void*) ptr;
		return true;
	}

	bool read_float(float & v) {
		if (data == NULL || read_index + sizeof(float) > size)
			return false;
		char* ptr = (char*) data;
		ptr += read_index;
		read_index += sizeof(float);
		v = *((float*) ptr);
		return true;
	}

	bool read_byte(int & v) {
		if (data == NULL || read_index + sizeof(char) > size)
			return false;
		char* ptr = (char*) data;
		ptr += read_index;
		read_index += sizeof(char);
		v = *((char*) ptr);
		return true;
	}

	bool read_short(int & v) {
		if (data == NULL || read_index + sizeof(short) > size)
			return false;
		char* ptr = (char*) data;
		ptr += read_index;
		read_index += sizeof(short);
		v = *((short*) ptr);
		return true;
	}

	bool read_int(int & v) {
		if (data == NULL || read_index + sizeof(int) > size)
			return false;
		char* ptr = (char*) data;
		ptr += read_index;
		read_index += sizeof(int);
		v = *((int*) ptr);
		return true;
	}

	uint32_t get_size() const {
		return size;
	}

	uint32_t get_capacity() const {
		return capacity;
	}

	void* get_data() const {
		return data;
	}

	void set_data(void* data, int size) {
		release();
		this->data = data;
		this->size = size;
		this->capacity = size;
		this->read_index = 0;
	}

#ifdef DEBUG

	void print_buffer_float(int num_components) const {
		LOGD("==================");
		int s = size / sizeof(float);
		float* a = (float*) data;
		uint32_t* ui = (uint32_t*) data;
		for (int i = 0; i < s; i++) {
			LOGD( "%d. v[%d][%d] = %f (%x)", i, i / num_components, i % num_components, a[i], ui[i]);
		}
		LOGD("==================");
	}

	void print_buffer_short() {
		LOGD("==================");
		int s = size / sizeof(short);
		short* a = (short*) data;
		for (int i = 0; i < s; i++)
			LOGD("a[%d] = %d", i, a[i]);
		LOGD("==================");
	}

#endif

};

} // namespace fte

#endif // buffer_H
