#ifndef map_utils_H
#define map_utils_H

#include "fte_math.h"
#include "fte_utils_3d.h"

#include <vector>

namespace fte {

inline void angular_point_to_world_point(const Vec3 & angular_point, Vec3 & world_point, RADIUS_FUNC radius_func, float radius_offset = 0.0f) {

	float radius = radius_func(angular_point.y, angular_point.x) + radius_offset;

	world_point.x = radius * sin_fast(angular_point.x);
	world_point.y = angular_point.y;
	world_point.z = radius * cos_fast(angular_point.x);
}

inline void map_point_to_angular_point(const Vec3 & map_point, Vec3 & angular_point, float scale_factor_x = 1.0f, float scale_factor_y = 1.0f) {
	angular_point.x = map_point.x * scale_factor_x;
	angular_point.y = map_point.y * scale_factor_y;
	angular_point.z = 0.0f;
}

void map_points_to_angular_points(const std::vector<Vec3> & map_points, std::vector<Vec3> & angular_points, float scale_factor_x = 1.0f, float scale_factor_y = 1.0f);
void map_points_to_angular_points(const std::vector<Vec3>::const_iterator map_points_begin, const std::vector<Vec3>::const_iterator map_points_end, std::vector<Vec3> & angular_points, float scale_factor_x = 1.0f, float scale_factor_y = 1.0f);
void angular_points_to_world_points(const std::vector<Vec3> & angular_points, std::vector<Vec3> & world_points, RADIUS_FUNC radius_func, bool loop);

} // namespace fte

#endif /* map_utils_H */
