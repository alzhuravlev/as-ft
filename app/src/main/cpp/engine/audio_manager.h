#ifndef audio_manager_H
#define audio_manager_H

#include "engine.h"
#include "external.h"

namespace fte {

class AudioManager {
private:
	AUDIO_MANAGER_ENV aenv;
	FTE_ENV env;

public:
	void init(FTE_ENV env) {
		this->env = env;
		this->aenv = AudioManager_init(env);
	}

	void release() {
		AudioManager_release(env, aenv);
		aenv = 0;
		env = 0;
	}

	void pause() {
		AudioManager_pause(env, aenv);
	}

	void resume(FTE_ENV env) {
		this->env = env;
		AudioManager_resume(env, aenv);
	}

	SOUND_ID create_sound(const std::string & file_name) const {
		return AudioManager_create_sound(env, aenv, file_name);
	}

	MUSIC_ID create_music(const std::string & file_name) const {
		return AudioManager_create_music(env, aenv, file_name);
	}

	void destroy_sound(SOUND_ID sound_id) const {
		AudioManager_destroy_sound(env, aenv, sound_id);
	}

	void destroy_music(MUSIC_ID music_id) const {
		AudioManager_destroy_music(env, aenv, music_id);
	}

	STREAM_ID play_sound(SOUND_ID id, bool loop = false) const {
		return AudioManager_play_sound(env, aenv, id, loop);
	}

	void stop_sound(SOUND_ID id) const {
		AudioManager_stop_sound(env, aenv, id);
	}

	void play_music(MUSIC_ID id, bool loop = false) const {
		AudioManager_play_music(env, aenv, id, loop);
	}

	void stop_music(MUSIC_ID id) const {
		AudioManager_stop_music(env, aenv, id);
	}
};

} // namespace fte

#endif /* audio_manager_H */
