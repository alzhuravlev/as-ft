#include "fte_math.h"

namespace fte {

Vec3 tmp_v;
Vec3 tmp_uv, tmp_uuv;
Vec3 tmp_QuatVector;

float vec_len(const Vec3 & v1, const Vec3 & v2) {
	Vec3 tmp;
	vec_sub(v2, v1, tmp);
	return vec_len(tmp);
}

void vec_center(const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = v1.x + (v2.x - v1.x) * 0.5f;
	res.y = v1.y + (v2.y - v1.y) * 0.5f;
	res.z = v1.z + (v2.z - v1.z) * 0.5f;
}

void vec_cross(const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	Vec3 tmp1;
	vec_sub(v1, v0, tmp1);

	Vec3 tmp2;
	vec_sub(v2, v0, tmp2);

	vec_cross(tmp1, tmp2, res);
}

void vec_avg_normal(const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, const Vec3 & v3, Vec3 & res) {
	Vec3 tmp1;
	vec_cross(v0, v1, v2, tmp1);
	vec_normalize(tmp1);

	Vec3 tmp2;
	vec_cross(v0, v2, v3, tmp2);
	vec_normalize(tmp2);

	vec_add(tmp1, tmp2, res);
	vec_normalize(res);
}

float vec_dot(const Vec3 & v1_1, const Vec3 & v1_2, const Vec3 & v2_1, const Vec3 & v2_2) {
	Vec3 v1, v2;
	vec_sub(v1_2, v1_1, v1);
	vec_sub(v2_2, v2_1, v2);
	vec_normalize(v1);
	vec_normalize(v2);
	return vec_dot(v1, v2);
}

bool vec_is_ray_intersects_plane(const Vec3 & r0, const Vec3 & rd, const Vec3 & p1, const Vec3 & p2, const Vec3 & p3, Vec3 & out) {
//	float A = p1.y * (p2.z - p3.z) + p2.y * (p3.z - p1.z) + p3.y * (p1.z - p2.z);
//	float B = p1.y * (p2.z - p3.z) + p2.y * (p3.z - p1.z) + p3.y * (p1.z - p2.z);
//	float C = p1.y * (p2.z - p3.z) + p2.y * (p3.z - p1.z) + p3.y * (p1.z - p2.z);
	return false;
}

bool vec_is_ray_intersects_plane(const Vec3 & r0, const Vec3 & rd, const Vec3 & pn, float pd, Vec3 & out) {
	float vd = vec_dot(pn, rd);
	if (F_EQ(vd, 0.0f))
		return false;

	float v0 = -(vec_dot(pn, r0) + pd);

	float t = v0 / vd;
	if (F_LS(t, 0.0f))
		return false;

	out.x = r0.x + rd.x * t;
	out.y = r0.y + rd.y * t;
	out.z = r0.z + rd.z * t;

	return true;
}

const static int SIN_BITS = 21; // you may adjust this for accuracy.
const static int SIN_MASK = ~(-1 << SIN_BITS);
const static int SIN_COUNT = SIN_MASK + 1;

const static float radFull = M_PI * 2.0f;
const static float radToIndex = SIN_COUNT / M_PI * 0.5f;
const static float degToIndex = SIN_COUNT / 360.0f;

static float sin_table[SIN_COUNT];

static bool __init_sin_table__initialized = false;
void __init_sin_table__() {
	for (int i = 0; i < SIN_COUNT; i++)
		sin_table[i] = sinf((i + 0.5) / SIN_COUNT * radFull);
	for (int i = 0; i < 360; i += 90)
		sin_table[(int) (i * degToIndex) & SIN_MASK] = sinf(i * M_DEG_TO_RAD);
	__init_sin_table__initialized = true;
}

static float cos_table[SIN_COUNT];

static bool __init_cos_table__initialized = false;
void __init_cos_table__() {
	for (int i = 0; i < SIN_COUNT; i++)
		cos_table[i] = cosf((i + 0.5f) / SIN_COUNT * radFull);
	for (int i = 0; i < 360; i += 90)
		cos_table[(int) (i * degToIndex) & SIN_MASK] = cosf(i * M_DEG_TO_RAD);
	__init_cos_table__initialized = true;
}

float sin_fast(float rad) {
//	if (!__init_sin_table__initialized)
//		__init_sin_table__();
//	return sin_table[(int) (rad * radToIndex) & SIN_MASK];
	return sinf(rad);
}

float cos_fast(float rad) {
//	if (!__init_cos_table__initialized)
//		__init_cos_table__();
//	return cos_table[(int) (rad * radToIndex) & SIN_MASK];
	return cosf(rad);
}

/**
 * Math utils
 */

float calc_penetration_a_in_interval(float a, float left, float right, bool from_left) {
	if (F_EQ(left, right) && !(F_EQ(right, a)))
		return 0.;

	if (left < right) {

		// normal interval

		if (left < a && a < right) {
			return from_left ? a - left : right - a;
		}

	} else {

		// abnormal interval

		if (left < a && a < M_PI) {

			return from_left ? a - left : right + M_2xPI - a;

		} else if (-M_PI < a && a < right) {

			return from_left ? M_PI - left + a + M_PI : right - a;

		}
	}

	return 0.;
}

bool is_intervals_intersects(float a1, float a2, float b1, float b2) {
	if (a1 > a2 && b1 > b2)
		return true;

	if (a1 > a2 && (b1 < a2 || b2 > a1))
		return true;

	if (b1 > b2 && (a1 < b2 || a2 > b1))
		return true;

	if (a1 < a2 && b1 < b2 && ((b1 < a2 && b2 > a1) || (b1 > a2 && b2 < a1)))
		return true;

	return false;
}

} // namespace fte
