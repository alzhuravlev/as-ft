#include "interpolate_utils.h"

namespace fte {

void __point_linear_interpolate(const Vec3 & start, const Vec3 & direction, float scale, Vec3 & res) {
	Vec3 tmp;
	tmp = direction;
	vec_mul(tmp, scale);
	vec_add(start, tmp, res);
}

Vec3 __bezier_point(const Vec3 & p1, const Vec3 & p2, float k) {
	Vec3 tmp;
	tmp.x = p1.x + (p2.x - p1.x) * k;
	tmp.y = p1.y + (p2.y - p1.y) * k;
	return tmp;
}

Vec3 __bezier_mirror_point(const Vec3 & point, const Vec3 & support_point) {
	Vec3 dir;
	vec_sub(point, support_point, dir);

	Vec3 res;
	vec_add(point, dir, res);

	return res;
}

void cubic_beizer_interpolate(const Vec3 & p1, const Vec3 & p2, const Vec3 & p3, const Vec3 & p4, std::vector<Vec3> & interpolated_map_points, float step) {

	interpolated_map_points.push_back(p1);

	float current_step = step;
	do {

		Vec3 pp1 = __bezier_point(p1, p2, current_step);
		Vec3 pp2 = __bezier_point(p2, p3, current_step);
		Vec3 pp3 = __bezier_point(p3, p4, current_step);

		Vec3 ppp1 = __bezier_point(pp1, pp2, current_step);
		Vec3 ppp2 = __bezier_point(pp2, pp3, current_step);

		Vec3 pppp1 = __bezier_point(ppp1, ppp2, current_step);

		interpolated_map_points.push_back(pppp1);

		current_step += step;
	} while (current_step < 1.0f);

	interpolated_map_points.push_back(p4);
}

void beizer_interpolate(const std::vector<Vec3> & map_points, const std::vector<Vec3> & support_points, std::vector<Vec3> & interpolated_map_points, float step) {
	if (map_points.size() < 2) {
		LOGE("road_beizer_interpolate: points size must be at least 2!");
		return;
	}

	if (map_points.size() != support_points.size()) {
		LOGE("road_beizer_interpolate: map_points must be equal to support_points!");
		return;
	}

	for (int i = 0; i < map_points.size() - 1; i++) {
		Vec3 p1 = map_points[i];
		Vec3 p2 = support_points[i];
		Vec3 p3 = __bezier_mirror_point(map_points[i + 1], support_points[i + 1]);
		Vec3 p4 = map_points[i + 1];

		cubic_beizer_interpolate(p1, p2, p3, p4, interpolated_map_points, step);
	}
}

void linear_interpolate(const std::vector<Vec3> & map_points, std::vector<Vec3> & interpolated_map_points, float step) {
	if (map_points.size() < 2) {
		LOGE("road_linear_interpolate: points size must be at least 2!");
		return;
	}

	Vec3 p1, p2;
	p1 = map_points[0];

	interpolated_map_points.push_back(p1);

	for (int i = 1; i < map_points.size(); i++) {

		p2 = map_points[i];

		Vec3 dir;
		vec_sub(p2, p1, dir);

		float len = vec_len(dir);

		vec_normalize(dir);

		float current_scale = 0;
		do {

			current_scale += step;

			Vec3 tmp;
			if (current_scale < len)
				__point_linear_interpolate(p1, dir, current_scale, tmp);
			else
				tmp = p2;

			interpolated_map_points.push_back(tmp);

		} while (current_scale < len);

		p1 = p2;
	}
}

} // namespace fte
