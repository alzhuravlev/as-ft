#ifndef gjk_H
#define gjk_H

namespace fte {

bool is_polyhedron_intersects(float shape1[], int vert_count1, int stride1, float shape2[], int vert_count2, int stride2);

} // namespace fte

#endif /* gjk_H */
