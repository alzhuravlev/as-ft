#ifndef particle_effect_H
#define particle_effect_H

#include "engine.h"
#include "particle_emitter.h"

#include <vector>

namespace fte {

class ParticleEffect {
private:
	bool active;
	std::vector<ParticleEmitter*> emitters;

protected:

	virtual void do_init(std::vector<ParticleEmitter*> & emitters, FTE_ENV env) = 0;

public:

	ParticleEffect() {
		active = false;
	}

	virtual ~ParticleEffect() {
	}

	void init(FTE_ENV env) {
		do_init(emitters, env);
	}

	void release() {
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			emitter->release();
		}
		emitters.clear();
	}

	void update(float delta, const Camera & camera) {
		if (!active)
			return;
		active = false;
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			active |= emitter->update(delta, camera);
		}
	}

	void render(RenderCommand* command) {
		if (!active)
			return;
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			emitter->render(command);
		}
	}

	void start(bool single_loop = false) {
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			emitter->start(single_loop);
		}
		active = true;
	}

	void stop(bool immediate = false) {
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			emitter->stop(immediate);
		}
	}

	inline bool is_active() {
		return active;
	}

	bool is_stopping() {
		for (std::vector<ParticleEmitter*>::iterator it = emitters.begin(), end = emitters.end(); it != end; ++it) {
			ParticleEmitter* emitter = *it;
			if (emitter->is_stopping())
				return true;
		}
		return false;
	}
};

} // namespace fte

#endif /* particle_effect_H */
