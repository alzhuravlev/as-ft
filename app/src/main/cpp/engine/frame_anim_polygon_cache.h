#ifndef frame_anim_polygon_cache_H
#define frame_anim_polygon_cache_H

#include "polygon.h"
#include "polygon_builder.h"
#include "texture_atlas_loader.h"
#include "obj_model_loader.h"
#include <map>
#include <string>

namespace fte {

class FrameAnimationPolygonCache {
private:

	std::map<std::string, std::map<int, Polygon*> > data;

	const std::string format_obj_filename(const std::string & obj_path, const std::string & base_name, int frame) {
		std::string frame_str = to_string(frame);
		if (frame_str.size() < 6)
			frame_str.insert(0, 6 - frame_str.size(), '0');
		return obj_path + base_name + "_" + frame_str + ".obj";
	}

	const std::string format_obj_direct_load_filename(const std::string & obj_path, const std::string & base_name, int frame) {
		std::string frame_str = to_string(frame);
		if (frame_str.size() < 6)
			frame_str.insert(0, 6 - frame_str.size(), '0');
		return obj_path + base_name + "_" + frame_str + ".obj.polygon";
	}

	void create_polygons_direct_load(const std::string & obj_path, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, const std::string & base_name, int start_frame, int end_frame, FTE_ENV env) {

		std::map<int, Polygon*> & polygon_map = data[base_name];

		for (int i = start_frame; i <= end_frame; i++) {

			if (polygon_map.find(i) != polygon_map.end()) {
				LOGW("fte: FrameAnimationPolygonCache: create_polygons_direct_load: polygon already cached '%s', frame=%d", base_name.c_str(), i);
				continue;
			}

			Polygon* p = PolygonBuiler::create_textured_model_direct(format_obj_direct_load_filename(obj_path, base_name, i), atlas_loader, texture_name, env);
			polygon_map[i] = p;
		}

	}

	void create_polygons(const std::string & obj_path, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, const std::string & base_name, int start_frame, int end_frame, FTE_ENV env) {

		std::map<int, Polygon*> & polygon_map = data[base_name];

		for (int i = start_frame; i <= end_frame; i++) {

			if (polygon_map.find(i) != polygon_map.end()) {
				LOGW("fte: FrameAnimationPolygonCache: create_polygons: polygon already cached '%s', frame=%d", base_name.c_str(), i);
				continue;
			}

			ObjModelLoader model_loader(format_obj_filename(obj_path, base_name, i), env);

			std::vector<std::string> model_names;
			model_loader.get_names(model_names);
			std::sort(model_names.begin(), model_names.end());

			PolygonCache tmp;

			for (std::vector<std::string>::iterator it = model_names.begin(), end = model_names.end(); it != end; ++it) {
				Polygon* p = PolygonBuiler::create_textured_model(model_loader, atlas_loader, *it, texture_name, true, true, false);
				tmp.add(p);
				DELETE(p);
			}

			polygon_map[i] = tmp.create_composite();
		}

	}

	Polygon* get_polygon(const std::string & name, int frame) {

		Polygon* res = NULL;
		std::map<int, Polygon*> & polygon_map = data[name];
		std::map<int, Polygon*>::iterator it_polygon_map = polygon_map.find(frame);
		if (it_polygon_map == polygon_map.end()) {
//			LOGE("fte: FrameAnimationPolygonCache: get_polygon: no polygon cached for '%s', frame=%d", name.c_str(), frame);
			return NULL;
		}
		return it_polygon_map->second;
	}

public:

	FrameAnimationPolygonCache() {
	}

	~FrameAnimationPolygonCache() {
		release();
	}

	void add_to_cache(const std::string & obj_path, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, const std::string & name, int start_frame, int end_frame, FTE_ENV env) {
		create_polygons(obj_path, atlas_loader, texture_name, name, start_frame, end_frame, env);
	}

	void add_to_cache_polygon_direct_load(const std::string & obj_path, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, const std::string & name, int start_frame, int end_frame, FTE_ENV env) {
		create_polygons_direct_load(obj_path, atlas_loader, texture_name, name, start_frame, end_frame, env);
	}

	void release() {
		for (std::map<std::string, std::map<int, Polygon*> >::iterator it_data = data.begin(), end_data = data.end(); it_data != end_data; ++it_data) {
			std::map<int, Polygon*> & polygon_map = it_data->second;
			for (std::map<int, Polygon*>::iterator it_polygon_map = polygon_map.begin(), end_polygon_map = polygon_map.end(); it_polygon_map != end_polygon_map; ++it_polygon_map) {
				Polygon* p = it_polygon_map->second;
				DELETE(p);
			}
		}
		data.clear();
	}

	inline Polygon* get(const std::string & name, int frame) {
		return get_polygon(name, frame);
	}

}
;

} // namespace fte

#endif /* frame_anim_polygon_cache_H */
