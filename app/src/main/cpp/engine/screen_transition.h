#ifndef screen_transition_H
#define screen_transition_H

#include "command.h"
#include "statable.h"

namespace fte {

const int SCREEN_TRANS_STATE_DEFAULT = STATE_DEFAULT;
const int SCREEN_TRANS_STATE_RUNNING = 1;
const int SCREEN_TRANS_STATE_DONE = 2;

class ScreenTransition: public Statable {
private:
	float width;
	float height;

protected:

	inline float get_width() {
		return width;
	}

	inline float get_height() {
		return height;
	}

	virtual void do_update(float delta, RenderCommand* command, Camera & camera) = 0;
	virtual void do_restart() = 0;

	virtual void do_change_state(int state) {
		switch (state) {
		case SCREEN_TRANS_STATE_DEFAULT:
			break;

		case SCREEN_TRANS_STATE_RUNNING:
			do_restart();
			break;

		case SCREEN_TRANS_STATE_DONE:
			break;
		}
	}

public:

	ScreenTransition() {
	}

	virtual ~ScreenTransition() {
	}

	void resize(int w, int h) {
		this->width = w;
		this->height = h;
	}

	void restart() {
		switch (get_state()) {
		case SCREEN_TRANS_STATE_DEFAULT:
		case SCREEN_TRANS_STATE_DONE:
			change_state(SCREEN_TRANS_STATE_RUNNING);
			break;
		}
	}

	void stop() {
		change_state(SCREEN_TRANS_STATE_DONE);
	}

	void update(float delta, RenderCommand* command, Camera & camera) {
		switch (get_state()) {
		case SCREEN_TRANS_STATE_RUNNING:
		case SCREEN_TRANS_STATE_DONE:
			do_update(delta, command, camera);
			break;
		}
	}
};

} // namespace fte

#endif /* screen_transition_H */
