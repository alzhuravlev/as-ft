#ifndef ring_anim_H
#define ring_anim_H

#include "polygon.h"
#include "polygon_builder.h"
#include "anim.h"
#include "command.h"
#include "texture_atlas_loader.h"

#include <string>

namespace fte {

struct RingAnimConfig {

	int point_count;

	float inner_radius;
	float outer_radius;
	float amplitude;

	float velocity_start;
	float velocity_end;

	float scale_start;
	float scale_end;

	float alpha_start;
	float alpha_end;

	int waves_count;
	float antialiasing;

	bool cycle;

	RingAnimConfig() {
		point_count = 32;

		inner_radius = .8;
		outer_radius = 1.;

		amplitude = 0.1f;

		velocity_start = 1.0f;
		velocity_end = 1.0f;

		scale_start = 1.0f;
		scale_end = 1.0f;

		alpha_start = 1.0f;
		alpha_end = 1.0f;

		waves_count = 1;

		antialiasing = 0.1f;

		cycle = true;
	}
};

class RingAnim {
private:
	Polygon* p;

	VertexRingAnimation v_anim;
	ScaleToAnimation s_anim;
	AlphaAnimation a_anim;

	DecelerateInterpolator interpolator;

	AnimationBundle ab;

	void init(const RingAnimConfig & config, float duration, int offset = 0) {

		ab.set_cycle(config.cycle);

		v_anim.set_duration(duration);
		v_anim.set_start_time(0.0f);
		v_anim.set(config.amplitude, p, config.velocity_start, config.velocity_end, config.waves_count, offset);

		if (F_NEQ(config.scale_start, config.scale_end)) {
			s_anim.set_duration(duration);
			s_anim.set_scale(config.scale_start, config.scale_end);
			s_anim.set_interpolator(&interpolator);
			ab.add(&s_anim);
		}

		if (F_NEQ(config.alpha_start, config.alpha_end)) {
			a_anim.set_duration(duration);
			a_anim.set_alpha(config.alpha_start, config.alpha_end);
			a_anim.set_interpolator(&interpolator);
			ab.add(&a_anim);
		}

		ab.add(&v_anim);
	}

public:

	RingAnim() {
		p = NULL;
	}

	~RingAnim() {
		DELETE(p);
	}

	void init_with_color(const RingAnimConfig & config, float duration, uint32_t color) {
		uint32_t inner_color = color;
		uint32_t mid_color = color;
		uint32_t outer_color = color;

		change_alpha(inner_color, 0.0f);
		change_alpha(outer_color, 0.0f);

		if (F_EQ(config.antialiasing, 0.0f))
			p = PolygonBuiler::create_ring_sprite(config.point_count, config.inner_radius, config.outer_radius, inner_color, mid_color, outer_color);
		else
			p = PolygonBuiler::create_ring_sprite_antialiasing(config.point_count, config.inner_radius, config.outer_radius, inner_color, mid_color, outer_color, config.antialiasing);

		init(config, duration);
	}

	void init_with_model(const RingAnimConfig & config, float duration, const ObjModelLoader & model_loader, std::string model_name) {
		p = PolygonBuiler::create_model(model_loader, model_name, true);
		init(config, duration);
	}

	void init_with_textured_shape(const RingAnimConfig & config, float duration, const TextureAtlasLoader & atlas, const Box2dLoader & shape_loader, std::string texture_name, std::string shape_name, uint32_t color = 0xffffffff) {
		p = PolygonBuiler::create_shaped_textured_sprite(atlas, shape_loader, texture_name, shape_name, color);
		init(config, duration, 1);
	}

	void init_with_texture(const RingAnimConfig & config, float duration, const TextureAtlasLoader & atlas, std::string texture_name, uint32_t color) {
		if (F_EQ(config.antialiasing, 0.0f))
			p = PolygonBuiler::create_textured_ring_sprite(atlas, texture_name, config.point_count, config.inner_radius, config.outer_radius, color);
		else
			p = PolygonBuiler::create_textured_ring_sprite_antialiasing(atlas, texture_name, config.point_count, config.inner_radius, config.outer_radius, config.antialiasing, color);
		init(config, duration);
	}

	void start() {
		ab.start();
	}

	void update(float delta) {
		ab.update(p, delta);
	}

	void update_polygon_scale(float scale) {
		p->set_scale(scale);
	}

	void update_polygon_position(const Vec3 & pos) {
		p->set_translate(pos.x, pos.y, pos.z);
	}

	void update_polygon_rotation(const Vec3 & rotation) {
		p->set_angle_xyz(rotation.x, rotation.y, rotation.z);
	}

	void update_polygon_axis_rotation(const Vec3 & axis, float axis_angle) {
		p->set_axis_rotation(true);
		p->set_axis(axis.x, axis.y, axis.z, axis_angle);
	}

	float is_active() {
		return ab.is_active();
	}

	void render(RenderCommand* command) {
		if (ab.is_active())
			command->add(p);
	}
};

} // namespace fte

#endif /* ring_anim_H */
