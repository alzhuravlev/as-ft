#ifndef renderer_H
#define renderer_H

#include "texture.h"
#include "engine.h"
#include "buffer.h"
#include "command.h"
#include "external.h"
#include "fte_utils.h"
#include "engine.h"
#include "shader_program.h"
#include "shader_program_data.h"

#include "unistd.h"
#include <vector>
#include <map>

#include "fte_gl.h"
#include "fte_utils_gl.h"

#include "math.h"

namespace fte {

const static int MAX_STATIC_DATA_COUNT = 64;

/**
 * Do actual OpenGL render
 */
class Renderer {
private:

	std::map<TextureDescriptor, Texture*> textures;

	std::map<TYPE_STATIC_BATCH_ID, ShaderProgramStaticData*> shader_static_data;
	std::map<int, ShaderProgram*> shader_programs;

	ShaderProgramDynamicData shader_dynamic_data;

	int save_w, save_h;

	int dirty_resize_count;

	GLuint static_data_buffers[2];

	void init_shader_programs(const std::vector<ShaderDescriptor> & descriptors, FTE_ENV env) {
		for (std::vector<ShaderDescriptor>::const_iterator it = descriptors.begin(); it != descriptors.end(); ++it) {
			const ShaderDescriptor & d = *it;
			ShaderProgram* p = new ShaderProgram(d.vs_file_name, d.fs_file_name);
			p->init(env);
			shader_programs[d.id] = p;
		}
	}

	void release_shader_programs() {
		for (std::map<int, ShaderProgram*>::const_iterator it = shader_programs.begin(); it != shader_programs.end(); ++it) {
			ShaderProgram* sp = it->second;
			sp->release();
			delete sp;
		}
		shader_programs.clear();
	}

	void release_static_data() {
		for (std::map<TYPE_STATIC_BATCH_ID, ShaderProgramStaticData*>::const_iterator it = shader_static_data.begin(); it != shader_static_data.end(); ++it) {
			ShaderProgramStaticData* sd = it->second;
			sd->release();
			delete sd;
		}
		shader_static_data.clear();

		glDeleteBuffers(2, static_data_buffers);
		static_data_buffers[0] = 0;
		static_data_buffers[1] = 0;
	}

	void release_textures() {
		for (std::map<TextureDescriptor, Texture*>::const_iterator it = textures.begin(); it != textures.end(); ++it) {
			Texture* texture = it->second;
			texture->release();
			delete texture;
		}
		textures.clear();
	}

	void init_textures(FTE_ENV env) {
		GLint texture_unit = GL_TEXTURE0;

		int w, h;
		render_query_screen_size(env, w, h);

		for (std::map<TextureDescriptor, Texture*>::const_iterator it = textures.begin(); it != textures.end(); ++it) {
			Texture* texture = it->second;
			texture->init(env, texture_unit, w, h);
			texture_unit++;
		}
	}

	GLenum get_texture_unit(const TextureDescriptor & td) {
		std::map<TextureDescriptor, Texture*>::iterator it = textures.find(td);
		if (it != textures.end())
			return it->second->get_texture_unit();
		return 0;
	}

	GLenum get_texture_framebuffer(const TextureDescriptor & td) {
		std::map<TextureDescriptor, Texture*>::iterator it = textures.find(td);
		if (it != textures.end())
			return it->second->get_framebuffer_id();
		return 0;
	}

	void init_renderer(const InitRendererCommand & command, FTE_ENV env) {
		release_textures();
		release_static_data();

		glGenBuffers(2, static_data_buffers);

		std::vector<TextureDescriptor> texture_descriptors;
		command.get_texture_descriptors(texture_descriptors);
		for (std::vector<TextureDescriptor>::const_iterator it = texture_descriptors.begin(); it != texture_descriptors.end(); ++it) {
			TextureDescriptor td = *it;
			Texture* texture = new Texture(td);
			textures[td] = texture;
		}
		init_textures(env);

		std::vector<ShaderDescriptor> shader_descriptors;
		command.get_shader_descriptors(shader_descriptors);
		init_shader_programs(shader_descriptors, env);
	}

	void check_resize(Command & command, FTE_ENV env, bool force) {

		if (dirty_resize_count < 31)
			dirty_resize_count++;

		if (dirty_resize_count <= 30 || force) {

			int w, h;
			render_query_screen_size(env, w, h);

			if (w != save_w || h != save_h) {

				save_w = w;
				save_h = h;

				LOGI("fte: RENDERER: detect screen resize, generate RESIZE command %d x %d", w, h);

				ResizeCommand* resize_command = command.produce_resize_command();
				resize_command->set_wh(w, h);

				glViewport(0, 0, w, h);
				CHECK_GL_ERROR("glViewport");
			}

		}
	}

	void render_data(const RenderBatch & batch, const ShaderProgramData & data, const RenderCommand & command) {
		if (data.is_enable_depth())
			glEnable(GL_DEPTH_TEST);
		else
			glDisable(GL_DEPTH_TEST);

		if (data.get_shader_toon_id()) {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			shader_programs[data.get_shader_toon_id()]->render_data(data, command);
			glCullFace(GL_BACK);
		}

		if (data.is_enable_cull_face()) {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
		} else
			glDisable(GL_CULL_FACE);

		if (batch.render_to_texture_descriptor.to_string() == "") {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		} else {
			glBindFramebuffer(GL_FRAMEBUFFER, get_texture_framebuffer(batch.render_to_texture_descriptor));
		}

		shader_programs[data.get_shader_id()]->render_data(data, command);
	}

	void render(const RenderCommand & command, FTE_ENV env) {

		float cc_r, cc_g, cc_b, cc_a;
		command.get_clear_color(cc_r, cc_g, cc_b, cc_a);
		glClearColor(cc_r, cc_g, cc_b, cc_a);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		CHECK_GL_ERROR("glClear");

		shader_dynamic_data.pre(command);

		if (command.get_static_batch_count()) {

			int static_vert_size = 0;
			int static_ind_size = 0;

			const int _start_idx = 0;
			const int _end_idx = command.get_static_batch_count();

			for (int i = _start_idx; i < _end_idx; i++) {
				const RenderBatch & batch = command.get_batch(i);

				if (batch.static_batch_id && batch.vertex_size) {

					std::map<TYPE_STATIC_BATCH_ID, ShaderProgramStaticData*>::iterator it = shader_static_data.find(batch.static_batch_id);

					if (it == shader_static_data.end()) {
#ifdef DEBUG
						LOGD("static id=%d vertex count = %d", batch.static_batch_id, batch.vertex_size / (batch.num_components * sizeof(float)));
#endif
						ShaderProgramStaticData* data = new ShaderProgramStaticData;
						data->init();
						data->wrap(batch, command, get_texture_unit(batch.texture_descriptor));
						shader_static_data[batch.static_batch_id] = data;

					}

					static_vert_size += batch.vertex_size;
					static_ind_size += batch.index_size;
				}
			}

			if (static_vert_size) {
				glBindBuffer(GL_ARRAY_BUFFER, static_data_buffers[0]);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, static_data_buffers[1]);

				glBufferData(GL_ARRAY_BUFFER, static_vert_size, command.get_vert_ptr(), GL_STATIC_DRAW);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_ind_size, command.get_ind_ptr(), GL_STATIC_DRAW);

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}

		}

		if (command.get_static_batch_nodata_count()) {

			const int _start_idx = command.get_static_batch_count();
			const int _end_idx = _start_idx + command.get_static_batch_nodata_count();

			for (int i = _start_idx; i < _end_idx; i++) {
				const RenderBatch & batch = command.get_batch(i);

				if (batch.static_batch_id && !batch.vertex_size) {

					std::map<TYPE_STATIC_BATCH_ID, ShaderProgramStaticData*>::iterator it = shader_static_data.find(batch.static_batch_id);

					if (it != shader_static_data.end()) {
						ShaderProgramStaticData* data = it->second;
						data->wrap(batch, command, 0);

						glBindBuffer(GL_ARRAY_BUFFER, static_data_buffers[0]);
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, static_data_buffers[1]);

						render_data(batch, *data, command);

						glBindBuffer(GL_ARRAY_BUFFER, 0);
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
					}

				}

			}
		}

		const int _start_idx = command.get_static_batch_count() + command.get_static_batch_nodata_count();
		const int _end_idx = command.get_batch_count();

		for (int i = _start_idx; i < _end_idx; i++) {

			const RenderBatch & batch = command.get_batch(i);

			if (!batch.static_batch_id && batch.vertex_size) {
				shader_dynamic_data.wrap(batch, command, get_texture_unit(batch.texture_descriptor));
				render_data(batch, shader_dynamic_data, command);
			}
		}
		render_flush(env);
	}

	void init_gl(FTE_ENV env) {

		render_init_display(env);

		glEnable(GL_CULL_FACE);
		glDisable(GL_STENCIL_TEST);
		glEnable(GL_DEPTH_TEST);

		glCullFace(GL_BACK);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		CHECK_GL_ERROR("glClearColor");

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		printGLString("Version", GL_VERSION);
		printGLString("Vendor", GL_VENDOR);
		printGLString("Renderer", GL_RENDERER);
		printGLString("Extensions", GL_EXTENSIONS);
		printGLString("GLSL ver.", GL_SHADING_LANGUAGE_VERSION);
		printGLInteger("GL_DEPTH_BITS", GL_DEPTH_BITS);
		printGLInteger("GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS", GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);

		int range[2], precision;

		glGetShaderPrecisionFormat(GL_VERTEX_SHADER, GL_HIGH_FLOAT, range, &precision);
		LOGD("vert: highp: range=(%d, %d), precision=%d", range[0], range[1], precision);
		glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_HIGH_FLOAT, range, &precision);
		LOGD("frag: highp: range=(%d, %d), precision=%d", range[0], range[1], precision);

		glGetShaderPrecisionFormat(GL_VERTEX_SHADER, GL_MEDIUM_FLOAT, range, &precision);
		LOGD("vert: mediump: range=(%d, %d), precision=%d", range[0], range[1], precision);
		glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_MEDIUM_FLOAT, range, &precision);
		LOGD("frag: mediump: range=(%d, %d), precision=%d", range[0], range[1], precision);

		glGetShaderPrecisionFormat(GL_VERTEX_SHADER, GL_LOW_FLOAT, range, &precision);
		LOGD("vert: lowp: range=(%d, %d), precision=%d", range[0], range[1], precision);
		glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_LOW_FLOAT, range, &precision);
		LOGD("frag: lowp: range=(%d, %d), precision=%d", range[0], range[1], precision);

//		printGLInteger("GL_MAX_VERTEX_ATTRIBS", GL_MAX_VERTEX_ATTRIBS);

	}

	void release_gl(FTE_ENV env) {
		render_term_display(env);
	}

public:

	Renderer() {
		dirty_resize_count = 0;

		static_data_buffers[0] = 0;
		static_data_buffers[1] = 0;

		save_w = 0;
		save_h = 0;
	}

	void init(FTE_ENV env) {
		LOGI("fte: RENDERER: init");
		init_gl(env);
		save_w = 0;
		save_h = 0;
		shader_dynamic_data.init();
	}

	void release(FTE_ENV env) {
		release_textures();
		release_gl(env);
		release_static_data();
		shader_dynamic_data.release();
		release_shader_programs();
		LOGI("fte: RENDERER: release");
	}

	void process_resize() {
		dirty_resize_count = 0;
	}

	void process(Command & command, FTE_ENV env) {
		int cmd_type = command.get_cmd_type();
		switch (cmd_type) {
		case COMMAND_RENDER:
			render(*command.consume_render_command(), env);
			check_resize(command, env, false);
			break;
		case COMMAND_INIT_RENDERER:
			init_renderer(*command.consume_init_renderer_command(), env);
			check_resize(command, env, false);
			break;
		case COMMAND_RESIZE:
			check_resize(command, env, true);
			break;
		default:
			LOGW("fte: RENDERER: skip command: %d", cmd_type);
			break;
		}
	}
}
;

} // namespace fte

#endif // renderer_H
