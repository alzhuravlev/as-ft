#ifndef statable_H
#define statable_H

namespace fte {

const int STATE_DEFAULT = 0;

class Statable {
private:
	int state;
	int pending_state;
	bool in_transition;
	float transition_time;
	float state_time;

protected:
	virtual void do_change_state(int state) {
	}

public:

	Statable();

	virtual ~Statable();

	void change_state(int state, bool force = false);
	void change_state_transition(int state, float transition_time);

	void update_state(float delta);

	inline int get_state() const {
		return this->state;
	}

	inline int get_pending_state() const {
		return this->pending_state;
	}

	inline bool is_in_transition() const {
		return this->in_transition;
	}

	inline float get_transition_time() const {
		return this->transition_time;
	}

	inline float get_state_time() const {
		return this->state_time;
	}

	inline void set_state_time(float state_time) {
		this->state_time = state_time;
	}
};

} // namespace fte
#endif /* statable_H */
