#ifndef box2d_utils_H
#define box2d_utils_H

#include "box2d_loader.h"
#include "Box2D/Box2D.h"

namespace fte {

void convert_vec3_to_b2vec2(const std::vector<Vec3> & in, std::vector<b2Vec2> & out);

void attach_fixture_to_body(b2Body* body, b2FixtureDef* fd, Box2dItem* item, float scale_factor_x = 1.0f, float scale_factor_y = 1.0f);
void attach_edge_fixture_to_body(b2Body* body, b2FixtureDef* fd, const std::vector<b2Vec2> & points, float height = 0.0f);

}

#endif /* box2d_utils_H */
