#ifndef render_commandable_H
#define render_commandable_H

#include "texture_descriptor.h"

namespace fte {

/**
 * Abstract class, RenderCommand operate in terms of this class
 */
class RenderCommandable {
protected:

	TextureDescriptor texture_descriptor;

	int num_components;

	int color_component_offset;
	int uv_component_offset;
	int normal_component_offset;
	int nm_uv_component_offset;
	int tangent_component_offset;
	int bitangent_component_offset;

	bool enabled;

public:

	RenderCommandable() {
		num_components = 0.;

		color_component_offset = 0;
		uv_component_offset = 0;
		nm_uv_component_offset = 0;
		normal_component_offset = 0;
		tangent_component_offset = 0;
		bitangent_component_offset = 0;

		enabled = true;
	}

	virtual ~RenderCommandable() {
	}

	RenderCommandable(const RenderCommandable & p) {
		color_component_offset = p.color_component_offset;
		uv_component_offset = p.uv_component_offset;
		nm_uv_component_offset = p.nm_uv_component_offset;
		normal_component_offset = p.normal_component_offset;
		tangent_component_offset = p.tangent_component_offset;
		bitangent_component_offset = p.bitangent_component_offset;
		texture_descriptor = p.texture_descriptor;
		num_components = p.num_components;
		enabled = p.enabled;
	}

	RenderCommandable & operator=(const RenderCommandable & rhs) {
		color_component_offset = rhs.color_component_offset;
		uv_component_offset = rhs.uv_component_offset;
		nm_uv_component_offset = rhs.nm_uv_component_offset;
		normal_component_offset = rhs.normal_component_offset;
		tangent_component_offset = rhs.tangent_component_offset;
		bitangent_component_offset = rhs.bitangent_component_offset;
		texture_descriptor = rhs.texture_descriptor;
		num_components = rhs.num_components;
		enabled = rhs.enabled;
		return *this;
	}

	void set_texture_descriptor(const TextureDescriptor & texture_descriptor) {
		this->texture_descriptor = texture_descriptor;
	}

	const TextureDescriptor & get_texture_descriptor() const {
		return texture_descriptor;
	}

	inline int get_num_components() const {
		return num_components;
	}

	inline int get_color_component_offset() const {
		return color_component_offset;
	}

	inline int get_uv_component_offset() const {
		return uv_component_offset;
	}

	inline int get_nm_uv_component_offset() const {
		return nm_uv_component_offset;
	}

	inline int get_tangent_component_offset() const {
		return tangent_component_offset;
	}

	inline int get_bitangent_component_offset() const {
		return bitangent_component_offset;
	}

	inline int get_normal_component_offset() const {
		return normal_component_offset;
	}

	inline void set_enabled(bool enabled) {
		this->enabled = enabled;
	}

	inline bool is_enabled() const {
		return enabled;
	}

	virtual int get_vert_count() const = 0;
	virtual void write_to_vert_buffer(Buffer* vert_buf, int &size) = 0;
};

/**
 * Multiple instances of this class can be added to RenderBatch.
 * Because it can increment their index values before write to buffer.
 */
class RenderCommandableIncIndexes: public RenderCommandable {
public:

	RenderCommandableIncIndexes() :
			RenderCommandable() {
	}

	virtual ~RenderCommandableIncIndexes() {
	}

	RenderCommandableIncIndexes(const RenderCommandableIncIndexes & p) :
			RenderCommandable(p) {
	}

	RenderCommandableIncIndexes & operator=(const RenderCommandableIncIndexes & rhs) {
		RenderCommandable::operator=(rhs);
		return *this;
	}

	virtual void write_to_index_buffer(Buffer* index_buf, int offset, int &size) = 0;
};

/**
 * Only one instance of this class can be added to RenderBatch.
 * Because instances don't know how to rebuild indexes and only hold indexes in the cache.
 */
class RenderCommandableFixedIndexes: public RenderCommandable {
public:
	virtual ~RenderCommandableFixedIndexes() {
	}

	virtual void write_to_index_buffer(Buffer* index_buf, int &size) = 0;

	virtual bool has_data() = 0;
};

} // namespace fte

#endif /* render_commandable_H */
