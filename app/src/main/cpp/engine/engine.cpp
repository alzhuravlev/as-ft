#include "engine.h"
#include "updater.h"
#include "screen.h"
#include "buffer.h"
#include "command.h"
#include "queue.h"
#include "renderer.h"
#include "external.h"

#include "pthread.h"

namespace fte {

const static int TOUCH_QUEUE_SIZE = 20;
const static int RESIZE_QUEUE_SIZE = 1;
const static int CUSTOM_COMMAND_QUEUE_SIZE = 10;

///////////////////////////////////
// AbstractEngine
///////////////////////////////////

class AbstractEngine {
public:
	virtual ~AbstractEngine() {
	}

	virtual void start() = 0;
	virtual void stop() = 0;
	virtual bool step() {
		return false;
	}
	;
	virtual void send_resize() = 0;
	virtual void send_touch(const TouchEvent &event) = 0;
	virtual void send_custom_command(int command) = 0;
};

///////////////////////////////////
// TwoThreadEngine
///////////////////////////////////

const static int TWO_THR_ENGINE_COMMAND_COUNT = 2;

class TwoThreadEngine: public AbstractEngine {
private:
	Updater updater;
	Renderer renderer;
	pthread_t update_thread;
	pthread_t render_thread;
	FTE_ENV env;
	Queue<ResizeEvent> resize_queue;
	Queue<TouchEvent> touch_queue;
	Queue<Command*> update_queue;
	Queue<Command*> render_queue;
	Command commands[TWO_THR_ENGINE_COMMAND_COUNT];

	volatile bool running;

	static void* update(void* context) {
		((TwoThreadEngine*) context)->do_update();
		pthread_exit(NULL);
		return NULL;
	}

	static void* render(void* context) {
		((TwoThreadEngine*) context)->do_render();
		pthread_exit(NULL);
		return NULL;
	}

	void do_update() {
		LOGI("fte: TWO_THR_ENGINE: update thread started");

		updater.init(env);

		Command* cmd;
		TouchEvent touch_events[TOUCH_QUEUE_SIZE];
		int touch_size = 0;

		while (running) {

			touch_queue.consume_all_no_wait(touch_events, touch_size);
			updater.process_touch(touch_events, touch_size);

			if (update_queue.consume(cmd)) {
				updater.process(*cmd, env);
				render_queue.produce(cmd);
			}
		}

		updater.release(env);

		LOGI("fte: TWO_THR_ENGINE: update thread stopped");
	}

	void do_render() {
		LOGI("fte: TWO_THR_ENGINE: render thread started");

		renderer.init(env);

		Command* cmd;
		ResizeEvent resize_events[RESIZE_QUEUE_SIZE];
		int resize_size = 0;

		while (running) {

			resize_queue.consume_all_no_wait(resize_events, resize_size);
			if (resize_size > 0)
				renderer.process_resize();

			if (render_queue.consume(cmd)) {
				renderer.process(*cmd, env);
				update_queue.produce(cmd);
			}
		}

		renderer.release(env);

		LOGI("fte: TWO_THR_ENGINE: render thread stopped");
	}

public:
	TwoThreadEngine(Screen* screen, FTE_ENV env) :
			update_queue(TWO_THR_ENGINE_COMMAND_COUNT), render_queue(TWO_THR_ENGINE_COMMAND_COUNT), touch_queue(TOUCH_QUEUE_SIZE), resize_queue(RESIZE_QUEUE_SIZE) {
		this->updater.set_pending_screen(screen);
		this->update_thread = 0;
		this->render_thread = 0;
		this->env = env;
		//
		this->running = false;
	}

	~TwoThreadEngine() {
	}

	void start();
	void stop();
	void send_resize();
	void send_touch(const TouchEvent &event);
	void send_custom_command(int command);
};

void TwoThreadEngine::start() {
	if (running || update_thread || render_thread) {
		LOGW("Engine already started");
		return;
	}

	this->running = true;

	this->update_queue.clear();
	this->render_queue.clear();
	this->touch_queue.clear();
	this->resize_queue.clear();

	for (int i = 0; i < TWO_THR_ENGINE_COMMAND_COUNT; i++)
		this->update_queue.produce(&commands[i]);

	int rc = pthread_create(&update_thread, NULL, update, this);
	if (rc) {
		LOGE("fte: TWO_THR_ENGINE: Failed to create updater thread");
		this->running = false;
		return;
	}

	rc = pthread_create(&render_thread, NULL, render, this);
	if (rc) {
		LOGE("fte: TWO_THR_ENGINE: Failed to create renderer thread");
		this->running = false;
		return;
	}
}

void TwoThreadEngine::stop() {
	if (!running) {
		LOGW("fte: TWO_THR_ENGINE: Engine already stopped");
		return;
	}

	running = false;
	update_queue.wake_up();
	render_queue.wake_up();
	pthread_join(update_thread, 0);
	pthread_join(render_thread, 0);
	update_thread = 0;
	render_thread = 0;

	for (int i = 0; i < TWO_THR_ENGINE_COMMAND_COUNT; i++)
		commands[i].release();

	LOGI("fte: TWO_THR_ENGINE: engine complete stopped");
}

void TwoThreadEngine::send_resize() {
	ResizeEvent e;
	resize_queue.produce_no_wait(e);
}

void TwoThreadEngine::send_touch(const TouchEvent &event) {
	touch_queue.produce_no_wait(event);
}

void TwoThreadEngine::send_custom_command(int command) {
}

///////////////////////////////////
// SingleThreadEngine
///////////////////////////////////

class SingleThreadEngine: public AbstractEngine {
private:
	Updater updater;
	Renderer renderer;
	pthread_t thread;
	Queue<TouchEvent> touch_queue;
	Queue<ResizeEvent> resize_queue;
	Queue<int> custom_command_queue;
	FTE_ENV env;

	volatile bool running;

	static void* process(void* context) {
		((SingleThreadEngine*) context)->do_process();
		pthread_exit(NULL);
		return NULL;
	}

	void do_process() {
		LOGI("fte: SINGLE_THR_ENGINE: thread started");

		updater.init(env);
		renderer.init(env);

		Command cmd;

		TouchEvent touch_events[TOUCH_QUEUE_SIZE];
		int touch_size = 0;

		int custom_commands[CUSTOM_COMMAND_QUEUE_SIZE];
		int custom_commands_size = 0;

		ResizeEvent resize_events[RESIZE_QUEUE_SIZE];
		int resize_size = 0;

		while (running) {

			resize_queue.consume_all_no_wait(resize_events, resize_size);
			if (resize_size > 0)
				renderer.process_resize();

			touch_queue.consume_all_no_wait(touch_events, touch_size);
			updater.process_touch(touch_events, touch_size);

			custom_command_queue.consume_all_no_wait(custom_commands, custom_commands_size);
			updater.process_custom_command(custom_commands, custom_commands_size);

			updater.process(cmd, env);
			renderer.process(cmd, env);
		}

		updater.release(env);
		renderer.release(env);
		cmd.release();

		LOGI("fte: SINGLE_THR_ENGINE: thread stopped");
	}

public:
	SingleThreadEngine(Screen* screen, FTE_ENV env) :
			touch_queue(TOUCH_QUEUE_SIZE), resize_queue(RESIZE_QUEUE_SIZE), custom_command_queue(CUSTOM_COMMAND_QUEUE_SIZE) {

		this->updater.set_pending_screen(screen);
		this->thread = 0;
		this->env = env;
		//
		this->running = false;
	}

	~SingleThreadEngine() {
	}

	void start();
	void stop();
	void send_resize();
	void send_touch(const TouchEvent &event);
	void send_custom_command(int command);
}
;

void SingleThreadEngine::start() {
	if (running || thread) {
		LOGW("fte: SINGLE_THR_ENGINE: Engine already started");
		return;
	}

	touch_queue.clear();
	resize_queue.clear();

	running = true;

	pthread_attr_t pta;
	pthread_attr_init(&pta);

//	struct sched_param param;
//	param.sched_priority = 2; //sched_get_priority_max(SCHED_FIFO);

//	pthread_attr_setschedpolicy(&pta, SCHED_FIFO);
//	pthread_attr_setschedparam(&pta, &param);

	int rc = pthread_create(&thread, &pta, process, this);
	if (rc) {
		LOGE("fte: SINGLE_THR_ENGINE: Failed to create thread");
		this->running = false;
		return;
	}
}

void SingleThreadEngine::stop() {
	if (!running) {
		LOGW("fte: SINGLE_THR_ENGINE: Engine already stopped");
		return;
	}

	running = false;
	pthread_join(thread, 0);
	thread = 0;

	touch_queue.clear();
	resize_queue.clear();

	LOGI("fte: SINGLE_THR_ENGINE: engine complete stopped");
}

void SingleThreadEngine::send_resize() {
	ResizeEvent e;
	resize_queue.produce_no_wait(e);
}

void SingleThreadEngine::send_touch(const TouchEvent &event) {
	touch_queue.produce_no_wait(event);
}

void SingleThreadEngine::send_custom_command(int command) {
	custom_command_queue.produce_no_wait(command);
}

///////////////////////////////////
// NoThreadEngine
///////////////////////////////////

class NoThreadEngine: public AbstractEngine {
private:
	Updater updater;
	Renderer renderer;
	Queue<TouchEvent> touch_queue;
	Queue<ResizeEvent> resize_queue;
	FTE_ENV env;

	bool running;
	Command cmd;

public:
	NoThreadEngine(Screen* screen, FTE_ENV env) :
			touch_queue(TOUCH_QUEUE_SIZE), resize_queue(RESIZE_QUEUE_SIZE) {

		this->updater.set_pending_screen(screen);
		this->env = env;
		//
		this->running = false;
	}

	~NoThreadEngine() {
	}

	void start();
	void stop();
	void send_resize();
	void send_touch(const TouchEvent &event);
	void send_custom_command(int command);

	bool step() {

		TouchEvent touch_events[TOUCH_QUEUE_SIZE];
		int touch_size = 0;

		ResizeEvent resize_events[RESIZE_QUEUE_SIZE];
		int resize_size = 0;

		resize_queue.consume_all_no_wait(resize_events, resize_size);
		if (resize_size > 0)
			renderer.process_resize();

		touch_queue.consume_all_no_wait(touch_events, touch_size);
		updater.process_touch(touch_events, touch_size);

		updater.process(cmd, env);
		renderer.process(cmd, env);

		return cmd.get_cmd_type() == COMMAND_RENDER;
	}

};

void NoThreadEngine::start() {

	if (running) {
		LOGW("fte: NO_THR_ENGINE: Engine already started");
		return;
	}

	touch_queue.clear();
	resize_queue.clear();

	updater.init(env);
	renderer.init(env);

	running = true;
}

void NoThreadEngine::stop() {
	if (!running) {
		LOGW("fte: NO_THR_ENGINE: Engine already stopped");
		return;
	}

	updater.release(env);
	renderer.release(env);

	running = false;

	touch_queue.clear();
	resize_queue.clear();

	cmd.release();

	LOGI("fte: NO_THR_ENGINE: engine complete stopped");
}

void NoThreadEngine::send_resize() {
	ResizeEvent e;
	resize_queue.produce_no_wait(e);
}

void NoThreadEngine::send_touch(const TouchEvent &event) {
	touch_queue.produce_no_wait(event);
}

void NoThreadEngine::send_custom_command(int command) {
}

///////////////////////////////////
// Engine's external interface
///////////////////////////////////

FTE_ENGINE fteCreateEngine(FTE_SCREEN screen, FTE_ENV env) {
	LOGI("fte engine create");
	return new SingleThreadEngine((Screen*) screen, env);
//	return new TwoThreadEngine((Screen*) screen, env);
}

FTE_ENGINE fteCreateEngineNoThread(FTE_SCREEN screen, FTE_ENV env) {
	LOGI("fte engine create");
	return new NoThreadEngine((Screen*) screen, env);
}

void fteDestroyEngine(FTE_ENGINE &engine) {
	LOGI("fte engine destroy");
	delete (AbstractEngine*) engine;
	engine = NULL;
}

void fteStart(FTE_ENGINE engine) {
	LOGI("fte engine start");
	((AbstractEngine*) engine)->start();
}

void fteStop(FTE_ENGINE engine) {
	LOGI("fte engine stop");
	((AbstractEngine*) engine)->stop();
}

void fteSendResize(FTE_ENGINE engine) {
	((AbstractEngine*) engine)->send_resize();
}

bool fteStep(FTE_ENGINE engine) {
	return ((AbstractEngine*) engine)->step();
}

void fteSendTouch(FTE_ENGINE engine, const TouchEvent & command) {
	((AbstractEngine*) engine)->send_touch(command);
}

void fteSendCustomCommand(FTE_ENGINE engine, int command) {
	((AbstractEngine*) engine)->send_custom_command(command);
}

} // namespace fte

