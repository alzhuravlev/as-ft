#ifndef texture_atlas_loader_H
#define texture_atlas_loader_H

#include "engine.h"
#include "texture_descriptor.h"
#include "fte_utils.h"
#include "command.h"
#include "sprite.h"

#include "png.h"

#include <algorithm>
#include <vector>
#include <string>
#include <streambuf>
#include <istream>
#include "regex.h"

namespace fte {

struct Page;

struct PageItem {
	int x, y, w, h;
	int index;
	std::string name;
	Page* page;
};

struct Page {
	int width;
	int height;
	bool inverse_v;
	std::string texture_file_name;
	std::vector<PageItem*> items;
};

bool page_item_comparator(PageItem* i1, PageItem* i2);

const bool TextureAtlas_VERBOSE = false;

/**
 * Load files created with LibGdx Packer tool
 */
class TextureAtlasLoader {
private:

	std::string file_name;
	std::vector<Page*> pages;

	bool compile_regexp(const char* pattern, regex_t &regexp) {
		int reti;
		reti = regcomp(&regexp, pattern, REG_EXTENDED);
		if (reti) {
			char message[256];
			regerror(reti, &regexp, message, sizeof(message));
			LOGE("fte could not compile regexp: %s message: %s", pattern, message);
			return false;
		}
		return true;
	}

	void load(FTE_ENV env) {
		regex_t r_tex_file_name;
		regex_t r_tex_prop;
		regex_t r_spr_name;
		regex_t r_spr_prop;

		const char* p_tex_png = "^([^[:space:]]+\\.(png|pkm))";
		const char* p_tex_prop = "^([^[:space:]]+)\\:[[:space:]]*(.+)";
		const char* p_spr_name = "^([^[:space:]]+)";
		const char* p_spr_prop = "^[[:space:]]+([^[:space:]]+)\\:[[:space:]]*(.+)";

		if (!compile_regexp(p_tex_png, r_tex_file_name))
			return;

		if (!compile_regexp(p_tex_prop, r_tex_prop))
			return;

		if (!compile_regexp(p_spr_name, r_spr_name))
			return;

		if (!compile_regexp(p_spr_prop, r_spr_prop))
			return;

		std::vector<std::string> lines;
		read_resource_lines(file_name, env, lines);

		size_t nmatches = 4;
		regmatch_t matches[nmatches];

		Page* current_page = NULL;
		PageItem* current_page_item = NULL;

		for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::string line = *it;

			if (line.length() == 0) {

				current_page = new Page;
				pages.push_back(current_page);

			} else {

				if (regexec(&r_tex_file_name, line.c_str(), nmatches, matches, REG_ICASE) == 0) {

					if (current_page == NULL) {
						LOGE("fte invalid atlas file format (error 1): %s on line: %s", file_name.c_str(), line.c_str());
						break;
					}

					std::string tex_file_name = line.substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
					replace_file_name(file_name, tex_file_name, current_page->texture_file_name);

					if (ends_with(tex_file_name, ".png")) {
						read_png_dim(current_page->texture_file_name, current_page->width, current_page->height, env);
						current_page->inverse_v = true;
					} else if (ends_with(tex_file_name, ".pkm")) {
						read_pkm_dim(current_page->texture_file_name, current_page->width, current_page->height, env);
						current_page->inverse_v = false;
					}

					if (TextureAtlas_VERBOSE)
						LOGD("fte atlas parser: texture found: '%s' %dx%d", current_page->texture_file_name.c_str(), current_page->width, current_page->height);

				} else if (regexec(&r_tex_prop, line.c_str(), nmatches, matches, REG_ICASE) == 0) {

					std::string prop_name = line.substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);

					std::string prop_val = line.substr(matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);

					if (TextureAtlas_VERBOSE)
						LOGD("fte atlas parser: tex prop found: %s.%s = '%s'", current_page->texture_file_name.c_str(), prop_name.c_str(), prop_val.c_str());

				} else if (regexec(&r_spr_name, line.c_str(), nmatches, matches, REG_ICASE) == 0) {

					if (current_page == NULL) {
						LOGE("fte invalid atlas file format (error 2): %s on line: %s", file_name.c_str(), line.c_str());
						break;
					}

					current_page_item = new PageItem;
					current_page_item->name = line.substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
					current_page_item->page = current_page;

					current_page->items.push_back(current_page_item);

					if (TextureAtlas_VERBOSE)
						LOGD("fte atlas parser: sprite found: '%s'", current_page_item->name.c_str());

				} else if (regexec(&r_spr_prop, line.c_str(), nmatches, matches, REG_ICASE) == 0) {

					if (current_page_item == NULL) {
						LOGE("fte invalid atlas file format (error 3): %s on line: %s", file_name.c_str(), line.c_str());
						break;
					}

					std::string prop_name = line.substr(matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);

					std::string prop_val = line.substr(matches[2].rm_so, matches[2].rm_eo - matches[2].rm_so);

					std::vector<std::string> tmp;
					split(prop_val, ',', tmp);

					if (prop_name.compare("xy") == 0) {
						current_page_item->x = atoi(tmp[0].c_str());
						current_page_item->y = atoi(tmp[1].c_str());
					} else if (prop_name.compare("size") == 0) {
						current_page_item->w = atoi(tmp[0].c_str());
						current_page_item->h = atoi(tmp[1].c_str());
					} else if (prop_name.compare("index") == 0) {
						current_page_item->index = atoi(tmp[0].c_str());
					}

					if (TextureAtlas_VERBOSE)
						LOGD("fte atlas parser: sprite prop found: %s.%s = '%s'", current_page_item->name.c_str(), prop_name.c_str(), prop_val.c_str());
				}
			}
		}

		regfree(&r_tex_file_name);
		regfree(&r_tex_prop);
		regfree(&r_spr_name);
		regfree(&r_spr_prop);

	}

public:

	TextureAtlasLoader(const std::string & file_name, FTE_ENV env) {
		this->file_name = file_name;
		load(env);
	}

	~TextureAtlasLoader() {
		while (!pages.empty()) {
			Page* page = pages.back();
			while (!page->items.empty()) {
				delete page->items.back();
				page->items.pop_back();
			}
			delete page;
			pages.pop_back();
		}
	}

	void find_items_by_name(std::vector<PageItem*> &result, const std::string & name) const {
		for (std::vector<Page*>::const_iterator it = pages.begin(); it != pages.end(); it++) {
			Page* page = *it;
			for (std::vector<PageItem*>::iterator it = page->items.begin(); it != page->items.end(); it++) {
				PageItem* item = *it;
				if (strcmp(item->name.c_str(), name.c_str()) == 0) {
					result.push_back(item);
				}
			}
		}

		std::sort(result.begin(), result.end(), page_item_comparator);

		if (result.size() == 0)
			LOGE("fte: TextureAtals: sprite '%s' not found in atlas!", name.c_str());
	}

	void add_texture_descriptors(InitRendererCommand* command) const {
		for (std::vector<Page*>::const_iterator it = pages.begin(); it != pages.end(); it++) {
			Page* page = *it;
			command->add(TextureDescriptor(page->texture_file_name));

			if (TextureAtlas_VERBOSE)
				LOGD("fte: TextureAtlas: texture descriptor added to command: %s", page->texture_file_name.c_str());
		}
	}

	const std::string & get_file_name() const {
		return file_name;
	}
};

} // namespace fte

#endif /* texture_atlas_loader_H */
