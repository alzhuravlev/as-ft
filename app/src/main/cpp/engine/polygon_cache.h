#ifndef polygon_cache_H
#define polygon_cache_H

#include "polygon.h"
#include "buffer.h"
#include "render_commandable.h"

namespace fte {

/**
 * Useful when we need to draw huge static data.
 * We pass instance of this class to Renderer only once. Afterward data will be stored in VBO.
 * Therefore we don't need to pass static data every frame. We could apply view-projection matrix to transform this huge body as whole.
 *
 * Another usage of this class is to build composite bodies. See create_composite.
 * We could use Polygon created via create_composite as usual (i.e. make any transformation: translate, scale, rotate)
 */
class PolygonCache: public RenderCommandableFixedIndexes {
private:
	Buffer vert_buffer;
	Buffer ind_buffer;
	int vertex_counter;

	bool first_polygon_added;

public:

	PolygonCache() :
			vert_buffer(1024 * 256), ind_buffer(1024 * 256) {
		vertex_counter = 0;
		first_polygon_added = false;
	}

	bool has_data() {
		return first_polygon_added;
	}

	void add(const std::vector<Polygon*> & polygons) {
		for (std::vector<Polygon*>::const_iterator it = polygons.begin(), end = polygons.end(); it != end; ++it)
			add(*it);
	}

	/**
	 * All polygons must have same num_components and get_texture_descriptor. Otherwise the result will be unpredicted.
	 *
	 * Add all polygon's verts and indexes to cache. This cache does not hold references to added polygon.
	 * So it can be destroyed thereafter.
	 */
	void add(Polygon* polygon) {

		TextureDescriptor td = polygon->get_texture_descriptor();
		if (!first_polygon_added) {

			texture_descriptor = td;

			num_components = polygon->get_num_components();
			color_component_offset = polygon->get_color_component_offset();
			uv_component_offset = polygon->get_uv_component_offset();
			normal_component_offset = polygon->get_normal_component_offset();
			nm_uv_component_offset = polygon->get_nm_uv_component_offset();
			tangent_component_offset = polygon->get_tangent_component_offset();
			bitangent_component_offset = polygon->get_bitangent_component_offset();

			first_polygon_added = true;

		} else {

#ifdef DEBUG
			if (num_components != polygon->get_num_components()) {
				LOGE("fte: PolygonCache: add: trying to add Polygon with num_components = %d. expected %d", polygon->get_num_components(), num_components);
				return;
			}

			if (color_component_offset ^ polygon->get_color_component_offset()) {
				LOGE("fte: PolygonCache: add: color_component_offset must be same. actual %d. expected %d", polygon->get_color_component_offset(), color_component_offset);
				return;
			}

			if (uv_component_offset ^ polygon->get_uv_component_offset()) {
				LOGE("fte: PolygonCache: add: uv_component_offset must be same. actual %d. expected %d", polygon->get_uv_component_offset(), uv_component_offset);
				return;
			}

			if (normal_component_offset ^ polygon->get_normal_component_offset()) {
				LOGE("fte: PolygonCache: add: normal_component_offset must be same. actual %d. expected %d", polygon->get_normal_component_offset(), normal_component_offset);
				return;
			}

			if (nm_uv_component_offset ^ polygon->get_nm_uv_component_offset()) {
				LOGE("fte: PolygonCache: add: nm_uv_component_offset must be same. actual %d. expected %d", polygon->get_nm_uv_component_offset(), nm_uv_component_offset);
				return;
			}

			if (tangent_component_offset ^ polygon->get_tangent_component_offset()) {
				LOGE("fte: PolygonCache: add: tangent_component_offset must be same. actual %d. expected %d", polygon->get_tangent_component_offset(), tangent_component_offset);
				return;
			}

			if (bitangent_component_offset ^ polygon->get_bitangent_component_offset()) {
				LOGE("fte: PolygonCache: add: bitangent_component_offset must be same. actual %d. expected %d", polygon->get_bitangent_component_offset(), bitangent_component_offset);
				return;
			}

			if (td != polygon->get_texture_descriptor()) {
				std::string actual = polygon->get_texture_descriptor().to_string();
				std::string expected = td.to_string();
				LOGE("fte: PolygonCache: add: trying to add Polygon with texture_descriptor = '%s'. expected '%s'", actual.c_str(), expected.c_str());
				return;
			}
#endif
		}

		int size;
		polygon->write_to_vert_buffer(&vert_buffer, size);
		polygon->write_to_index_buffer(&ind_buffer, vertex_counter, size);

		vertex_counter += polygon->get_vert_count();

	}

	Polygon* create_composite() {
		if (!has_data()) {
			LOGW("fte: PolygonCache: unable to create_composite. empty cache");
			return NULL;
		}

		Polygon* pc = new Polygon(color_component_offset, uv_component_offset, normal_component_offset, false, nm_uv_component_offset);
		pc->assign_data(vert_buffer.get_data(), vertex_counter, ind_buffer.get_data(), ind_buffer.get_size() / sizeof(short));
		pc->set_texture_descriptor(texture_descriptor);

		return pc;
	}

	void reset() {
		vert_buffer.reset();
		ind_buffer.reset();

		vertex_counter = 0;

		num_components = 0;

		color_component_offset = 0;
		uv_component_offset = 0;
		normal_component_offset = 0;
		nm_uv_component_offset = 0;
		tangent_component_offset = 0;
		bitangent_component_offset = 0;

		first_polygon_added = false;
	}

	int get_vert_count() const {
		return vertex_counter;
	}

	void write_to_vert_buffer(Buffer* vert_buf, int &size) {
		size = this->vert_buffer.get_size();
		vert_buf->write(this->vert_buffer.get_data(), size);
	}

	void write_to_index_buffer(Buffer* index_buf, int &size) {
		size = this->ind_buffer.get_size();
		index_buf->write(this->ind_buffer.get_data(), size);
	}
};

} // namespace fte

#endif /* polygon_cache_H */
