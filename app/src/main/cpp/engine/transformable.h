#ifndef transformable_H
#define transformable_H

#include "fte_math.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace fte {

class Transformable {
private:
	glm::mat4 model_matrix;

protected:
	float origin_x;
	float origin_y;
	float origin_z;

	float scale_x;
	float scale_y;
	float scale_z;

	float x;
	float y;
	float z;

	float angle_x;
	float angle_y;
	float angle_z;

	float cos_x;
	float cos_y;
	float cos_z;

	float sin_x;
	float sin_y;
	float sin_z;

	float qx, qy, qz, qw;

	float axis_angle;
	float axis_x;
	float axis_y;
	float axis_z;

	uint32_t color;
	float alpha;

	bool dirty;
	bool dirty_color;

	bool euler_rotation;
	bool axis_rotation;

public:
	Transformable() {
		origin_x = 0.0f;
		origin_y = 0.0f;
		origin_z = 0.0f;

		angle_x = 0.0f;
		angle_y = 0.0f;
		angle_z = 0.0f;

		cos_x = 1.0f;
		cos_y = 1.0f;
		cos_z = 1.0f;

		sin_x = 0.0f;
		sin_y = 0.0f;
		sin_z = 0.0f;

		axis_angle = 0.0f;
		axis_x = 0.0f;
		axis_y = 0.0f;
		axis_z = 1.0f;

		qw = 1.0f;
		qx = 0.0f;
		qy = 0.0f;
		qz = 0.0f;

		scale_x = 1.0f;
		scale_y = 1.0f;
		scale_z = 1.0f;

		x = 0.0f;
		y = 0.0f;
		z = 0.0f;

		color = 0xffffffff;
		alpha = 1.0f;

		dirty = false;
		dirty_color = false;

		euler_rotation = true;
		axis_rotation = false;
	}

	Transformable(const Transformable & p) {
		apply(p);
	}

	Transformable & operator=(const Transformable & rhs) {
		apply(rhs);
		return *this;
	}

// origin

	/**
	 * sets origins in local coordinate system
	 */
	inline void set_origin_xyz(float origin_x, float origin_y, float origin_z) {
		this->origin_x = origin_x;
		this->origin_y = origin_y;
		this->origin_z = origin_z;
		dirty = true;
	}

	inline void set_origin_xy(float origin_x, float origin_y) {
		this->origin_x = origin_x;
		this->origin_y = origin_y;
		dirty = true;
	}

	inline void set_origin_x(float origin_x) {
		this->origin_x = origin_x;
		dirty = true;
	}

	inline void set_origin_y(float origin_y) {
		this->origin_y = origin_y;
		dirty = true;
	}

	inline void set_origin_z(float origin_z) {
		this->origin_z = origin_z;
		dirty = true;
	}

	inline void get_origin(float & origin_x, float & origin_y, float & origin_z) const {
		origin_x = this->origin_x;
		origin_y = this->origin_y;
		origin_z = this->origin_z;
	}

	// euler angles

	inline float get_angle_x() const {
		return angle_x;
	}

	inline float get_angle_y() const {
		return angle_y;
	}

	inline float get_angle_z() const {
		return angle_z;
	}

	inline void set_angle(float angle) {
		set_angle_x(angle);
		set_angle_y(angle);
		set_angle_z(angle);
	}

	inline void set_angle_xyz(float angle_x, float angle_y, float angle_z) {
		set_angle_x(angle_x);
		set_angle_y(angle_y);
		set_angle_z(angle_z);
	}

	inline void set_angle_x(float angle_x) {
		if (!euler_rotation)
			return;
		if (F_NEQ(this->angle_x, angle_x)) {
			this->dirty = true;
			this->angle_x = angle_x;
			this->cos_x = cos_fast(angle_x);
			this->sin_x = sin_fast(angle_x);
		}
	}

	inline void set_angle_y(float angle_y) {
		if (!euler_rotation)
			return;
		if (F_NEQ(this->angle_y, angle_y)) {
			this->dirty = true;
			this->angle_y = angle_y;
			this->cos_y = cos_fast(angle_y);
			this->sin_y = sin_fast(angle_y);
		}
	}

	inline void set_angle_z(float angle_z) {
		if (!euler_rotation)
			return;
		if (F_NEQ(this->angle_z, angle_z)) {
			this->dirty = true;
			this->angle_z = angle_z;
			this->cos_z = cos_fast(angle_z);
			this->sin_z = sin_fast(angle_z);
		}
	}

	// rotate around axis

	inline void set_axis(float axis_x, float axis_y, float axis_z, float axis_angle) {
		bool local_dirty = !F_EQ(this->axis_x, axis_x) || !F_EQ(this->axis_y, axis_y) || !F_EQ(this->axis_z, axis_z) || !F_EQ(this->axis_angle, axis_angle);

		dirty = local_dirty || dirty;

		if (local_dirty) {
			this->axis_x = axis_x;
			this->axis_y = axis_y;
			this->axis_z = axis_z;
			this->axis_angle = axis_angle;

			axis_angle *= 0.5f;
			const float Sin = sin_fast(axis_angle);
			const float Cos = cos_fast(axis_angle);
			const float Two = 2.0f;

			qw = Two * Cos;
			qx = axis_x * Sin;
			qy = axis_y * Sin;
			qz = axis_z * Sin;
		}
	}

	inline float get_axis_angle() const {
		return this->axis_angle;
	}

	// scale

	inline float get_scale_x() const {
		return this->scale_x;
	}

	inline float get_scale_y() const {
		return this->scale_y;
	}

	inline float get_scale_z() const {
		return this->scale_z;
	}

	inline void set_scale_xyz(float scale_x, float scale_y, float scale_z) {
		dirty = dirty || !F_EQ(this->scale_x, scale_x) || !F_EQ(this->scale_y, scale_y) || !F_EQ(this->scale_z, scale_z);
		this->scale_x = scale_x;
		this->scale_y = scale_y;
		this->scale_z = scale_z;
	}

	inline void set_scale_xy(float scale_x, float scale_y) {
		dirty = dirty || !F_EQ(this->scale_x, scale_x) || !F_EQ(this->scale_y, scale_y);
		this->scale_x = scale_x;
		this->scale_y = scale_y;
	}

	inline void set_scale_x(float scale_x) {
		dirty = dirty || !F_EQ(this->scale_x, scale_x);
		this->scale_x = scale_x;
	}

	inline void set_scale_y(float scale_y) {
		dirty = dirty || !F_EQ(this->scale_y, scale_y);
		this->scale_y = scale_y;
	}

	inline void set_scale_z(float scale_z) {
		dirty = dirty || !F_EQ(this->scale_z, scale_z);
		this->scale_z = scale_z;
	}

	inline void set_scale(float scale) {
		dirty = dirty || !F_EQ(this->scale_x, scale) || !F_EQ(this->scale_y, scale) || !F_EQ(this->scale_z, scale);
		this->scale_x = scale;
		this->scale_y = scale;
		this->scale_z = scale;
	}

	// translate

	inline float get_translate_x() const {
		return x;
	}

	inline float get_translate_y() const {
		return y;
	}

	inline float get_translate_z() const {
		return z;
	}

	inline void set_translate(float x, float y, float z) {
		dirty = dirty || !F_EQ(this->x, x) || !F_EQ(this->y, y) || !F_EQ(this->z, z);
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline void set_translate_xy(float x, float y) {
		dirty = dirty || !F_EQ(this->x, x) || !F_EQ(this->y, y);
		this->x = x;
		this->y = y;
	}

	inline void set_translate_xz(float x, float z) {
		dirty = dirty || !F_EQ(this->x, x) || !F_EQ(this->z, z);
		this->x = x;
		this->z = z;
	}

	inline void set_translate_x(float x) {
		dirty = dirty || !F_EQ(this->x, x);
		this->x = x;
	}

	inline void set_translate_y(float y) {
		dirty = dirty || !F_EQ(this->y, y);
		this->y = y;
	}

	inline void set_translate_z(float z) {
		dirty = dirty || !F_EQ(this->z, z);
		this->z = z;
	}

	// color

	inline void set_color(uint32_t color) {
		dirty_color = dirty_color || this->color != color;
		this->color = color;
	}

	inline uint32_t get_color() const {
		return color;
	}

	inline float get_alpha() const {
		return alpha;
	}

	inline void set_alpha(float alpha) {
		dirty_color = dirty_color || !F_EQ(this->alpha, alpha);
		this->alpha = alpha;
	}

	//

	inline void set_euler_rotation(bool euler_rotation) {
		this->euler_rotation = euler_rotation;
	}

	inline bool is_euler_rotation() const {
		return this->euler_rotation;
	}

	inline void set_axis_rotation(bool axis_rotation) {
		this->axis_rotation = axis_rotation;
	}

	inline bool is_axis_rotation() const {
		return this->axis_rotation;
	}

	void apply(const Transformable & p) {
		origin_x = p.origin_x;
		origin_y = p.origin_y;
		origin_z = p.origin_z;

		angle_x = p.angle_x;
		angle_y = p.angle_y;
		angle_z = p.angle_z;

		cos_x = p.cos_x;
		cos_y = p.cos_y;
		cos_z = p.cos_z;

		sin_x = p.sin_x;
		sin_y = p.sin_y;
		sin_z = p.sin_z;

		axis_angle = p.axis_angle;
		axis_x = p.axis_x;
		axis_y = p.axis_y;
		axis_z = p.axis_z;

		qw = p.qw;
		qx = p.qx;
		qy = p.qy;
		qz = p.qz;

		scale_x = p.scale_x;
		scale_y = p.scale_y;
		scale_z = p.scale_z;

		x = p.x;
		y = p.y;
		z = p.z;

		color = p.color;
		alpha = p.alpha;

		dirty = p.dirty;
		dirty_color = p.dirty_color;

		euler_rotation = p.euler_rotation;
		axis_rotation = p.axis_rotation;
	}

	bool is_dirty() const {
		return dirty;
	}

	void update_model_matrix() {
		if (dirty) {
			dirty = false;
			model_matrix = IDENTITY;
			model_matrix = glm::translate(model_matrix, glm::vec3(x, y, z));
			model_matrix = glm::rotate(model_matrix, -angle_y, glm::vec3(0.0f, 1.0f, 0.0f));
			model_matrix = glm::rotate(model_matrix, angle_z, glm::vec3(0.0f, 0.0f, 1.0f));
			model_matrix = glm::rotate(model_matrix, -angle_x, glm::vec3(1.0f, 0.0f, 0.0f));
			model_matrix = glm::scale(model_matrix, glm::vec3(scale_x, scale_y, scale_z));
			model_matrix = glm::translate(model_matrix, glm::vec3(-origin_x, -origin_y, -origin_z));
		}
	}

	const float* get_model_matrix() const {
		return glm::value_ptr(model_matrix);
	}
};

} // namespace fte

#endif /* transformable_H */
