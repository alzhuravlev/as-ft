#include "map_utils.h"

namespace fte {

void map_points_to_angular_points(const std::vector<Vec3> & map_points, std::vector<Vec3> & angular_points, float scale_factor_x, float scale_factor_y) {
	map_points_to_angular_points(map_points.begin(), map_points.end(), angular_points, scale_factor_x, scale_factor_y);
}

void map_points_to_angular_points(const std::vector<Vec3>::const_iterator map_points_begin, const std::vector<Vec3>::const_iterator map_points_end, std::vector<Vec3> & angular_points, float scale_factor_x, float scale_factor_y) {
	Vec3 tmp;
	for (std::vector<Vec3>::const_iterator it = map_points_begin, end = map_points_end; it != end; ++it) {
		const Vec3 & map_point = *it;
		map_point_to_angular_point(map_point, tmp, scale_factor_x, scale_factor_y);
		angular_points.push_back(tmp);
	}
}

void angular_points_to_world_points(const std::vector<Vec3> & angular_points, std::vector<Vec3> & world_points, RADIUS_FUNC radius_func, bool loop) {
	Vec3 world_point;


	for (std::vector<Vec3>::const_iterator it = angular_points.begin(), end = angular_points.end(); it != end; ++it) {
		const Vec3 & angular_point = *it;
		angular_point_to_world_point(angular_point, world_point, radius_func);
		world_points.push_back(world_point);
	}

	if (loop) {
		const Vec3 first = world_points.front();
		const Vec3 last = world_points.back();
		world_points.insert(world_points.begin(), last);
		world_points.push_back(first);
	}
}

} // namespace fte
