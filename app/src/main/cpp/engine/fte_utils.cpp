#include "fte_utils.h"
#include "polygon.h"

#include <sstream>
#include <algorithm>

namespace fte {

#ifdef DEBUG
int DEBUG_APPLY_TRANSFORMATIONS_COUNTER = 0;
int DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER = 0;
#endif

void unpack_color_f(const uint32_t & color, float & r, float & g, float & b, float & a) {
	a = ((color & 0xff000000) >> 24) / 255.0f;
	b = ((color & 0x00ff0000) >> 16) / 255.0f;
	g = ((color & 0x0000ff00) >> 8) / 255.0f;
	r = (color & 0x000000ff) / 255.0f;
}

void pack_color_f(uint32_t & color, const float & r, const float & g, const float & b, const float & a) {
	color = 0x00000000;
	color |= (uint32_t) (255.0f * a) << 24;
	color |= (uint32_t) (255.0f * b) << 16;
	color |= (uint32_t) (255.0f * g) << 8;
	color |= (uint32_t) (255.0f * r);
}

void pack_color_c(uint32_t & color, const uint8_t & r, const uint8_t & g, const uint8_t & b, const uint8_t & a) {
	color = 0x00000000;
	color |= (uint32_t) a << 24;
	color |= (uint32_t) b << 16;
	color |= (uint32_t) g << 8;
	color |= (uint32_t) r;
}

void mix_color_c(const uint32_t & color1, const uint32_t & color2, float a, uint32_t & res) {
	uint8_t* c1 = (uint8_t*) &color1;
	uint8_t* c2 = (uint8_t*) &color2;
	uint8_t* r = (uint8_t*) &res;
	r[0] = (uint8_t) (c1[0] + (c2[0] - c1[0]) * a);
	r[1] = (uint8_t) (c1[1] + (c2[1] - c1[1]) * a);
	r[2] = (uint8_t) (c1[2] + (c2[2] - c1[2]) * a);
	r[3] = (uint8_t) (c1[3] + (c2[3] - c1[3]) * a);
}

void add_color_c(const uint32_t & color1, const uint32_t & color2, uint32_t & res) {
	uint8_t* c1 = (uint8_t*) &color1;
	uint8_t* c2 = (uint8_t*) &color2;
	uint8_t* r = (uint8_t*) &res;
	r[0] = c1[0] + c2[0];
	r[1] = c1[1] + c2[1];
	r[2] = c1[2] + c2[2];
	r[3] = c1[3] + c2[3];

	LOGD("col = %x", res);
}

float get_time_diff(fte_timespec t1, fte_timespec t2) {
	return t2.tv_sec - t1.tv_sec + (t2.tv_nsec - t1.tv_nsec) / 1.e9;
}

/**
 * String utils
 */

void split(const std::string &s, char delim, std::vector<std::string> &elems) {

	if (s.length() == 0)
		return;

	membuf mb((char*) s.c_str(), strlen(s.c_str()));
	std::istream istr(&mb);
	std::string item;

	while (std::getline(istr, item, delim)) {
		elems.push_back(item);
	}
}

void join(const std::vector<std::string> &elems, char delim, std::string &s) {
	std::vector<std::string>::const_iterator begin = elems.begin();
	std::vector<std::string>::const_iterator end = elems.end();
	for (std::vector<std::string>::const_iterator it = begin; it != end; ++it) {
		if (it != begin)
			s.append(&delim, 1);
		s.append(*it);
	}
}

void replace_file_name(const std::string & path, const std::string & new_file_name, std::string & new_path_to_file) {
	std::vector<std::string> v;

	split(path, '/', v);

	if (v.size() > 1) {
		v[v.size() - 1] = new_file_name;
		new_path_to_file.clear();
		join(v, '/', new_path_to_file);
	} else {
		new_path_to_file = new_file_name;
	}
}

bool starts_with(const std::string &s, const std::string &prefix) {
	if (s.size() < prefix.size())
		return false;
	return s.compare(0, prefix.size(), prefix) == 0;
}

bool ends_with(const std::string &s, const std::string &suffix) {
	if (s.size() < suffix.size())
		return false;
	return s.compare(s.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void remove_chars(std::string &s, const char* chars_to_remove) {
	std::size_t pos = 0;
	while ((pos = s.find_first_of(chars_to_remove, pos)) != std::string::npos) {
		s.erase(pos, 1);
	}
}

void remove_chars_w(std::wstring &s, const wchar_t* chars_to_remove) {
	std::size_t pos = 0;
	while ((pos = s.find_first_of(chars_to_remove, pos)) != std::string::npos) {
		s.erase(pos, 1);
	}
}

void convert_string_to_wstring(const std::string & in, std::wstring & out) {
	std::copy(in.begin(), in.end(), std::back_inserter(out));
}

void convert_wstring_to_string(const std::wstring & in, std::string & out) {
	std::copy(in.begin(), in.end(), std::back_inserter(out));
//	for (std::wstring::const_iterator it = in.begin(); it != in.end(); ++it) {
//		char c = (char) *it;
//		out.append(&c, 1);
//	}
}

bool read_external_lines(const std::string & file_name, FTE_ENV env, std::vector<std::string> & lines) {

	void* buf;
	long size;

	if (!load_external_file(env, file_name, buf, size)) {
		LOGE("fte: failed to read external file '%s'", file_name.c_str());
		return false;
	}

	membuf mb((char*) buf, size);

	std::istream istr(&mb);
	std::string line;

	while (getline(istr, line)) {
		remove_chars(line, "\n\r");
		lines.push_back(line);
	}

	free(buf);

	return true;
}

bool read_resource_lines(const std::string & file_name, FTE_ENV env, std::vector<std::string> & lines) {

	void* buf;
	long size;

	if (!load_resource_file(env, file_name, buf, size)) {
		LOGE("fte: failed to read resource file '%s'", file_name.c_str());
		return false;
	}

	membuf mb((char*) buf, size);

	std::istream istr(&mb);
	std::string line;

	while (getline(istr, line)) {
		remove_chars(line, "\n\r");
		lines.push_back(line);
	}

	free(buf);

	return true;
}

/**
 * Only UTF-16 encoded files are supported!
 */
bool read_resource_lines_w(const std::string & file_name, FTE_ENV env, std::vector<std::wstring> & lines) {

	void* buf;
	long size;

	if (!load_resource_file(env, file_name, buf, size)) {
		LOGE("fte: failed to read resource file '%s'", file_name.c_str());
		return false;
	}

	// skip 2 bytes (as we deal with UTF-16)
	char* start = ((char*) buf) + 2;
	char* end = ((char*) buf) + size - 1;

	std::wstring line;

	char c[2];

	while (start <= end) {

		// because android and ios are little-endian OS
		c[1] = *start;
		start++;
		c[0] = *start;
		start++;

		unsigned short us = *((unsigned short*) &c);
		wchar_t wc = us;

		if (wc != '\n' && wc != '\r') {
			line.append(&wc, 1);
		}

		if (wc == '\n' || wc == '\r' || start > end) {
			if (line.size() > 0) {
				lines.push_back(line);
				line.clear();
			}
		}
	}

	free(buf);

	return true;
}

bool read_resource_to_string(const std::string & file_name, FTE_ENV env, std::string & s) {

	void* buf;
	long size;

	if (!load_resource_file(env, file_name, buf, size)) {
		LOGE("fte: failed to read resource file '%s'", file_name.c_str());
		return false;
	}

	s.assign((char*) buf, size);

	free(buf);

	return true;
}

bool read_internal_to_string(const std::string & file_name, FTE_ENV env, std::string & s) {

	void* buf;
	long size;

	if (!load_internal_file(env, file_name, buf, size)) {
		LOGE("fte: failed to read internal file '%s'", file_name.c_str());
		return false;
	}

	s.assign((char*) buf, size);

	free(buf);

	return true;
}

std::string to_stringf(float f) {
	std::ostringstream o;
	if (!(o << f))
		return "";
	return o.str();
}

std::string to_string(int i, bool force_plus) {
	std::string str;
	std::ostringstream temp;
	if (force_plus && i > 0)
		temp << "+";
	temp << i;
	return temp.str();
}

int fast_atoi(const char * str) {
	int val = 0;
	while (*str) {
		val = val * 10 + (*str++ - '0');
	}
	return val;
}

void fast_itoa(int value, std::string & out) {
	if (value == 0) {
		out = "0";
		return;
	}
	while (value > 0) {
		char c = (value % 10) + '0';
		out.append(&c, 1);
		value = value / 10;
	}
	std::reverse(out.begin(), out.end());
}

// png utils

void my_read_png(png_structp png_ptr, png_bytep data, png_size_t length) {
	read_in* in = (read_in*) png_get_io_ptr(png_ptr);
	in->read(data, length);
}

void read_pkm_dim(std::string file_name, int &w, int &h, FTE_ENV env) {
	void* buf;
	long length;

	if (!load_resource_file(env, file_name, buf, length))
		return;

	w = swap_bytes(((ETC1Header*) buf)->origWidth);
	h = swap_bytes(((ETC1Header*) buf)->origHeight);

	LOGD("eeeeeeeeeeeeeeee %d %d", w, h);

	free(buf);
}

void read_png_dim(std::string file_name, int &w, int &h, FTE_ENV env) {

	read_in in;

	if (!in.init(file_name, env)) {
		LOGE("fte unable to load atlas texture. file not found: %s", file_name.c_str());
		return;
	}

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) {
		LOGE("fte error png_create_read_struct: %s", file_name.c_str());
		return;
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		LOGE("fte error png_create_info_struct: %s", file_name.c_str());
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return;
	}

	png_set_read_fn(png_ptr, &in, my_read_png);

	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

	png_uint_32 width, height;
	int color_type, interlace_type;
	int bit_depth;
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, NULL, NULL);

	w = width;
	h = height;

	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

}

} // namespace fte
