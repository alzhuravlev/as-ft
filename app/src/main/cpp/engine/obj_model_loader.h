#ifndef obj_model_loader_H
#define obj_model_loader_H

#include "engine.h"
#include "external.h"
#include "fte_utils.h"

#include <string>
#include <map>
#include <vector>

namespace fte {

struct FaceItem {
	int v_index;
	int vt_index;
	int vn_index;
};

struct Face {
	std::vector<FaceItem> face_items;
};

struct Vertex {
	float x, y, z;
};

struct VertexT {
	float u, v;
};

struct VertexN {
	float x, y, z;
};

struct FaceItemCompare {
	bool operator()(const FaceItem& a, const FaceItem& b) const {
		if (a.v_index < b.v_index)
			return true;
		else if (a.v_index == b.v_index)
			if (a.vn_index < b.vn_index)
				return true;
			else if (a.vn_index == b.vn_index)
				if (a.vt_index < b.vt_index)
					return true;
		return false;
	}
};

struct ObjModelItem {
	std::vector<Face> faces;

	void collapse_same_verts(std::map<FaceItem, int, FaceItemCompare> & m, int & ind_count) {
		int idx = 0;
		ind_count = 0;

		for (std::vector<Face>::const_iterator it = faces.begin(), end = faces.end(); it != end; ++it) {
			const Face &face = *it;

			ind_count += (face.face_items.size() - 2) * 3;

			for (std::vector<FaceItem>::const_iterator it_items = face.face_items.begin(), end_items = face.face_items.end(); it_items != end_items; ++it_items) {
				const FaceItem & face_item = *it_items;

				std::map<FaceItem, int, FaceItemCompare>::const_iterator it = m.find(face_item);
				if (it == m.end()) {
					m[face_item] = idx++;
				}
			}
		}
	}
};

/**
 * Loads OBJ file
 */
class ObjModelLoader {
private:

	std::map<std::string, ObjModelItem*> items;

	std::vector<Vertex> vertices;
	std::vector<VertexT> vertices_t;
	std::vector<VertexN> vertices_n;

	std::string file_name;

	void load(FTE_ENV env) {

		std::vector<std::string> lines;
		read_resource_lines(file_name, env, lines);

		std::string o_prefix("o ");
		std::string v_prefix("v ");
		std::string vt_prefix("vt ");
		std::string vn_prefix("vn ");
		std::string f_prefix("f ");

		ObjModelItem* current_item = NULL;
		std::vector<std::string> tmp;
		std::vector<std::string> tmp2;

		for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it) {

			std::string line = *it;

			remove_chars(line, "\n\r");

			if (line.length() == 0)
				continue;

			if (starts_with(line, o_prefix)) {
				tmp.clear();
				split(line, ' ', tmp);
				if (tmp.size() > 1) {
					current_item = new ObjModelItem;
					items[tmp[1]] = current_item;
				}
			} else if (current_item != NULL && starts_with(line, v_prefix)) {
				tmp.clear();
				split(line, ' ', tmp);
				if (tmp.size() > 3) {
					Vertex v;
					v.x = atof(tmp[1].c_str());
					v.y = atof(tmp[2].c_str());
					v.z = atof(tmp[3].c_str());

					vertices.push_back(v);
				}
			} else if (current_item != NULL && starts_with(line, vt_prefix)) {
				tmp.clear();
				split(line, ' ', tmp);
				if (tmp.size() > 2) {
					VertexT vt;
					vt.u = atof(tmp[1].c_str());
					vt.v = atof(tmp[2].c_str());
					vertices_t.push_back(vt);
				}
			} else if (current_item != NULL && starts_with(line, vn_prefix)) {
				tmp.clear();
				split(line, ' ', tmp);
				if (tmp.size() > 3) {
					VertexN vn;
					vn.x = atof(tmp[1].c_str());
					vn.y = atof(tmp[2].c_str());
					vn.z = atof(tmp[3].c_str());
					vertices_n.push_back(vn);
				}
			} else if (current_item != NULL && starts_with(line, f_prefix)) {
				tmp.clear();
				split(line, ' ', tmp);
				if (tmp.size() > 3) {
					tmp.erase(tmp.begin());
					if (tmp.size() > 0) {
						current_item->faces.push_back(Face());
						Face & face = current_item->faces.back();
						for (std::vector<std::string>::const_iterator it = tmp.begin(); it != tmp.end(); ++it) {
							std::string str = *it;

							tmp2.clear();
							split(str, '/', tmp2);

							FaceItem fi;
							fi.v_index = tmp2.size() > 0 ? atoi(tmp2[0].c_str()) : 0;
							fi.vt_index = tmp2.size() > 1 ? atoi(tmp2[1].c_str()) : 0;
							fi.vn_index = tmp2.size() > 2 ? atoi(tmp2[2].c_str()) : 0;

							face.face_items.push_back(fi);
						}
					}
				}
			}
		}

		LOGI("fte: ObjModelLoader: %d models are loaded from file '%s'.", items.size(), file_name.c_str());
	}

public:
	ObjModelLoader(const std::string & file_name, FTE_ENV env) {
		this->file_name = file_name;
		load(env);
	}

	~ObjModelLoader() {
		for (std::map<std::string, ObjModelItem*>::iterator it = items.begin(); it != items.end(); ++it) {
			delete it->second;
		}
	}

	void get_names(std::vector<std::string> & names) {
		for (std::map<std::string, ObjModelItem*>::const_iterator it = items.begin(), end = items.end(); it != end; ++it)
			names.push_back(it->first);
	}

	ObjModelItem* find_item(const std::string & name) const {
		std::map<std::string, ObjModelItem*>::const_iterator it = items.find(name);
		if (it == items.end()) {
			LOGE( "fte: ObjModelLoader: item '%s' not found!", name.c_str());
			return NULL;
		}
		return it->second;
	}

	const Vertex & getVertex(int index) const {
		return vertices[index - 1];
	}

	const VertexT & getVertexT(int index) const {
		return vertices_t[index - 1];
	}

	const VertexN & getVertexN(int index) const {
		return vertices_n[index - 1];
	}

	const std::string & get_file_name() const {
		return file_name;
	}
};

} // namespace fte

#endif /* obj_model_loader_H */
