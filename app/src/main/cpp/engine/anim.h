#ifndef anim_H
#define anim_H

#include "polygon.h"
#include "math.h"
#include <vector>
#include "transformable.h"
#include <cstdlib>

namespace fte {

/**
 * Various animations & interpolators. It operates on Transformable.
 */

/////////////////////////////////
// Interpolator
/////////////////////////////////
class Interpolator {
public:
	virtual ~Interpolator() {
	}
	virtual float interpolate(float value) const {
		return value;
	}
};

/////////////////////////////////
// AccelerateDecelerateInterpolator
/////////////////////////////////

class AccelerateDecelerateInterpolator: public Interpolator {
public:
	float interpolate(float t) const {
		return cos_fast((t + 1.0f) * M_PI) * 0.5f + 0.5f;
	}
};

/////////////////////////////////
// AccelerateInterpolator
/////////////////////////////////

class AccelerateInterpolator: public Interpolator {
private:
	float factor;
	float doubleFactor;

public:
	AccelerateInterpolator(float factor = 1.0f) {
		this->factor = factor;
		this->doubleFactor = 2.0f * factor;
	}

	float interpolate(float t) const {
		if (F_EQ(factor, 1.0f))
			return t * t;
		return std::pow(t, doubleFactor);
	}
};

/////////////////////////////////
// BounceInterpolator
/////////////////////////////////

class BounceInterpolator: public Interpolator {
private:

	inline static float bounce(float t) {
		return t * t * 8.0f;
	}

public:
	float interpolate(float t) const {
		t *= 1.1226f;
		if (t < 0.3535f)
			return bounce(t);
		else if (t < 0.7408f)
			return bounce(t - 0.54719f) + 0.7f;
		else if (t < 0.9644f)
			return bounce(t - 0.8526f) + 0.9f;
		else
			return bounce(t - 1.0435f) + 0.95f;
	}
};

/////////////////////////////////
// OvershootInterpolator
/////////////////////////////////

class OvershootInterpolator: public Interpolator {
private:
	float tension;
public:
	OvershootInterpolator(float tension = 1.0f) {
		this->tension = tension;
	}

	float interpolate(float t) const {
		t -= 1.0f;
		return t * t * ((tension + 1.0f) * t + tension) + 1.0f;
	}
};

/////////////////////////////////
// AnticipateInterpolator
/////////////////////////////////

class AnticipateInterpolator: public Interpolator {
private:
	float tension;
public:
	AnticipateInterpolator(float tension = 1.0f) {
		this->tension = tension;
	}

	float interpolate(float t) const {
		return t * t * ((tension + 1.0f) * t - tension);
	}
};

/////////////////////////////////
// AnticipateOvershootInterpolator
/////////////////////////////////

class AnticipateOvershootInterpolator: public Interpolator {
private:
	float tension;

	static float a(float t, float s) {
		return t * t * ((s + 1.0f) * t - s);
	}

	static float o(float t, float s) {
		return t * t * ((s + 1.0f) * t + s);
	}

public:
	AnticipateOvershootInterpolator(float tension = 1.0f) {
		this->tension = tension;
	}

	float interpolate(float t) const {
		if (t < 0.5f)
			return 0.5f * a(t * 2.0f, tension);
		else
			return 0.5f * (o(t * 2.0f - 2.0f, tension) + 2.0f);
	}
};

/////////////////////////////////
// DecelerateInterpolator
/////////////////////////////////

class DecelerateInterpolator: public Interpolator {
private:
	float factor;

public:
	DecelerateInterpolator(float factor = 1.0f) {
		this->factor = factor;
	}

	float interpolate(float t) const {
		float result;
		if (factor == 1.0f) {
			result = 1.0f - (1.0f - t) * (1.0f - t);
		} else {
			result = 1.0f - std::pow((1.0f - t), 2.0f * factor);
		}
		return result;
	}
};

/////////////////////////////////
// ReturningInterpolator
/////////////////////////////////

class ReturningInterpolator: public Interpolator {
private:
	float factor;

public:
	ReturningInterpolator(float factor = 1.0f) {
		this->factor = factor;
	}

	float interpolate(float t) const {
		float v = (4.0f * t * (1.0f - t)) * factor;
		return v;
	}
};

/////////////////////////////////
// FrameAnimationExt
/////////////////////////////////

struct FrameAnimationExtData {
	std::string id;
	int start_frame;
	int end_frame;
	bool immediate_transition;
	float duration;

	FrameAnimationExtData() {
	}

	FrameAnimationExtData(const std::string & id, int start_frame, int end_frame, float duration, bool immediate_transition = false) {
		this->id = id;
		this->start_frame = start_frame;
		this->end_frame = end_frame;
		this->immediate_transition = immediate_transition;
		this->duration = duration;
//		this->duration = (end_frame - start_frame) / 58.0f;
	}
};

struct FrameAnimationExtPendingData {
	std::string id;
	bool loop;

	FrameAnimationExtPendingData(const std::string & id, bool loop) {
		this->id = id;
		this->loop = loop;
	}
};

class FrameAnimationExt {
private:
	std::map<std::string, FrameAnimationExtData> data_map;

	int current_frame;
	float state_time;

	FrameAnimationExtData* current_animation;
	bool active;
	bool stopping;

	bool loop;

	std::vector<FrameAnimationExtPendingData> pending_list;

public:

	FrameAnimationExt() {
		this->current_animation = NULL;
		this->state_time = 0.0f;
		this->current_frame = 0;
		this->active = false;
		this->stopping = false;
		this->loop = false;
	}

	void clear_data() {
		data_map.clear();
	}

	void add_data(const FrameAnimationExtData & d) {
		data_map[d.id] = d;
	}

	void update(float delta) {
		if (!active)
			return;

		bool need_next_animation = false;

		if (!current_animation)
			need_next_animation = true;
		else {
			current_frame = current_animation->start_frame + (current_animation->end_frame - current_animation->start_frame) * state_time / current_animation->duration;

			state_time += delta;

			if (current_frame > current_animation->end_frame)
				if (loop && pending_list.size() == 0) {
					current_frame = current_animation->start_frame;
					state_time = 0.0f;
				} else {
					current_frame = current_animation->end_frame;
					need_next_animation = true;
				}
		}

		if (need_next_animation) {
			if (pending_list.size() == 0) {
				stop(true);
				return;
			}
			FrameAnimationExtPendingData pd = pending_list.back();
			pending_list.pop_back();

			loop = pd.loop;

			current_animation = &data_map.at(pd.id);
			current_frame = current_animation->start_frame;
			state_time = 0.0f;
		}
	}

	inline int get_current_frame() {
		return current_frame;
	}

	bool is_animation_playing(const std::string & id) {
		return active;
	}

	bool is_animation_playing() {
		return active;
	}

	const int get_data_size() const {
		return data_map.size();
	}

	FrameAnimationExtData* get_current_animation_data() {
		return current_animation;
	}

	bool has_id(const std::string & id) const {
		return data_map.find(id) != data_map.end();
	}

	void play(const std::string & id, bool loop = false) {
		std::map<std::string, FrameAnimationExtData>::const_iterator it = data_map.find(id);
		if (it == data_map.end()) {
//			LOGE("fte: FrameAnimationExt: animation '%s' not found!", id.c_str());
			return;
		}
		const FrameAnimationExtData & d = it->second;

		if (d.immediate_transition) {
			pending_list.clear();
			current_animation = NULL;
		}

		this->loop = false;

		if (current_animation) {
			const std::string out_id = current_animation->id + "_out";
			std::map<std::string, FrameAnimationExtData>::const_iterator it = data_map.find(out_id);
			if (it != data_map.end()) {
				pending_list.insert(pending_list.begin(), FrameAnimationExtPendingData(out_id, false));
			}
		}

		const std::string in_id = d.id + "_in";
		std::map<std::string, FrameAnimationExtData>::const_iterator it_in = data_map.find(in_id);
		if (it_in != data_map.end()) {
			pending_list.insert(pending_list.begin(), FrameAnimationExtPendingData(in_id, false));
		}

		pending_list.insert(pending_list.begin(), FrameAnimationExtPendingData(d.id, loop));

		active = true;
		stopping = false;
	}

	void stop(bool immediate = false) {
		if (immediate) {
			pending_list.clear();
			state_time = 0.0f;
			active = false;
			stopping = false;
			current_animation = NULL;
			loop = false;
		} else {
			stopping = true;
		}
	}

};

/////////////////////////////////
// FrameAnimation
/////////////////////////////////

const static int FRAME_ANIMATION_NO_RANGE = -1;

class FrameAnimation {
private:
	float duration;
	int frame_count;
	int frame_count_1;
	float state_time;
	int current_frame;
	bool cycle;
	bool pingpong;
	bool forward;
	Interpolator* interpolator;

	int range_start;
	int range_end;

	float frame_duration;

	void recalc() {
		frame_count_1 = frame_count - 1;
		frame_duration = duration / (float) frame_count;
	}

public:
	FrameAnimation() {
		this->duration = 0;
		this->frame_count = 0;
		this->cycle = false;
		this->pingpong = false;

		this->state_time = 0.;
		this->current_frame = 0;
		this->forward = true;

		this->interpolator = NULL;

		this->range_start = -1;
		this->range_end = -1;
	}

	void update(float delta) {
		if (!cycle && is_finished())
			return;

		if (forward && state_time > duration) {
			if (pingpong) {
				forward = !forward;
				state_time = duration;
			} else
				state_time = 0.0f;
		}

		if (!forward && state_time < 0.0f) {
			if (pingpong) {
				forward = !forward;
				state_time = 0.0f;
			} else
				state_time = duration;
		}

		float k = interpolator == NULL ? state_time / duration : interpolator->interpolate(state_time / duration);

		int r_start = range_start == FRAME_ANIMATION_NO_RANGE ? 0 : range_start;
		int r_end = range_end == FRAME_ANIMATION_NO_RANGE ? frame_count_1 : range_end;

		current_frame = r_start + (r_end - r_start) * k;

		state_time += forward ? delta : -delta;
	}

	void start(bool forward = true) {
		this->state_time = forward ? 0.0f : duration;
		this->current_frame = forward ? 0 : frame_count_1;
		this->forward = forward;

		if (pingpong)
			cycle = true;
	}

	bool is_finished() {
		return forward ? state_time > duration : state_time < 0.0f;
	}

	inline int get_current_frame() {
		return current_frame;
	}

	inline int get_frame_count() {
		return frame_count;
	}

	inline float get_state_time() {
		return state_time;
	}

	void set_cycle(bool cycle) {
		this->cycle = cycle;
	}

	void set_pingpong(bool pingpong) {
		this->pingpong = pingpong;
	}

	void set_duration(float duration) {
		this->duration = duration;
		recalc();
	}

	void set_ingterpolator(Interpolator* interpolator) {
		this->interpolator = interpolator;
	}

	void set_frame_count(int frame_count, int range_start = FRAME_ANIMATION_NO_RANGE, int range_end = FRAME_ANIMATION_NO_RANGE) {
		this->frame_count = frame_count;

		this->range_start = FRAME_ANIMATION_NO_RANGE;
		this->range_end = FRAME_ANIMATION_NO_RANGE;

		if (range_start >= 0 && range_end >= 0)
			if (range_start < frame_count && range_end < frame_count)
				if (range_start <= range_end) {
					this->range_start = range_start;
					this->range_end = range_end;
				}
		recalc();
	}
};

template<class T>
class BaseAnimation {
private:
	float duration;
	Interpolator* interpolator;
	float state_time;
	float start_time;
	bool active;
	bool calculated;
	bool forward;

protected:
	virtual void do_recalc(T o) {
	}

	virtual void do_update(T o, float k) {
	}

public:
	BaseAnimation() {
		interpolator = NULL;
		duration = 1.0f;
		state_time = 0.0f;
		start_time = 0.0f;
		active = false;
		calculated = false;
		forward = true;
	}

	virtual ~BaseAnimation() {
	}

	void set_duration(float duration) {
		this->duration = duration;
	}

	void set_interpolator(Interpolator* interpolator) {
		this->interpolator = interpolator;
	}

	void set_start_time(float start_time) {
		this->start_time = start_time;
	}

	void recalc(T o) {
		do_recalc(o);
	}

	void start(bool forward = true) {
		this->active = true;
		this->state_time = forward ? 0.0f : start_time + duration;
		this->calculated = false;
		this->forward = forward;
	}

	void stop() {
		active = false;
	}

	void update(T o, float delta) {
		if (!active)
			return;

		if (forward && state_time - start_time > duration) {
			float k = interpolator == NULL ? 1.0f : interpolator->interpolate(1.0f);
			do_update(o, k);
			active = false;
			return;
		}

		if (!forward && state_time < 0.0f) {
			float k = interpolator == NULL ? 0.0f : interpolator->interpolate(0.0f);
			do_update(o, 0.);
			active = false;
			return;
		}

		if (state_time >= start_time) {

			if (!calculated) {
				do_recalc(o);
				calculated = true;
			}

			float t = (state_time - start_time) / duration;
			float k = interpolator == NULL ? t : interpolator->interpolate(t);
			do_update(o, k);
		}

		state_time += forward ? delta : -delta;
	}

	bool is_active() {
		return active;
	}

	bool is_forward() {
		return forward;
	}

};

/////////////////////////////////
// TransformableAnimation
/////////////////////////////////

class TransformableAnimation: public BaseAnimation<Transformable*> {
protected:
	void do_recalc(Transformable* o) {
	}

	void do_update(Transformable* o, float k) {
	}
};

/////////////////////////////////
// AnimationBundle
/////////////////////////////////

class AnimationBundle {
private:
	std::vector<TransformableAnimation*> anims;
	bool cycle;
	bool pingpong;
	bool restart_after_all_done;
	bool active;

public:
	AnimationBundle() {
		cycle = false;
		pingpong = false;
		restart_after_all_done = true;
		active = false;
	}

	void set_cycle(bool cycle) {
		this->cycle = cycle;
	}

	void set_pingpong(bool pingpong) {
		this->pingpong = pingpong;
	}

	void set_restart_after_all_done(bool restart_after_all_done) {
		this->restart_after_all_done = restart_after_all_done;
	}

	void recalc(Transformable* o) {
		std::vector<TransformableAnimation*>::const_iterator end = anims.end();
		for (std::vector<TransformableAnimation*>::const_iterator it = anims.begin(); it != end; ++it) {
			TransformableAnimation* a = *it;
			a->recalc(o);
		}
	}

	void start() {
		std::vector<TransformableAnimation*>::const_iterator end = anims.end();
		for (std::vector<TransformableAnimation*>::const_iterator it = anims.begin(); it != end; ++it) {
			TransformableAnimation* a = *it;
			bool forward = pingpong ? !a->is_forward() : a->is_forward();
			a->start(forward);
		}
		active = true;
	}

	void update(Transformable* trans, float delta) {
		if (!active)
			return;

		bool all_finished = true;

		for (std::vector<TransformableAnimation*>::const_iterator it = anims.begin(), end = anims.end(); it != end; ++it) {

			TransformableAnimation* a = *it;
			a->update(trans, delta);

			if (a->is_active())
				all_finished = false;
			else if (cycle && !restart_after_all_done) {
				bool forward = pingpong ? !a->is_forward() : a->is_forward();
				a->start(forward);
			}
		}

		if (cycle && restart_after_all_done && all_finished) {
			start();
		} else if (all_finished) {
			active = false;
		}
	}

	bool is_active() {
		return active;
	}

	void add(TransformableAnimation* a) {
		anims.push_back(a);
	}
};

/////////////////////////////////
// ScaleByAnimation
/////////////////////////////////

class ScaleByAnimation: public TransformableAnimation {
private:
	float scale_x_from;
	float scale_y_from;
	float scale_z_from;
	float scale_x_to;
	float scale_y_to;
	float scale_z_to;
	float scale;

	void do_update(Transformable* trans, float k) {
		float sx = scale_x_from + (scale_x_to - scale_x_from) * k;
		float sy = scale_y_from + (scale_y_to - scale_y_from) * k;
		float sz = scale_z_from + (scale_z_to - scale_z_from) * k;
		trans->set_scale_xyz(sx, sy, sz);
	}

	void do_recalc(Transformable* trans) {
		scale_x_from = trans->get_scale_x();
		scale_y_from = trans->get_scale_y();
		scale_z_from = trans->get_scale_z();
		scale_x_to = trans->get_scale_x() * scale;
		scale_y_to = trans->get_scale_y() * scale;
		scale_z_to = trans->get_scale_z() * scale;
	}

public:

	ScaleByAnimation() {
		scale = 1.;
		scale_x_from = 1.;
		scale_y_from = 1.;
		scale_z_from = 1.;
		scale_x_to = 1.;
		scale_y_to = 1.;
		scale_z_to = 1.;
	}

	void set_scale_by(float scale) {
		this->scale = scale;
	}
};

/////////////////////////////////
// ScaleToAnimation
/////////////////////////////////

class ScaleToAnimation: public TransformableAnimation {
private:
	float scale_from;
	float scale_to;

	void do_update(Transformable* trans, float k) {
		float s = scale_from + (scale_to - scale_from) * k;
		trans->set_scale(s);
	}

	void do_recalc(Transformable* trans) {
	}

public:

	ScaleToAnimation() {
		scale_from = 1.;
		scale_to = 1.;
	}

	void set_scale(float scale_from, float scale_to) {
		this->scale_from = scale_from;
		this->scale_to = scale_to;
	}
};

/////////////////////////////////
// RotateByAxisAnimation
/////////////////////////////////

class RotateByAxisAnimation: public TransformableAnimation {
private:
	float rad;
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
//		trans->set_axis_angle(r);
	}

	void do_recalc(Transformable* trans) {
		rad_from = trans->get_axis_angle();
		rad_to = rad_from + rad;
	}

public:

	RotateByAxisAnimation() {
		rad = 0.;
		rad_from = 0.;
		rad_to = 0.;
	}

	void set_rotate_by(float rad) {
		this->rad = rad;
	}
};

/////////////////////////////////
// RotateByXAnimation
/////////////////////////////////

class RotateByXAnimation: public TransformableAnimation {
private:
	float rad;
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_x(r);
	}

	void do_recalc(Transformable* trans) {
		rad_from = trans->get_angle_x();
		rad_to = rad_from + rad;
	}

public:

	RotateByXAnimation() {
		rad = 0.;
		rad_from = 0.;
		rad_to = 0.;
	}

	void set_rotate_by(float rad) {
		this->rad = rad;
	}
};

/////////////////////////////////
// RotateByYAnimation
/////////////////////////////////

class RotateByYAnimation: public TransformableAnimation {
private:
	float rad;
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_y(r);
	}

	void do_recalc(Transformable* trans) {
		rad_from = trans->get_angle_y();
		rad_to = rad_from + rad;
	}

public:

	RotateByYAnimation() {
		rad = 0.0f;
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate_by(float rad) {
		this->rad = rad;
	}
};

/////////////////////////////////
// RotateByZAnimation
/////////////////////////////////

class RotateByZAnimation: public TransformableAnimation {
private:
	float rad;
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_z(r);
	}

	void do_recalc(Transformable* trans) {
		rad_from = trans->get_angle_z();
		rad_to = rad_from + rad;
	}

public:

	RotateByZAnimation() {
		rad = 0.0f;
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate_by(float rad) {
		this->rad = rad;
	}
};

/////////////////////////////////
// RotateToAxisAnimation
/////////////////////////////////

class RotateToAxisAnimation: public TransformableAnimation {
private:
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
//		trans->set_axis_angle(r);
	}

public:

	RotateToAxisAnimation() {
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate(float rad_from, float rad_to) {
		this->rad_from = rad_from;
		this->rad_to = rad_to;
	}
};

/////////////////////////////////
// RotateToXAnimation
/////////////////////////////////

class RotateToXAnimation: public TransformableAnimation {
private:
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_x(r);
	}

public:

	RotateToXAnimation() {
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate(float rad_from, float rad_to) {
		this->rad_from = rad_from;
		this->rad_to = rad_to;
	}
};

/////////////////////////////////
// RotateToYAnimation
/////////////////////////////////

class RotateToYAnimation: public TransformableAnimation {
private:
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_y(r);
	}

public:

	RotateToYAnimation() {
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate(float rad_from, float rad_to) {
		this->rad_from = rad_from;
		this->rad_to = rad_to;
	}

	void set_rotate_to(float rad_to) {
		this->rad_to = rad_to;
	}
};

/////////////////////////////////
// RotateToZAnimation
/////////////////////////////////

class RotateToZAnimation: public TransformableAnimation {
private:
	float rad_from;
	float rad_to;

	void do_update(Transformable* trans, float k) {
		float r = rad_from + (rad_to - rad_from) * k;
		trans->set_angle_z(r);
	}

public:

	RotateToZAnimation() {
		rad_from = 0.0f;
		rad_to = 0.0f;
	}

	void set_rotate(float rad_from, float rad_to) {
		this->rad_from = rad_from;
		this->rad_to = rad_to;
	}
};

/////////////////////////////////
// TranslateToYAnimation
/////////////////////////////////

class TranslateToYAnimation: public TransformableAnimation {
private:
	float from;
	float to;

	void do_update(Transformable* trans, float k) {
		float v = from + (to - from) * k;
		trans->set_translate_y(v);
	}

public:

	TranslateToYAnimation() {
		from = 0.0f;
		to = 0.0f;
	}

	void set_translate_y(float from, float to) {
		this->from = from;
		this->to = to;
	}
};

/////////////////////////////////
// TranslateToAnimation
/////////////////////////////////

class TranslateToAnimation: public TransformableAnimation {
private:
	float from_x;
	float from_y;
	float from_z;
	float to_x;
	float to_y;
	float to_z;

	void do_update(Transformable* trans, float k) {
		float x = from_x + (to_x - from_x) * k;
		float y = from_y + (to_y - from_y) * k;
		float z = from_z + (to_z - from_z) * k;
		trans->set_translate(x, y, z);
	}

public:

	TranslateToAnimation() {
		from_x = 0.0f;
		from_y = 0.0f;
		from_z = 0.0f;
		to_x = 0.0f;
		to_y = 0.0f;
		to_z = 0.0f;
	}

	void set_translate(float from_x, float from_y, float from_z, float to_x, float to_y, float to_z) {
		this->from_x = from_x;
		this->from_y = from_y;
		this->from_z = from_z;
		this->to_x = to_x;
		this->to_y = to_y;
		this->to_z = to_z;
	}

	void set_translate_to(float to_x, float to_y, float to_z) {
		this->to_x = to_x;
		this->to_y = to_y;
		this->to_z = to_z;
	}
};

/////////////////////////////////
// AlphaAnimation
/////////////////////////////////

class AlphaAnimation: public TransformableAnimation {
private:
	float alpha_to;
	float alpha_from;

	void do_update(Transformable* trans, float k) {
		float a = alpha_from + (alpha_to - alpha_from) * k;
		trans->set_alpha(a);
	}

public:

	AlphaAnimation() {
		alpha_to = 0.0f;
		alpha_from = 0.0f;
	}

	void set_alpha(float alpha_from, float alpha_to) {
		this->alpha_from = alpha_from;
		this->alpha_to = alpha_to;
	}
};

/////////////////////////////////
// VertexRingAnimation
/////////////////////////////////

class VertexRingAnimation: public TransformableAnimation {
private:
	std::vector<Vec3> starts;
	std::vector<Vec3> ends;
	std::vector<float> shifts;

	float velocity_from;
	float velocity_to;

	int offset;

	void do_update(Transformable* trans, float k) {
		Polygon* p = (Polygon*) trans;
		int vert_count = p->get_vert_count() - offset;

		float vel = velocity_from + (velocity_to - velocity_from) * k;
		float x, y, z, kk;
		float shift;

		Vec3* _starts = &starts.front();
		Vec3* _ends = &ends.front();
		float* _shifts = &shifts.front();

		for (int i = 0; i < vert_count; i++) {

			int idx = i + offset;
//			if (idx % 3 == 0)
//				continue;

			Vec3 & start = _starts[i];
			Vec3 & end = _ends[i];
			shift = _shifts[i];

			kk = (sin_fast(vel * k + shift) + 1.0f) * 0.5f;

			x = start.x + (end.x - start.x) * kk;
			y = start.y + (end.y - start.y) * kk;
			z = 0.0f;

			p->set_vert_xyz(idx, x, y, z);
		}
	}

public:

	VertexRingAnimation() {
		this->velocity_from = 1.0f;
		this->velocity_to = 1.0f;
		this->offset = 0;
	}

	void set(float amplitude, Transformable* trans, float velocity_from = 1.0f, float velocity_to = 1.0f, int waves_count = 1, int offset = 0) {
		this->offset = offset;

		Polygon* p = (Polygon*) trans;
		int vert_count = p->get_vert_count() - offset;

		this->velocity_from = velocity_from * M_2xPI;
		this->velocity_to = velocity_to * M_2xPI;

		srand(get_time().tv_sec);

		amplitude *= 0.5f;

		Vec3 o;
		p->get_origin(o.x, o.y, o.z);

		for (int i = 0; i < vert_count; i++) {

			Vec3 v;
			p->get_vert_xyz(i + offset, v.x, v.y, v.z);

			Vec3 dir;
			vec_sub(v, o, dir);
			float len = vec_len(dir);
			vec_normalize(dir);

			Vec3 start(dir);
			vec_mul(start, len - amplitude);
			vec_add(start, o, start);

			Vec3 end(dir);
			vec_mul(end, len + amplitude);
			vec_add(end, o, end);

			starts.push_back(start);
			ends.push_back(end);

			float v_ind_f = (i / 3) / (vert_count / 3.0f);
//			float v_ind_f = (float) i / vert_count;
			float shift = M_2xPI * (sin_fast(waves_count * M_2xPI * v_ind_f) + 1.0f) * 0.5f;
			shifts.push_back(shift);
		}
	}
};

} // namespace fte

#endif // anim_H