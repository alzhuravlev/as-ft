#ifndef particle_strategy_H
#define particle_strategy_H

#include "fte_math.h"
#include "camera.h"
#include "particle_emitter.h"

#include "glm/glm.hpp"

namespace fte {

///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
// some common particle strategies implementations
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////

/////////////////////////////////////////
// ParticleStartingPositionStrategy
/////////////////////////////////////////

class ParticleStartingPositionStrategy_Point: public ParticleStrategy {
protected:

	Vec3 point;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		p.set_position(point);
	}

public:

	virtual ~ParticleStartingPositionStrategy_Point() {
	}

	inline void set_point(const Vec3 & point) {
		this->point = point;
	}
};

class ParticleStartingPositionStrategy_Volume: public ParticleStrategy {
protected:

	Vec3 point1;
	Vec3 point2;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		Vec3 v;
		v.x = randf(std::min(point1.x, point2.x), std::max(point1.x, point2.x));
		v.y = randf(std::min(point1.y, point2.y), std::max(point1.y, point2.y));
		v.z = randf(std::min(point1.z, point2.z), std::max(point1.z, point2.z));
		p.set_position(v);
	}

public:

	ParticleStartingPositionStrategy_Volume() {
		this->point1.set(0.0f);
		this->point2.set(0.0f);
	}

	virtual ~ParticleStartingPositionStrategy_Volume() {
	}

	inline void set_points(const Vec3 & point1, const Vec3 & point2) {
		this->point1 = point1;
		this->point2 = point2;
	}

	inline void set_points_x(float x1, float x2) {
		this->point1.x = x1;
		this->point2.x = x2;
	}

	inline void set_points_y(float y1, float y2) {
		this->point1.y = y1;
		this->point2.y = y2;
	}

	inline void set_points_z(float z1, float z2) {
		this->point1.z = z1;
		this->point2.z = z2;
	}
};

class ParticleStartingPositionStrategy_AlongVector_Sequence: public ParticleStrategy {
protected:

	Vec3 point1;
	Vec3 point2;
	int steps;
	int current_step;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		current_step++;
		float k = (float) current_step / steps;
		float l = vec_len(point1, point2);

		Vec3 v;
		vec_sub(point2, point1, v);
		vec_mul(v, k * l);
		vec_add(point1, v, v);

		p.set_position(v);

		if (current_step > steps)
			current_step = 0;
	}

public:

	ParticleStartingPositionStrategy_AlongVector_Sequence() {
		this->point1.set(0.0f);
		this->point2.set(0.0f);
		this->steps = 10;
		this->current_step = 0;
	}

	virtual ~ParticleStartingPositionStrategy_AlongVector_Sequence() {
	}

	inline void set_points(const Vec3 & point1, const Vec3 & point2, int steps) {
		this->point1 = point1;
		this->point2 = point2;
		this->steps = steps;
		this->current_step = 0;
	}
};

class ParticleStartingPositionStrategy_Sphere: public ParticleStrategy {
protected:

	Vec3 centroid;
	float radius;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		Vec3 v(0., 0., radius);
		float ax = randf(0., M_2xPI);
		float ay = randf(0., M_2xPI);
		float az = randf(0., M_2xPI);
		rotate_point_fast(ax, ay, az, v.x, v.y, v.z);
		vec_add(centroid, v, v);
		p.set_position(v);
	}

public:

	virtual ~ParticleStartingPositionStrategy_Sphere() {
	}

	inline void set_centroid(const Vec3 & centroid) {
		this->centroid = centroid;
	}

	inline void set_radius(float radius) {
		this->radius = radius;
	}
};

///////////////////////////////////////////
// ParticleStartingLinearVelocityStrategy
///////////////////////////////////////////

class ParticleStartingLinearVelocityStrategy_Circular: public ParticleStrategy {
protected:

	float min_angle_y;
	float max_angle_y;

	float min_angle_z;
	float max_angle_z;

	float min_scalar;
	float max_scalar;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 v;

		float angle_y = randf(min_angle_y, max_angle_y);
		float angle_z = randf(min_angle_z, max_angle_z);
		float scalar = randf(min_scalar, max_scalar);

		v.x = 1.0f;
		v.y = 0.0f;
		v.z = 0.0f;

		rotate_point_fast(0.0f, angle_y, angle_z, v.x, v.y, v.z);

		vec_mul(v, scalar);

		p.set_linear_velocity(v);
	}

public:

	ParticleStartingLinearVelocityStrategy_Circular() {
		min_angle_y = 0.0f;
		max_angle_y = M_2xPI;

		min_angle_z = 0.0f;
		max_angle_z = M_2xPI;

		min_scalar = 0.1f;
		max_scalar = 0.1f;
	}

	virtual ~ParticleStartingLinearVelocityStrategy_Circular() {
	}

	inline void set_angle_y(float min, float max) {
		this->min_angle_y = min;
		this->max_angle_y = max;
	}

	inline void set_angle_z(float min, float max) {
		this->min_angle_z = min;
		this->max_angle_z = max;
	}

	inline void set_scalar(float min, float max) {
		this->min_scalar = min;
		this->max_scalar = max;
	}
};

class ParticleStartingLinearVelocityStrategy_AroundVector: public ParticleStrategy {
protected:

	Vec3 vector;
	float epsilon_angle;

	float min_scalar;
	float max_scalar;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		float scalar = randf(min_scalar, max_scalar);
		float e = tanf(randf(0.0f, epsilon_angle));
		float a = randf(0.0f, M_2xPI);

		Vec3 v1;

		vec_perp(vector, v1);
		vec_normalize(v1);
		vec_mul(v1, e);

		rotate_point_around_axis(vector.x, vector.y, vector.z, a, v1.x, v1.y, v1.z);

		vec_add(vector, v1, v1);
		vec_normalize(v1);
		vec_mul(v1, scalar);

		p.set_linear_velocity(v1);
	}

public:

	ParticleStartingLinearVelocityStrategy_AroundVector() {
		epsilon_angle = M_PI_2;

		vector.set(1.0f, 0.0f, 0.0f);

		min_scalar = 0.1f;
		max_scalar = 0.1f;
	}

	virtual ~ParticleStartingLinearVelocityStrategy_AroundVector() {
	}

	inline void set_epsilon_angle(float epsilon_angle) {
		this->epsilon_angle = epsilon_angle;
	}

	inline void set_vector(const Vec3 & vector) {
		this->vector = vector;
		vec_normalize(this->vector);
	}

	inline void set_scalar(float min, float max) {
		this->min_scalar = min;
		this->max_scalar = max;
	}
};

////////////////////////////////////////////
// ParticleLinearVelocityResolverStrategy
////////////////////////////////////////////

class ParticleLinearVelocityResolverStrategy_WithGravity: public ParticleStrategy {
protected:

	Vec3 gravity;
	float damping;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 current_velocity(p.get_linear_velocity());
		if (F_NEQ(damping, 0.))
			vec_mul(current_velocity, (1. - delta * damping));

		Vec3 v(gravity);
		vec_mul(v, delta);
		vec_add(current_velocity, v, current_velocity);

		p.set_linear_velocity(current_velocity);

		// change pos

		Vec3 current_position(p.get_position());

		vec_mul(current_velocity, delta);
		vec_add(current_position, current_velocity, current_position);

		p.set_position(current_position);
	}

public:

	ParticleLinearVelocityResolverStrategy_WithGravity() {
		this->gravity = Vec3(0.0f, -1.0f, 0.0f);
		this->damping = 0.0f;
	}

	virtual ~ParticleLinearVelocityResolverStrategy_WithGravity() {
	}

	inline void set_gravity(const Vec3 & gravity) {
		this->gravity = gravity;
	}

	inline void set_damping(float damping) {
		this->damping = damping;
	}
};

class ParticleLinearVelocityResolverStrategy_Simple: public ParticleStrategy {
protected:

	float damping;

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 current_velocity(p.get_linear_velocity());
		if (F_NEQ(damping, 0.0f)) {
			vec_mul(current_velocity, (1.0f - delta * damping));
			p.set_linear_velocity(current_velocity);
		}

		// change pos

		Vec3 current_position(p.get_position());

		vec_mul(current_velocity, delta);
		vec_add(current_position, current_velocity, current_position);

		p.set_position(current_position);
	}

public:

	ParticleLinearVelocityResolverStrategy_Simple() {
		this->damping = 0.0f;
	}

	virtual ~ParticleLinearVelocityResolverStrategy_Simple() {
	}

	inline void set_damping(float damping) {
		this->damping = damping;
	}
};

class ParticleLinearVelocityResolverStrategy_WithGravityAndWind: public ParticleStrategy {
private:
	Vec3 gravity;

	float wind_min_angle_y;
	float wind_max_angle_y;

	float wind_min_angle_z;
	float wind_max_angle_z;

	float wind_min_scalar;
	float wind_max_scalar;

	int change_points;

	Vec3* winds;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 current_velocity(p.get_linear_velocity());

		Vec3 v(gravity);
		vec_mul(v, delta);
		vec_add(current_velocity, v, current_velocity);

		//apply wind
		float k = p.get_interpolated_time();
		float w = 1.0f / change_points;
		int wind_index = (int) (k / w);

		wind_index %= change_points;

		Vec3 wind = winds[wind_index];
		vec_mul(wind, delta);
		vec_add(current_velocity, wind, current_velocity);

		p.set_linear_velocity(current_velocity);

		// change pos

		Vec3 current_position(p.get_position());

		vec_mul(current_velocity, delta);
		vec_add(current_position, current_velocity, current_position);

		p.set_position(current_position);
	}

public:

	ParticleLinearVelocityResolverStrategy_WithGravityAndWind() {
		this->gravity.set(0.0f, -1.0f, 0.0f);

		winds = NULL;

		wind_min_angle_y = 0.0f;
		wind_max_angle_y = M_2xPI;

		wind_min_angle_z = 0.0f;
		wind_max_angle_z = M_2xPI;

		wind_min_scalar = 0.0f;
		wind_max_scalar = 0.2f;

		change_points = 10;

		recalc();
	}

	virtual ~ParticleLinearVelocityResolverStrategy_WithGravityAndWind() {
		free(winds);
	}

	inline void set_gravity(const Vec3 & gravity) {
		this->gravity = gravity;
	}

	inline void set_wind_angle_y(float min, float max) {
		this->wind_min_angle_y = min;
		this->wind_max_angle_y = max;
	}

	inline void set_wind_angle_z(float min, float max) {
		this->wind_min_angle_z = min;
		this->wind_max_angle_z = max;
	}

	inline void set_wind_scalar(float min, float max) {
		this->wind_min_scalar = min;
		this->wind_max_scalar = max;
	}

	inline void set_change_points(int change_points) {
		this->change_points = change_points;
	}

	void recalc() {
		free(winds);
		winds = (Vec3*) malloc(change_points * sizeof(Vec3));

		for (int i = 0; i != change_points; i++) {
			Vec3 & v = winds[i];

			float angle_y = randf(wind_min_angle_y, wind_max_angle_y);
			float angle_z = randf(wind_min_angle_z, wind_max_angle_z);
			float scalar = randf(wind_min_scalar, wind_max_scalar);

			v.x = 1.0f;
			v.y = 0.0f;
			v.z = 0.0f;

			rotate_point_fast(0., angle_y, angle_z, v.x, v.y, v.z);

			scalar = (i % 2 == 0) ? scalar : -scalar;
			if (i > 0) {
				Vec3 & v_prev = winds[i - 1];
				float scalar_prev = vec_len(v_prev);
				if (scalar > 0.)
					scalar += scalar_prev;
				else
					scalar -= scalar_prev;
			}

			vec_mul(v, scalar);

//			LOGD("%d. wind (%f, %f, %f) scalar=%f; angle_y=%f; angle_z=%f", i, v.x, v.y, v.z, scalar, angle_y, angle_z);
		}
	}
};

////////////////////////////////////////////
// ParticleRotationResolverStrategy
////////////////////////////////////////////

class ParticleStartingRotationStrategy_RandomizeAngularVelocity: public ParticleStrategy {
private:
	float min;
	float max;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		p.set_angular_velocity(randf(min, max));
	}

public:

	ParticleStartingRotationStrategy_RandomizeAngularVelocity() {
		min = -M_PI;
		max = M_PI;
	}

	virtual ~ParticleStartingRotationStrategy_RandomizeAngularVelocity() {
	}

	inline void set_angular_velocity(float min, float max) {
		this->min = min;
		this->max = max;
	}
};

class ParticleStartingRotationStrategy_RandomizeAxisAngle: public ParticleStrategy {
private:
	float min;
	float max;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		p.set_axis_angle(randf(min, max));
	}

public:

	ParticleStartingRotationStrategy_RandomizeAxisAngle() {
		min = -M_PI;
		max = M_PI;
	}

	virtual ~ParticleStartingRotationStrategy_RandomizeAxisAngle() {
	}

	inline void set_axis_angle(float min, float max) {
		this->min = min;
		this->max = max;
	}
};

class ParticleRotationResolverStrategy_FaceToCameraAndAngularVelocity: public ParticleStrategy {
private:
protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 rotation;

		rotation.x = camera.get_angle_x();
		rotation.y = camera.get_angle_y();
		rotation.z = camera.get_angle_z();

		p.set_rotation(rotation);

		const Vec3 & Z = camera.get_Z();
		Vec3 axis(-Z.x, -Z.y, -Z.z);

		p.set_axis(axis);

		float axis_angle = p.get_axis_angle();
		axis_angle += p.get_angular_velocity() * delta;
		p.set_axis_angle(axis_angle);
	}

public:

	ParticleRotationResolverStrategy_FaceToCameraAndAngularVelocity() {

	}

	virtual ~ParticleRotationResolverStrategy_FaceToCameraAndAngularVelocity() {
	}
};

class ParticleRotationResolverStrategy_FaceToCamera: public ParticleStrategy {
private:
protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		Vec3 rotation;

		rotation.x = camera.get_angle_x();
		rotation.y = camera.get_angle_y();
		rotation.z = camera.get_angle_z();

		p.set_rotation(rotation);
	}

public:

	ParticleRotationResolverStrategy_FaceToCamera() {

	}

	virtual ~ParticleRotationResolverStrategy_FaceToCamera() {
	}
};

class ParticleRotationResolverStrategy_FaceToLinearVelocity: public ParticleStrategy {
private:
protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		const Vec3 pt(0.0f, 1.0f, 0.0f);

		Vec3 lv = p.get_linear_velocity();
		vec_normalize(lv);

		Vec3 r = p.get_rotation();
		r.y += p.get_angular_velocity() * delta;
		p.set_rotation(r);

		float cos = vec_dot(lv, pt);
		if (F_EQ(cos, 1.0f) || F_EQ(cos, -1.0f)) {
			p.set_axis_angle(0.0f);
		} else {
			Vec3 axis;
			vec_cross(pt, lv, axis);

			vec_normalize(axis);

			float a = acosf(cos);

			p.set_axis(axis);
			p.set_axis_angle(a);
		}
	}

public:

	ParticleRotationResolverStrategy_FaceToLinearVelocity() {

	}

	virtual ~ParticleRotationResolverStrategy_FaceToLinearVelocity() {
	}
};

////////////////////////////////////////////
// ParticleScaleResolverStrategy
////////////////////////////////////////////

class ParticleStartingScaleResolverStrategy_Randomize: public ParticleStrategy {
private:

	float min_x;
	float max_x;
	float min_y;
	float max_y;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float vx = min_x + (max_x - min_x) * randf(0.0f, 1.0f);
		float vy = min_y + (max_y - min_y) * randf(0.0f, 1.0f);

		Vec3 scale(p.get_scale());

		scale.set(vx, vy, 1.0f);

		p.set_scale(scale);
	}

public:

	ParticleStartingScaleResolverStrategy_Randomize() {
		this->min_x = 1.0f;
		this->max_x = 1.0f;
		this->min_y = 1.0f;
		this->max_y = 1.0f;
	}

	virtual ~ParticleStartingScaleResolverStrategy_Randomize() {
	}

	inline void set_scale(float min_x, float max_x, float min_y, float max_y) {
		this->min_x = min_x;
		this->max_x = max_x;
		this->min_y = min_y;
		this->max_y = max_y;
	}
};

class ParticleStartingScaleResolverStrategy_Sequence: public ParticleStrategy {
private:

	float x;
	float dx;
	float y;
	float dy;
	int steps;
	int current_step;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		current_step++;

		float k = (float) current_step / steps;

		float vx = x + dx * k;
		float vy = y + dy * k;

		Vec3 scale(p.get_scale());

		scale.set(vx, vy, 1.0f);

		p.set_scale(scale);

		if (current_step > steps)
			current_step = 0;
	}

public:

	ParticleStartingScaleResolverStrategy_Sequence() {
		this->x = 1.0f;
		this->dx = 0.0f;
		this->y = 1.0f;
		this->dy = 0.0f;
		this->steps = 10;
		this->current_step = 0;
	}

	virtual ~ParticleStartingScaleResolverStrategy_Sequence() {
	}

	inline void set_scale(float min_x, float max_x, float min_y, float max_y, int steps) {
		this->x = min_x;
		this->dx = max_x - min_x;
		this->y = min_y;
		this->dy = max_y - min_y;
		this->steps = steps;
		this->current_step = 0;
	}
};

template<class T>
class ParticleScaleResolverStrategy_WithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float value;
	float delta;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());
		float v = value + delta * k;

		Vec3 scale(v, v, v);
		p.set_scale(scale);
	}

public:

	ParticleScaleResolverStrategy_WithInterpolator() {
		this->value = 1.0f;
		this->delta = 0.0f;
	}

	virtual ~ParticleScaleResolverStrategy_WithInterpolator() {
	}

	inline void set_scale(float min, float max) {
		this->value = min;
		this->delta = max - min;
	}
};

template<class T>
class ParticleScaleXYResolverStrategy_WithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float x;
	float dx;
	float y;
	float dy;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());
		float vx = x + dx * k;
		float vy = y + dy * k;

		Vec3 scale(vx, vy, 1.0);
		p.set_scale(scale);
	}

public:

	ParticleScaleXYResolverStrategy_WithInterpolator() {
		this->x = 1.0f;
		this->dx = 0.0f;

		this->y = 1.0f;
		this->dy = 0.0f;
	}

	virtual ~ParticleScaleXYResolverStrategy_WithInterpolator() {
	}

	inline void set_scale(float x1, float x2, float y1, float y2) {
		this->x = x1;
		this->dx = x2 - x1;
		this->y = y1;
		this->dy = y2 - y1;
	}
};

template<class T>
class ParticleScaleResolverStrategy_PulseWithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float x;
	float dx;
	float y;
	float dy;
	float pulse_scale_x;
	float pulse_velocity_x;
	float pulse_scale_y;
	float pulse_velocity_y;

	bool same_scale;
	bool same_pulse;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());

		float vx = x + dx * k;
		float vy = same_scale ? vx : y + dy * k;

		float dx = pulse_scale_x * cos_fast(p.get_state_time() * pulse_velocity_x);
		float dy = same_pulse ? dx : pulse_scale_y * cos_fast(p.get_state_time() * pulse_velocity_y);

		vx += dx;
		vy -= dy;

		p.set_scale(Vec3(vx, vy, 1.0f));
	}

public:

	ParticleScaleResolverStrategy_PulseWithInterpolator() {
		this->x = this->y = 1.0f;
		this->dx = this->dy = 0.0f;
		this->pulse_scale_x = this->pulse_scale_y = 0.05f;
		this->pulse_velocity_x = this->pulse_velocity_y = 3.0f * M_2xPI;
		this->same_scale = true;
		this->same_pulse = true;
	}

	virtual ~ParticleScaleResolverStrategy_PulseWithInterpolator() {
	}

	inline void set_scale(float min, float max) {
		this->x = min;
		this->dx = max - min;
		this->y = x;
		this->dy = dx;
		this->same_scale = true;
	}

	inline void set_scale_x(float min_x, float max_x) {
		this->x = min_x;
		this->dx = max_x - min_x;
		this->same_scale = F_EQ(this->x, this->y) && F_EQ(this->dx, this->dy);
	}

	inline void set_scale_y(float min_y, float max_y) {
		this->y = min_y;
		this->dy = max_y - min_y;
		this->same_scale = F_EQ(this->x, this->y) && F_EQ(this->dx, this->dy);
	}

	inline void set_pulse_x(float pulse_scale, float pulse_velocity) {
		this->pulse_scale_x = pulse_scale;
		this->pulse_velocity_x = pulse_velocity * M_2xPI;
		this->same_pulse = F_EQ(this->pulse_scale_x, this->pulse_scale_y) && F_EQ(this->pulse_velocity_x, this->pulse_velocity_y);
	}

	inline void set_pulse_y(float pulse_scale, float pulse_velocity) {
		this->pulse_scale_y = pulse_scale;
		this->pulse_velocity_y = pulse_velocity * M_2xPI;
		this->same_pulse = F_EQ(this->pulse_scale_x, this->pulse_scale_y) && F_EQ(this->pulse_velocity_x, this->pulse_velocity_y);
	}

	inline void set_pulse(float pulse_scale, float pulse_velocity) {
		set_pulse_x(pulse_scale, pulse_velocity);
		set_pulse_y(pulse_scale, pulse_velocity);
		this->same_pulse = true;
	}
};

class ParticleScaleResolverStrategy_AppearDisapear: public ParticleStrategy {
private:

	float appear_time;
	float disapear_time;
	float normal_scale;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = p.get_interpolated_time();

		float scale;

		if (k < appear_time)
			scale = normal_scale * k / appear_time;
		else if (k > (1.0f - disapear_time))
			scale = normal_scale * (1.0f - k) / disapear_time;
		else
			scale = normal_scale;

		Vec3 v(scale, scale, scale);
		p.set_scale(v);
	}

public:

	ParticleScaleResolverStrategy_AppearDisapear() {
		this->appear_time = 0.2f;
		this->disapear_time = 0.2f;
		this->normal_scale = 1.0f;
	}

	virtual ~ParticleScaleResolverStrategy_AppearDisapear() {
	}

	inline void set_time(float appear_time, float disapear_time) {
		this->appear_time = appear_time;
		this->disapear_time = disapear_time;
	}

	inline void set_normal_scale(float normal_scale) {
		this->normal_scale = normal_scale;
	}

	inline void set_normal_scale(float min, float max) {
		this->normal_scale = randf(min, max);
	}
};
////////////////////////////////////////
// ParticleColorResolverStrategy
////////////////////////////////////////

template<class T>
class ParticleColorResolverStrategy_WithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float b_r;
	float b_g;
	float b_b;
	float b_a;

	float d_r;
	float d_g;
	float d_b;
	float d_a;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());
		float r = b_r + d_r * k;
		float g = b_g + d_g * k;
		float b = b_b + d_b * k;
		float a = b_a + d_a * k;

		uint32_t color = p.get_color();
		pack_color_f(color, r, g, b, a);
		p.set_color(color);
	}

public:

	ParticleColorResolverStrategy_WithInterpolator() {
		b_r = 1.0f;
		b_g = 1.0f;
		b_b = 1.0f;
		b_a = 1.0f;

		d_r = 0.0f;
		d_g = 0.0f;
		d_b = 0.0f;
		d_a = 0.0f;
	}

	virtual ~ParticleColorResolverStrategy_WithInterpolator() {
	}

	inline void set_color(uint32_t begin, uint32_t end) {
		unpack_color_f(begin, b_r, b_g, b_b, b_a);
		unpack_color_f(end, d_r, d_g, d_b, d_a);
		d_r -= b_r;
		d_g -= b_g;
		d_b -= b_b;
		d_a -= b_a;
	}
};

class ParticleStartingColorResolverStrategy_Randomize: public ParticleStrategy {
private:

	float b_r;
	float b_g;
	float b_b;
	float b_a;

	float e_r;
	float e_g;
	float e_b;
	float e_a;

	bool use_luminance_colors;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {

		int i = randi(0, 2);

		bool full_r = !use_luminance_colors ? false : i == 0;
		bool full_g = !use_luminance_colors ? false : i == 1;
		bool full_b = !use_luminance_colors ? false : i == 2;

		float r = full_r ? 1.0f : b_r + (e_r - b_r) * randf(0.0f, 1.0f);
		float g = full_g ? 1.0f : b_g + (e_g - b_g) * randf(0.0f, 1.0f);
		float b = full_b ? 1.0f : b_b + (e_b - b_b) * randf(0.0f, 1.0f);

		float a = b_a + (e_a - b_a) * randf(0.0f, 1.0f);

		uint32_t color = p.get_color();
		pack_color_f(color, r, g, b, a);
		p.set_original_color(color);
		p.set_color(color);
		p.set_alpha(a);
		p.set_original_alpha(a);
	}

public:

	ParticleStartingColorResolverStrategy_Randomize() {
		unpack_color_f(0xffffffff, b_r, b_g, b_b, b_a);
		unpack_color_f(0xff000000, e_r, e_g, e_b, e_a);
		use_luminance_colors = true;
	}

	virtual ~ParticleStartingColorResolverStrategy_Randomize() {
	}

	inline void set_color(uint32_t begin, uint32_t end, bool use_luminance_colors = true) {
		this->use_luminance_colors = use_luminance_colors;
		unpack_color_f(begin, b_r, b_g, b_b, b_a);
		unpack_color_f(end, e_r, e_g, e_b, e_a);
	}
};

class ParticleStartingColorResolverStrategy_Simple: public ParticleStrategy {
private:

	uint32_t color;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		p.set_original_color(color);
		p.set_color(color);
	}

public:

	ParticleStartingColorResolverStrategy_Simple() {
		color = 0xffffffff;
	}

	virtual ~ParticleStartingColorResolverStrategy_Simple() {
	}

	inline void set_color(uint32_t color) {
		this->color = color;
	}
};

class ParticleColorResolverStrategy_DistanceFromStart: public ParticleStrategy {
private:

	uint32_t color1;
	uint32_t color2;
	Vec3 start_position;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		Vec3 current_position(p.get_position());
		Vec3 v;
		vec_sub(current_position, start_position, v);
		v.x *= 4.0f;
		v.y *= 0.5f;
		v.z *= 4.0f;
		float d = vec_len(v);
		d = CLAMP(d, 0.0f, 1.0f);
		uint32_t c;
		mix_color_c(color1, color2, d, c);
		p.set_color(c);
	}

public:

	ParticleColorResolverStrategy_DistanceFromStart() {
		color1 = 0xff0000ff;
		color2 = 0xffffffff;
	}

	virtual ~ParticleColorResolverStrategy_DistanceFromStart() {
	}

	inline void set_colors(uint32_t color1, uint32_t color2) {
		this->color1 = color1;
		this->color2 = color2;
	}

	inline void set_start_position(const Vec3 & start_position) {
		this->start_position = start_position;
	}
};

class ParticleStartingColorResolverStrategy_RandomizeBrightness: public ParticleStrategy {
private:

	float begin;
	float end;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = randf(begin, end);

		uint32_t original_color = p.get_original_color();
		float o_a, o_r, o_g, o_b;
		unpack_color_f(original_color, o_r, o_g, o_b, o_a);

		float r = o_r + k;
		float g = o_g + k;
		float b = o_b + k;
		float a = o_a;

		r = CLAMP(r, 0., 1.);
		g = CLAMP(g, 0., 1.);
		b = CLAMP(b, 0., 1.);

		uint32_t color = p.get_color();
		pack_color_f(color, r, g, b, a);
		p.set_original_color(color);
		p.set_color(color);
		p.set_alpha(o_a);
		p.set_original_alpha(o_a);
	}

public:

	ParticleStartingColorResolverStrategy_RandomizeBrightness() {
		begin = 0.0;
		end = 0.2;
	}

	virtual ~ParticleStartingColorResolverStrategy_RandomizeBrightness() {
	}

	inline void set_brightness(float begin, float end) {
		this->begin = begin;
		this->end = end;
	}
};

template<class T>
class ParticleColorResolverStrategy_BrightnessWithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float begin;
	float end;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());
		k = begin + (end - begin) * k;

		uint32_t original_color = p.get_original_color();
		float o_a, o_r, o_g, o_b;
		unpack_color_f(original_color, o_r, o_g, o_b, o_a);

		float r = o_r + k;
		float g = o_g + k;
		float b = o_b + k;
		float a = o_a;

		r = CLAMP(r, 0.0f, 1.0f);
		g = CLAMP(g, 0.0f, 1.0f);
		b = CLAMP(b, 0.0f, 1.0f);

		uint32_t color;
		pack_color_f(color, r, g, b, a);
		p.set_color(color);
	}

public:

	ParticleColorResolverStrategy_BrightnessWithInterpolator() {
		begin = 0.0f;
		end = 0.3f;
	}

	virtual ~ParticleColorResolverStrategy_BrightnessWithInterpolator() {
	}

	inline void set_brightness(float begin, float end) {
		this->begin = begin;
		this->end = end;
	}
};

////////////////////////////////////////
// ParticleAlphaResolverStrategy
////////////////////////////////////////

template<class T>
class ParticleAlphaResolverStrategy_WithInterpolator: public ParticleStrategy {
private:
	T interpolator;

	float min;
	float max;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = interpolator.interpolate(p.get_interpolated_time());
		float alpha = min + (max - min) * k;
		p.set_alpha(alpha);
	}

public:

	ParticleAlphaResolverStrategy_WithInterpolator() {
		this->min = 0.0f;
		this->max = 1.0f;
	}

	virtual ~ParticleAlphaResolverStrategy_WithInterpolator() {
	}

	inline void set_alpha(float min, float max) {
		this->min = min;
		this->max = max;
	}
};

class ParticleAlphaResolverStrategy_AppearDisapearAndPulse: public ParticleStrategy {
private:

	float appear_time;
	float disapear_time;
	float pulse_scale;
	float pulse_velocity;
	bool pulse_enabled;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
		float k = p.get_interpolated_time();

		float alpha = p.get_original_alpha();

		if (k < appear_time)
			alpha *= k / appear_time;
		else if (k > (1.0f - disapear_time))
			alpha *= (1.0f - k) / disapear_time;

		if (pulse_enabled) {
			alpha *= 1.0f + pulse_scale * cos_fast(p.get_state_time() * pulse_velocity);
		}

		p.set_alpha(alpha);
	}

public:

	ParticleAlphaResolverStrategy_AppearDisapearAndPulse() {
		this->appear_time = 0.4f;
		this->disapear_time = 0.4f;
		this->pulse_scale = 0.0f;
		this->pulse_velocity = 0.0f;
		this->pulse_enabled = false;
	}

	virtual ~ParticleAlphaResolverStrategy_AppearDisapearAndPulse() {
	}

	inline void set_time(float appear_time, float disapear_time) {
		this->appear_time = appear_time;
		this->disapear_time = disapear_time;
	}

	inline void set_pulse(float pulse_scale, float pulse_velocity) {
		this->pulse_scale = pulse_scale;
		this->pulse_velocity = pulse_velocity;
		this->pulse_enabled = F_NEQ(pulse_scale, 0.0f) && F_NEQ(pulse_velocity, 0.0f);
	}
};

/////////////////////////////////////////
// ParticleCullingResolverStrategy
/////////////////////////////////////////

class ParticleCullingResolverStrategy_Tower: public ParticleStrategy {
private:

	float radius;

protected:

	virtual void do_resolve(float delta, Particle & p, const Camera & camera) {
//		Vec3 PARTICLE_POSITION(p.get_position().x, 0.0f, p.get_position().z);
//		float PARTICLE_DIST = vec_len(PARTICLE_POSITION);
//		if (PARTICLE_DIST < radius) {
//			p.set_visible(false);
//			return;
//		}
//
//		Vec3 CAMERA_POSITION(camera.get_camera_position().x, 0.0f, camera.get_camera_position().z);
//		float CAMERA_DIST = vec_len(CAMERA_POSITION);
//
//		float A = asinf(radius / CAMERA_DIST);
//		float cos_cutoff = cos_fast(A);
//
//		Vec3 PARTICLE;
//		Vec3 Z(-CAMERA_POSITION.x, 0.0f, -CAMERA_POSITION.z);
//
//		vec_sub(PARTICLE_POSITION, CAMERA_POSITION, PARTICLE);
//		float d = vec_len(PARTICLE);
//		vec_normalize(PARTICLE);
//		vec_normalize(Z);
//
//		float cos = vec_dot(Z, PARTICLE);

//		LOGD("cutoff=%f   cos=%f, radius=%f, A=%f, B=%f, CAM_DIST=%f, d=%f, PARTICLE_DIST=%f", cos_cutoff, cos, radius, A * M_RAD_TO_DEG, acosf(cos)* M_RAD_TO_DEG, CAMERA_DIST, d, PARTICLE_DIST);

//		p.set_visible(cos < cos_cutoff);
	}

public:

	ParticleCullingResolverStrategy_Tower() {
		this->radius = 1.0f;
	}

	virtual ~ParticleCullingResolverStrategy_Tower() {
	}

	inline void set_radius(float radius) {
		this->radius = radius;
	}
};

} // namespace fte

#endif /* particle_strategy_H */
