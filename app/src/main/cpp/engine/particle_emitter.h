#ifndef particle_emitter_H
#define particle_emitter_H

#include "command.h"
#include "polygon.h"
#include "camera.h"
#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "obj_model_loader.h"
#include "polygon_builder.h"
#include "fte_math.h"

namespace fte {

struct ParticleEmitterConfig {
	int min_count;
	int max_count;

	float min_delay;
	float max_delay;

	float min_life_time;
	float max_life_time;

	ParticleEmitterConfig() {
		min_count = 10;
		min_count = 20;

		min_delay = .1;
		max_delay = .4;

		min_life_time = 1.;
		max_life_time = 2.;
	}
};

class Particle;

class ParticleStrategy {
protected:
	virtual void do_resolve(float delta, Particle & p, const Camera & camera) = 0;
public:

	virtual ~ParticleStrategy() {
	}

	void resolve(float delta, Particle & p, const Camera & camera) {
		do_resolve(delta, p, camera);
	}
};

class Particle {
private:

	Vec3 position;
	Vec3 rotation;
	Vec3 scale;

	Vec3 axis;
	float axis_angle;

	uint32_t color;
	uint32_t original_color;
	float alpha;
	float original_alpha;

	Vec3 linear_velocity;
	float angular_velocity;

	float state_time;
	float start_time;
	float life_time;

	bool visible;
	bool initialized;

	Particle* next;

	static Particle* first_available;

public:

	Particle() {
	}

	static Particle* create();
	static void release(Particle* particle);

	inline float get_angular_velocity() {
		return angular_velocity;
	}

	void set_angular_velocity(float angular_velocity) {
		this->angular_velocity = angular_velocity;
	}

	uint32_t get_original_color() const {
		return original_color;
	}

	void set_original_color(uint32_t original_color) {
		this->original_color = original_color;
	}

	inline float get_alpha() const {
		return alpha;
	}

	inline void set_alpha(float alpha) {
		this->alpha = alpha;
	}

	inline float get_original_alpha() const {
		return original_alpha;
	}

	inline void set_original_alpha(float original_alpha) {
		this->original_alpha = original_alpha;
	}

	inline uint32_t get_color() const {
		return color;
	}

	inline void set_color(uint32_t color) {
		this->color = color;
	}

	inline const Vec3 & get_linear_velocity() const {
		return linear_velocity;
	}

	inline void set_linear_velocity(const Vec3 & linearVelocity) {
		linear_velocity = linearVelocity;
	}

	inline const Vec3 & get_position() const {
		return position;
	}

	void set_position(const Vec3 & position) {
		this->position = position;
	}

	const Vec3 & get_rotation() const {
		return rotation;
	}

	void set_rotation(const Vec3 & rotation) {
		this->rotation = rotation;
	}

	const Vec3 & get_axis() const {
		return axis;
	}

	void set_axis(const Vec3 & axis) {
		this->axis = axis;
	}

	float get_axis_angle() const {
		return axis_angle;
	}

	void set_axis_angle(float axis_angle) {
		this->axis_angle = axis_angle;
	}

	const Vec3 & get_scale() const {
		return scale;
	}

	void set_scale(const Vec3 & scale) {
		this->scale = scale;
	}

	void set_scale(float v) {
		this->scale.set(v);
	}

	inline float get_interpolated_time() const {
		return (state_time - start_time) / life_time;
	}

	inline float get_state_time() const {
		return state_time;
	}

	void init(float start_time, float life_time) {
		this->state_time = 0.0f;

		this->start_time = start_time;
		this->life_time = life_time;

		this->position.set_to_zero();
		this->scale.set_to_zero();
		this->linear_velocity.set_to_zero();

		this->rotation.set_to_zero();
		this->axis.set_to_zero();

		this->angular_velocity = 0.0f;

		this->axis_angle = 0.0f;

		this->initialized = false;
		this->visible = true;

		color = 0xffffffff;
		original_color = 0xffffffff;
		alpha = 1.0f;
		original_alpha = 1.0f;
	}

	inline bool is_done() {
		bool b = F_GR(state_time, life_time + start_time);
		return b;
	}

	inline bool is_active() {
		bool b = F_GR(state_time, start_time);
		return b;
	}

	inline void update_state_time(float delta) {
		state_time += delta;
	}

	inline bool update_polygon_and_check_frustrum(Polygon* p, Camera & camera) {
		if (!camera.point_in_frustrum(position.x, position.y, position.z))
			return false;
		update_polygon(p);
		return true;
	}

	inline void update_polygon(Polygon* p) {

		p->set_translate(position.x, position.y, position.z);
		p->set_scale_xyz(scale.x, scale.y, scale.z);
		p->set_color(color);
		p->set_alpha(alpha);

		p->set_angle_x(rotation.x);
		p->set_angle_y(rotation.y);
		p->set_angle_z(rotation.z);

		p->set_axis(axis.x, axis.y, axis.z, axis_angle);
	}

	inline void apply(float delta, const Camera & camera, const std::vector<ParticleStrategy*> & strategies) {
		std::vector<ParticleStrategy*>::const_iterator s_it = strategies.begin();
		std::vector<ParticleStrategy*>::const_iterator s_it_end = strategies.end();

		if (visible) {
			while (s_it != s_it_end) {
				ParticleStrategy* strategy = *s_it;
				strategy->resolve(delta, *this, camera);
				++s_it;
			}
		} else if (s_it != s_it_end) {
			ParticleStrategy* strategy = *s_it;
			strategy->resolve(delta, *this, camera);
		}
	}

	inline bool is_initialized() {
		return initialized;
	}

	inline void set_initialized(bool inilialized) {
		this->initialized = inilialized;
	}

	inline bool is_visible() {
		return visible;
	}

	inline void set_visible(bool visible) {
		this->visible = visible;
	}
};

class ParticleEmitter {
private:

	Polygon* p;
	ParticleEmitterConfig config;

	std::vector<Particle*> particles;

	std::vector<ParticleStrategy*> initial_strategies;
	std::vector<ParticleStrategy*> strategies;

	bool active;
	bool stopping;
	bool single_loop;

	void init(const ParticleEmitterConfig & config) {
		this->config = config;
		active = false;
		stopping = false;
		single_loop = false;

		p->set_axis_rotation(true);
	}

	void issue_particle(bool no_delay, const Camera & camera) {
		Particle* p = Particle::create();
		particles.push_back(p);

		float start_time = no_delay ? 0. : randf(config.min_delay, config.max_delay);
		float life_time = randf(config.min_life_time, config.max_life_time);

		p->init(start_time, life_time);
	}

public:

	ParticleEmitter() {
		p = NULL;
		single_loop = false;
		stopping = false;
		active = false;
	}

	void init_with_model(const ParticleEmitterConfig & config, const ObjModelLoader & model_loader, std::string model_name) {
		p = PolygonBuiler::create_model(model_loader, model_name, false);
		init(config);
	}

	void init_with_textured_model(const ParticleEmitterConfig & config, const ObjModelLoader & model_loader, const TextureAtlasLoader & atlas, const std::string & model_name, const std::string & texture_name) {
		p = PolygonBuiler::create_textured_model(model_loader, atlas, model_name, texture_name, true, false);
		init(config);
	}

	void init_with_shape(const ParticleEmitterConfig & config, const Box2dLoader & shape_loader, std::string shape_name, uint32_t color) {
		p = PolygonBuiler::create_shaped_sprite(shape_loader, shape_name, color);
		init(config);
	}

	void init_with_textured_shape(const ParticleEmitterConfig & config, const TextureAtlasLoader & atlas, const Box2dLoader & shape_loader, const std::string & texture_name, const std::string & shape_name) {
		p = PolygonBuiler::create_shaped_textured_sprite(atlas, shape_loader, texture_name, shape_name);
		init(config);
	}

	void init_with_sprite(const ParticleEmitterConfig & config, const TextureAtlasLoader & atlas, const std::string & texture_name, uint16_t origin_to = ORIGIN_CENTER) {
		p = PolygonBuiler::create_sprite(atlas, texture_name, true, false);
		p->set_origin_to(origin_to);
//		p->set_preserve_vert_transform(true);
		init(config);
	}

	void release() {
		DELETE(p);
		releate_all_particles();
		initial_strategies.clear();
		strategies.clear();
	}

	void releate_all_particles() {
		std::vector<Particle*>::iterator it = particles.begin();
		while (it != particles.end()) {
			Particle* particle = *it;
			Particle::release(particle);
			it = particles.erase(it);
		}
	}

	bool update(float delta, const Camera & camera) {
		if (!active)
			return false;

		if (!p) {
			LOGE("fte: ParticleEmitter: not initialized (p = NULL)!");
			return false;
		}

		if (!stopping) {
			int count = config.min_count - particles.size();
			int i = 0;
			while (i < count) {
				issue_particle(true, camera);
				i++;
			}

			count = config.max_count - particles.size();
			i = 0;
			while (i < count) {
				issue_particle(false, camera);
				i++;
			}
		}

		std::vector<Particle*>::iterator it = particles.begin();
		while (it != particles.end()) {
			Particle* particle = *it;

			bool active = particle->is_active();
			if (active) {
				if (!particle->is_initialized()) {
					particle->apply(delta, camera, initial_strategies);
					particle->set_initialized(true);
				}
				particle->apply(delta, camera, strategies);
			}

			particle->update_state_time(delta);

			if (particle->is_done()) {
				Particle::release(particle);
				it = particles.erase(it);
			} else {
				++it;
			}
		}

		if (single_loop)
			stopping = true;

//		LOGD("particles count = %d", particles.size());

		if (stopping)
			active = particles.size() > 0;

		return active;
	}

	void render(RenderCommand* command) {
		if (!active)
			return;

		std::vector<Particle*>::iterator it = particles.begin();
		while (it != particles.end()) {
			Particle* particle = *it;
			bool active = particle->is_active();
			bool visible = particle->is_visible();
			if (active && visible) {
				particle->update_polygon(p);
				command->add(p);
			}
			++it;
		}
	}

	void start(bool single_loop = false) {
		this->active = true;
		this->stopping = false;
		this->single_loop = single_loop;
	}

	void stop(bool immediate = false) {
		if (immediate) {
			this->active = false;
			this->stopping = false;
			releate_all_particles();
		} else
			this->stopping = true;
	}

	bool is_active() {
		return active;
	}

	bool is_stopping() {
		return stopping;
	}

	void add_initial_strategy(ParticleStrategy* strategy) {
		initial_strategies.push_back(strategy);
	}

	void add_strategy(ParticleStrategy* strategy) {
		strategies.push_back(strategy);
	}

	void set_count(int min_count, int max_count) {
		config.min_count = min_count;
		config.max_count = max_count;
	}

	void set_delay(float min_delay, float max_delay) {
		config.min_delay = min_delay;
		config.max_delay = max_delay;
	}

	void set_lifetime(float min_lifetime, float max_lifetime) {
		config.min_life_time = min_lifetime;
		config.max_life_time = max_lifetime;
	}
}
;

} // namespace fte

#endif /* particle_emitter_H */
