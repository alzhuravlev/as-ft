#ifndef coloredpolygon_H
#define coloredpolygon_H

#include "polygon.h"
#include "fte_utils.h"

namespace fte {

class ColoredPolygon: public Polygon {
public:
	ColoredPolygon(int vert_count) :
			Polygon(true, false, false) {
		set_vert_count(vert_count);
	}
};

} // namespace fte

#endif // coloredpolygon_H
