#ifndef fte_utils_gl_H
#define fte_utils_gl_H

#include "fte_gl.h"

namespace fte {

void printGLString(const char *name, GLenum s);
void printGLInteger(const char *name, GLenum s);
void __checkGlError__(const char* op);
GLuint loadShader(GLenum shaderType, const char* pSource);
GLuint createProgram(const char* pVertexSource, const char* pFragmentSource);

#ifdef DEBUG
#define CHECK_GL_ERROR(op) ((void)__checkGlError__(op))
#else
#define CHECK_GL_ERROR(op)
#endif

} // namespace fte

#endif /* fte_utils_gl_H */
