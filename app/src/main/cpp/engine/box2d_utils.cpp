#include "box2d_utils.h"

namespace fte {

void convert_vec3_to_b2vec2(const std::vector<Vec3> & in, std::vector<b2Vec2> & out) {
	for (int i = 0; i < in.size(); i++) {
		Vec3 v3 = in[i];
		b2Vec2 v2(v3.x, v3.y);
		out.push_back(v2);
	}
}

void attach_fixture_to_body(b2Body* body, b2FixtureDef* fd, Box2dItem* item, float scale_factor_x, float scale_factor_y) {
	for (int i = 0; i < item->polygons.size(); i++) {

		const Box2dItemPolygon & polygon_shape = item->polygons[i];

		std::vector<b2Vec2> v;

		for (int j = 0; j < polygon_shape.polygon.size(); j++) {
			float x = polygon_shape.polygon[j].x * scale_factor_x;
			float y = polygon_shape.polygon[j].y * scale_factor_y;

			b2Vec2 vec2;

			vec2.x = x;
			vec2.y = y;

			v.push_back(vec2);
		}

		b2PolygonShape shape;
		shape.Set(&v.front(), v.size());

		fd->shape = &shape;
		body->CreateFixture(fd);
	}

	for (int i = 0; i < item->cyrcles.size(); i++) {

		Box2dItemCyrcle cyrcle_shape = item->cyrcles[i];

		b2CircleShape shape;
		shape.m_p = b2Vec2(cyrcle_shape.x * scale_factor_x, cyrcle_shape.y * scale_factor_y);
		shape.m_radius = cyrcle_shape.r * scale_factor_x;

		fd->shape = &shape;
		body->CreateFixture(fd);
	}
}

void attach_edge_fixture_to_body(b2Body* body, b2FixtureDef* fd, const std::vector<b2Vec2> & points, float height) {

	b2EdgeShape shape;
	fd->shape = &shape;

	if (F_NEQ(height, 0.0f)) {
		const b2Vec2 & first1 = points.front();
		b2Vec2 first2 = points.front();
		first2.y -= height;
		shape.Set(first1, first2);
		body->CreateFixture(fd);

		const b2Vec2 & last1 = points.back();
		b2Vec2 last2 = points.back();
		last2.y -= height;
		shape.Set(last1, last2);
		body->CreateFixture(fd);
	}

	int cnt = points.size() - 1;
	for (int i = 0; i < cnt; i++) {
		const b2Vec2 & prev = points[i];
		const b2Vec2 & next = points[i + 1];

		shape.Set(prev, next);
		body->CreateFixture(fd);

		if (F_NEQ(height, 0.0f)) {
			b2Vec2 prev_bottom = points[i];
			b2Vec2 next_bottom = points[i + 1];

			prev_bottom.y -= height;
			next_bottom.y -= height;

			shape.Set(prev_bottom, next_bottom);
			body->CreateFixture(fd);
		}
	}

}

} // namespace fte
