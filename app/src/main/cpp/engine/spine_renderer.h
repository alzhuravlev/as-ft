#ifndef spine_renderer_H
#define spine_renderer_H

#include "spine/spine.h"
#include "spine/extension.h"

#include "texture_atlas_loader.h"
#include "obj_model_loader.h"
#include "command.h"
#include "camera.h"
#include "polygon_cache.h"

#include "spine_data.h"

#include <vector>
#include <map>

namespace fte {

class SpineRenderer {
private:

	spSkeleton* skeleton;
	spAnimationState* animation_state;
	SpineData* data;

public:
	SpineRenderer(SpineData* data);
	~SpineRenderer();

	void set_animation(const std::string & animation, bool loop);
	void add_animation(const std::string & animation, bool loop);
	void clear_animation();
	bool is_animation_complete();
	const char* get_current_animation_name();

	void reset_bones();

	void update_animation(float delta);
	void update(float delta, const Vec3 & translate, const Vec3 & rotate, const Vec3 & scale);
	void render(RenderCommand * command);
	void render_batches(RenderCommand * command, int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const Camera & camera);
	void render_static_batch(RenderCommand * command, TYPE_STATIC_BATCH_ID start_batch_id);
	void render_static_batch_no_data(RenderCommand * command, TYPE_STATIC_BATCH_ID start_batch_id, int shader_id, int shader_toon_id, bool enable_depth, bool enable_cull_face, const Camera & camera);

	void render_to_cache(PolygonCache* cache);
};

} // namespace fte

#endif /* spine_renderer_H */
