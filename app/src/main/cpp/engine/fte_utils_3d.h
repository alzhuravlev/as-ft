#ifndef fte_utils_3d_H
#define fte_utils_3d_H

#include "polygon_cache.h"
#include "sprite.h"
#include "fte_math.h"

namespace fte {

typedef float (*RADIUS_FUNC)(float y, float angle);

void create_tube_around_path(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int num_edges, float frame_radius, float frame_radius_amplitude, float frame_radius_cycles, const std::vector<Vec3> & angular_points, RADIUS_FUNC radius_func,
		bool loop, float v1_scale = 0.0f, float v2_scale = 1.0f);

void create_semi_tube_around_path(PolygonCache* cache, const Sprite* sprite, int num_edges, RADIUS_FUNC radius_func1, RADIUS_FUNC radius_func2, const std::vector<Vec3> & angular_points1, const std::vector<Vec3> & angular_points2, bool loop = true);

void create_shape(PolygonCache* cache, const Sprite* sprite, RADIUS_FUNC radius_func, const std::vector<Vec3> & angular_points);

void create_cylinder(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int repeat_y, int num_edges, int num_edges_for_texture, float step_v, float height, float v_scale, RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_y,
		float translate_x, float translate_y, float translate_z, float start_angle = 0.0f, float full_angle = M_2xPI);

void create_cylinder_with_holes(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_nm, int repeat_y, int num_edges, int num_edges_for_texture, float step_v, float height, float v_scale, const std::vector<std::vector<Vec3> > & holes,
		RADIUS_FUNC radius_func, RADIUS_FUNC radius_derivative_func_y, float translate_x = 0.0f, float translate_y = 0.0f, float translate_z = 0.0f, float start_angle = 0.0f, float full_angle = M_2xPI);

void create_cone(PolygonCache* cache, Sprite* sprite, int num_edges, int num_edges_for_texture, float radius_bottom, float radius_top, float height, float translate_x, float translate_y, float translate_z, bool vert_clockwise);

void create_tiled_map(PolygonCache* cache, Sprite* sprite, int repeat_x, int repeat_y, const Transformable & transform);

void create_v_half_ellipse(PolygonCache* cache, const Sprite* sprite, const Sprite* sprite_floor, int num_edges, int num_edges_for_texture, float radius_x, float radius_y, float depth, float translate_x, float translate_y, float translate_z,
		float angle_y, bool vert_clockwise);

void create_cube(PolygonCache* cache, bool use_normal = false);

void create_tetrahedron(PolygonCache* cache);

void create_sphere(PolygonCache* cache, float radius_min = 1.0f, float radius_max = 1.0f, std::vector<Polygon*>* debris = NULL, std::vector<Vec3>* directions = NULL);

void create_ship(PolygonCache* cache);
void create_jet(PolygonCache* cache);
void create_rocket(PolygonCache* cache);
void create_rocket_jet(PolygonCache* cache);

} // namespace fte

#endif /* fte_utils_3d_H */
