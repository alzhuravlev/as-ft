#ifndef fte_utils_H
#define fte_utils_H

#include "engine.h"
#include "external.h"

#include "png.h"

#include "glm/glm.hpp"

#include "fte_time.h"
#include <streambuf>
#include <string>
#include <vector>
#include <map>
#include <iostream>

namespace fte {

#ifdef DEBUG
extern int DEBUG_APPLY_TRANSFORMATIONS_COUNTER;
extern int DEBUG_APPLY_TRANSFORMATIONS_VERTEX_COUNTER;
#endif

const static glm::mat4 IDENTITY;

inline uint16_t swap_bytes(uint16_t v) {
	return ((v & 0x00FF) << 8) | ((v & 0xFF00) >> 8);
}

/**
 * float utils
 */

void unpack_color_f(const uint32_t & color, float & r, float & g, float & b, float & a);
void pack_color_f(uint32_t & color, const float & r, const float & g, const float & b, const float & a);
void pack_color_c(uint32_t & color, const uint8_t & r, const uint8_t & g, const uint8_t & b, const uint8_t & a);
void mix_color_c(const uint32_t & color1, const uint32_t & color2, float a, uint32_t & res);
void add_color_c(const uint32_t & color1, const uint32_t & color2, uint32_t & res);

inline void change_alpha(uint32_t & color, float a) {
	color &= 0x00ffffff;
	color |= (((uint32_t) (255 * a)) << 24);
}

inline void apply_alpha(uint32_t & color, float a) {
	a *= (color >> 24) / 255.0f;
	color &= 0x00ffffff;
	color |= (((uint32_t) (255 * a)) << 24);
}

#define PACK_COLOR_I(abgr) (*((float*) (&abgr)))
#define UNPACK_COLOR_I(f) (*((uint32_t*) (&(f))))

/**
 * sys time utils
 */
float get_time_diff(fte_timespec t1, fte_timespec t2);

/**
 * useful when we want to read from mem like from file (ie, getline(...))
 */
class membuf: public std::streambuf {
public:
	membuf(char* p, size_t n) {
		setg(p, p, p + n);
	}
};

class wmembuf: public std::wstreambuf {
public:
	wmembuf(char* p, size_t size) {

		wchar_t* b = (wchar_t*) p;
		wchar_t* e = (wchar_t*) p;

		e += size / sizeof(wchar_t) - 1;

		setg(b, b, e);
	}
};

/**
 * png reading utils
 */
struct read_in {
	void* buf;
	long length;
	long read_count;

	bool init(std::string file_name, FTE_ENV env) {
		read_count = 0;
		LOGD("fte read_in.init: %s", file_name.c_str());
		return load_resource_file(env, file_name, buf, length);
	}

	void read(void* data, long size) {
		char* c = (char*) buf;
		if (read_count + size > length) {
			LOGE("fte read_in.read: buffer overflow");
			return;
		}
		c += read_count;
		memcpy(data, c, size);
		read_count += size;
	}

	~read_in() {
		free(buf);
		buf = NULL;
		LOGD("fte read_in.release");
	}
};

void my_read_png(png_structp png_ptr, png_bytep data, png_size_t length);
void read_png_dim(std::string file_name, int &w, int &h, FTE_ENV env);

typedef struct {
	char tag[6]; // "PKM 10"
	uint16_t format; // Format == number of mips (== zero)
	uint16_t texWidth; // Texture dimensions, multiple of 4 (big-endian)
	uint16_t texHeight;
	uint16_t origWidth; // Original dimensions (big-endian)
	uint16_t origHeight;
} ETC1Header;

void read_pkm_dim(std::string file_name, int &w, int &h, FTE_ENV env);

/**
 * String utils
 */

bool starts_with(const std::string &s, const std::string &prefix);
bool ends_with(const std::string &s, const std::string &suffix);
void split(const std::string &s, char delim, std::vector<std::string> &elems);
void join(const std::vector<std::string> &elems, char delim, std::string &s);
void replace_file_name(const std::string & path, const std::string & new_file_name, std::string & new_path_to_file);
void remove_chars(std::string &s, const char* chars_to_remove);
void remove_chars_w(std::wstring &s, const wchar_t* chars_to_remove);
void convert_wstring_to_string(const std::wstring & in, std::string & out);
void convert_string_to_wstring(const std::string & in, std::wstring & out);

bool read_external_lines(const std::string & file_name, FTE_ENV env, std::vector<std::string> & lines);

bool read_resource_lines(const std::string & file_name, FTE_ENV env, std::vector<std::string> & lines);
bool read_resource_lines_w(const std::string & file_name, FTE_ENV env, std::vector<std::wstring> & lines);

bool read_resource_to_string(const std::string & file_name, FTE_ENV env, std::string & s);
bool read_internal_to_string(const std::string & file_name, FTE_ENV env, std::string & s);

std::string to_stringf(float f);
std::string to_string(int i, bool force_plus = false);
int fast_atoi(const char * str);
void fast_itoa(int value, std::string & out);

/**
 * Misc
 */
#define DELETE(p) {if (p) {delete p; p = NULL;}}

template<class T>
void delete_vector_elements(std::vector<T> & v) {
	for (typename std::vector<T>::const_iterator it = v.begin(); it != v.end(); ++it) {
		delete *it;
	}
	v.clear();
}

template<class K, class V>
void delete_map_values(std::map<K, V> & map) {
	for (typename std::map<K, V>::const_iterator it = map.begin(); it != map.end(); ++it) {
		V v = it->second;
		delete v;
	}
	map.clear();
}

struct Padding {
	float t, r, b, l;
};

struct Cell {
	float row, col, rows, cols;
};

} // namespace fte

#endif // fte_utils_H
