#ifndef camera_H
#define camera_H

#include "fte_utils.h"
#include "transformable.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include <math.h>

namespace fte {

class Camera: public Transformable {
private:
	Vec3 camera_position;
	Vec3 camera_look_at;

	Vec3 up;

	// for culling
	Vec3 X, Y, Z;

//	glm::quat tmp_q;

	glm::mat4 view_proj_matrix;

	void recalc_view_proj_matrix() {

		dirty = false;

		view_proj_matrix = IDENTITY;

//		if (origin_x != 0. || origin_y != 0. || origin_z != 0.) {
//			view_proj_matrix = glm::translate(view_proj_matrix, glm::vec3(origin_x, origin_y, origin_z));
//		}

// in orthographic. do not use Z-scale because we use distansing instead
// in ortho, Z scaling has no effect in view, it only make influence on z-buffer
		if (scaling)
			if (F_NEQ(scale_x, 1.0f) || F_NEQ(scale_y, 1.0f)) {
				view_proj_matrix = glm::scale(view_proj_matrix, glm::vec3(scale_x, scale_y, 1.0f));
			}

		float distance;
		if (distancing)
			distance = width / (2.0f * scale_z * tanf(fovx * 0.5f));
		else {
			// we are in ortho projection.
			// instead of Z-scaling "step out" from "point of view" by z_scale.
			// it is not allow any of our objects to get "behind camera".
			// this is expected behavior. exactly same as in perspective projection
			distance = scale_z;
		}

		float xd = 0.0f;
		float yd = 0.0f;
		float zd = distance;

		rotate_point_fast(angle_x, angle_y, angle_z, xd, yd, zd);

		camera_look_at.x = x;
		camera_look_at.y = y;
		camera_look_at.z = z;

		camera_position.x = xd + x;
		camera_position.y = yd + y;
		camera_position.z = zd + z;

		vec_sub(camera_look_at, camera_position, Z);
		vec_normalize(Z);

		vec_cross(Z, up, X);
		vec_normalize(X);

		vec_cross(X, Z, Y);
		vec_normalize(Y);

		view_proj_matrix = glm::lookAt(glm::vec3(camera_position.x, camera_position.y, camera_position.z), glm::vec3(camera_look_at.x, camera_look_at.y, camera_look_at.z), glm::vec3(up.x, up.y, up.z)) * view_proj_matrix;

		if (F_NEQ(axis_angle, 0.0f)) {

//			glm::quat q(glm::vec3(axis_x, axis_y, axis_z));
//			glm::vec4 axis = q * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

			glm::quat q = glm::rotate(glm::quat(), axis_angle, glm::vec3(axis_x, axis_y, axis_z));
			view_proj_matrix = glm::mat4_cast(q) * view_proj_matrix;
		}

//		LOGD("fte: Camera: recalc_view_proj_matrix; distance = %f", distance);

		view_proj_matrix = proj_matrix * view_proj_matrix;
	}

protected:
	glm::mat4 proj_matrix;

	float width;
	float height;

	float near;
	float far;

	float aspect;

	float fovy;
	float fovx;

	float tanx;
	float tany;

	float sphere_factor_y;
	float sphere_factor_x;

	bool scaling;
	bool distancing;

public:

	Camera() {
		up = Vec3(0.0f, 1.0f, 0.0f);
	}

	inline void update_view_proj_matrix() {
		if (dirty)
			recalc_view_proj_matrix();
	}

	const float* get_view_proj_matrix() const {
		return glm::value_ptr(view_proj_matrix);
	}

	void project(float & x, float & y, float & z, float w, float h) const {
		glm::vec3 obj(x, y, z);
		glm::vec4 vp(0.0f, 0.0f, w, h);

		glm::vec3 win = glm::project(obj, glm::mat4(), view_proj_matrix, vp);

		x = win.x;
		y = win.y;
		z = win.z;
	}

	void unproject(float & x, float & y, float w, float h) const {
		float z = 0.0f;
		unproject(x, y, z, w, h);
	}

	// works correctly only in ortho projection (((
	void unproject(float & x, float & y, float & z, float w, float h) const {

		glm::vec3 win(x, y, z);
		glm::vec4 vp(0.0f, 0.0f, w, h);

		glm::vec3 obj = glm::unProject(win, glm::mat4(), view_proj_matrix, vp);

		x = obj.x;
		y = obj.y;
		z = obj.z;
	}

	inline const Vec3 & get_camera_position() const {
		return camera_position;
	}

	inline const Vec3 & get_Z() const {
		return Z;
	}

	bool point_in_frustrum(const float & x, const float & y, const float & z) const {

		Vec3 p(x, y, z);

		float pcz, pcx, pcy, aux;

		// compute vector from camera position to p
		Vec3 v;
		vec_sub(p, camera_position, v);

		// compute and test the Z coordinate
		pcz = vec_dot(v, Z);
//		if (pcz > far || pcz < near) {
//			return false;
//		}

		// compute and test the Y coordinate
		pcy = vec_dot(v, Y);
		aux = pcz * tany;
		if (pcy > aux || pcy < -aux) {
			return false;
		}

		// compute and test the X coordinate
//		pcx = glm::dot(v, X);
//		aux = aux * aspect;
//		if (pcx > aux || pcx < -aux) {
//			return false;
//		}

		return true;
	}

	// http://www.lighthouse3d.com/tutorials/view-frustum-culling/radar-approach-implementation-ii/
	bool sphere_in_frustrum(const float & x, const float & y, const float & z, const float & radius) const {

		Vec3 p(x, y, z);

		float d;
		float pcz, pcy;

		Vec3 v;
		vec_sub(p, camera_position, v);

		pcz = vec_dot(v, Z);
		pcy = vec_dot(v, Y);
		d = sphere_factor_y * radius;
		pcz = pcz * tany;
		if (pcy > pcz + d || pcy < -pcz - d)
			return false;

		return true;
	}

	void set_up(const Vec3 & up) {
		this->dirty = this->dirty || F_NEQ(this->up.x, up.x) || F_NEQ(this->up.y, up.y) || F_NEQ(this->up.z, up.z);
		this->up.x = up.x;
		this->up.y = up.y;
		this->up.z = up.z;
	}
};

class OrthographicCamera: public Camera {
public:

	/**
	 * Depth of the frustrum will be twice of the max dimension.
	 * So we have to translate z-coord of all our objects somewhere in [-max_dist; max_dist].
	 */
	void update(int w, int h) {
		scaling = true;
		distancing = false;

		dirty = true;

		width = w;
		height = h;

		float max_dist = width > height ? width : height;
		near = 0.01f;
		far = max_dist * 2.0f;

		aspect = width / height;

		fovx = 0.0f;
		fovy = 0.0f;

		tanx = 0.0f;
		tany = 0.0f;

		sphere_factor_x = 0.0f;
		sphere_factor_x = 0.0f;

		float w_2 = 0.5f * width;
		float h_2 = 0.5f * height;

		proj_matrix = glm::ortho<float>(-w_2, w_2, -h_2, h_2, near, far);
	}
};

class PerspectiveCamera: public Camera {
public:

	/**
	 * Depth of the frustrum will be twice of the max dimension.
	 * So we have to translate z-coord of all our objects somewhere in [-max_dist; max_dist].
	 *
	 */
	void update(int w, int h, float fov = M_PI_4) {
		scaling = false;
		distancing = true;

		dirty = true;

		width = w;
		height = h;

		float max_dist = width > height ? width : height;
		near = 0.01f;
		far = max_dist * 2.0f;

		aspect = width / height;

		fovx = width < height ? atanf(tanf(fov * 0.5f) * aspect) * 2.0f : fov;
		fovy = width > height ? atanf(tanf(fov * 0.5f) / aspect) * 2.0f : fov;

		tanx = tanf(fovx * 0.5f);
		tany = tanf(fovy * 0.5f);

		sphere_factor_x = 1.0f / cos_fast(fovx);
		sphere_factor_y = 1.0f / cos_fast(fovy);

		LOGD("fte: PerspectiveCamera.update: fov = %f', fovy = %f', fovx = %f', aspect = %f", fov * M_RAD_TO_DEG, fovy * M_RAD_TO_DEG, fovx * M_RAD_TO_DEG, aspect);

		proj_matrix = glm::perspective<float>(fovy, aspect, near, far);
	}
};

} // namespace fte

#endif
