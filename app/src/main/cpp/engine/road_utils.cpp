#include "road_utils.h"

namespace fte {

void road_point_to_far_point(const Vec3 & world_point, float width, Vec3 & res) {
	Vec3 center_point(0., world_point.y, 0.);

	Vec3 dir;
	vec_sub(world_point, center_point, dir);
	vec_normalize(dir);

	vec_mul(dir, width);

	vec_add(world_point, dir, res);
}

void road_point_far_dir(const Vec3 & world_point, Vec3 & res) {
	Vec3 center_point(0., world_point.y, 0.);

	Vec3 dir;
	vec_sub(world_point, center_point, dir);
	vec_normalize(dir);

	res = dir;
}

} // namespace fte
