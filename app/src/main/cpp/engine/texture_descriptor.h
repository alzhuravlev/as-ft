#ifndef texture_descriptor_H
#define texture_descriptor_H

#include <string>
#include "external.h"

namespace fte {

struct TextureDescriptor {
	char data[32];

	bool operator==(const TextureDescriptor & td) const {
		return strcmp((char*) &data, (char*) &td.data) == 0;
	}

	bool operator!=(const TextureDescriptor & td) const {
		return strcmp((char*) &data, (char*) &td.data) != 0;
	}

	bool operator<(const TextureDescriptor & td) const {
		return strcmp((char*) &data, (char*) &td.data) > 0;
	}

//	TextureDescriptor& operator=(const std::string& s) {
//		const char* c = s.c_str();
//#ifdef DEBUG
//		if (strlen(c) + 1 > sizeof(data)) {
//			LOGE("td: texture descriptor is too big (%s)!", s.c_str());
//			return *this;
//		}
//#endif
//		strcpy((char*) &data, c);
//		return *this;
//	}

	TextureDescriptor(const std::string & file_name) {
		const char* c = file_name.c_str();
#ifdef DEBUG
		if (strlen(c) + 1 > sizeof(data)) {
			LOGE("td: texture descriptor is too big (%s)!", file_name.c_str());
			data[0] = 0;
			return;
		}
#endif
		strcpy((char*) &data, c);
	}

	TextureDescriptor() {
		data[0] = 0;
	}

	bool is_empty() const {
		return data[0] == 0;
	}

	std::string to_string() const {
		std::string s;
		const char* c = (char*) &data;
		s.assign(c);
		return s;
	}
};

} // namespace fte

#endif // texture_descriptor_H
