#ifndef queue_H
#define queue_H

#include <vector>
#include "pthread.h"
#include "time.h"
#include "fte_utils.h"

#include "external.h"

namespace fte {

template<class T>
class Queue {
private:
	int max_size;
	std::vector<T> data;

	pthread_mutex_t mutex;
	pthread_cond_t cond;

	bool produce_internal(T value) {
		if (data.size() < max_size) {
			data.push_back(value);
			return true;
		}
		return false;
	}

	bool consume_internal(T &value) {
		if (!data.empty()) {
			value = data.front();
			data.erase(data.begin());
			return true;
		}
		return false;
	}

	void consume_all_internal(T value[], int & size) {
		int i = 0;
		while (!data.empty()) {
			value[i++] = data.front();
			data.erase(data.begin());
		}
		size = i;
	}

public:

	Queue(int max_size) {
		LOGI("queue_create");
		this->max_size = max_size;
		pthread_mutex_init(&mutex, NULL);
		pthread_cond_init(&cond, NULL);
	}

	~Queue() {
		LOGI("queue_destroy");
		pthread_mutex_destroy(&mutex);
		pthread_cond_destroy(&cond);
	}

	bool produce(T value) {
		pthread_mutex_lock(&mutex);
		bool res = produce_internal(value);

		if (!res) {
			pthread_cond_wait(&cond, &mutex);
			res = produce_internal(value);
		}

		if (res)
			pthread_cond_broadcast(&cond);

		pthread_mutex_unlock(&mutex);
		return res;
	}

	bool produce_no_wait(T value) {
		pthread_mutex_lock(&mutex);
		bool res = produce_internal(value);
		if (res)
			pthread_cond_broadcast(&cond);
		pthread_mutex_unlock(&mutex);
		return res;
	}

	bool consume(T &value) {
		pthread_mutex_lock(&mutex);
		bool res = consume_internal(value);

		if (!res) {
			pthread_cond_wait(&cond, &mutex);
			res = consume_internal(value);
		}

		if (res)
			pthread_cond_broadcast(&cond);

		pthread_mutex_unlock(&mutex);
		return res;
	}

	bool consume_no_wait(T &value) {
		pthread_mutex_lock(&mutex);
		bool res = consume_internal(value);
		if (res)
			pthread_cond_broadcast(&cond);
		pthread_mutex_unlock(&mutex);
		return res;
	}

	void consume_all_no_wait(T value[], int & size) {
		pthread_mutex_lock(&mutex);
		consume_all_internal(value, size);
		pthread_cond_broadcast(&cond);
		pthread_mutex_unlock(&mutex);
	}

	void clear() {
		pthread_mutex_lock(&mutex);
		data.clear();
		pthread_mutex_unlock(&mutex);
	}

	void wake_up() {
		pthread_cond_broadcast(&cond);
	}
};

} // namespace fte

#endif // queue_H
