#ifndef external_H
#define external_H

#include "engine.h"
#include "fte_time.h"

#include <stdint.h>
#include <string>

/**
 * Platform dependent functions.
 */

extern bool load_resource_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size);

extern bool load_internal_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size);
extern bool save_internal_file(fte::FTE_ENV env, const std::string & file_name, const void* buf, const long &size);

extern bool load_external_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size);
extern bool save_external_file(fte::FTE_ENV env, const std::string & file_name, const void* buf, const long &size);

extern void render_init_display(fte::FTE_ENV env);
extern void render_term_display(fte::FTE_ENV env);
extern void render_flush(fte::FTE_ENV env);
extern void render_query_screen_size(fte::FTE_ENV env, int &w, int &h);

extern void __logi(const char* fmt, ...);
extern void __loge(const char* fmt, ...);
extern void __logw(const char* fmt, ...);
extern void __logd(const char* fmt, ...);
extern void __log_touch(const char* fmt, ...);
extern void __log_fps(int fps);

extern fte::fte_timespec get_time();

#ifdef DEBUG

#define LOGI(...) ((void)__logi(__VA_ARGS__))
#define LOGW(...) ((void)__logw(__VA_ARGS__))
#define LOGE(...) ((void)__loge(__VA_ARGS__))
#define LOGD(...) ((void)__logd(__VA_ARGS__))
#define LOG_TOUCH(...) ((void)__log_touch(__VA_ARGS__))
#define LOG_FPS(fps) ((void)__log_fps(fps))

#else

#define LOGI(...)
#define LOGW(...)
#define LOGE(...)
#define LOGD(...)
#define LOG_TOUCH(...)
#define LOG_FPS(fps)

#endif

// audio

typedef void* AUDIO_MANAGER_ENV;
typedef int SOUND_ID;
typedef int STREAM_ID;
typedef int MUSIC_ID;

extern AUDIO_MANAGER_ENV AudioManager_init(fte::FTE_ENV env);
extern void AudioManager_release(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv);
extern void AudioManager_pause(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv);
extern void AudioManager_resume(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv);

extern SOUND_ID AudioManager_create_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, const std::string & file_name);
extern MUSIC_ID AudioManager_create_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, const std::string & file_name);

extern void AudioManager_destroy_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID sound_id);
extern void AudioManager_destroy_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID music_id);

extern STREAM_ID AudioManager_play_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID id, bool loop);
extern void AudioManager_stop_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID id);

extern void AudioManager_play_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID id, bool loop);
extern void AudioManager_stop_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID id);

// ads
extern bool Ads_is_ready_to_show(fte::FTE_ENV env, const std::string & location);
extern void Ads_show(fte::FTE_ENV env, const std::string & location);

// inapps

extern bool Iap_is_purchased(fte::FTE_ENV env, const std::string & id);
extern void Iap_purchase(fte::FTE_ENV env, const std::string & id);
extern std::string Iap_get_info_title(fte::FTE_ENV env, const std::string & id);
extern std::string Iap_get_info_price(fte::FTE_ENV env, const std::string & id);

// l10n

extern std::string get_locale(fte::FTE_ENV env);

// misc

extern void rate_game(fte::FTE_ENV env);

extern void finish_application(fte::FTE_ENV env);

#endif /* external_H */
