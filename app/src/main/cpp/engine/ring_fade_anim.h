#ifndef ring_fade_anim_H
#define ring_fade_anim_H

#include "polygon.h"
#include "polygon_builder.h"
#include "anim.h"
#include "command.h"
#include "texture_atlas_loader.h"

#include <string>

namespace fte {

struct RingFadeAnimConfig {
	int point_count;

	float scale;

	float inner_radius;
	float outer_radius;

	float amplitude;

	float velocity;

	int waves_count;
	float antialiasing;

	float fade_in_duration;
	float fade_out_duration;

	RingFadeAnimConfig() {
		point_count = 32;
		inner_radius = 0.8f;
		outer_radius = 1.0f;
		scale = 0.25f;
		amplitude = 0.1f;
		velocity = 2.0f;
		waves_count = 1;
		antialiasing = 0.1f;
		fade_in_duration = 0.33f;
		fade_out_duration = 0.33f;
	}
};

class RingFadeAnim {
private:
	Polygon* p;

	VertexRingAnimation v_anim;
	AlphaAnimation a_anim;
	AccelerateInterpolator accelerate_interpolator;
	DecelerateInterpolator decelerate_interpolator;

	RingFadeAnimConfig config;

	bool active;
	bool stopping;

	void init(const RingFadeAnimConfig & config, float duration, int offset = 0) {

		this->config = config;

		v_anim.set_duration(duration);
		v_anim.set(config.amplitude, p, config.velocity, config.velocity, config.waves_count, offset);

		p->set_scale(config.scale);
		p->set_alpha(0.0f);
	}

public:

	RingFadeAnim() {
		p = NULL;
		active = false;
	}

	~RingFadeAnim() {
		DELETE(p);
	}

	void init_with_color(const RingFadeAnimConfig & config, float duration, uint32_t color) {
		uint32_t inner_color = color;
		uint32_t mid_color = color;
		uint32_t outer_color = color;

//		change_alpha(inner_color, 0.);
//		change_alpha(outer_color, 0.);

		if (F_EQ(config.antialiasing, 0.0f))
			p = PolygonBuiler::create_ring_sprite(config.point_count, config.inner_radius, config.outer_radius, inner_color, mid_color, outer_color);
		else
			p = PolygonBuiler::create_ring_sprite_antialiasing(config.point_count, config.inner_radius, config.outer_radius, inner_color, mid_color, outer_color, config.antialiasing);

		init(config, duration);
	}

	void init_with_model(const RingFadeAnimConfig & config, float duration, const ObjModelLoader & model_loader, std::string model_name) {
		p = PolygonBuiler::create_model(model_loader, model_name, true);
		init(config, duration);
	}

	void init_with_textured_shape(const RingFadeAnimConfig & config, float duration, const TextureAtlasLoader & atlas, const Box2dLoader & shape_loader, std::string texture_name, std::string shape_name) {
		p = PolygonBuiler::create_shaped_textured_sprite(atlas, shape_loader, texture_name, shape_name);
		init(config, duration, 1);
	}

	void init_with_texture(const RingFadeAnimConfig & config, float duration, const TextureAtlasLoader & atlas, std::string texture_name) {
		if (F_EQ(config.antialiasing, 0.0f))
			p = PolygonBuiler::create_textured_ring_sprite(atlas, texture_name, config.point_count, config.inner_radius, config.outer_radius, 0xffffffff);
		else
			p = PolygonBuiler::create_textured_ring_sprite_antialiasing(atlas, texture_name, config.point_count, config.inner_radius, config.outer_radius, config.antialiasing, 0xffffffff);
		init(config, duration);
	}

	void update(float delta) {
		if (!active)
			return;

		v_anim.update(p, delta);
		a_anim.update(p, delta);

		if (stopping)
			active = a_anim.is_active();
	}

	void update_position_and_angle(const Vec3 & pos, const Vec3 & angle) {
		p->set_angle_xyz(angle.x, angle.y, angle.z);
		p->set_translate(pos.x, pos.y, pos.z);
	}

	void update_position(const Vec3 & pos) {
		p->set_translate(pos.x, pos.y, pos.z);
	}

	void update_angle(const Vec3 & angle) {
		p->set_angle_xyz(angle.x, angle.y, angle.z);
	}

	void start() {
		a_anim.set_duration(config.fade_in_duration);
		a_anim.set_alpha(p->get_alpha(), 1.0f);
		a_anim.start();
		v_anim.start();
		active = true;
		stopping = false;
	}

	void stop() {
		a_anim.set_duration(config.fade_out_duration);
		a_anim.set_alpha(p->get_alpha(), 0.0f);
		a_anim.start();
		stopping = true;
	}

	float is_active() {
		return active;
	}

	void render(RenderCommand* command, bool enable_depth = false) {
		if (!active)
			return;

		command->add(p);
	}
}
;

} // namespace fte

#endif /* ring_fade_anim_H */
