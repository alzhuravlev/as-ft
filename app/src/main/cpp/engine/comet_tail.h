#ifndef comet_tail_H
#define comet_tail_H

#include "texture_atlas_loader.h"
#include "polygon.h"
#include "command.h"
#include "polygon_builder.h"
#include "camera.h"

#include <vector>

namespace fte {

class CometTail {
private:
	Polygon* p;
	float alpha;
	int point_count;
	int real_point_count;
	float min_height_2;
	float max_height_2;
	std::vector<LinearInterpolatedVec3> points;
	std::vector<Vec3> real_points;

	Vec3 eye;

	void create_polygon(Sprite* sprite, uint32_t color) {
		DELETE(p);

		p = new Polygon(true, true, false);
		p->set_texture_descriptor(sprite->get_texture_descriptor());
		p->set_preserve_vert_colors(true);
		p->set_preserve_vert_transform(true);

		int v_count = point_count * 2;
		int i_count = (point_count - 1) * 2 * 3;

		p->set_vert_count(v_count, false);
		p->set_ind_count(i_count);

		float u1 = sprite->get_u1();
		float u2 = sprite->get_u2();

		float v1 = sprite->get_v1();
		float v2 = sprite->get_v2();

		float point_count_1 = point_count - 1;

		float r, g, b, a;
		unpack_color_f(color, r, g, b, a);

		for (int i = 0; i < point_count; i++) {

			int v_offset = i * 2;

			uint32_t c = color;
			change_alpha(c, (1.0f - i / point_count_1) * a);

			p->set_vert_color(v_offset, c);
			p->set_vert_color(v_offset + 1, c);

			p->set_vert_u(v_offset, u1);
			p->set_vert_u(v_offset + 1, u2);

			if (i % 2 == 0) {
				p->set_vert_v(v_offset, v1);
				p->set_vert_v(v_offset + 1, v1);
			} else {
				p->set_vert_v(v_offset, v2);
				p->set_vert_v(v_offset + 1, v2);
			}
		}

		int i_idx = 0;
		for (int i = 0; i < point_count - 1; i++) {

			int v_offset = i * 2;

			int i0 = v_offset;
			int i1 = v_offset + 2;
			int i2 = v_offset + 2 + 1;

			int i3 = v_offset;
			int i4 = v_offset + 2 + 1;
			int i5 = v_offset + 1;

			p->set_index_value(i_idx++, i0);
			p->set_index_value(i_idx++, i1);
			p->set_index_value(i_idx++, i2);

			p->set_index_value(i_idx++, i3);
			p->set_index_value(i_idx++, i4);
			p->set_index_value(i_idx++, i5);
		}
	}

public:

	CometTail() {
		p = NULL;
		alpha = 1.0f;
		point_count = 4;
		real_point_count = 0;
		min_height_2 = 0.001f;
		max_height_2 = 0.5f;
	}

	void init_textured(const TextureAtlasLoader & atlas_loader, const std::string & texture_name, int point_count = 4, uint32_t color = 0xffffffff) {

		if (point_count < 2) {
			LOGE("fte: CometTail::init_textured: point_count must be at least 2, actual: %d", point_count);
			return;
		}

		this->point_count = point_count;
		this->points.clear();

		Sprite* s = PolygonBuiler::create_sprite(atlas_loader, texture_name, false, false);
		create_polygon(s, color);
		DELETE(s);
	}

	void release() {
		DELETE(p);
		points.clear();
		real_points.clear();
	}

	void reset() {
		points.clear();
		real_points.clear();
	}

	void update_height(float min_height, float max_height) {
		this->min_height_2 = min_height * 0.5f;
		this->max_height_2 = max_height * 0.5f;
	}

	void update_alpha(float alpha) {
		this->alpha = alpha;
	}

	void update(float delta, const Vec3 & head_position, const Camera & camera) {

		real_point_count = 0;

		if (points.size() == 0) {

			for (int i = 0; i != point_count; i++) {
				points.push_back(LinearInterpolatedVec3(head_position, head_position, alpha));
				real_points.push_back(head_position);
			}

		} else {

			Vec3 target = head_position;
			Vec3 tmp;

			for (int i = 0; i != point_count; i++) {
				LinearInterpolatedVec3 & v = points[i];
				if (i == 0) {
					v.set_current(head_position);
					v.set_target(head_position);

					real_points[real_point_count++] = head_position;
				} else {
					tmp = target;
					v.set_target(target);
					v.update(delta);
					target = v.get_current();

					if (!vec_equal(target, tmp))
						real_points[real_point_count++] = target;
				}
			}
		}

		const Vec3 & Z = camera.get_Z();
		eye.x = -Z.x;
		eye.y = -Z.y;
		eye.z = -Z.z;
	}

	void render(RenderCommand * command) {

		if (real_point_count < 2)
			return;

		int real_point_count_1 = real_point_count - 1;

		Vec3 prev_eye_cp;

		Vec3 v1;
		Vec3 v2;

		for (int i = 0; i != real_point_count; i++) {
			const Vec3 & point = real_points[i];

			Vec3 cp;

			if (i == 0) {

				const Vec3 & point_next = real_points[i + 1];
				Vec3 v;
				vec_sub(point_next, point, v);
				vec_cross(v, eye, cp);
				prev_eye_cp = cp;

			} else if (i == real_point_count_1) {

				cp = prev_eye_cp;

			} else {

				const Vec3 & point_next = real_points[i + 1];
				Vec3 v;
				vec_sub(point_next, point, v);

				Vec3 eye_cp;
				vec_cross(v, eye, eye_cp);

				vec_interpolate(eye_cp, prev_eye_cp, 0.5f, cp);

				prev_eye_cp = eye_cp;
			}

			vec_normalize(cp);

			float k = (float) i / real_point_count_1 - 1.0f;
			float h = min_height_2 + k * k * (max_height_2 - min_height_2);
			vec_mul(cp, h);

			vec_add(point, cp, v1);
			vec_sub(point, cp, v2);

			int v_offset = i * 2;
			p->set_vert_xyz(v_offset, v1.x, v1.y, v1.z);
			p->set_vert_xyz(v_offset + 1, v2.x, v2.y, v2.z);
		}

		for (int i = real_point_count; i < point_count; i++) {
			int v_offset = i * 2;
			p->set_vert_xyz(v_offset, v1.x, v1.y, v1.z);
			p->set_vert_xyz(v_offset + 1, v2.x, v2.y, v2.z);
		}

		command->add(p);
	}
}
;

} // namespace fte

#endif /* comet_tail_H */
