#ifndef polygon_builder_H
#define polygon_builder_H

#include "texture_atlas_loader.h"
#include "box2d_loader.h"
#include "obj_model_loader.h"
#include "polygon_cache.h"
#include "font_loader.h"
#include "map_utils.h"
#include "road_utils.h"
#include "fte_utils_3d.h"

#include <string>
#include <map>

namespace fte {

class PolygonBuiler {
private:

	static std::map<std::string, Polygon*> model_cache;

	static std::string model_cache_key_builder(const std::string & model_loader_file_name, const std::string & atlas_file_name, const std::string & model_name, const std::string & texture_name, bool use_color, bool use_normal, bool face_normal = false) {
		return model_loader_file_name + "$$$" + atlas_file_name + "$$$" + model_name + "$$$" + texture_name + (use_color ? "use_color" : "not_use_color") + "$$$" + (use_normal ? "use_normal" : "not_use_normal") + "$$$"
				+ (face_normal ? "face_normal" : "vertex_normal");
	}

	static void calc_uv(PageItem* item, float &u1, float &u2, float &v1, float &v2, float &ratio) {
		u1 = (float) item->x / item->page->width;
		v1 = item->page->inverse_v ? 1.0f - (float) (item->y + item->h) / item->page->height : (float) (item->y + item->h) / item->page->height;

		u2 = (float) (item->x + item->w) / item->page->width;
		v2 = item->page->inverse_v ? 1.0f - (float) item->y / item->page->height : (float) item->y / item->page->height;

		ratio = (float) item->w / item->h;
	}

	static Sprite* create_sprite_for_item(PageItem* item, bool use_color, bool use_normal) {

		float u1, v1, u2, v2;
		float ratio;

		calc_uv(item, u1, u2, v1, v2, ratio);

		Sprite* s = new Sprite(use_color, use_normal);

		s->set_texture_descriptor(TextureDescriptor(item->page->texture_file_name));

		s->set_vert_count(4);
		s->set_origin_to(ORIGIN_BOTTOM_LEFT);

		s->set_vert_uv(0, u1, v1);
		s->set_vert_uv(1, u2, v1);
		s->set_vert_uv(2, u2, v2);
		s->set_vert_uv(3, u1, v2);

		s->set_vert_xyz(0, 0.0f, 0.0f, 0.0f);
		s->set_vert_xyz(1, ratio, 0.0f, 0.0f);
		s->set_vert_xyz(2, ratio, 1.0f, 0.0f);
		s->set_vert_xyz(3, 0.0f, 1.0f, 0.0f);

		if (use_color) {
			s->set_vert_color(0, 0xffffffff);
			s->set_vert_color(1, 0xffffffff);
			s->set_vert_color(2, 0xffffffff);
			s->set_vert_color(3, 0xffffffff);
		}

		if (use_normal) {
			s->set_vert_normal(0, 0.0f, 0.0f, 1.0f);
			s->set_vert_normal(1, 0.0f, 0.0f, 1.0f);
			s->set_vert_normal(2, 0.0f, 0.0f, 1.0f);
			s->set_vert_normal(3, 0.0f, 0.0f, 1.0f);
		}

		return s;
	}

	static void update_glyph(Polygon & p, const FontChar & font_char, Sprite* sprite = NULL) {

		float u1, u2, v1, v2;

		if (sprite) {
			u1 = sprite->get_u1() + sprite->get_u_size() * font_char.u1;
			u2 = sprite->get_u1() + sprite->get_u_size() * font_char.u2;
			v1 = sprite->get_v1() + sprite->get_v_size() * font_char.v1;
			v2 = sprite->get_v1() + sprite->get_v_size() * font_char.v2;
		} else {
			u1 = font_char.u1;
			u2 = font_char.u2;
			v1 = font_char.v1;
			v2 = font_char.v2;
		}

		p.set_vert_uv(0, u1, v1);
		p.set_vert_uv(1, u2, v1);
		p.set_vert_uv(2, u2, v2);
		p.set_vert_uv(3, u1, v2);

		p.set_vert_xyz(0, font_char.x1, font_char.y1, 0.0f);
		p.set_vert_xyz(1, font_char.x2, font_char.y2, 0.0f);
		p.set_vert_xyz(2, font_char.x3, font_char.y3, 0.0f);
		p.set_vert_xyz(3, font_char.x4, font_char.y4, 0.0f);

		p.set_vert_color(0, 0xffffffff);
		p.set_vert_color(1, 0xffffffff);
		p.set_vert_color(2, 0xffffffff);
		p.set_vert_color(3, 0xffffffff);

	}

	static void calc_road_normal(const Vec3 & sp_point_prev, const Vec3 & sp_point, const Vec3 & sp_point_next, const float a, const float bend_angle, Vec3 & normal) {
		Vec3 v1;
		Vec3 v2;

		vec_sub(sp_point_prev, sp_point, v1);
		vec_sub(sp_point_next, sp_point, v2);

		vec_normalize(v1);
		vec_normalize(v2);

		// bisector
		vec_add(v1, v2, normal);
		vec_normalize(normal);

//		Vec3 cp;
//		vec_cross(v1, v2, cp);
//		if (cp.z > 0.0f)
		vec_neg(normal);

		if (F_NEQ(bend_angle, 0.0f)) {
			Vec3 z(0.0f, 0.0f, 1.0f);
			Vec3 c;
			vec_cross(normal, z, c);
			rotate_point_around_axis(c.x, c.y, c.z, bend_angle, normal.x, normal.y, normal.z);
		}

		rotate_point_fast(0.0f, a, 0.0f, normal.x, normal.y, normal.z);
	}

public:

	static Sprite* create_sprite(const TextureAtlasLoader & atlas_loader, const std::string & name, bool use_color, bool use_normal) {
		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, name);
		return items.size() == 0 ? NULL : create_sprite_for_item(*items.begin(), use_color, use_normal);
	}

	static void create_sprites(const TextureAtlasLoader & atlas_loader, const std::string & name, std::vector<Sprite*> & sprites, bool use_color, bool use_normal) {

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, name);

		if (items.size() == 0) {
			return;
		}

		for (std::vector<PageItem*>::iterator it = items.begin(); it != items.end(); it++) {
			PageItem* item = *it;
			Sprite* s = create_sprite_for_item(item, use_color, use_normal);
			sprites.push_back(s);
		}
	}

	static Polygon* create_shaped_sprite(const Box2dLoader & shape_loader, const std::string & shape_name, uint32_t color) {
		Box2dItem* shape_item = shape_loader.find_item(shape_name);
		if (shape_item == NULL)
			return NULL;

		PolygonCache tmp;
		Polygon p(true, false, false);

		int vert_count = shape_item->polygon.size();
		int ind_count = vert_count * 3;

		// center vert
		p.set_vert_count(vert_count + 1, false);
		p.set_ind_count(ind_count);

		p.set_vert_xyz(0, shape_item->origin.x, shape_item->origin.x, 0.0f);
		p.set_vert_color(0, color);

		// assign verts
		int v_idx = 1;
		for (std::vector<Vec3>::const_iterator it = shape_item->polygon.begin(); it != shape_item->polygon.end(); ++it) {
			const Vec3 & vert = *it;

			p.set_vert_xyz(v_idx, vert.x, vert.y, vert.z);
			p.set_vert_color(v_idx, color);

			v_idx++;
		}

		// build indexes
		int i_idx = 0;
		for (int i = 0; i != vert_count; i++) {
			p.set_index_value(i_idx++, 0);
			p.set_index_value(i_idx++, i + 1);
			p.set_index_value(i_idx++, (i + 1) % vert_count + 1);
		}

		tmp.add(&p);

		LOGD("fte: create_shaped_sprite: shape_name '%s' with %d vertices", shape_name.c_str(), tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		res->set_origin_xyz(shape_item->origin.x, shape_item->origin.y, 0.);
		return res;

	}

	static Polygon* create_shaped_textured_sprite(const TextureAtlasLoader & atlas_loader, const Box2dLoader & shape_loader, const std::string & texture_name, const std::string & shape_name, uint32_t color = 0xffffffff) {
		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		PageItem* page_item = *items.begin();

		Box2dItem* shape_item = shape_loader.find_item(shape_name);
		if (shape_item == NULL)
			return NULL;

		PolygonCache tmp;
		Polygon p(true, true, false);

		p.set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		int vert_count = shape_item->polygon.size();
		int ind_count = vert_count * 3;

		p.set_vert_count(vert_count + 1, false);
		p.set_ind_count(ind_count);

		float u1, u2, v1, v2, ratio;
		calc_uv(page_item, u1, u2, v1, v2, ratio);

		float u, v;
		u = u1 + (u2 - u1) * shape_item->origin.x;
		v = v1 + (v2 - v1) * shape_item->origin.y * ratio;
		p.set_vert_uv(0, u, v);

		p.set_vert_xyz(0, shape_item->origin.x, shape_item->origin.y, 0.0f);
		p.set_vert_color(0, 0xffffffff);

		// assign verts
		int v_idx = 1;
		for (std::vector<Vec3>::const_iterator it = shape_item->polygon.begin(); it != shape_item->polygon.end(); ++it) {
			const Vec3 & vert = *it;

			u = u1 + (u2 - u1) * vert.x;
			v = v1 + (v2 - v1) * vert.y * ratio;
			p.set_vert_uv(v_idx, u, v);

			p.set_vert_xyz(v_idx, vert.x, vert.y, vert.z);
			p.set_vert_color(v_idx, 0xffffffff);

			v_idx++;
		}

		// build indexes
		int i_idx = 0;
		for (int i = 0; i != vert_count; i++) {
			p.set_index_value(i_idx++, 0);
			p.set_index_value(i_idx++, i + 1);
			p.set_index_value(i_idx++, (i + 1) % vert_count + 1);
		}

		tmp.add(&p);

		LOGD("fte: create_shaped_textured_sprite: texture_name '%s', shape_name '%s' with %d vertices", texture_name.c_str(), shape_name.c_str(), tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		res->set_origin_xyz(shape_item->origin.x, shape_item->origin.y, 0.);
		res->set_color(color);
		return res;

	}

	static void create_scaled_shaped_textured_sprite(PolygonCache* cache, const TextureAtlasLoader & atlas_loader, const Box2dLoader & shape_loader, const std::string & shape_name, const std::string & texture_name, const std::string & texture_name_nm,
			const Vec3 &scale, const Vec3 & rotate, const Vec3 & translate, bool use_color, bool use_normal, bool clockwise_index_order, uint32_t color = 0xffffffff) {

		Box2dItem* shape_item = shape_loader.find_item(shape_name);
		if (shape_item == NULL) {
			LOGE("fte: create_scaled_shaped_textured_sprite: shape_name '%s', not found!", shape_name.c_str());
			return;
		}

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0) {
			LOGE("fte: create_scaled_shaped_textured_sprite: texture_name '%s', not found in atlas!", texture_name.c_str());
			return;
		}

		PageItem* page_item = *items.begin();

		float u1, u2, v1, v2, ratio;
		calc_uv(page_item, u1, u2, v1, v2, ratio);

		float u_size = u2 - u1;
		float v_size = v2 - v1;
		float u, v;
		u = u1 + u_size * 0.5f;
		v = v1 + v_size * 0.5f;

		std::vector<PageItem*> nm_items;
		atlas_loader.find_items_by_name(nm_items, texture_name_nm);

		bool use_nm_uv = use_normal && nm_items.size() != 0;

		float nm_u, nm_v;
		if (use_nm_uv) {

			PageItem* nm_page_item = *nm_items.begin();

			float nm_u1, nm_u2, nm_v1, nm_v2, nm_ratio;
			calc_uv(nm_page_item, nm_u1, nm_u2, nm_v1, nm_v2, nm_ratio);

			float nm_u_size = nm_u2 - nm_u1;
			float nm_v_size = nm_v2 - nm_v1;
			nm_u = nm_u1 + nm_u_size * 0.5f;
			nm_v = nm_v1 + nm_v_size * 0.5f;
		}

		Polygon p(use_color, true, use_normal, clockwise_index_order, use_nm_uv);
		p.set_normals_rotation(false);

		p.set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		int size = shape_item->polygon.size();
		p.set_vert_count(size);

		p.set_scale_xyz(scale.x, scale.y, scale.z);
		p.set_angle_xyz(rotate.x, rotate.y, rotate.z);
		p.set_translate(translate.x, translate.y, translate.z);

		int v_idx = 0;

		for (int i = 0; i < size; i++) {
			const Vec3 & vert = shape_item->polygon[i];
			const Vec3 & vert_prev = shape_item->polygon[i == 0 ? size - 1 : i - 1];
			const Vec3 & vert_next = shape_item->polygon[(i + 1) % size];

			p.set_vert_xyz(v_idx, vert.x, vert.y, 0.0f);
			p.set_vert_uv(v_idx, u, v);

			if (use_color)
				p.set_vert_color(v_idx, color);

			if (use_normal) {
				Vec3 normal;
				float bend_angle = clockwise_index_order ? M_PI_4 : -M_PI_4;
				calc_road_normal(vert_prev, vert, vert_next, rotate.y, bend_angle, normal);
				p.set_vert_normal(v_idx, normal.x, normal.y, normal.z);

				if (use_nm_uv) {
					p.set_vert_nm_uv(v_idx, nm_u, nm_v);

					Vec3 tangent;
					Vec3 bitangent;

					Vec3 wwp(translate.x, 0.0f, translate.z);

					vec_cross(wwp, normal, tangent);
					vec_normalize(tangent);

					vec_cross(tangent, normal, bitangent);
//					vec_neg(bitangent);
					vec_normalize(bitangent);

					p.set_vert_tangent(v_idx, tangent.x, tangent.y, tangent.z);
					p.set_vert_bitangent(v_idx, bitangent.x, bitangent.y, bitangent.z);
				}
			}

			v_idx++;
		}

		cache->add(&p);

//		for (std::vector<Box2dItemPolygon>::const_iterator it = shape_item->polygons.begin(); it != shape_item->polygons.end(); ++it) {
//			const Box2dItemPolygon & b_polygon = *it;
//
//			int size = b_polygon.polygon.size();
//			p.set_vert_count(size);
//
//			p.set_scale_xyz(scale.x, scale.y, scale.z);
//			p.set_angle_xyz(rotate.x, rotate.y, rotate.z);
//			p.set_translate(translate.x, translate.y, translate.z);
//
//			int v_idx = 0;
//
//			for (int i = 0; i != size; i++) {
//				const Vec3 & vert = b_polygon.polygon[i];
//				const Vec3 & vert_prev = b_polygon.polygon[i == 0 ? size - 1 : i - 1];
//				const Vec3 & vert_next = b_polygon.polygon[(i + 1) % size];
//
//				p.set_vert_xyz(v_idx, vert.x, vert.y, 0.0f);
//				p.set_vert_uv(v_idx, u, v);
//
//				if (use_color)
//					p.set_vert_color(v_idx, color);
//
//				if (use_normal) {
//
//					Vec3 normal;
//					float bend_angle = clockwise_index_order ? -M_PI_4 : M_PI_4;
//					calc_road_normal(vert_prev, vert, vert_next, rotate.y, bend_angle, normal);
//					p.set_vert_normal(v_idx, normal.x, normal.y, normal.z);
//				}
//
//				v_idx++;
//			}
//
//			cache->add(&p);
//		}
	}

	static Polygon* create_textured_ring_sprite(const TextureAtlasLoader & atlas_loader, const std::string & texture_name, int point_count, float inner_radius, float outer_radius, uint32_t color) {
		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		PageItem* page_item = *items.begin();

		PolygonCache tmp;
		Polygon p(true, true, false);

		p.set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		int vert_count = point_count * 2;
		int ind_count = point_count * 3 * 2;

		p.set_vert_count(vert_count);
		p.set_ind_count(ind_count);

		float u1, u2, v1, v2, ratio;
		calc_uv(page_item, u1, u2, v1, v2, ratio);

		Vec3 inner_vert;
		Vec3 outer_vert;

		// assign verts
		for (int i = 0; i < vert_count; i += 2) {

			float a = M_2xPI * i / vert_count;

			inner_vert.x = inner_radius * cos_fast(a);
			inner_vert.y = inner_radius * sin_fast(a);
			inner_vert.z = 0.0f;

			outer_vert.x = outer_radius * cos_fast(a);
			outer_vert.y = outer_radius * sin_fast(a);
			outer_vert.z = 0.0f;

			float inner_u = u1 + (u2 - u1) * (inner_vert.x + outer_radius) / outer_radius * .5;
			float inner_v = v1 + (v2 - v1) * (inner_vert.y + outer_radius) * ratio * .5 / outer_radius;

			float outer_u = u1 + (u2 - u1) * (outer_vert.x + outer_radius) / outer_radius * .5;
			float outer_v = v1 + (v2 - v1) * (outer_vert.y + outer_radius) * ratio * .5 / outer_radius;

			p.set_vert_uv(i, inner_u, inner_v);
			p.set_vert_uv(i + 1, outer_u, outer_v);

			p.set_vert_xyz(i, inner_vert.x, inner_vert.y, inner_vert.z);
			p.set_vert_xyz(i + 1, outer_vert.x, outer_vert.y, outer_vert.z);

			p.set_vert_color(i, color);
			p.set_vert_color(i + 1, color);
		}

		// build indexes
		int idx = 0;
		for (int i = 0; i < vert_count; i += 2) {
			int inner_1, inner_2, outer_1, outer_2;

			inner_1 = i;
			inner_2 = (i + 2) % vert_count;

			outer_1 = (i + 1) % vert_count;
			outer_2 = (i + 1 + 2) % vert_count;

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, inner_1);
			p.set_index_value(idx++, outer_1);

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, outer_1);
			p.set_index_value(idx++, outer_2);
		}

		tmp.add(&p);

		LOGD("fte: create_textured_ring_sprite: texture_name '%s', with %d vertices", texture_name.c_str(), tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		res->set_color(color);
		return res;

	}

	static Polygon* create_textured_ring_sprite_antialiasing(const TextureAtlasLoader & atlas_loader, const std::string & texture_name, int point_count, float inner_radius, float outer_radius, float antialiasing, uint32_t color) {
		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		PageItem* page_item = *items.begin();

		PolygonCache tmp;
		Polygon p(true, true, false);
		p.set_preserve_vert_colors(true);

		p.set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		uint32_t border_color = color;
		change_alpha(border_color, 0.0f);

		int vert_count = point_count * 4;
		int ind_count = point_count * 3 * 2 * 3;

		float mid_inner_radius = inner_radius + (outer_radius - inner_radius) * antialiasing;
		float mid_outer_radius = inner_radius + (outer_radius - inner_radius) * (1. - antialiasing);

		p.set_vert_count(vert_count);
		p.set_ind_count(ind_count);

		float u1, u2, v1, v2, ratio;
		calc_uv(page_item, u1, u2, v1, v2, ratio);

		Vec3 inner_vert;
		Vec3 outer_vert;
		Vec3 mid_inner_vert;
		Vec3 mid_outer_vert;

// assign verts
		for (int i = 0; i < vert_count; i += 4) {

			float a = M_2xPI * i / vert_count;

			inner_vert.x = inner_radius * cos_fast(a);
			inner_vert.y = inner_radius * sin_fast(a);
			inner_vert.z = 0.;

			mid_inner_vert.x = mid_inner_radius * cos_fast(a);
			mid_inner_vert.y = mid_inner_radius * sin_fast(a);
			mid_inner_vert.z = 0.;

			mid_outer_vert.x = mid_outer_radius * cos_fast(a);
			mid_outer_vert.y = mid_outer_radius * sin_fast(a);
			mid_outer_vert.z = 0.;

			outer_vert.x = outer_radius * cos_fast(a);
			outer_vert.y = outer_radius * sin_fast(a);
			outer_vert.z = 0.;

			float inner_u = u1 + (u2 - u1) * (inner_vert.x + outer_radius) / outer_radius * .5;
			float inner_v = v1 + (v2 - v1) * (inner_vert.y + outer_radius) * ratio * .5 / outer_radius;

			float mid_inner_u = u1 + (u2 - u1) * (mid_inner_vert.x + outer_radius) / outer_radius * .5;
			float mid_inner_v = v1 + (v2 - v1) * (mid_inner_vert.y + outer_radius) * ratio * .5 / outer_radius;

			float mid_outer_u = u1 + (u2 - u1) * (mid_outer_vert.x + outer_radius) / outer_radius * .5;
			float mid_outer_v = v1 + (v2 - v1) * (mid_outer_vert.y + outer_radius) * ratio * .5 / outer_radius;

			float outer_u = u1 + (u2 - u1) * (outer_vert.x + outer_radius) / outer_radius * .5;
			float outer_v = v1 + (v2 - v1) * (outer_vert.y + outer_radius) * ratio * .5 / outer_radius;

			p.set_vert_uv(i, inner_u, inner_v);
			p.set_vert_uv(i + 1, mid_inner_u, mid_inner_v);
			p.set_vert_uv(i + 2, mid_outer_u, mid_outer_v);
			p.set_vert_uv(i + 3, outer_u, outer_v);

			p.set_vert_xyz(i, inner_vert.x, inner_vert.y, inner_vert.z);
			p.set_vert_xyz(i + 1, mid_inner_vert.x, mid_inner_vert.y, mid_inner_vert.z);
			p.set_vert_xyz(i + 2, mid_outer_vert.x, mid_outer_vert.y, mid_outer_vert.z);
			p.set_vert_xyz(i + 3, outer_vert.x, outer_vert.y, outer_vert.z);

			p.set_vert_color(i, border_color);
			p.set_vert_color(i + 1, color);
			p.set_vert_color(i + 2, color);
			p.set_vert_color(i + 3, border_color);
		}

// build indexes
		int idx = 0;
		for (int i = 0; i < vert_count; i += 4) {
			int inner_1, inner_2, outer_1, outer_2;
			int mid_inner_1, mid_inner_2, mid_outer_1, mid_outer_2;

			inner_1 = i;
			inner_2 = (i + 4) % vert_count;

			mid_inner_1 = (i + 1) % vert_count;
			mid_inner_2 = (i + 1 + 4) % vert_count;

			mid_outer_1 = (i + 2) % vert_count;
			mid_outer_2 = (i + 2 + 4) % vert_count;

			outer_1 = (i + 3) % vert_count;
			outer_2 = (i + 3 + 4) % vert_count;

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, inner_1);
			p.set_index_value(idx++, mid_inner_1);

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, mid_inner_1);
			p.set_index_value(idx++, mid_inner_2);

			p.set_index_value(idx++, mid_inner_2);
			p.set_index_value(idx++, mid_inner_1);
			p.set_index_value(idx++, mid_outer_1);

			p.set_index_value(idx++, mid_inner_2);
			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, mid_outer_2);

			p.set_index_value(idx++, mid_outer_2);
			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, outer_2);

			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, outer_1);
			p.set_index_value(idx++, outer_2);
		}

		tmp.add(&p);

		LOGD("fte: create_textured_ring_sprite_antialiasing: texture_name '%s', with %d vertices", texture_name.c_str(), tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		res->set_preserve_vert_colors(true);
		return res;

	}

	static Polygon* create_ring_sprite_antialiasing(int point_count, float inner_radius, float outer_radius, uint32_t inner_color, uint32_t mid_color, uint32_t outer_color, float antialiasing) {

		PolygonCache tmp;
		Polygon p(true, false, false);
		p.set_preserve_vert_colors(true);

		int vert_count = point_count * 4;
		int ind_count = point_count * 3 * 2 * 3;

		float mid_inner_radius = inner_radius + (outer_radius - inner_radius) * antialiasing;
		float mid_outer_radius = inner_radius + (outer_radius - inner_radius) * (1. - antialiasing);

		p.set_vert_count(vert_count);
		p.set_ind_count(ind_count);

		Vec3 inner_vert;
		Vec3 outer_vert;
		Vec3 mid_inner_vert;
		Vec3 mid_outer_vert;

// assign verts
		for (int i = 0; i < vert_count; i += 4) {

			float a = M_2xPI * i / vert_count;

			inner_vert.x = inner_radius * cos_fast(a);
			inner_vert.y = inner_radius * sin_fast(a);
			inner_vert.z = 0.;

			mid_inner_vert.x = mid_inner_radius * cos_fast(a);
			mid_inner_vert.y = mid_inner_radius * sin_fast(a);
			mid_inner_vert.z = 0.;

			mid_outer_vert.x = mid_outer_radius * cos_fast(a);
			mid_outer_vert.y = mid_outer_radius * sin_fast(a);
			mid_outer_vert.z = 0.;

			outer_vert.x = outer_radius * cos_fast(a);
			outer_vert.y = outer_radius * sin_fast(a);
			outer_vert.z = 0.;

			p.set_vert_xyz(i, inner_vert.x, inner_vert.y, inner_vert.z);
			p.set_vert_xyz(i + 1, mid_inner_vert.x, mid_inner_vert.y, mid_inner_vert.z);
			p.set_vert_xyz(i + 2, mid_outer_vert.x, mid_outer_vert.y, mid_outer_vert.z);
			p.set_vert_xyz(i + 3, outer_vert.x, outer_vert.y, outer_vert.z);

			p.set_vert_color(i, inner_color);
			p.set_vert_color(i + 1, mid_color);
			p.set_vert_color(i + 2, mid_color);
			p.set_vert_color(i + 3, outer_color);
		}

// build indexes
		int idx = 0;
		for (int i = 0; i < vert_count; i += 4) {
			int inner_1, inner_2, outer_1, outer_2;
			int mid_inner_1, mid_inner_2, mid_outer_1, mid_outer_2;

			inner_1 = i;
			inner_2 = (i + 4) % vert_count;

			mid_inner_1 = (i + 1) % vert_count;
			mid_inner_2 = (i + 1 + 4) % vert_count;

			mid_outer_1 = (i + 2) % vert_count;
			mid_outer_2 = (i + 2 + 4) % vert_count;

			outer_1 = (i + 3) % vert_count;
			outer_2 = (i + 3 + 4) % vert_count;

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, inner_1);
			p.set_index_value(idx++, mid_inner_1);

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, mid_inner_1);
			p.set_index_value(idx++, mid_inner_2);

			p.set_index_value(idx++, mid_inner_2);
			p.set_index_value(idx++, mid_inner_1);
			p.set_index_value(idx++, mid_outer_1);

			p.set_index_value(idx++, mid_inner_2);
			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, mid_outer_2);

			p.set_index_value(idx++, mid_outer_2);
			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, outer_2);

			p.set_index_value(idx++, mid_outer_1);
			p.set_index_value(idx++, outer_1);
			p.set_index_value(idx++, outer_2);
		}

		tmp.add(&p);

		LOGD("fte: create_ring_sprite_antialiasing: with %d vertices", tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		res->set_preserve_vert_colors(true);
		return res;
	}

	static Polygon* create_ring_sprite(int point_count, float inner_radius, float outer_radius, uint32_t inner_color, uint32_t mid_color, uint32_t outer_color) {

		PolygonCache tmp;

		Polygon p(true, false, false);

		int vert_count = point_count * 3;
		int ind_count = point_count * 3 * 2 * 2;

		p.set_vert_count(vert_count);
		p.set_ind_count(ind_count);

		float mid_radius = inner_radius + (outer_radius - inner_radius) * .5;

		Vec3 inner_vert;
		Vec3 mid_vert;
		Vec3 outer_vert;

// assign verts
		for (int i = 0; i < vert_count; i += 3) {

			float a = M_2xPI * i / vert_count;

			inner_vert.x = inner_radius * cos_fast(a);
			inner_vert.y = inner_radius * sin_fast(a);
			inner_vert.z = 0.;

			mid_vert.x = mid_radius * cos_fast(a);
			mid_vert.y = mid_radius * sin_fast(a);
			mid_vert.z = 0.;

			outer_vert.x = outer_radius * cos_fast(a);
			outer_vert.y = outer_radius * sin_fast(a);
			outer_vert.z = 0.;

			p.set_vert_xyz(i, inner_vert.x, inner_vert.y, inner_vert.z);
			p.set_vert_xyz(i + 1, mid_vert.x, mid_vert.y, mid_vert.z);
			p.set_vert_xyz(i + 2, outer_vert.x, outer_vert.y, outer_vert.z);

			p.set_vert_color(i, inner_color);
			p.set_vert_color(i + 1, mid_color);
			p.set_vert_color(i + 2, outer_color);
		}

// build indexes
		int idx = 0;
		for (int i = 0; i < vert_count; i += 3) {
			int inner_1, inner_2, outer_1, outer_2;
			int mid_1, mid_2;

			inner_1 = i;
			inner_2 = (i + 3) % vert_count;

			mid_1 = (i + 1) % vert_count;
			mid_2 = (i + 1 + 3) % vert_count;

			outer_1 = (i + 2) % vert_count;
			outer_2 = (i + 2 + 3) % vert_count;

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, inner_1);
			p.set_index_value(idx++, mid_1);

			p.set_index_value(idx++, inner_2);
			p.set_index_value(idx++, mid_1);
			p.set_index_value(idx++, mid_2);

			p.set_index_value(idx++, mid_2);
			p.set_index_value(idx++, mid_1);
			p.set_index_value(idx++, outer_1);

			p.set_index_value(idx++, mid_2);
			p.set_index_value(idx++, outer_1);
			p.set_index_value(idx++, outer_2);
		}

		tmp.add(&p);

		LOGD("fte: create_ring_sprite: with %d vertices", tmp.get_vert_count());

		Polygon* res = tmp.create_composite();
		return res;
	}

	static Polygon* create_model(const ObjModelLoader & model_loader, const std::string & model_name, bool use_normal, bool invert_normals = false) {

		std::string key = model_cache_key_builder(model_loader.get_file_name(), "no_atlas", model_name, "no_texture", true, use_normal);

		std::map<std::string, Polygon*>::iterator cache_it = model_cache.find(key);
		if (cache_it != model_cache.end()) {
			Polygon* clone = new Polygon(*cache_it->second);
			return clone;
		}

		ObjModelItem* item = model_loader.find_item(model_name);
		if (item == NULL)
			return NULL;

		Polygon* p = new Polygon(true, false, use_normal);

		std::map<FaceItem, int, FaceItemCompare> m;
		int ind_count;
		item->collapse_same_verts(m, ind_count);

		p->set_vert_count(m.size(), false);
		p->set_ind_count(ind_count);

// assign verts
		for (std::map<FaceItem, int, FaceItemCompare>::const_iterator it = m.begin(); it != m.end(); ++it) {
			const FaceItem & face_item = it->first;
			const int v_ind = it->second;

			const Vertex & v = model_loader.getVertex(face_item.v_index);
			p->set_vert_xyz(v_ind, v.x, v.y, v.z);
			p->set_vert_color(v_ind, 0xffffffff);

			if (use_normal) {
				const VertexN & vn = model_loader.getVertexN(face_item.vn_index);
				Vec3 n(vn.x, vn.y, vn.z);
				if (invert_normals)
					vec_neg(n);
				vec_normalize(n);
				p->set_vert_normal(v_ind, n.x, n.y, n.z);
			}
		}

// build indexes
		int idx = 0;
		for (std::vector<Face>::const_iterator it = item->faces.begin(), end = item->faces.end(); it != end; ++it) {
			const Face &face = *it;

			int face_vert_count = face.face_items.size();
			for (int i = 1, end = face_vert_count - 1; i < end; i++) {
				p->set_index_value(idx++, m[face.face_items[0]]);
				p->set_index_value(idx++, m[face.face_items[i]]);
				p->set_index_value(idx++, m[face.face_items[i + 1]]);
			}
		}

		LOGD("fte: create_model: create model '%s' with %d vertices", model_name.c_str(), p->get_vert_count());

		Polygon* cached_copy = new Polygon(*p);
		model_cache[key] = cached_copy;

		return p;
	}

	static Polygon* create_textured_model_direct(const std::string & file_name, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, FTE_ENV env) {

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		float u1, v1, u2, v2;
		float ratio;

		PageItem* page_item = *items.begin();

		calc_uv(page_item, u1, u2, v1, v2, ratio);

		void* buf;
		long int size;

		load_resource_file(env, file_name, buf, size);

		Buffer b;
		b.set_data(buf, size);

		int num_components;
		b.read_int(num_components);

		int vert_count;
		b.read_int(vert_count);

		Polygon* p = new Polygon(true, true, true);

		p->set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		p->set_vert_count(vert_count, false);

		for (int i = 0; i < vert_count; i++) {
			float x, y, z;
			b.read_float(x);
			b.read_float(y);
			b.read_float(z);
			p->set_vert_xyz(i, x, y, z);

			float u, v;
			b.read_float(u);
			b.read_float(v);
			u = u1 + (u2 - u1) * u;
			v = v1 + (v2 - v1) * v;
			p->set_vert_uv(i, u, v);

			p->set_vert_color(i, 0xffffffff);

			float n;
			b.read_float(n);
			uint32_t nc = *((uint32_t*) (&n));
			float na, nx, ny, nz;
			unpack_color_f(nc, nx, ny, nz, na);
			nx = nx * 2.0f - 1.0f;
			ny = ny * 2.0f - 1.0f;
			nz = nz * 2.0f - 1.0f;
			p->set_vert_normal(i, nx, ny, nz);
		}

		int ind_count;
		b.read_int(ind_count);

		p->set_ind_count(ind_count);
		void* ind_ptr;
		b.read(ind_ptr, ind_count * sizeof(short));
		p->assign_ind_data(ind_ptr);

		LOGD("fte: create_textured_model_direct: '%s', texture:'%s', vertices=%d, indexes=%d ", file_name.c_str(), texture_name.c_str(), p->get_vert_count(), p->get_ind_count());

		return p;
	}

	static Polygon* create_textured_model(const ObjModelLoader & model_loader, const TextureAtlasLoader & atlas_loader, const std::string & model_name, const std::string & texture_name, bool use_color, bool use_normal, bool invert_normals = false) {

		std::string key = model_cache_key_builder(model_loader.get_file_name(), atlas_loader.get_file_name(), model_name, texture_name, use_color, use_normal);

		std::map<std::string, Polygon*>::iterator cache_it = model_cache.find(key);
		if (cache_it != model_cache.end()) {
			Polygon* clone = new Polygon(*cache_it->second);
			return clone;
		}

		ObjModelItem* item = model_loader.find_item(model_name);
		if (item == NULL)
			return NULL;

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		float u1, v1, u2, v2;
		float ratio;

		PageItem* page_item = *items.begin();

		calc_uv(page_item, u1, u2, v1, v2, ratio);

		Polygon* p = new Polygon(use_color, true, use_normal);

		p->set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		std::map<FaceItem, int, FaceItemCompare> m;
		int ind_count;
		item->collapse_same_verts(m, ind_count);

		p->set_vert_count(m.size(), false);
		p->set_ind_count(ind_count);

// assign verts
		for (std::map<FaceItem, int, FaceItemCompare>::const_iterator it = m.begin(); it != m.end(); ++it) {
			const FaceItem & face_item = it->first;
			const int v_ind = it->second;

			const Vertex & vert = model_loader.getVertex(face_item.v_index);
			p->set_vert_xyz(v_ind, vert.x, vert.y, vert.z);
			if (use_color)
				p->set_vert_color(v_ind, 0xffffffff);

			const VertexT & vt = model_loader.getVertexT(face_item.vt_index);
			float u = u1 + (u2 - u1) * vt.u;
			float v = v1 + (v2 - v1) * vt.v;
			p->set_vert_uv(v_ind, u, v);

			if (use_normal) {
				const VertexN & vn = model_loader.getVertexN(face_item.vn_index);
				Vec3 n(vn.x, vn.y, vn.z);
				if (invert_normals)
					vec_neg(n);
				vec_normalize(n);
				p->set_vert_normal(v_ind, n.x, n.y, n.z);
			}
		}

// build indexes
		int idx = 0;
		for (std::vector<Face>::const_iterator it = item->faces.begin(), end = item->faces.end(); it != end; ++it) {
			const Face &face = *it;

			int face_vert_count = face.face_items.size();
			for (int i = 1, end = face_vert_count - 1; i < end; i++) {
				p->set_index_value(idx++, m[face.face_items[0]]);
				p->set_index_value(idx++, m[face.face_items[i]]);
				p->set_index_value(idx++, m[face.face_items[i + 1]]);
			}
		}

		LOGD("fte: create_textured_model: '%s','%s' vertices=%d, indexes=%d ", texture_name.c_str(), model_name.c_str(), p->get_vert_count(), p->get_ind_count());

		Polygon* cached_copy = new Polygon(*p);
		model_cache[key] = cached_copy;

		return p;
	}

	static Polygon* create_textured_model_face_normal(const ObjModelLoader & model_loader, const TextureAtlasLoader & atlas_loader, const std::string & model_name, const std::string & texture_name, bool use_color, bool invert_normals = false) {

		std::string key = model_cache_key_builder(model_loader.get_file_name(), atlas_loader.get_file_name(), model_name, texture_name, use_color, true, true);

		std::map<std::string, Polygon*>::iterator cache_it = model_cache.find(key);
		if (cache_it != model_cache.end()) {
			Polygon* clone = new Polygon(*cache_it->second);
			return clone;
		}

		ObjModelItem* item = model_loader.find_item(model_name);
		if (item == NULL)
			return NULL;

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0)
			return NULL;

		float u1, v1, u2, v2;
		float ratio;

		PageItem* page_item = *items.begin();

		calc_uv(page_item, u1, u2, v1, v2, ratio);

		Polygon* p = new Polygon(use_color, true, true);

		p->set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		p->set_vert_count(item->faces.size() * 3);

		for (std::vector<Face>::const_iterator it = item->faces.begin(), end = item->faces.end(); it != end; ++it) {
			const Face & face = *it;

			Vec3 normal;
			{
				const Vertex & vert0 = model_loader.getVertex(face.face_items[0].v_index);
				const Vertex & vert1 = model_loader.getVertex(face.face_items[1].v_index);
				const Vertex & vert2 = model_loader.getVertex(face.face_items[2].v_index);

				Vec3 v0(vert0.x, vert0.y, vert0.z);
				Vec3 v1(vert1.x, vert1.y, vert1.z);
				Vec3 v2(vert2.x, vert2.y, vert2.z);

				vec_cross(v0, v1, v2, normal);
				vec_normalize(normal);
				if (invert_normals)
					vec_neg(normal);
			}

			int v_ind = 0;
			for (std::vector<FaceItem>::const_iterator face_item_it = face.face_items.begin(), face_item_end = face.face_items.end(); face_item_it != face_item_end; ++face_item_it) {
				const FaceItem & face_item = *face_item_it;

				const Vertex & vert = model_loader.getVertex(face_item.v_index);
				p->set_vert_xyz(v_ind, vert.x, vert.y, vert.z);
				if (use_color)
					p->set_vert_color(v_ind, 0xffffffff);

				const VertexT & vt = model_loader.getVertexT(face_item.vt_index);
				float u = u1 + (u2 - u1) * vt.u;
				float v = v1 + (v2 - v1) * vt.v;
				p->set_vert_uv(v_ind, u, v);

				p->set_vert_normal(v_ind, normal.x, normal.y, normal.z);

				v_ind++;
			}
		}

		LOGD("fte: create_textured_model_face_normal: '%s','%s' with %d vertices", texture_name.c_str(), model_name.c_str(), p->get_vert_count());

		Polygon* cached_copy = new Polygon(*p);
		model_cache[key] = cached_copy;

		return p;
	}

	static Polygon* create_model_face_normal(const ObjModelLoader & model_loader, const std::string & model_name) {

		std::string key = model_cache_key_builder(model_loader.get_file_name(), "no_atlas", model_name, "no_texture", true, true, true);

		std::map<std::string, Polygon*>::iterator cache_it = model_cache.find(key);
		if (cache_it != model_cache.end()) {
			Polygon* clone = new Polygon(*cache_it->second);
			return clone;
		}

		ObjModelItem* item = model_loader.find_item(model_name);
		if (item == NULL)
			return NULL;

		PolygonCache pc;

		Polygon* p = new Polygon(true, false, true);

		for (std::vector<Face>::const_iterator it = item->faces.begin(), end = item->faces.end(); it != end; ++it) {
			const Face & face = *it;

			p->set_vert_count(face.face_items.size());

			Vec3 normal;
			{
				const Vertex & vert0 = model_loader.getVertex(face.face_items[0].v_index);
				const Vertex & vert1 = model_loader.getVertex(face.face_items[1].v_index);
				const Vertex & vert2 = model_loader.getVertex(face.face_items[2].v_index);

				Vec3 v0(vert0.x, vert0.y, vert0.z);
				Vec3 v1(vert1.x, vert1.y, vert1.z);
				Vec3 v2(vert2.x, vert2.y, vert2.z);

				vec_cross(v0, v1, v2, normal);
				vec_normalize(normal);
			}

			int v_ind = 0;
			for (std::vector<FaceItem>::const_iterator face_item_it = face.face_items.begin(), face_item_end = face.face_items.end(); face_item_it != face_item_end; ++face_item_it) {
				const FaceItem & face_item = *face_item_it;

				const Vertex & vert = model_loader.getVertex(face_item.v_index);
				p->set_vert_xyz(v_ind, vert.x, vert.y, vert.z);
				p->set_vert_color(v_ind, 0xffffffff);

				p->set_vert_normal(v_ind, normal.x, normal.y, normal.z);

				v_ind++;
			}

			pc.add(p);
		}

		DELETE(p);
		p = pc.create_composite();

		LOGD("fte: create_model_face_normal: '%s' with %d vertices", model_name.c_str(), p->get_vert_count());

		Polygon* cached_copy = new Polygon(*p);
		model_cache[key] = cached_copy;

		return p;
	}

	static void create_textured_road(const Box2dLoader & shape_loader, const std::string & shape_name, const TextureAtlasLoader & atlas_loader, const std::string & texture_name, const std::string & texture_name_side, const std::string & texture_name_nm,
			const std::string & texture_name_side_nm, PolygonCache* cache, RADIUS_FUNC radius_func, float road_width, float road_height, const std::vector<Vec3> & angular_points, bool use_color = true, bool use_normal = true, bool stub_on_left = false,
			bool stub_on_right = false, bool zero_in_middle = false, float amplitude = 0.0f, float cycles = 1.0f, float amplitude_incline = 0.0f, float cycles_incline = 1.0f, float u1_scale = 0.0f, float u2_scale = 1.0f) {

		if (angular_points.size() < 2) {
			LOGE("create_textured_road: angular_points size must be at least 2! actual %d", angular_points.size());
			return;
		}

		std::vector<PageItem*> items;
		atlas_loader.find_items_by_name(items, texture_name);
		if (items.size() == 0) {
			LOGE("create_textured_road: texture_name '%s' not found in atlas", texture_name.c_str());
			return;
		}

		float u1, v1, u2, v2;
		float ratio;
		PageItem* page_item = *items.begin();
		calc_uv(page_item, u1, u2, v1, v2, ratio);
		float u_size = u2 - u1;
		float v_size = v2 - v1;
		u1 += u_size * u1_scale;
		u_size *= (u2_scale - u1_scale);

		std::vector<PageItem*> nm_items;
		atlas_loader.find_items_by_name(nm_items, texture_name_nm);
		bool use_uv_nm = use_normal && nm_items.size() != 0;

		float nm_u1, nm_v1, nm_u2, nm_v2;
		float nm_ratio;
		float nm_u_size;
		float nm_v_size;
		if (use_uv_nm) {
			PageItem* nm_page_item = *nm_items.begin();
			calc_uv(nm_page_item, nm_u1, nm_u2, nm_v1, nm_v2, nm_ratio);
			nm_u_size = nm_u2 - nm_u1;
			nm_v_size = nm_v2 - nm_v1;
			nm_u1 += nm_u_size * u1_scale;
			nm_u_size *= (u2_scale - u1_scale);
		}

		const Box2dItem* shape_item = shape_loader.find_item(shape_name);
		int num_edges = shape_item->polygon.size();

		// calc shape length
		float shape_length = 0.0f;
		for (int i_sp = 0; i_sp != num_edges; ++i_sp) {
			const Vec3 & sp_point = shape_item->polygon[i_sp];
			const Vec3 & sp_point_next = shape_item->polygon[(i_sp + 1) % num_edges];
			float l = vec_len(sp_point, sp_point_next);
			shape_length += l;
		}

		// calc road length
		const Vec3 & ap_first = angular_points.front();
		const Vec3 & ap_last = angular_points.back();
		const float ap_length_x = ap_last.x - ap_first.x;

		const int ap_count = angular_points.size();

		const int v_count_in_row = num_edges + 1;
		const int v_count = v_count_in_row * ap_count;
		const int i_count = (ap_count - 1) * num_edges * 3 * 2;

		Polygon p(use_color, true, use_normal, false, use_uv_nm);
		p.set_vert_count(v_count, false);
		p.set_ind_count(i_count);
		p.set_fixed_rotation(true);

		p.set_texture_descriptor(TextureDescriptor(page_item->page->texture_file_name));

		int v_idx = 0;

		Vec3 to_scale;
		Vec3 to_rotate;
		Vec3 to_translate;

		for (int i_ap = 0; i_ap != ap_count; ++i_ap) {
			const Vec3 & ap_point = angular_points[i_ap];

			Vec3 wp_point;
			angular_point_to_world_point(ap_point, wp_point, radius_func, amplitude * cos_fast(ap_point.x * cycles));

			float sp_length_accum = 0.0f;

			to_scale.x = road_width;
			to_scale.y = road_height;
			to_scale.z = 1.0f;

			to_rotate.x = 0.0f;
			to_rotate.y = -ap_point.x + M_PI_2;
			to_rotate.z = amplitude_incline * cos_fast(cycles_incline * ap_point.x);

			to_translate.x = wp_point.x;
			to_translate.y = wp_point.y;
			to_translate.z = wp_point.z;

			bool is_left_stub = stub_on_left && i_ap == 0;
			bool is_right_stub = stub_on_right && i_ap == ap_count - 1;

			if (is_left_stub || is_right_stub)
				create_scaled_shaped_textured_sprite(cache, atlas_loader, shape_loader, shape_name, texture_name_side, texture_name_side_nm, to_scale, to_rotate, to_translate, use_color, use_normal, is_left_stub);

			for (int i_sp = 0; i_sp != v_count_in_row; ++i_sp) {

				int sp_idx = i_sp % num_edges;
				const Vec3 & sp_point = shape_item->polygon[sp_idx];
				const Vec3 & sp_point_prev = shape_item->polygon[sp_idx == 0 ? num_edges - 1 : sp_idx - 1];
				const Vec3 & sp_point_next = shape_item->polygon[(sp_idx + 1) % num_edges];

				Vec3 wp;

				wp.x = sp_point.x * to_scale.x;
				if (zero_in_middle && i_ap > 0 && i_ap < ap_count - 1 && sp_point.x < 0.0f && sp_point.y > 0.0f) {
					if (i_ap > 2 && i_ap < ap_count - 3)
						wp.y = 0.0f * to_scale.y;
					else if (i_ap > 1 && i_ap < ap_count - 2)
						wp.y = 0.25f * sp_point.y * to_scale.y;
					else
						wp.y = 0.5f * sp_point.y * to_scale.y;
				} else
					wp.y = sp_point.y * to_scale.y;
				wp.z = 0.0f;

				rotate_point_fast(to_rotate.x, to_rotate.y, to_rotate.z, wp.x, wp.y, wp.z);

				wp.x += to_translate.x;
				wp.y += to_translate.y;
				wp.z += to_translate.z;

				p.set_vert_xyz(v_idx, wp.x, wp.y, wp.z);

				const float u_scale = (ap_point.x - ap_first.x) / ap_length_x;
				const float v_scale = sp_length_accum / shape_length;

				const float u = u1 + u_size * u_scale;
				const float v = v1 + v_size * v_scale;
				p.set_vert_uv(v_idx, u, v);

				if (use_color)
					p.set_vert_color(v_idx, 0xffffffff);

				if (use_normal) {

					Vec3 normal;

					float bend_angle = 0.0f;
					if (is_left_stub)
						bend_angle = M_PI_4;
					else if (is_right_stub)
						bend_angle = -M_PI_4;

					calc_road_normal(sp_point_prev, sp_point, sp_point_next, to_rotate.y, bend_angle, normal);
					p.set_vert_normal(v_idx, normal.x, normal.y, normal.z);

					if (use_uv_nm) {
						float nm_u = nm_u1 + nm_u_size * u_scale;
						float nm_v = nm_v1 + nm_v_size * v_scale;
						p.set_vert_nm_uv(v_idx, nm_u, nm_v);

						Vec3 tangent;
						Vec3 bitangent;

						Vec3 wwp(wp.x, 0.0f, wp.z);

						vec_cross(wwp, normal, tangent);
						vec_normalize(tangent);

						vec_cross(tangent, normal, bitangent);
						vec_neg(bitangent);
						vec_normalize(bitangent);

						p.set_vert_tangent(v_idx, tangent.x, tangent.y, tangent.z);
						p.set_vert_bitangent(v_idx, bitangent.x, bitangent.y, bitangent.z);
					}
				}

				v_idx++;
				sp_length_accum += vec_len(sp_point, sp_point_next);
			}
		}

		int i_idx = 0;

		for (int i = 0; i != ap_count - 1; ++i) {
			for (int j = 0; j != num_edges; ++j) {

				int i_curr1 = j;
				int i_curr2 = j + 1;

				int i_next1 = j;
				int i_next2 = j + 1;

				p.set_index_value(i_idx++, (i * (v_count_in_row) + i_curr1));
				p.set_index_value(i_idx++, (i * (v_count_in_row) + i_curr2));
				p.set_index_value(i_idx++, ((i + 1) * (v_count_in_row) + i_next2));

				p.set_index_value(i_idx++, (i * (v_count_in_row) + i_curr1));
				p.set_index_value(i_idx++, ((i + 1) * (v_count_in_row) + i_next2));
				p.set_index_value(i_idx++, ((i + 1) * (v_count_in_row) + i_next1));
			}
		}

		cache->add(&p);
	}

	static Polygon* create_character(const FontLoader & font_loader, int code, Sprite* sprite = NULL) {
		FontChar font_char;
		if (!font_loader.get_font_char(code, font_char)) {
			LOGE("fte: PolygonBuilder: create_static_text: font '%s' does not contain character with code=%d!", font_loader.get_file_name().c_str(), code);
			return NULL;
		}

		FontPage font_page;
		if (!font_loader.get_font_page(font_char.page, font_page)) {
			LOGE("fte: PolygonBuilder: create_static_text: font '%s'. page %d not found!", font_loader.get_file_name().c_str(), font_char.page);
			return NULL;
		}

		Polygon* p = new Polygon(true, true, false);

		p->set_vert_count(4);

		if (sprite) {
			p->set_color(sprite->get_color());
			p->set_texture_descriptor(sprite->get_texture_descriptor());
		} else {
			p->set_texture_descriptor(TextureDescriptor(font_page.texture_file_name));
		}

		update_glyph(*p, font_char, sprite);
		return p;
	}

	static Polygon* create_static_text(const TextureAtlasLoader & atlas, const FontLoader & font_loader, const std::string & sprite_name, const std::wstring & text) {
		Sprite* s = PolygonBuiler::create_sprite(atlas, sprite_name, true, false);
		Polygon* p = create_static_text(font_loader, text, s);
		DELETE(s);
		return p;
	}

	static Polygon* create_static_text(const FontLoader & font_loader, const std::wstring & text, Sprite* sprite = NULL) {

		FontChar space_char;
		if (!font_loader.get_font_char(32, space_char)) {
			LOGE("fte: PolygonBuilder: create_static_text: font '%s' does not contain space char (code=32)!", font_loader.get_file_name().c_str());
			return NULL;
		}

		FontPage font_page;
		if (!font_loader.get_font_page(space_char.page, font_page)) {
			LOGE("fte: PolygonBuilder: create_static_text: font '%s'. page %d not found!", font_loader.get_file_name().c_str(), space_char.page);
			return NULL;
		}

		const FontCommon & font_common = font_loader.get_font_common();

		Polygon p(true, true, false);

		p.set_vert_count(4);

		if (sprite) {
			p.set_texture_descriptor(sprite->get_texture_descriptor());
		} else {
			p.set_texture_descriptor(TextureDescriptor(font_page.texture_file_name));
		}

		float x = 0.0f;
		float y = 0.0f;

		PolygonCache tmp;

		for (std::wstring::const_iterator it = text.begin(); it != text.end(); ++it) {

			int code = *it;

			FontChar font_char;
			if (!font_loader.get_font_char(code, font_char)) {
				LOGW("fte: PolygonBuilder: create_static_text: font '%s' does not contain character with code=%d !", font_loader.get_file_name().c_str(), code);
				continue;
			}

			switch (code) {
			case ' ':
				x += space_char.xadvance;
				break;

			case '\n':
			case '\r':
				x = 0.0f;
				y -= font_common.line_height;
				break;

			default:
				update_glyph(p, font_char, sprite);
				p.set_translate_x(x);
				p.set_translate_y(y);
				tmp.add(&p);

				x += font_char.xadvance;
				break;
			}
		}

		Polygon* res = tmp.create_composite();
		return res;
	}
}
;

} // namespace fte

#endif /* polygon_builder_H */
