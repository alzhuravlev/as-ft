#ifndef fte_math_H
#define fte_math_H

#include "math.h"
#include "stdlib.h"
#include <vector>
#include <map>

namespace fte {

#define EPSILON 1e-6f

#define F_GE(f1, f2) (((f1) - (f2)) > -EPSILON)
#define F_GR(f1, f2) (((f1) - (f2)) > EPSILON)
#define F_LE(f1, f2) (((f1) - (f2)) < EPSILON)
#define F_LS(f1, f2) (((f1) - (f2)) < -EPSILON)
#define F_EQ(f1, f2) (fabs((f1) - (f2)) < EPSILON)
#define F_NEQ(f1, f2) (fabs((f1) - (f2)) > EPSILON)

#define F_EQ_EPS(f1, f2, eps) (fabs(f1 - (f2)) < eps)
#define F_NEQ_EPS(f1, f2, eps) (fabs(f1 - (f2)) > eps)

struct Matrix4x4 {
	float m[16];
};

struct Vec2 {
	float x, y;
};

struct Vec4 {
	float x, y, z, w;

	Vec4(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 0.0f) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
};

struct Vec3 {
	float x, y, z;

	Vec3(float x = 0.0f, float y = 0.0f, float z = 0.0f) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vec3(const Vec3 & v) {
		this->x = v.x;
		this->y = v.y;
		this->z = v.z;
	}

	Vec3 & operator=(const Vec3 & rhs) {
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		return *this;
	}

	inline void set(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline void set(float v) {
		this->x = v;
		this->y = v;
		this->z = v;
	}

	inline void set_to_zero() {
		this->x = 0.0f;
		this->y = 0.0f;
		this->z = 0.0f;
	}

	inline void mul(const Vec3 & v) {
		this->x *= v.x;
		this->y *= v.y;
		this->z *= v.z;
	}
};

const Vec3 VEC3_ZERO;
const Vec3 VEC3_ONE(1.0f, 1.0f, 1.0f);

#define VEC3(vec3, v0, v1, v2) \
	vec3.x = v0;\
	vec3.y = v1;\
	vec3.z = v2;

inline bool vec_equal(const Vec3 & v1, const Vec3 & v2, float eps = 1e-6f) {
	if (F_NEQ_EPS(v1.x, v2.x, eps))
		return false;
	if (F_NEQ_EPS(v1.y, v2.y, eps))
		return false;
	if (F_NEQ_EPS(v1.z, v2.z, eps))
		return false;
	return true;
}

inline float vec_len(const Vec3 & v) {
	return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}

inline float vec_len_2(const Vec3 & v) {
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

float vec_len(const Vec3 & v1, const Vec3 & v2);

void vec_center(const Vec3 & v1, const Vec3 & v2, Vec3 & res);

inline void vec_cross(const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = v1.y * v2.z - v2.y * v1.z;
	res.y = v1.z * v2.x - v2.z * v1.x;
	res.z = v1.x * v2.y - v2.x * v1.y;
}

void vec_cross(const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, Vec3 & res);

void vec_avg_normal(const Vec3 & v0, const Vec3 & v1, const Vec3 & v2, const Vec3 & v3, Vec3 & res);

inline float vec_dot(const Vec3 & v1, const Vec3 & v2) {
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

float vec_dot(const Vec3 & v1_1, const Vec3 & v1_2, const Vec3 & v2_1, const Vec3 & v2_2);

inline void vec_sub(const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = v1.x - v2.x;
	res.y = v1.y - v2.y;
	res.z = v1.z - v2.z;
}

inline void vec_add(const Vec3 & v1, const Vec3 & v2, Vec3 & res) {
	res.x = v1.x + v2.x;
	res.y = v1.y + v2.y;
	res.z = v1.z + v2.z;
}

/**
 * r0, rd - ray origin and ray direction
 * p1, p2, p3 - plane
 */
bool vec_is_ray_intersects_plane(const Vec3 & r0, const Vec3 & rd, const Vec3 & p1, const Vec3 & p2, const Vec3 & p3, Vec3 & out);

/**
 * r0, rd - ray origin and ray direction
 * pn - plane normal
 * pd - plane D
 */
bool vec_is_ray_intersects_plane(const Vec3 & r0, const Vec3 & rd, const Vec3 & pn, float pd, Vec3 & out);

inline void vec_normalize(Vec3 & v) {
	float k = 1.0f / vec_len(v);
	v.x *= k;
	v.y *= k;
	v.z *= k;
}

inline void vec_neg(Vec3 & v) {
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
}

inline void vec_mul(Vec3 & v, float k) {
	v.x *= k;
	v.y *= k;
	v.z *= k;
}

inline void vec_mul(const Vec3 & v, Vec3 & res) {
	res.x *= v.x;
	res.y *= v.y;
	res.z *= v.z;
}

inline void vec_perp(const Vec3 & v, Vec3 & res) {
	if (F_NEQ(v.x, 0.0f)) {
		res.x = -(v.y + v.z) / v.x;
		res.y = 1.0f;
		res.z = 1.0f;
	} else if (F_NEQ(v.y, 0.0f)) {
		res.x = 1.0f;
		res.y = -(v.x + v.z) / v.y;
		res.z = 1.0f;
	} else if (F_NEQ(v.z, 0.0f)) {
		res.x = 1.0f;
		res.y = 1.0f;
		res.z = -(v.x + v.y) / v.z;
	} else {
		res.x = res.y = res.z = 1.0f;
	}
}

inline void vec_interpolate(const Vec3 & v1, const Vec3 & v2, float alpha, Vec3 & res) {
	res.x = v1.x + (v2.x - v1.x) * alpha;
	res.y = v1.y + (v2.y - v1.y) * alpha;
	res.z = v1.z + (v2.z - v1.z) * alpha;
}

#define CLAMP(v, min, max) ((v > max ? max : v < min ? min : v))

#define M_RAD_TO_DEG 57.2957795130823208f
#define M_DEG_TO_RAD  0.0174532925199432f
#define M_2xPI 6.283185307179586476925286766559f
#define M_PI_8 0.39269908169872415480783042290994f
#define M_PI_16 0.19634954084936207740391521145497f
#define M_PI_32 0.09817477f
#define M_PI_64 0.049087385f

float sin_fast(float rad);
float cos_fast(float rad);

inline void rotate_point_fast_fast(const float & cos_x, const float & sin_x, const float & cos_y, const float & sin_y, const float & cos_z, const float & sin_z, float &x, float &y, float &z) {

	float x_res;
	float y_res;
	float z_res;

//	if (F_NEQ(cos_x, 1.) || F_NEQ(sin_x, 0.)) {
	// rotate x
	z_res = z * cos_x - y * sin_x;
	y_res = z * sin_x + y * cos_x;

	y = y_res;
	z = z_res;
//	}

//	if (F_NEQ(cos_z, 1.) || F_NEQ(sin_z, 0.)) {
	// rotate z
	x_res = x * cos_z - y * sin_z;
	y_res = x * sin_z + y * cos_z;

	x = x_res;
	y = y_res;
//	}

	//	if (F_NEQ(cos_y, 1.) || F_NEQ(sin_y, 0.)) {
	// rotate y
	x_res = x * cos_y - z * sin_y;
	z_res = x * sin_y + z * cos_y;

	x = x_res;
	z = z_res;
//	}
}

inline void rotate_point_fast(float ax, float ay, float az, float &x, float &y, float &z) {

	float x_res;
	float y_res;
	float z_res;

	if (F_NEQ(ax, 0.0f)) {
		// rotate x
		float sin_x = sin_fast(ax);
		float cos_x = cos_fast(ax);

		y_res = z * sin_x + y * cos_x;
		z_res = z * cos_x - y * sin_x;

		y = y_res;
		z = z_res;
	}

	if (F_NEQ(az, 0.0f)) {
		// rotate z
		float sin_z = sin_fast(az);
		float cos_z = cos_fast(az);

		x_res = x * cos_z - y * sin_z;
		y_res = x * sin_z + y * cos_z;

		x = x_res;
		y = y_res;
	}

	if (F_NEQ(ay, 0.0f)) {
		// rotate y
		float sin_y = sin_fast(ay);
		float cos_y = cos_fast(ay);

		x_res = x * cos_y - z * sin_y;
		z_res = x * sin_y + z * cos_y;

		x = x_res;
		z = z_res;
	}
}

inline void rotate_point_fast_z(float az, float &x, float &y) {

	float x_res;
	float y_res;

	if (F_NEQ(az, 0.0f)) {
		// rotate z
		float sin_z = sin_fast(az);
		float cos_z = cos_fast(az);

		x_res = x * cos_z - y * sin_z;
		y_res = x * sin_z + y * cos_z;

		x = x_res;
		y = y_res;
	}
}

extern Vec3 tmp_v;
extern Vec3 tmp_uv, tmp_uuv;
extern Vec3 tmp_QuatVector;

inline void rotate_point_around_axis_fast(float qw, float qx, float qy, float qz, float &x, float &y, float &z) {
	const float Two = 2.0f;

	tmp_v.set(x, y, z);
	tmp_QuatVector.set(qx, qy, qz);

	vec_cross(tmp_QuatVector, tmp_v, tmp_uv);
	vec_cross(tmp_QuatVector, tmp_uv, tmp_uuv);

	x = tmp_v.x + tmp_uv.x * qw + tmp_uuv.x * Two;
	y = tmp_v.y + tmp_uv.y * qw + tmp_uuv.y * Two;
	z = tmp_v.z + tmp_uv.z * qw + tmp_uuv.z * Two;
}

inline void rotate_point_around_axis(float axis_x, float axis_y, float axis_z, float axis_angle, float &x, float &y, float &z) {
//	glm::vec3 axis = glm::vec3(axis_x, axis_y, axis_z);
//	glm::quat q = glm::rotate(glm::quat(), axis_angle, axis);
//	glm::vec4 res = q * glm::vec4(x, y, z, 1.);

	axis_angle *= 0.5f;
	const float Sin = sin_fast(axis_angle);
	const float Cos = cos_fast(axis_angle);
	const float Two = 2.0f;

	float qx, qy, qz, qw;

	qw = Two * Cos;
	qx = axis_x * Sin;
	qy = axis_y * Sin;
	qz = axis_z * Sin;

//	resqw = iqw * qw - iqx * qx - iqy * qy - iqz * qz;
//	resqx = iqw * qx + iqx * qw + iqy * qz - iqz * qy;
//	resqy = iqw * qy + iqy * qw + iqz * qx - iqx * qz;
//	resqz = iqw * qz + iqz * qw + iqx * qy - iqy * qx;

	tmp_v.set(x, y, z);
	tmp_QuatVector.set(qx, qy, qz);

	vec_cross(tmp_QuatVector, tmp_v, tmp_uv);
	vec_cross(tmp_QuatVector, tmp_uv, tmp_uuv);

	x = tmp_v.x + tmp_uv.x * qw + tmp_uuv.x * Two;
	y = tmp_v.y + tmp_uv.y * qw + tmp_uuv.y * Two;
	z = tmp_v.z + tmp_uv.z * qw + tmp_uuv.z * Two;
}

inline float randf(float min, float max) {
	float random = ((float) rand()) / (float) RAND_MAX;
	return min + (max - min) * random;
}

inline int randi(int min, int max) {
	return min + rand() % (max - min + 1);
}

class rand_distribution {
private:
	std::map<int, int> distribution;
	int max;

public:
	rand_distribution(std::vector<int> d) {
		int sum = 0;

		for (std::vector<int>::iterator it = d.begin(), it_end = d.end(); it != it_end; ++it)
			sum += *it;

		if (sum != 0) {
			int upper = 0;
			int i = 0;
			max = 0;

			for (std::vector<int>::iterator it = d.begin(), it_end = d.end(); it != it_end; ++it) {
				int delta = (int) (1000000 * ((float) *it / sum));
				upper += delta;
				if (delta != 0)
					distribution[upper] = i;
				i++;
				max = std::max(max, upper);
			}
		}
	}

	int next() {

		if (distribution.size() == 0)
			return -1;

		int r = randf(0.0f, 1.0f) * max;

		std::map<int, int>::const_iterator it = distribution.lower_bound(r);
		if (it != distribution.end())
			return it->second;
		return -1;
	}
};

float calc_penetration_a_in_interval(float a, float left, float right, bool from_left);
bool is_intervals_intersects(float a1, float a2, float b1, float b2);

struct Rect {
	float x1;
	float y1;
	float x2;
	float y2;

	void set(float x1, float y1, float x2, float y2) {
		this->x1 = x1;
		this->x2 = x2;
		this->y1 = y1;
		this->y2 = y2;
	}

	void mul(float v) {
		this->x1 *= v;
		this->x2 *= v;
		this->y1 *= v;
		this->y2 *= v;
	}

	void mul(float vx, float vy) {
		this->x1 *= vx;
		this->x2 *= vx;
		this->y1 *= vy;
		this->y2 *= vy;
	}

	void translate(float dx, float dy) {
		this->x1 += dx;
		this->x2 += dx;
		this->y1 += dy;
		this->y2 += dy;
	}

	inline bool is_point_in_rect(float x, float y) const {
		return this->x1 < x && x < this->x2 && this->y1 < y && y < this->y2;
	}
};

class LinearInterpolatedVec3 {
private:
	Vec3 current;
	Vec3 target;
	float alpha;

	bool less_x;
	bool less_y;
	bool less_z;

	bool active_x;
	bool active_y;
	bool active_z;

	inline void check_active() {
		active_x = !less_x ? target.x < current.x : target.x > current.x;
		active_y = !less_y ? target.y < current.y : target.y > current.y;
		active_z = !less_z ? target.z < current.z : target.z > current.z;
	}

public:

	LinearInterpolatedVec3() {
		alpha = 0.1f;
		active_x = false;
		active_y = false;
		active_z = false;
		less_x = false;
		less_y = false;
		less_z = false;
	}

	LinearInterpolatedVec3(const Vec3 & current, const Vec3 & target, float alpha) {
		this->current = current;
		this->target = target;
		this->alpha = alpha;
		check_active();
	}

	inline void set_current(const Vec3 & current) {
		this->current = current;
		this->less_x = this->current.x < target.x;
		this->less_y = this->current.y < target.y;
		this->less_z = this->current.z < target.z;
		check_active();
	}

	inline const Vec3 & get_current() const {
		return current;
	}

	inline bool is_active() {
		return active_x || active_y || active_z;
	}

	inline void update(float delta, bool normalize = false) {
		if (!is_active())
			return;

		float a = alpha * delta;
		if (active_x)
			current.x = current.x + (target.x - current.x) * a;
		if (active_y)
			current.y = current.y + (target.y - current.y) * a;
		if (active_z)
			current.z = current.z + (target.z - current.z) * a;

		if (normalize)
			vec_normalize(current);

		check_active();
	}

	void set_target(const Vec3 & target) {
		this->target = target;
		this->less_x = this->current.x < this->target.x;
		this->less_y = this->current.y < this->target.y;
		this->less_z = this->current.z < this->target.z;
		check_active();
	}

	void set_alpha(float alpha) {
		this->alpha = alpha;
	}
};

class LinearInterpolatedValue {
private:
	float current;
	float target;
	float alpha;
	bool active;
	bool less;

	inline void check_active() {
		active = !less ? target < current : target > current;
	}

public:

	LinearInterpolatedValue() {
		this->current = 0.0f;
		this->target = 0.0f;
		this->alpha = 0.1f;
		this->active = false;
		this->less = false;
	}

	inline void set_current(float current) {
		this->current = current;
		this->less = this->current < this->target;
		check_active();
	}

	inline float get_value() {
		return current;
	}

	inline bool is_active() {
		return active;
	}

	inline float update(float delta) {
		if (active) {
			current = current + (target - current) * alpha * delta;
			check_active();
		}
		return current;
	}

	void set_alpha(float alpha) {
		this->alpha = alpha;
	}

	void set_target(float target) {
		this->target = target;
		this->less = this->current < this->target;
		check_active();
	}

	void clamp_cycle_target(float max) {
		if (fabs(target - current) > max * 0.5f) {
			if (target > current)
				target -= max;
			else
				target += max;
		}

		if (current > max && target > max) {
			current -= max;
			target -= max;
		} else if (current < 0.0f && target < 0.0f) {
			current += max;
			target += max;
		}
	}
};

} // namespace fte

#endif /* fte_math_H */
