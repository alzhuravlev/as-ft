#include "jni.h"
#include "errno.h"

#include "EGL/egl.h"
#include "EGL/eglplatform.h"
#include "GLES2/gl2.h"

#include "android_native_app_glue.h"
#include "android/window.h"
#include "android/log.h"
#include "android/asset_manager.h"

#ifdef PROF
#include <prof.h>
#endif

#include "engine.h"
#include "external.h"
#include "ft_start.h"
#include "fte_utils.h"
#include "audio_manager.h"

#include "sys/types.h"

#include "pthread.h"

static fte::FTE_ENGINE fte_engine;

static jclass MyNativeActivity;
static jmethodID AudioManager_init_method;
static jmethodID AudioManager_release_method;
static jmethodID AudioManager_pause_method;
static jmethodID AudioManager_resume_method;
static jmethodID AudioManager_create_sound_method;
static jmethodID AudioManager_create_music_method;
static jmethodID AudioManager_destroy_sound_method;
static jmethodID AudioManager_destroy_music_method;
static jmethodID AudioManager_play_sound_method;
static jmethodID AudioManager_stop_sound_method;
static jmethodID AudioManager_play_music_method;
static jmethodID AudioManager_stop_music_method;
static jmethodID Locale_get_current_locale;

static jmethodID Ads_is_ready_to_show_method;
static jmethodID Ads_show_method;

static jmethodID Iap_is_purchased_method;
static jmethodID Iap_purchase_method;
static jmethodID Iap_get_info_title_method;
static jmethodID Iap_get_info_price_method;

/**
 * Shared state for our app.
 */
struct Engine {
	struct android_app* app;

	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;
};

fte::fte_timespec get_time() {
	timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	fte::fte_timespec fte_ts;
	fte_ts.tv_nsec = ts.tv_nsec;
	fte_ts.tv_sec = ts.tv_sec;
	return fte_ts;
}

AUDIO_MANAGER_ENV AudioManager_init(fte::FTE_ENV env) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_init_method);

	engine->app->activity->vm->DetachCurrentThread();

	return (void*) 1;
}

void AudioManager_release(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_release_method);

	engine->app->activity->vm->DetachCurrentThread();
}

void AudioManager_pause(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_pause_method);

	engine->app->activity->vm->DetachCurrentThread();
}

void AudioManager_resume(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_resume_method);

	engine->app->activity->vm->DetachCurrentThread();
}

SOUND_ID AudioManager_create_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, const std::string & file_name) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(file_name.c_str());

	int id = jni->CallIntMethod(engine->app->activity->clazz, AudioManager_create_sound_method, s);

	engine->app->activity->vm->DetachCurrentThread();

	return id;
}

MUSIC_ID AudioManager_create_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, const std::string & file_name) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(file_name.c_str());

	int id = jni->CallIntMethod(engine->app->activity->clazz, AudioManager_create_music_method, s);

	engine->app->activity->vm->DetachCurrentThread();

	return id;
}

void AudioManager_destroy_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID sound_id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_destroy_sound_method, sound_id);

	engine->app->activity->vm->DetachCurrentThread();
}

void AudioManager_destroy_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID music_id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_destroy_music_method, music_id);

	engine->app->activity->vm->DetachCurrentThread();
}

STREAM_ID AudioManager_play_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID id, bool loop) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	int stream_id = jni->CallIntMethod(engine->app->activity->clazz, AudioManager_play_sound_method, id, loop);

	engine->app->activity->vm->DetachCurrentThread();

	return stream_id;
}

void AudioManager_stop_sound(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, SOUND_ID id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_stop_sound_method, id);

	engine->app->activity->vm->DetachCurrentThread();
}

void AudioManager_play_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID id, bool loop) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_play_music_method, id, loop);

	engine->app->activity->vm->DetachCurrentThread();
}

void AudioManager_stop_music(fte::FTE_ENV env, AUDIO_MANAGER_ENV aenv, MUSIC_ID id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jni->CallVoidMethod(engine->app->activity->clazz, AudioManager_stop_music_method, id);

	engine->app->activity->vm->DetachCurrentThread();
}

std::string get_locale(fte::FTE_ENV env) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring res = (jstring) jni->CallObjectMethod(engine->app->activity->clazz, Locale_get_current_locale);

	std::string str;
	const char* s = jni->GetStringUTFChars(res, NULL);
	str.assign(s);
	jni->ReleaseStringUTFChars(res, s);

	engine->app->activity->vm->DetachCurrentThread();

	return str;
}

bool load_resource_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size) {

	std::string real_file_name = file_name;

//	LOGD("loading file: '%s'", real_file_name.c_str());

	Engine* engine = (Engine*) env;
	AAssetManager* am = engine->app->activity->assetManager;
	AAsset* a = AAssetManager_open(am, real_file_name.c_str(), AASSET_MODE_BUFFER);
	if (a == NULL) {
		LOGE("asset file: '%s' not found", real_file_name.c_str());
		return false;
	}
	size = AAsset_getLength(a);
	buf = malloc(size);
	bool res = AAsset_read(a, buf, size) >= 0;
	if (!res) {
		free(buf);
		LOGE("error reading file: '%s'", real_file_name.c_str());
	}
	AAsset_close(a);
	return res;
}

bool load_internal_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size) {
	Engine* engine = (Engine*) env;
	std::string ext_path = std::string(engine->app->activity->internalDataPath);

	std::string real_file_name = ext_path + "/" + file_name;

//	LOGD("loading internal file: '%s'", real_file_name.c_str());

	FILE *file = fopen(real_file_name.c_str(), "rb");
	if (!file) {
		LOGE("error loading internal file: '%s'", real_file_name.c_str());
		return false;
	}

	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);

	buf = malloc(size);
	fread(buf, 1, size, file);
	fclose(file);

	return true;
}

bool save_internal_file(fte::FTE_ENV env, const std::string & file_name, const void* buf, const long &size) {
	Engine* engine = (Engine*) env;
	std::string ext_path = std::string(engine->app->activity->internalDataPath);

	std::string real_file_name = ext_path + "/" + file_name;

	LOGD("saving internal file: '%s'", real_file_name.c_str());

	FILE *file = fopen(real_file_name.c_str(), "wb");
	if (!file) {
		LOGE("error saving internal file: '%s'", real_file_name.c_str());
		return false;
	}

	fwrite(buf, 1, size, file);
	fclose(file);

	return true;
}

bool load_external_file(fte::FTE_ENV env, const std::string & file_name, void* &buf, long &size) {
	Engine* engine = (Engine*) env;
	std::string ext_path = std::string(engine->app->activity->externalDataPath);

	std::string real_file_name = ext_path + "/" + file_name;

	LOGD("loading external file: '%s'", real_file_name.c_str());

	FILE *file = fopen(real_file_name.c_str(), "rb");
	if (!file)
		return false;

	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);

	buf = malloc(size);
	fread(buf, 1, size, file);
	fclose(file);

	return true;
}

void __logi(const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	__android_log_vprint(ANDROID_LOG_INFO, "fte", fmt, args);
	va_end(args);
}

void __loge(const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	__android_log_vprint(ANDROID_LOG_ERROR, "fte", fmt, args);
	va_end(args);
}

void __logw(const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	__android_log_vprint(ANDROID_LOG_WARN, "fte", fmt, args);
	va_end(args);
}

void __logd(const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	__android_log_vprint(ANDROID_LOG_DEBUG, "fte", fmt, args);
	va_end(args);
}

void __log_touch(const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	__android_log_vprint(ANDROID_LOG_DEBUG, "TOUCH", fmt, args);
	va_end(args);
}

void __log_fps(int fps) {
	__android_log_print(fps > 58 ? ANDROID_LOG_DEBUG : ANDROID_LOG_WARN, "FPS", "fps: %d", fps);
}

bool Ads_is_ready_to_show(fte::FTE_ENV env, const std::string & location) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(location.c_str());

	jboolean res = jni->CallBooleanMethod(engine->app->activity->clazz, Ads_is_ready_to_show_method, s);

	engine->app->activity->vm->DetachCurrentThread();

	return res;
}

void Ads_show(fte::FTE_ENV env, const std::string & location) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(location.c_str());

	jni->CallVoidMethod(engine->app->activity->clazz, Ads_show_method, s);

	engine->app->activity->vm->DetachCurrentThread();
}

bool Iap_is_purchased(fte::FTE_ENV env, const std::string & id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(id.c_str());

	jboolean res = jni->CallBooleanMethod(engine->app->activity->clazz, Iap_is_purchased_method, s);

	engine->app->activity->vm->DetachCurrentThread();

	return res;
}

void Iap_purchase(fte::FTE_ENV env, const std::string & id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(id.c_str());

	jni->CallVoidMethod(engine->app->activity->clazz, Iap_purchase_method, s);

	engine->app->activity->vm->DetachCurrentThread();
}

std::string Iap_get_info_title(fte::FTE_ENV env, const std::string & id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(id.c_str());

	jstring res = (jstring) jni->CallObjectMethod(engine->app->activity->clazz, Iap_get_info_title_method, s);

	std::string str;
	if (res) {
		const char* c = jni->GetStringUTFChars(res, NULL);
		str.assign(c);
		jni->ReleaseStringUTFChars(res, c);
	}

	engine->app->activity->vm->DetachCurrentThread();

	return str;
}

std::string Iap_get_info_price(fte::FTE_ENV env, const std::string & id) {
	Engine* engine = (Engine*) env;

	JNIEnv* jni;
	engine->app->activity->vm->AttachCurrentThread(&jni, NULL);

	jstring s = jni->NewStringUTF(id.c_str());

	jstring res = (jstring) jni->CallObjectMethod(engine->app->activity->clazz, Iap_get_info_price_method, s);

	std::string str;
	if (res) {
		const char* c = jni->GetStringUTFChars(res, NULL);
		str.assign(c);
		jni->ReleaseStringUTFChars(res, c);
	}

	engine->app->activity->vm->DetachCurrentThread();

	return str;
}

void finish_application(fte::FTE_ENV env) {
	Engine* engine = (Engine*) env;
	ANativeActivity_finish(engine->app->activity);
}

void print_egl_attrib(EGLDisplay display, EGLConfig config, EGLint attr, const char* name) {
	EGLint val;
	eglGetConfigAttrib(display, config, attr, &val);
	LOGD("%s = %d", name, val);
}

void print_egl_config(EGLDisplay display, EGLConfig config) {
	LOGD("=================== EGL Config ===================");
//	print_egl_attrib(display, config, EGL_BUFFER_SIZE, "EGL_BUFFER_SIZE");
	print_egl_attrib(display, config, EGL_ALPHA_SIZE, "EGL_ALPHA_SIZE");
	print_egl_attrib(display, config, EGL_BLUE_SIZE, "EGL_BLUE_SIZE");
	print_egl_attrib(display, config, EGL_GREEN_SIZE, "EGL_GREEN_SIZE");
	print_egl_attrib(display, config, EGL_RED_SIZE, "EGL_RED_SIZE");
	print_egl_attrib(display, config, EGL_DEPTH_SIZE, "EGL_DEPTH_SIZE");
	print_egl_attrib(display, config, EGL_STENCIL_SIZE, "EGL_STENCIL_SIZE");
//	print_egl_attrib(display, config, EGL_CONFIG_CAVEAT, "EGL_CONFIG_CAVEAT");
//	print_egl_attrib(display, config, EGL_CONFIG_ID, "EGL_CONFIG_ID");
//	print_egl_attrib(display, config, EGL_LEVEL, "EGL_LEVEL");
//	print_egl_attrib(display, config, EGL_MAX_PBUFFER_HEIGHT, "EGL_MAX_PBUFFER_HEIGHT");
//	print_egl_attrib(display, config, EGL_MAX_PBUFFER_PIXELS, "EGL_MAX_PBUFFER_PIXELS");
//	print_egl_attrib(display, config, EGL_MAX_PBUFFER_WIDTH, "EGL_MAX_PBUFFER_WIDTH");
//	print_egl_attrib(display, config, EGL_NATIVE_RENDERABLE, "EGL_NATIVE_RENDERABLE");
//	print_egl_attrib(display, config, EGL_NATIVE_VISUAL_ID, "EGL_NATIVE_VISUAL_ID");
//	print_egl_attrib(display, config, EGL_NATIVE_VISUAL_TYPE, "EGL_NATIVE_VISUAL_TYPE");
	print_egl_attrib(display, config, EGL_SAMPLES, "EGL_SAMPLES");
	print_egl_attrib(display, config, EGL_SAMPLE_BUFFERS, "EGL_SAMPLE_BUFFERS");
//	print_egl_attrib(display, config, EGL_TRANSPARENT_TYPE, "EGL_TRANSPARENT_TYPE");
//	print_egl_attrib(display, config, EGL_TRANSPARENT_BLUE_VALUE, "EGL_TRANSPARENT_BLUE_VALUE");
//	print_egl_attrib(display, config, EGL_TRANSPARENT_GREEN_VALUE, "EGL_TRANSPARENT_GREEN_VALUE");
//	print_egl_attrib(display, config, EGL_TRANSPARENT_RED_VALUE, "EGL_TRANSPARENT_RED_VALUE");
//	print_egl_attrib(display, config, EGL_BIND_TO_TEXTURE_RGB, "EGL_BIND_TO_TEXTURE_RGB");
//	print_egl_attrib(display, config, EGL_BIND_TO_TEXTURE_RGBA, "EGL_BIND_TO_TEXTURE_RGBA");
//	print_egl_attrib(display, config, EGL_MIN_SWAP_INTERVAL, "EGL_MIN_SWAP_INTERVAL");
//	print_egl_attrib(display, config, EGL_MAX_SWAP_INTERVAL, "EGL_MAX_SWAP_INTERVAL");
//	print_egl_attrib(display, config, EGL_LUMINANCE_SIZE, "EGL_LUMINANCE_SIZE");
//	print_egl_attrib(display, config, EGL_ALPHA_MASK_SIZE, "EGL_ALPHA_MASK_SIZE");
//	print_egl_attrib(display, config, EGL_COLOR_BUFFER_TYPE, "EGL_COLOR_BUFFER_TYPE");
//	print_egl_attrib(display, config, EGL_RENDERABLE_TYPE, "EGL_RENDERABLE_TYPE");
//	print_egl_attrib(display, config, EGL_MATCH_NATIVE_PIXMAP, "EGL_MATCH_NATIVE_PIXMAP");
//	print_egl_attrib(display, config, EGL_CONFORMANT, "EGL_CONFORMANT");
	LOGD("==================================================");
}

bool select_config(EGLDisplay display, EGLConfig* configs, EGLint configs_size, EGLint alpha, EGLint red, EGLint green, EGLint blue, EGLint depth, EGLint samples, EGLConfig & config) {

	for (int i = 0; i < configs_size; i++) {
		EGLConfig c = configs[i];

		EGLint c_alpha;
		EGLint c_red;
		EGLint c_green;
		EGLint c_blue;
		EGLint c_depth;
		EGLint c_stencil;
		EGLint c_samples;

		eglGetConfigAttrib(display, c, EGL_ALPHA_SIZE, &c_alpha);
		eglGetConfigAttrib(display, c, EGL_RED_SIZE, &c_red);
		eglGetConfigAttrib(display, c, EGL_GREEN_SIZE, &c_green);
		eglGetConfigAttrib(display, c, EGL_BLUE_SIZE, &c_blue);
		eglGetConfigAttrib(display, c, EGL_DEPTH_SIZE, &c_depth);
		eglGetConfigAttrib(display, c, EGL_STENCIL_SIZE, &c_stencil);
		eglGetConfigAttrib(display, c, EGL_SAMPLES, &c_samples);

		if (c_alpha == alpha && c_red == red && c_green == green && c_blue == blue && c_depth >= depth && c_samples >= samples) {
			config = c;
			return true;
		}
	}
	return false;
}

/**
 * Initialize an EGL context for the current display.
 */
void render_init_display(fte::FTE_ENV env) {

	LOGI("fte engine_init_display");

	Engine* engine = (Engine*) env;

	// initialize OpenGL ES and EGL

	/*
	 * Here specify the attributes of the desired configuration.
	 * Below, we select an EGLConfig with at least 8 bits per color
	 * component compatible with on-screen windows
	 */
	const EGLint attribs[] = { EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, EGL_BLUE_SIZE, 4, EGL_GREEN_SIZE, 4, EGL_RED_SIZE, 4, EGL_NONE };

	EGLint w, h, dummy, format;
	EGLint num_configs;
	EGLSurface surface;
	EGLContext context;

	EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if (display == EGL_NO_DISPLAY) {
		LOGE("Unable to eglGetDisplay");
		return;
	}

	if (eglInitialize(display, 0, 0) == EGL_FALSE) {
		LOGE("Unable to eglInitialize");
		return;
	}

//	eglSwapInterval(display, 0);

	eglChooseConfig(display, attribs, NULL, 0, &num_configs);

	LOGI("fte: RENDERER: there are %d EGL configs found", num_configs);

	EGLConfig choosed_config = NULL;

	EGLint configs_size = num_configs;
	EGLConfig configs[configs_size];

	eglChooseConfig(display, attribs, configs, configs_size, &num_configs);

	if (!select_config(display, configs, num_configs, 0, 5, 6, 5, 24, 2, choosed_config))
		if (!select_config(display, configs, num_configs, 0, 5, 6, 5, 24, 0, choosed_config))
			if (!select_config(display, configs, num_configs, 0, 5, 6, 5, 16, 2, choosed_config))
				if (!select_config(display, configs, num_configs, 0, 5, 6, 5, 16, 0, choosed_config)) {
					LOGW("fte: RENDERER: UNABLE TO CHOOSE EGL CONFIG!");
//					return;
				}

	LOGI("fte: RENDERER: EGL config choosed");

#ifdef DEBUG
	print_egl_config(display, choosed_config);
#endif

	/* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
	 * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
	 * As soon as we picked a EGLConfig, we can safely reconfigure the
	 * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
	eglGetConfigAttrib(display, choosed_config, EGL_NATIVE_VISUAL_ID, &format);

	ANativeWindow_setBuffersGeometry(engine->app->window, 0, 0, format);

	surface = eglCreateWindowSurface(display, choosed_config, engine->app->window, NULL);

	const EGLint attribs_context[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };
	context = eglCreateContext(display, choosed_config, NULL, attribs_context);
	if (context == EGL_NO_CONTEXT) {
		LOGE("Unable to eglCreateContext");
		return;
	}

	if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
		LOGE("Unable to eglMakeCurrent");
		return;
	}

	eglQuerySurface(display, surface, EGL_WIDTH, &w);
	eglQuerySurface(display, surface, EGL_HEIGHT, &h);

	engine->display = display;
	engine->context = context;
	engine->surface = surface;
}

/**
 * Just the current frame in the display.
 */
void render_flush(fte::FTE_ENV env) {
	Engine* engine = (Engine*) env;
	EGLBoolean res = eglSwapBuffers(engine->display, engine->surface);
	if (res == EGL_FALSE) {
		LOGE("Unable to eglSwapBuffers");
	}
}

void render_query_screen_size(fte::FTE_ENV env, int &w, int &h) {
	Engine* engine = (Engine*) env;
	if (eglQuerySurface(engine->display, engine->surface, EGL_WIDTH, &w) != EGL_TRUE)
		LOGE("Unable to eglQuerySurface");

	if (eglQuerySurface(engine->display, engine->surface, EGL_HEIGHT, &h) != EGL_TRUE)
		LOGE("Unable to eglQuerySurface");

	int32_t orient = AConfiguration_getOrientation(engine->app->config);
	switch (orient) {
	case ACONFIGURATION_ORIENTATION_PORT:
		if (h < w)
			std::swap(h, w);
		break;
	case ACONFIGURATION_ORIENTATION_LAND:
		if (h > w)
			std::swap(h, w);
		break;
	}
}

void render_term_display(fte::FTE_ENV env) {

	Engine* engine = (Engine*) env;

	if (engine->display != EGL_NO_DISPLAY) {

		LOGI("fte engine_term_display");

		if (eglMakeCurrent(engine->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT) == EGL_FALSE) {
			LOGI("Unalbe to eglMakeCurrent");
		}
		if (engine->context != EGL_NO_CONTEXT) {
			if (eglDestroyContext(engine->display, engine->context) == EGL_FALSE) {
				LOGI("Unalbe to eglDestroyContext");
			}
		}
		if (engine->surface != EGL_NO_SURFACE) {
			if (eglDestroySurface(engine->display, engine->surface) == EGL_FALSE) {
				LOGI("Unalbe to eglDestroySurface");
			}
		}
		if (eglTerminate(engine->display) == EGL_FALSE) {
			LOGI("Unalbe to eglTerminate");
		}
	}
	engine->display = EGL_NO_DISPLAY;
	engine->context = EGL_NO_CONTEXT;
	engine->surface = EGL_NO_SURFACE;
}

/**
 * Process the next input event.
 */

static int32_t p_index = 0;

static int32_t engine_handle_input(struct android_app* app, AInputEvent* event) {
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
		fte::TouchEvent touch_event;
		int32_t action = AMotionEvent_getAction(event);
		switch (action & AMOTION_EVENT_ACTION_MASK) {
		case AMOTION_EVENT_ACTION_DOWN:
			touch_event.action = fte::ATOUCH_DOWN;
			p_index = 0;
			break;
		case AMOTION_EVENT_ACTION_POINTER_DOWN:
			touch_event.action = fte::ATOUCH_MOVE;
			p_index = (action & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
			break;
		case AMOTION_EVENT_ACTION_POINTER_UP:
			touch_event.action = fte::ATOUCH_MOVE;
			p_index = (action & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
			p_index = p_index == 1 ? 0 : 1;
			break;
		case AMOTION_EVENT_ACTION_UP:
			touch_event.action = fte::ATOUCH_UP;
			break;
		case AMOTION_EVENT_ACTION_MOVE:
			touch_event.action = fte::ATOUCH_MOVE;
			break;
		default:
			return 0;
		}
		touch_event.nano_time = AMotionEvent_getEventTime(event);
		touch_event.x = AMotionEvent_getX(event, p_index);
		touch_event.y = AMotionEvent_getY(event, p_index);
		fteSendTouch(fte_engine, touch_event);
		return 1;

	} else if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_KEY) {
		switch (AKeyEvent_getAction(event)) {
		case AKEY_EVENT_ACTION_UP:
			if (AKeyEvent_getKeyCode(event) == AKEYCODE_BACK) {
				fte::TouchEvent touch_event;
				touch_event.action = fte::ATOUCH_BACK_PRESSED;
				fteSendTouch(fte_engine, touch_event);
			}
			break;
		}
	}
	return 0;
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app* app, int32_t cmd) {
	Engine* engine = (Engine*) app->userData;

	switch (cmd) {

	case APP_CMD_SAVE_STATE:
		break;

	case APP_CMD_START:
	case APP_CMD_INIT_WINDOW:
		if (engine->app->window != NULL) {
			fte::fteStart(fte_engine);
		}
		break;

	case APP_CMD_STOP:
	case APP_CMD_TERM_WINDOW:
		fte::fteStop(fte_engine);
		break;

	case APP_CMD_CONFIG_CHANGED:
		fte::fteSendResize(fte_engine);
		break;

	case APP_CMD_DESTROY:
		fte::fteDestroyEngine(fte_engine);
		break;
	}
}

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
	JNIEnv* env;
	if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
		return -1;
	}

	MyNativeActivity = env->FindClass("com/crane/funnytowers2/MyNativeActivity");
	MyNativeActivity = (jclass) env->NewGlobalRef(MyNativeActivity);
	AudioManager_init_method = env->GetMethodID(MyNativeActivity, "AudioManager_init", "()V");
	AudioManager_release_method = env->GetMethodID(MyNativeActivity, "AudioManager_release", "()V");
	AudioManager_pause_method = env->GetMethodID(MyNativeActivity, "AudioManager_pause", "()V");
	AudioManager_resume_method = env->GetMethodID(MyNativeActivity, "AudioManager_resume", "()V");
	AudioManager_create_sound_method = env->GetMethodID(MyNativeActivity, "AudioManager_create_sound", "(Ljava/lang/String;)I");
	AudioManager_create_music_method = env->GetMethodID(MyNativeActivity, "AudioManager_create_music", "(Ljava/lang/String;)I");
	AudioManager_destroy_sound_method = env->GetMethodID(MyNativeActivity, "AudioManager_destroy_sound", "(I)V");
	AudioManager_destroy_music_method = env->GetMethodID(MyNativeActivity, "AudioManager_destroy_music", "(I)V");
	AudioManager_play_sound_method = env->GetMethodID(MyNativeActivity, "AudioManager_play_sound", "(IZ)I");
	AudioManager_stop_sound_method = env->GetMethodID(MyNativeActivity, "AudioManager_stop_sound", "(I)V");
	AudioManager_play_music_method = env->GetMethodID(MyNativeActivity, "AudioManager_play_music", "(IZ)V");
	AudioManager_stop_music_method = env->GetMethodID(MyNativeActivity, "AudioManager_stop_music", "(I)V");

	Locale_get_current_locale = env->GetMethodID(MyNativeActivity, "Locale_get_current_locale", "()Ljava/lang/String;");

	Ads_is_ready_to_show_method = env->GetMethodID(MyNativeActivity, "Ads_is_ready_to_show", "(Ljava/lang/String;)Z");
	Ads_show_method = env->GetMethodID(MyNativeActivity, "Ads_show", "(Ljava/lang/String;)V");

	Iap_is_purchased_method = env->GetMethodID(MyNativeActivity, "Iap_is_purchased", "(Ljava/lang/String;)Z");
	Iap_purchase_method = env->GetMethodID(MyNativeActivity, "Iap_purchase", "(Ljava/lang/String;)V");
	Iap_get_info_title_method = env->GetMethodID(MyNativeActivity, "Iap_get_info_title", "(Ljava/lang/String;)Ljava/lang/String;");
	Iap_get_info_price_method = env->GetMethodID(MyNativeActivity, "Iap_get_info_price", "(Ljava/lang/String;)Ljava/lang/String;");

	return JNI_VERSION_1_6;
}

void android_main(struct android_app* state) {
	Engine engine;

	memset(&engine, 0, sizeof(engine));

	app_dummy();

	fte_engine = fte::fteCreateEngine(ft::ftCreateScreen(), (fte::FTE_ENV) &engine);

	state->userData = &engine;
	state->onAppCmd = engine_handle_cmd;
	state->onInputEvent = engine_handle_input;
	engine.app = state;

#ifdef PROF
	LOGW("PROF START");
	monstartup("libft-android.so");
#endif

	bool running = true;
	while (running) {
		int ident;
		int events;
		struct android_poll_source* source;

		while ((ident = ALooper_pollAll(-1, NULL, &events, (void**) &source)) >= 0) {

			if (source != NULL) {
				source->process(state, source);
			}

			if (state->destroyRequested != 0) {
				LOGI("main exit");
				running = false;
				break;
			}
		}
	}

#ifdef PROF
	moncleanup();
	LOGW("PROF STOP");
#endif

}
