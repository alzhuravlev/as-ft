package com.crane.funnytowers2;

import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;

public class MyActivity extends Activity {
	private SoundPool soundPool;
	private MediaPlayer mediaPlayer;
	private boolean musicPaused;

	static {
		System.loadLibrary("ft-android");
	}

	public void AudioManager_init() {
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		mediaPlayer = new MediaPlayer();
		musicPaused = false;
	}

	public void AudioManager_release() {
		if (soundPool != null) {
			soundPool.release();
			soundPool = null;
		}

		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void AudioManager_pause() {
		if (soundPool != null)
			soundPool.autoPause();

		if (mediaPlayer != null)
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				musicPaused = true;
			}
	}

	public void AudioManager_resume() {
		if (soundPool != null)
			soundPool.autoResume();

		if (mediaPlayer != null)
			if (musicPaused) {
				mediaPlayer.start();
				musicPaused = false;
			}
	}

	int AudioManager_create_sound(String fileName) {
		if (soundPool != null)
			try {
				return soundPool.load(getAssets().openFd(fileName), 1);
			} catch (IOException e) {
			}
		return 0;
	}

	int AudioManager_create_music(String fileName) {
		if (mediaPlayer != null)
			try {
				AssetFileDescriptor afd = getAssets().openFd(fileName);
				mediaPlayer.setDataSource(afd.getFileDescriptor(),
						afd.getStartOffset(), afd.getLength());
				mediaPlayer.prepare();
				return 1;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return 0;
	}

	public int AudioManager_play_sound(int soundId, boolean loop) {
		if (soundPool != null)
			return soundPool.play(soundId, 1, 1, 1, loop ? -1 : 0, 1);
		return -1;
	}

	public void AudioManager_stop_sound(int streamId) {
		if (soundPool != null)
			soundPool.stop(streamId);
	}

	public void AudioManager_play_music(int id, boolean loop) {
		if (mediaPlayer != null) {
			mediaPlayer.setLooping(loop);
			mediaPlayer.start();
		}
	}

	public void AudioManager_stop_music(int id) {
		if (mediaPlayer != null)
			mediaPlayer.stop();
	}

	public String Locale_get_current_locale() {
		return Locale.getDefault().getCountry();
	}

	public static native void init(AssetManager assetManager);

	public static native void destroy();

	public static native void step();

	public static native void resize(int w, int h);

	public static native void start();

	public static native void stop();

	GL2JNIView mView;

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mView = new GL2JNIView(getApplication());
		setContentView(mView);
		init(getAssets());
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		destroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mView.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mView.onResume();
	}
}
