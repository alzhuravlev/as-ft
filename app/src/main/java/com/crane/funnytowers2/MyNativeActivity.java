package com.crane.funnytowers2;

import android.app.NativeActivity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;

import com.crane.funnytowers2.iap.IabHelper;
import com.crane.funnytowers2.iap.IabResult;
import com.crane.funnytowers2.iap.Inventory;
import com.crane.funnytowers2.iap.Purchase;
import com.crane.funnytowers2.iap.SkuDetails;

import java.io.IOException;
import java.util.Locale;

public class MyNativeActivity extends NativeActivity {
	private SoundPool soundPool;
	private MediaPlayer mediaPlayer;
	private boolean musicPaused;
	private IabHelper mHelper;
	private Inventory inventory;
	// private VunglePub vunglePub = VunglePub.getInstance();

	static final String IAP_TAG = "IAP";

	static {
		System.loadLibrary("ft-android");
	}

	public void finish_application() {
		finish();
	}

	public void AudioManager_init() {
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		mediaPlayer = new MediaPlayer();
		musicPaused = false;
	}

	public void AudioManager_release() {
		if (soundPool != null) {
			soundPool.release();
			soundPool = null;
		}

		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void AudioManager_pause() {
		if (soundPool != null)
			soundPool.autoPause();

		if (mediaPlayer != null)
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				musicPaused = true;
			}
	}

	public void AudioManager_resume() {
		if (soundPool != null)
			soundPool.autoResume();

		if (mediaPlayer != null)
			if (musicPaused) {
				mediaPlayer.start();
				musicPaused = false;
			}
	}

	int AudioManager_create_sound(String fileName) {
		if (soundPool != null)
			try {
				return soundPool.load(getAssets().openFd(fileName), 1);
			} catch (IOException e) {
			}
		return 0;
	}

	int AudioManager_create_music(String fileName) {
		if (mediaPlayer != null)
			try {
				AssetFileDescriptor afd = getAssets().openFd(fileName);
				mediaPlayer.reset();
				mediaPlayer.setDataSource(afd.getFileDescriptor(),
						afd.getStartOffset(), afd.getLength());
				mediaPlayer.prepare();
				return 1;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return 0;
	}

	void AudioManager_destroy_sound(int id) {
		if (soundPool != null)
			soundPool.unload(id);
	}

	void AudioManager_destroy_music(int id) {
		if (mediaPlayer != null)
			mediaPlayer.reset();
	}

	public int AudioManager_play_sound(int soundId, boolean loop) {
		if (soundPool != null)
			return soundPool.play(soundId, 1, 1, 1, loop ? -1 : 0, 1);
		return -1;
	}

	public void AudioManager_stop_sound(int streamId) {
		if (soundPool != null)
			soundPool.stop(streamId);
	}

	public void AudioManager_play_music(int id, boolean loop) {
		if (mediaPlayer != null) {
			// mediaPlayer.setLooping(loop);
			// mediaPlayer.start();
		}
	}

	public void AudioManager_stop_music(int id) {
		if (mediaPlayer != null)
			mediaPlayer.stop();
	}

	public String Locale_get_current_locale() {
		return Locale.getDefault().getCountry();
	}

	public boolean Ads_is_ready_to_show(String location) {
		// boolean res = vunglePub.isCachedAdAvailable();
		// if (!res) {
//		boolean res = Chartboost.hasRewardedVideo(location);
//		if (!res) {
//			Chartboost.cacheRewardedVideo(location);
//			res = Chartboost.hasInterstitial(location);
//			if (!res)
//				Chartboost.cacheInterstitial(location);
//		}
		// }
//		return res;
		return false;
	}

	public void Ads_show(String location) {
		// if (vunglePub.isCachedAdAvailable())
		// vunglePub.playAd();
		// else
//		if (Chartboost.hasRewardedVideo(location))
//			Chartboost.showRewardedVideo(location);
//		else if (Chartboost.hasInterstitial(location))
//			Chartboost.showInterstitial(location);
	}

	public boolean Iap_is_purchased(String id) {
		if (inventory == null)
			return false;
		return inventory.hasPurchase(id);
	}

	public void Iap_purchase(String id) {
		if (mHelper == null)
			return;
		mHelper.launchPurchaseFlow(this, id, IabHelper.ITEM_TYPE_INAPP, 10001,
				mPurchaseFinishedListener, "123");
	}

	public String Iap_get_info_title(String id) {
		if (inventory == null)
			return null;
		SkuDetails details = inventory.getSkuDetails(id);
		if (details == null)
			return null;
		return details.getTitle();
	}

	public String Iap_get_info_price(String id) {
		if (inventory == null)
			return null;
		SkuDetails details = inventory.getSkuDetails(id);
		if (details == null)
			return null;
		return details.getPrice();
	}

	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d(IAP_TAG, "Query inventory finished.");

			if (mHelper == null)
				return;

			// Is it a failure?
			if (result.isFailure()) {
				Log.e(IAP_TAG, "Failed to query inventory: " + result);
				return;
			}

			MyNativeActivity.this.inventory = inventory;

			Log.d(IAP_TAG, "Query inventory was successful.");
		}
	};

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(IAP_TAG, "Purchase finished: " + result + ", purchase: "
					+ purchase);

			if (mHelper == null)
				return;

			if (result.isFailure()) {
				Log.e(IAP_TAG, "Error purchasing: " + result);
				return;
			}

			mHelper.queryInventoryAsync(mGotInventoryListener);

			Log.d(IAP_TAG, "Purchase successful.");
		}
	};

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

//		Chartboost.startWithAppId(this, "54b6295f0d602531da3b88c9",
//				"e779aed9b5493268747f48c1c22ad1bc9f5f8785");
//		// Chartboost.setDelegate(delegate);
//		Chartboost.setImpressionsUseActivities(true);
//		Chartboost.setLoggingLevel(Level.NONE);
//		Chartboost.onCreate(this);

		String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqAvjWEfLWzqXYJI4XzoKgx2OfF4tWiqLbFxlVMkmhk6OUVx1Z0GIRur1wYTzrFD8009gP4DUz+EZS3iBsaf7DLFiQwnbaezCb/4vlEL2RHcco3y4GlRJ06OJU0r974ALUr/gJfjKGrqbZMSJGUGDCCB6GEbnU+dycTjmgavtjqcLvAmN3iLfSqBWoSCknEZbFrKNSOFFcwjyP7LkffxrXLAcVWBpv2vlbaNkeHfhWoiwjcmcNbdTRLCIv3MTO7JMav/ytISzjpjRVOEcKerqhep6yl6cIx3A6PT9FyN/3H/Cs2tHODMCUdlmrRsHrMQgn19xhGJAgGPuuoNcxFMMDwIDAQAB";

		// vunglePub.init(this, "com.crane.funnytowers2");

		mHelper = new IabHelper(this, publicKey);
		mHelper.enableDebugLogging(BuildConfig.DEBUG);

		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					Log.e(IAP_TAG, "Problem setting up in-app billing: "
							+ result);
					return;
				}

				if (mHelper == null)
					return;

				Log.d(IAP_TAG, "Setup successful. Querying inventory.");
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper == null)
			return;

		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		} else {
			Log.d(IAP_TAG, "onActivityResult handled by IABUtil.");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
//		Chartboost.onStart(this);
	}

	@Override
	public void onResume() {
		super.onResume();
//		Chartboost.onResume(this);
		// vunglePub.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
//		Chartboost.onPause(this);
		// vunglePub.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
//		Chartboost.onStop(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
//		Chartboost.onDestroy(this);
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	@Override
	public void onBackPressed() {
//		Chartboost.onBackPressed();
	}
}
