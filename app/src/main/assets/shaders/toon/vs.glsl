precision highp float;
		
attribute vec4 a_position;
attribute vec3 a_normal;
attribute vec4 a_color;

uniform mat4 u_view_proj;
uniform float u_toon_weight;
uniform float u_toon_weight_delta;
uniform vec3 u_toon_eye;
uniform float u_state_time;
uniform float u_toon_fog_alpha_threshold;
uniform float u_toon_fog_a;
uniform float u_toon_fog_b;
uniform float u_vertex_vibration_freq;
uniform float u_vertex_vibration_amplitude;

varying float v_alpha;
varying float v_depth;

void main() {
	
    float z = dot(u_toon_eye, vec3(a_position.x, 0.0, a_position.z));
	float depth = clamp(z, u_toon_fog_alpha_threshold, 1.0);
	v_depth = clamp(1.0 - (z * u_toon_fog_a + u_toon_fog_b), 0.0, 1.0);

	v_alpha = a_color.a * depth;
	
	vec4 pos = vec4(a_normal * (u_toon_weight + u_toon_weight_delta * v_depth) + a_position.xyz, a_position.w);
	gl_Position = u_view_proj * pos;
	
//	float pd = length(gl_Position.xy);
//	pd = 1.0 + 1.0 / (0.001 * pow(pd, 10.0) + 3.0);
//	gl_Position.xy *= pd;
	
	float ksin = u_vertex_vibration_amplitude * v_depth * v_depth;
	float kcos = ksin * 0.4;
	float asin = gl_Position.y * u_vertex_vibration_freq + u_state_time;
	float acos = asin * 4.0; 
	gl_Position.x += sin(asin) * ksin + cos(acos) * kcos;
}