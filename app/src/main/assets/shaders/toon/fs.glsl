precision lowp float;
		
uniform vec3 u_toon_fog_color;
		
varying float v_alpha;
varying float v_depth;
	
void main() {
	gl_FragColor = vec4(u_toon_fog_color * v_depth, v_alpha);
}