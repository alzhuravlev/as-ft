precision highp float;

attribute vec4 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_uv;

uniform mat4 u_view_proj;
uniform float u_state_time;
uniform float u_vertex_vibration_freq;
uniform float u_vertex_vibration_amplitude;

varying vec2 v_uv;
varying float v_color_a;
varying float v_depth;
varying float v_light;
varying vec3 v_light_eye_half;
varying vec3 v_normal;
varying float v_spot_cos;

uniform vec3 u_light_direction;
uniform vec3 u_light_position;
uniform vec3 u_light_eye;
uniform float u_light_attenuation;
uniform float u_light_fog_a;
uniform float u_light_fog_b;

void main() {
	v_uv = a_uv;
	v_color_a = a_color.a;
	
    vec3 n = a_normal;
    vec3 l = normalize(u_light_position - a_position.xyz);
    
    v_spot_cos = dot(u_light_direction, l);
    
    //v_light = max(u_light_ambient_light, step(u_light_ambient_light, v_light));
    v_light = dot(n, l);
    
    v_light_eye_half = l + u_light_eye;
    v_normal = a_normal;
	
    float z = dot(u_light_eye, vec3(a_position.x, 0.0, a_position.z));
	float depth = clamp(1.0 - (z * u_light_fog_a + u_light_fog_b), 0.0, 1.0);
	
	v_depth = depth;
	
	gl_Position = u_view_proj * a_position;
	
	//float pd = length(gl_Position.xy);
	//pd = 1.0 + 1.0 / (0.001 * pow(pd, 10.0) + 3.0);
	//gl_Position.xy *= pd;
	
	float ksin = u_vertex_vibration_amplitude * depth * depth;
	float kcos = ksin * 0.4;
	float asin = gl_Position.y * u_vertex_vibration_freq + u_state_time;
	float acos = asin * 4.0; 
	gl_Position.x += sin(asin) * ksin + cos(acos) * kcos;
}