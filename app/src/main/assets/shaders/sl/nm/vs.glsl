precision highp float;

attribute vec4 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_uv;
attribute vec2 a_nm_uv;
attribute vec3 a_tangent;
attribute vec3 a_bitangent;

uniform mat4 u_view_proj;
uniform float u_state_time;
uniform float u_vertex_vibration_freq;
uniform float u_vertex_vibration_amplitude;

uniform vec3 u_light_eye;
uniform float u_light_fog_a;
uniform float u_light_fog_b;
uniform vec3 u_light_position;
uniform vec3 u_light_direction;

varying vec2 v_uv;
varying vec2 v_nm_uv;
varying float v_color_a;
varying float v_depth;
varying vec3 v_light_tangent;
varying vec3 v_light_eye_half;
varying float v_spot_cos;

void main() {
	v_uv = a_uv;
	v_nm_uv = a_nm_uv;
	v_color_a = a_color.a;

	mat3 TBN = mat3(
		a_tangent.x, a_bitangent.x, a_normal.x,
		a_tangent.y, a_bitangent.y, a_normal.y,
		a_tangent.z, a_bitangent.z, a_normal.z
	);


    vec3 l = u_light_position - a_position.xyz;

    v_light_tangent = TBN * (l);
    v_light_eye_half = normalize(v_light_tangent) + normalize(TBN * u_light_eye); 

    v_spot_cos = dot(u_light_direction, normalize(l));
	
    float z = dot(u_light_eye, vec3(a_position.x, 0.0, a_position.z));
	v_depth = clamp(1.0 - (z * u_light_fog_a + u_light_fog_b), 0.0, 1.0);
	
	gl_Position = u_view_proj * a_position;
	
	//float pd = length(gl_Position.xy);
	//pd = 1.0 + 1.0 / (0.001 * pow(pd, 10.0) + 3.0);
	//gl_Position.xy *= pd;
	
	float ksin = u_vertex_vibration_amplitude * v_depth * v_depth;
	float kcos = ksin * 0.4;
	float asin = gl_Position.y * u_vertex_vibration_freq + u_state_time;
	float acos = asin * 4.0; 
	gl_Position.x += sin(asin) * ksin + cos(acos) * kcos;
}