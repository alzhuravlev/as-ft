precision highp float;
		
attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_uv;

uniform mat4 u_view_proj;

varying vec2 v_uv;
varying vec4 v_color;

void main() {
	v_color = a_color;
	v_uv = a_uv;
	
	gl_Position = u_view_proj * a_position;
}