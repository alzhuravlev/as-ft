precision highp float;

attribute vec4 a_position;
attribute vec3 a_normal;

uniform mat4 u_view_proj;

varying float v_light;
varying float v_k;

uniform vec3 u_light_position;

void main() {
    v_k = 60.0 * a_position.x * a_position.z;
	
    v_light = dot(a_normal, normalize(u_light_position - a_position.xyz));
    
	gl_Position = u_view_proj * a_position;
}