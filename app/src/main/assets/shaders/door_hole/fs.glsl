precision lowp float;

uniform vec3 u_light_fog_color;
uniform float u_light_ambient_light;
uniform float u_state_time;

varying float v_light;
varying float v_k;

void main() {
	
    //float v = max(u_light_ambient_light, step(u_light_ambient_light, v_light));
    float v = mix(u_light_ambient_light, 1.0, max(0.0, v_light));
    
    v *= (sin(v_k + 10.0 * u_state_time) * 0.5 + 1.0);
	
	gl_FragColor = vec4(mix(vec3(v), u_light_fog_color, 0.9), 1.0);
}