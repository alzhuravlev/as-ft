precision lowp float;

uniform sampler2D u_texture;

uniform vec3 u_light_fog_color;
uniform float u_light_specular_exp;

varying vec2 v_uv;
varying float v_color_a;
varying float v_depth;
varying float v_light;
varying vec3 v_light_eye_half;
varying vec3 v_normal;

void main() {
	vec4 res = texture2D(u_texture, v_uv);
	
	//float specular = step(0.99, dot(normalize(v_normal), normalize(v_light_eye_half)));
	float specular = pow(max(0.0, dot(normalize(v_normal), normalize(v_light_eye_half))), u_light_specular_exp);
	
    //float v = max(u_light_ambient_light, step(u_light_ambient_light, v_light));
    //float v = mix(u_light_ambient_light, 1.0, max(0.0, v_light));
	
	gl_FragColor.rgb = mix(res.rgb * v_light + specular, u_light_fog_color, v_depth);
    gl_FragColor.a = res.a * v_color_a;
}