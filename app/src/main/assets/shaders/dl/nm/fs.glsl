precision lowp float;

uniform sampler2D u_texture;

uniform float u_light_ambient_light;
uniform vec3 u_light_fog_color;
uniform float u_light_specular_exp;

varying vec2 v_uv;
varying vec2 v_nm_uv;
varying float v_color_a;
varying float v_depth;
varying vec3 v_light_tangent;
varying vec3 v_light_eye_half;

void main() {
	vec4 res = texture2D(u_texture, v_uv);
	vec4 nm_uv = texture2D(u_texture, v_nm_uv);

	vec3 tn = normalize(nm_uv.rgb * 2.0 - 1.0);

	//float specular = step(0.99, dot(tn, normalize(v_light_eye_half)));
	float specular = pow(max(0.0, dot(tn, normalize(v_light_eye_half))), u_light_specular_exp);
    
    //float v = max(u_light_ambient_light, step(u_light_ambient_light, dot(tn, normalize(v_light_tangent))));
    //float v = mix(u_light_ambient_light, 1.0, max(0.0, dot(tn, normalize(v_light_tangent))));
    float v = max(u_light_ambient_light, dot(tn, normalize(v_light_tangent)));
    
	gl_FragColor = vec4(mix(res.rgb * v + specular, u_light_fog_color, v_depth), 1.0);
}