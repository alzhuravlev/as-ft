precision highp float;

attribute vec4 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_uv;

uniform mat4 u_view_proj;

uniform float u_state_time;
uniform float u_vertex_vibration_freq;
uniform float u_vertex_vibration_amplitude;

varying vec2 v_uv;
varying float v_color_a;
varying float v_depth;
varying float v_light;
varying vec3 v_light_eye_half;
varying vec3 v_normal;

uniform vec3 u_light_direction;
uniform vec3 u_light_position;
uniform vec3 u_light_eye;
uniform float u_light_attenuation;
uniform float u_light_fog_a;
uniform float u_light_fog_b;
uniform float u_light_ambient_light;

void main() {
	v_uv = a_uv;
	v_color_a = a_color.a;
    v_normal = a_normal;
	
    vec3 l = u_light_direction;
    
    //v_light = max(u_light_ambient_light, step(u_light_ambient_light, v_light));
    v_light = max(u_light_ambient_light, dot(a_normal, l));
    
    v_light_eye_half = l + u_light_eye;
	
    float z = dot(u_light_eye, vec3(a_position.x, 0.0, a_position.z));
	float depth = clamp(1.0 - (z * u_light_fog_a + u_light_fog_b), 0.0, 1.0);
	
	v_depth = depth;
	
	gl_Position = u_view_proj * a_position;
	
	//float pd = length(gl_Position.xy);
	//pd = 1.0 + 1.0 / (0.001 * pow(pd, 10.0) + 3.0);
	//gl_Position.xy *= pd;
	
	float ksin = u_vertex_vibration_amplitude * depth * depth;
	float kcos = ksin * 0.4;
	float asin = gl_Position.y * u_vertex_vibration_freq + u_state_time;
	float acos = asin * 4.0; 
	gl_Position.x += sin(asin) * ksin + cos(acos) * kcos;
}