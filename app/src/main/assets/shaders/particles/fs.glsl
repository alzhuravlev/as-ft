precision lowp float;

varying vec4 v_color;
varying vec2 v_uv;
varying float v_depth;

uniform sampler2D u_texture;

uniform vec3 u_light_fog_color;

void main() {
	vec4 res = texture2D(u_texture, v_uv);
    gl_FragColor.rgb = mix(res.rgb * v_color.rgb, u_light_fog_color, v_depth);
    gl_FragColor.a = res.a * v_color.a;
}