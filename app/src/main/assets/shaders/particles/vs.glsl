precision highp float;
		
attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_uv;

uniform vec3 u_light_eye;
uniform float u_light_fog_a;
uniform float u_light_fog_b;

uniform mat4 u_view_proj;

varying vec2 v_uv;
varying vec4 v_color;
varying float v_depth;

void main() {
	v_uv = a_uv;
	v_color = a_color;
	
    float z = dot(u_light_eye, vec3(a_position.x, 0.0, a_position.z));
	v_depth = clamp(1.0 - (z * u_light_fog_a + u_light_fog_b), 0.0, 1.0);
	
	gl_Position = u_view_proj * a_position;
}