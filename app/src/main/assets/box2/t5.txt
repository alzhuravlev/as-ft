w:16
continuous:false

bg_atlas:game/bg_atlas1.atlas
tower_atlas:game/tower_atlas1.atlas
items_atlas:game/items_atlas1.atlas

brick_height_scale:0.6
platform_depth:0.65
platform_gap:0.25
platform_amplitude:0.06
platform_c�cles:12

eye_scale:1.3
ball_scale:1.2
vagon_scale:1.2
pogo_scale:0.7

tower_tile_count_v:1
tower_tile_count_h:3

light_type:pl
light_ambient = 0.2
light_specular_exp = 256.0
light_spot_exp = 2.0
light_spot_cos_cutoff:0.70710678118654752440084436210485 

zipperfly_distance_from_base_radius:0.9
zipperfly_movement_alpha_x:4.0
zipperfly_movement_alpha_y:3.0
zipperfly_distance_min_x:0.3
zipperfly_distance_max_x:0.3
zipperfly_distance_min_y:0.0
zipperfly_distance_max_y:0.3

button1:on
button2:on
button3:on


r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:                   
r:000:   P a 
r:000:----------------   
r:000:                   
r:000:                   


meta:-:StepItem;DEFAULT
meta:~:StepItem;FALSY
meta:|:PipeItem

meta:A:DoorWithPadLockItem;1
meta:B:DoorWithPadLockItem;2
meta:C:DoorWithPadLockItem;3
meta:D:StepTogglerItem;1
meta:E:StepTogglerItem;2
meta:F:StepTogglerItem;3

meta:H:EyeItem;2;0

meta:O:BallItem

meta:P:PogoItem

meta:R:DoorItem

meta:V:EyeItem;0;2

meta:Z:FinishDoorItem

meta:1:KeyItem;1
meta:2:KeyItem;2
meta:3:KeyItem;3
meta:4:ButtonForPlatformItem;1
meta:5:ButtonForPlatformItem;2
meta:6:ButtonForPlatformItem;3

meta:a:LiftItem;3;6
meta:b:LiftItem;4;6
meta:c:LiftItem;5;6
meta:d:LiftItem;6;6
meta:e:LiftItem;7;6
meta:f:LiftItem;8;6
meta:g:LiftItem;9;6
meta:h:LiftItem;10;6
meta:i:LiftItem;11;6
meta:j:LiftItem;12;6
meta:k:LiftItem;13;6
meta:l:LiftItem;14;6
meta:m:LiftItem;15;6
meta:n:LiftItem;16;6
meta:o:LiftItem;17;6
meta:p:LiftItem;18;6
meta:q:LiftItem;19;6
meta:r:LiftItem;20;6
meta:s:LiftItem;21;6
meta:t:LiftItem;22;6
meta:u:LiftItem;23;6
meta:v:LiftItem;24;6
meta:w:LiftItem;25;6
meta:x:LiftItem;26;6
meta:y:LiftItem;27;6
meta:z:LiftItem;28;6
